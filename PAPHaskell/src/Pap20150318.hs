import Data.Char

len :: [a] -> Int
len [] = 0
len (x:xs) = 1 + len xs

-- l'operatore ++ esiste già, noi lo riscriviamo per esercizio
append :: [a] -> [a] -> [a]
append [] xs = xs
-- ragiono in modo ricorsivo, esprimo il concatenamento dicendo 
-- che aggiungo la lista in fondo solo quando la prima è vuota
append (x:xs) ys = x : append xs ys

-- e se ho una lista di liste? vedi flattening
{- isPresent :: Int -> [Int] -> Bool
isPresent _ [] = False
isPresent x (y:ys) = if x == y then True else isPresent x ys 

Soluzione più logica, ma i pattern non consentono di scriverlo
isPresent _ [] = False
isPresent x (x:_) = False
isPresent _ xs = isPresent xs

Formula più elegante, non voglio if ma guardie
isPresent :: Int -> [Int] -> Bool
isPresent x [y:ys] = 
    | x == y = True
    | otherwise = isPresent y xs
-}

-- e se la volessi generica? Eq definisce == e diverso controllando i tipi  
isPresent :: Eq a => a -> [a] -> Bool
isPresent _ [] = False
isPresent x (y:ys) 
    | x == y = True
    | otherwise = isPresent x ys
    
rev :: [a] -> [a]
rev [] = []
rev (x:xs) = rev xs ++ [x]

-- removeAll è generica, ecco perchè ci serve Eq a 
removeAll :: Eq a => [a] -> a -> [a]
removeAll [] _ = []
removeAll (x:xs) y 
    | x == y = removeAll xs y
    | x /= y = x : res
    where res = removeAll xs y 
-- res è una variabile locale che utilizzo nei punti che mi servono


-- 4) Implementare la funzione merge che date due liste ordinate di elementi interi, computa una sola lista ordinata
merge :: Ord a => [a] -> [a] -> [a]
-- merge [1,6,8] [5,9,20,21] -> [1,5,6,8,9,20,21]
merge (x:xs) [] = (x:xs)
merge [] (x:xs) = (x:xs)
merge (x:xs) (y:ys)
    | x < y = x : merge xs (y:ys)
    | otherwise = y : merge (x:xs) ys


-- 5) Data la struttura dati:
data BSTree a = Nil | Node a (BSTree a) (BSTree a)  deriving show
-- che rappresenta un albero binario di ricerca, implementare la funzione isPresentInBST generica che dato un valore e un BSTree restituisce vero o falso a seconda che il valore sia o meno presente
isPresentInBST :: a -> BSTree a -> Bool
isPresentInBST _ Nil = False
-- albero non è vuoto, esprimo il valore con un pattern. Le variabili le specifico
-- per catturare tutta la struttura dati che è in quel punto. Voglio fare il confronto
-- con il valore interno al pattern
isPresentInBst x (Node y l r)
    | x == y = True
    | x < y = isPresentInBST x l
    | otherwise = isPresentInBst x r

--Supponendo di avere
testTree :: BSTree String 
testTree = Node "pappa" (Node "albero" Nil Nil) (Node "zenzero" Nil Nil)

{- 6) 
Implementare la funzione buildBST che, data la una lista di stringhe, 
costruisce l’albero binario ordinato che contiene le stringhe e la loro 
posizione all’interno della lista.
-}

{- 7) Sia Shape un tipo di dato algebrico che rappresenta una figura 
bidimensionale (Triangolo, Quadrato, Cerchio), 
ognuna identificata da un identificatore stringa. Implementare 
la funzione calcPerim :: [Shape] -> [(String,Int)] che data una lista 
di Shape computi la lista di tuple date da identificatore della Shape e 
rispettivo perimetro.

data means that we're defining a new data type. 
The part before the = denotes the type, which is Bool. 
The parts after the = are value constructors. They specify the different 
values that this type can have. The | is read as or. So we can read this 
as: the Bool type can have a value of True or False. 
Both the type name and the value constructors have to be capital cased.
-}
--
--data Shape = Shap String Int  
--shapeTriangolo :: Shape
--shapeTriangolo = "Triangle" 100
--shapeQuadrato :: Shape
--shapeQuadrato = "Square" 1000
--shapeCerchio :: Shape
--shapeCerchio = "Circle" 10
--
--calcPerim :: [Shape] -> [(String,Int)]
--calcPerim [] = []
--calcPerim (Shape i p)
--    | i == "Triangle" =  
--    | i == "Square"
--    | otherwise = 
{- 8) Scrivere una funzione encode :: Int -> String -> String che implementi 
il cifrario di Cesare. Tale codifica consiste nel rimpiazzare ogni lettera 
della stringa passata come argomento con la lettera che sta n posti più avanti 
nell’alfabeto, dove n è il primo parametro passato, facendo il wrap se si 
deborda oltre la fine dell’alfabeto.
-}
encode :: Int -> String -> String
encode _ [] = []
encode n (x:xs) = chr ((n + ord x)) : encode n xs
{- 8) Scrivere una funzione freqs :: String -> [Float] che data una stringa 
computi la tabella delle frequenze (in percentuale) relative ad ogni lettera 
dell’alfabeto, ovvero ogni lettera dell’alfabeto quante volte compare nella 
stringa (in percentuale).
-}

{- 9) Scrivere una funzione crack :: String -> String che tenta di decifrare 
una stringa codificata con il cifrario di Cesare, considerando la lista delle 
frequenze. -}