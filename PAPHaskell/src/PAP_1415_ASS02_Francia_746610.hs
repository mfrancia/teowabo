import Screen

--  Matteo Francia, 746610
--  TEST(s)
--  makeShapeTree [Line (1,0) (2,0),Triangle (0,0) (3,0) (0,3),Line (2,0) (2,0),Rectangle (0,0) (2,2),Rectangle (1,1) (2,2)]
--  First of all ... 
--  drawAll houseAndTree
--  and then ...
--  drawAll ([Rectangle (0,0) (25,25)] ++ (inBBox houseAndTree (0,0) (25,25))) 
-- .. at the end
--  test' 

houseAndTree :: [Shape]
--houseAndTree = [Triangle (10,10) (20,10) (15,5), -- house
--                Rectangle (10,10) (20,19),
--                Rectangle (13,15) (17,19),
--                Rectangle (11,11) (13,13),
--                Rectangle (16,11) (18,13),
--                Line (0,19) (50,19), -- ground
--                Rectangle (30,12) (34,19), -- tree
--                Circle (30,10) 2,
--                Circle (34,10) 2,
--                Circle (32,7) 2,
--                Line (20,4) (22,6), -- bird1
--                Line (22,6) (24,4),
--                Line (27,3) (28,4), -- bird2
--                Line (28,4) (29,3)
--                ]
                
houseAndTree = [Triangle (10,10) (20,10) (15,5), -- house
                Rectangle (10,10) (20,19),
                Rectangle (13,15) (17,19),
                Rectangle (11,11) (13,13),
                Rectangle (16,11) (18,13),
                Line (0,19) (50,19), -- ground
                Composition (Rectangle (30,12) (34,19)) -- tree
                    (Composition (Circle (30,10) 2) 
                        (Composition (Circle (34,10) 2) (Circle (32,7) 2))),              
                Line (20,4) (22,6), -- bird1
                Line (22,6) (24,4),
                Line (27,3) (28,4), -- bird2
                Line (28,4) (29,3)
                ]
test' :: Bool
test' = perim (Line (1,0) (2,0)) == 1  &&
        truncate (perim (Triangle (0,0) (3,0) (0,3))) == 10 && 
        perim (Rectangle (1,1) (2,0)) == 4 &&
        perim (Composition (Rectangle (1,1) (2,0)) (Line (1,0) (2,0))) == 5 &&
        area (Line (1,0) (2,0)) == 0 &&
        area (Triangle (0,0) (3,0) (0,3)) == 4.5 && 
        area (Rectangle (0,2) (2,0)) == 4 &&
        area (Composition (Rectangle (0,2) (2,0)) (Line (1,0) (2,0))) == 4 &&
        area (maxArea [Line (1,0) (2,0), Triangle (0,0) (3,0) (0,3) , Rectangle (0,2) (2,0)]) == 4.5 &&
        move (Line (1,0) (2,0)) (1,1) == Line (2,1) (3,1) &&
        move (Triangle (1,0) (3,0) (2,1)) (1,1) == Triangle (2,1) (4,1) (3,2) &&
        move (Rectangle (1,1) (2,0)) (1,1) == Rectangle (2,2) (3,1) &&
        move (Circle (0,0) 2) (1,1) == Circle (1,1) 2 &&
        move (Composition (Rectangle (1,1) (2,0)) (Line (1,0) (2,0))) (1,1) == Composition (Rectangle (2,2) (3,1)) (Line (2,1) (3,01)) &&
        moveShapes [Line (1,0) (2,0),Line (1,0) (2,0)] (1,1) == [Line (2,1) (3,1),Line (2,1) (3,1)] &&
        inBBox houseAndTree (0,0) (25,25) == [Triangle (10,10) (20,10) (15,5), -- house
                                                Rectangle (10,10) (20,19),
                                                Rectangle (13,15) (17,19),
                                                Rectangle (11,11) (13,13),
                                                Rectangle (16,11) (18,13),
                                                Line (20,4) (22,6), -- bird1
                                                Line (22,6) (24,4)
                                                ]
type P2d = (Int, Int)
type V2d = (Int, Int)
type Perimeter = Float
type Area = Float
type Radius = Int
data Shape = Nil | Line P2d P2d | Triangle P2d P2d P2d | Rectangle P2d P2d | Circle P2d Radius | Composition Shape Shape
    deriving Show
data BSTree = Null | Node Shape BSTree BSTree
    deriving Show
class CShape a where
    perim :: a -> Perimeter
    --    data una CShape calcola il perimetro
    move :: a -> V2d -> a
    --    data una CShape e un vettore, calcola la nuova shape spostata del vettore 
    area :: a -> Area
    getLeftmostX :: a -> Int
instance Eq Shape where
    (Line (x,x') (y,y')) == (Line (a,a') (b,b')) = x == a && x' == a' && y == b && y' == b'
    (Rectangle (x,x') (y,y')) == (Rectangle (a,a') (b,b')) = x == a && x' == a' && y == b && y' == b'  
    (Triangle (x,x') (y,y') (z,z')) == (Triangle (a,a') (b,b') (c,c')) = x == a && x' == a' && y == b && y' == b' && z == c && z' == c'     
    (Circle (x,x') r) == (Circle (a,a') r') = x == a && x' == a' && r == r'
    (Composition s s') == (Composition x x') = s == x && s' == x'
    _ == _ = False
instance CShape Shape where  
    perim a = case a of
                (Line x y) -> euclideanDistance x y
                (Triangle x y z) -> euclideanDistance x y + euclideanDistance x z + euclideanDistance z y
                (Rectangle (x,x') (y,y')) -> fromIntegral ((y - x) + (x' - y')) * 2.0   
                (Circle _ r) -> 2 * fromIntegral r * pi
                (Composition s s') -> perim s + perim s'
    move a (w,w') = case a of
                (Line (x,x') (y,y')) -> Line (x+w,w'+x') (w+y,w+y')
                (Triangle (x,x') (y,y') (z,z')) -> Triangle (x+w,x'+w') (y+w,y'+w') (z+w,z'+w')
                (Rectangle (x,x') (y,y')) -> Rectangle (x+w,x'+w') (y+w,y'+w') 
                (Circle (x,x') r) -> Circle (x+w,x'+w') r
                (Composition s s') -> Composition (move s (w,w')) (move s' (w,w'))
    area a = case a of
                (Nil) -> 0
                (Line _ _) -> 0
                -- formula di erone per calcolo area
                (Triangle x y z) -> sqrt (p * (p - euclideanDistance x y) * (p - euclideanDistance x z) * (p - euclideanDistance z y))
                    where p = perim (Triangle x y z) / 2
                (Rectangle (x,x') (y,y')) -> fromIntegral ((y - x) * (x' - y'))       
                (Composition s s') -> area s + area s'
                (Circle _ r) -> fromIntegral (r * r) * pi
    getLeftmostX a = x
                    where 
                        (y,x,b,c,d) = boundary a

euclideanDistance :: P2d -> P2d -> Float
euclideanDistance (x,y) (x',y') = sqrt (fromIntegral ((x - x')^2 + (y - y')^2))
 
-- data una lista di figure, computa la lista di figure traslate di un vettore dv
moveShapes :: [Shape] -> V2d -> [Shape]
moveShapes list v = map (`move` v) list
 
-- data una lista di figure, determina la figura con area maggiore
maxArea :: [Shape] -> Shape
maxArea = foldr (\x y -> if area x >= area y then x else y) Nil
    
makeShapeTree :: [Shape] -> BSTree
-- data una lista di figure, costruisce un albero binario di ricerca con le figure ordinate lungo l'asse x
makeShapeTree = foldr insertInTree Null

insertInTree :: Shape -> BSTree -> BSTree
insertInTree x Null = Node x Null Null
insertInTree v (Node x left right)
--    | new' == x' = Node v left right
    | new' <= x' = Node x (insertInTree v left) right
    | otherwise = Node x left (insertInTree v right)
    where 
        new' = getLeftmostX v
        x' = getLeftmostX x
--  Definire la classe Drawable che estende CShape con funzioni:
--  draw
--  drawAll
class (CShape a) => Drawable a where
    draw :: a -> IO ()
--  che data una shape, produce un’azione il cui effetto è di disegnare la figura 
    drawAll :: [a] -> IO ()
--  data una lista di figure, produca un’azione il cui effetto è di disegnare sullo schermo tutte le figure della lista   
instance Drawable Shape where  
    draw a = case a of
                (Line x y) -> 
--                    cls >> 
                    bresenham x y -- disegna la linea
                    >> putStrLn ""
                (Triangle x y z) -> 
--                    cls >> 
                    bresenham x y >> -- disegna le tre linee
                    bresenham x z >> 
                    bresenham z y 
                    >> putStrLn ""
                (Rectangle (x,x') (y,y')) -> 
--                    cls >> 
                    bresenham (x,x') (y,x') >> -- disegna le quattro linee
                    bresenham (x,y') (y,y') >> 
                    bresenham (y,x') (y,y') >> 
                    bresenham (x,x') (x,y')
                     >> putStrLn ""  
                (Circle p r) -> 
--                    cls >> 
                    circleMidpoint (Circle p r) 
                (Composition s s') -> 
                    draw s >> 
                    draw s'
    drawAll list = cls >> foldr ((>>) . draw) (return()) list

-- calcolo dei boundary della figura
boundaries :: [Shape] -> [(Shape,Int,Int,Int,Int)]
boundaries = map boundary
boundary :: Shape -> (Shape,Int,Int,Int,Int)
boundary (Line (x,x') (y,y')) = (Line (x,x') (y,y'),min x y, max x y, max x' y', min x' y')
boundary (Rectangle (x,x') (y,y')) = (Rectangle (x,x') (y,y'),x, y, y', x')
boundary (Triangle (x,x') (y,y') (w, w')) = (Triangle (x,x') (y,y') (w, w'), min x (min y w), max x (max y w), max x' (max y' w'), min x (min y' w') )
boundary (Circle (x,y) r) = (Circle (x,y) r, x - r, x + r, y + r, y - r)
boundary (Composition s s') = (Composition s s', min a e, max b f, max d h, min c g)
    where 
        (x,a,b,c,d) = boundary s
        (y,e,f,g,h) = boundary s'
        
inBBox :: [Shape] -> P2d -> P2d -> [Shape]
inBBox [] _ _ = []
inBBox (s:xs) (x,x') (y,y') = [ shape | (shape,a,b,c,d) <- boundaries (s:xs), a >= x && b <= y && c <= y' && d >= x']           

-- BEGIN Midpoint Circle Algorithm
circleMidpoint :: Shape -> IO ()
circleMidpoint (Circle (xC,yC) r) = circleMidpoint' 0 r (1-r) 
    where
        circleMidpoint' :: Int -> Int -> Int -> IO ()
        circleMidpoint' x y p 
            | x < y = circleMidpoint' (x+1) y' p' >> circlePlotPoints (xC,yC) (x,y)
            | otherwise = return()
            where 
                y' = if p >= 0 then y-1 else y 
                p' = if p < 0 then p+2*x+1  else p+2*(x-y)+1
        circlePlotPoints :: P2d -> P2d -> IO ()         
        circlePlotPoints (xC,yC) (x,y) = 
            writeAt (xC+x, yC+y) "*" >>     
            writeAt (xC-x, yC+y) "*" >>
            writeAt (xC+x, yC-y) "*" >>
            writeAt (xC-x, yC-y) "*" >>
            writeAt (xC+y, yC+x) "*" >>
            writeAt (xC-y, yC+x) "*" >>
            writeAt (xC+y, yC-x) "*" >>
            writeAt (xC-y, yC-x) "*" 
-- END Midpoint Circle Algorithm

-- BEGIN Bresenham's line algorithm
-- See http://en.wikipedia.org/wiki/Bresenham%27s_line_algorithm
-- Bresenham's line algorithm INIT
bresenham :: P2d -> P2d -> IO ()
bresenham (x0,y0) (x1,y1) 
    | swap = bresenham' (x0,y0) (x1,y1) d dy' dx' 1 q s 0
    | otherwise = bresenham' (x0,y0) (x1,y1) d dx' dy' 0 q s 0
  where 
    dx = x1 - x0
    dx' = abs dx
    dy = y1 - y0
    dy' = abs dy
    swap = dy' > dx'
    d = if swap then 2 * dx' - dy' else 2 * dy' - dx'
    q = if x0 > x1 then - 1 else 1
    s = if y0 > y1 then - 1 else 1 
-- Bresenham's line algorithm LOOP
bresenham' :: P2d -> P2d -> Int -> Int -> Int -> Int -> Int -> Int -> Int -> IO ()
bresenham' (x0,y0) (x1,y1) d dx dy swap q s count =
    (if count < dx then
        if d > 0 then 
            bresenham' (x0 + q, y0 + s) (x1,y1) (d + 2*(dy-dx)) dx dy swap q s (count + 1)
        else 
            bresenham' (x',y') (x1,y1) (d + 2*dy) dx dy swap q s (count + 1)
    else return ()
    ) >> writeAt (x0,y0) "*"
  where 
    x' = if swap == 1 then x0 else x0 + q
    y' = if swap == 1 then y0 + s else y0
-- END Bresenham's line algorithm
