--Assignment #1 - 20150318 - Soluzioni
--Data una lista di elementi Elem così definiti:
data Elem = Dot | Star
--implementare in Haskell le seguenti funzioni:
countStar :: [Elem] -> Int
--conta il numero di elementi Star presenti nella lista passata come parametro
--Esempi:
--countStar [] → 0
--countStar [Dot, Dot] → 0
--countStar [Dot, Star, Star, Dot, Star ] → 3
--Soluzione
countStar [] = 0
countStar (Star:xs) = 1 + countStar xs
countStar (_:xs) = countStar xs

printableSeq :: [Elem] -> String
--restituisce una rappresentazione testuale della sequenza, ove Dot è rappresentato dal carattere ‘.’ e Star dal carattere ‘*’. Questa funzione è utile ogni volta si voglia stampare una lista di Elem.
--Esempi:
--printableSeq [] → “”
--printableSeq [Dot, Dot] → “..”
--printableSeq [Dot, Star, Star, Dot, Star ] → “.**.*”
--Soluzione
printableSeq [] = []
printableSeq (Dot:xs) = '.': printableSeq xs  
printableSeq (Star:xs) = '*': printableSeq xs  

swapSeq :: [Elem] -> [Elem]
--restituisce una sequenza pari a quella passata l in cui ogni valore Dot è sostituito con uno Star e viceversa.
--Esempi:
--swapSeq [] → []
--swapSeq [Dot, Dot] → [Star, Star]
--swapSeq [Dot, Star, Star, Dot, Star ] → [Star, Dot, Dot, Star, Dot ]
--Soluzione
swapSeq [] = []
swapSeq (Star:xs) = Dot : swapSeq xs
swapSeq (Dot:xs) = Star : swapSeq xs

zipSeq :: [Elem] -> [Elem]
--data una lista di Elem restituisce una lista in cui le sequenze di 1 o più Dot sono sostituite da un solo Dot.
--Esempi:
--zipSeq [] → []
--zipSeq [Dot, Dot] → [Dot]
--zipSeq [Dot, Star, Star, Dot, Star ] → [Dot, Star, Star, Dot, Star ]
--zipSeq [Dot, Dot, Dot, Dot] → [Dot]
--zipSeq [Dot, Dot, Star, Star, Dot, Dot, Dot, Star, Dot ] → [Dot, Star, Star, Dot, Star, Dot ]
--Soluzione
zipSeq [] = []
zipSeq (Dot:xs) = (Dot : zipSeq (skipDots xs))
zipSeq (Star:xs) = Star:zipSeq xs

skipDots :: [Elem] -> [Elem]
skipDots (Dot:xs) = skipDots xs
skipDots xs = xs

maxStarSeq :: [Elem] -> Int
--computa la lunghezza della sequenza di Star più lunga
--Esempi:
--maxStarSeq [] → 0
--maxStarSeq [Dot, Dot] → 0
--maxStarSeq [Dot, Star, Star, Dot, Star ] → 2
--Soluzione
maxStarSeq xs = maxStarSeq2 xs (-1) 0
maxStarSeq2 [] m c = max m c
maxStarSeq2 (Dot:xs) m c = maxStarSeq2 xs (max m c) 0
maxStarSeq2 (Star:xs) m c = maxStarSeq2 xs m (c + 1)

matchSeq :: [Elem] -> [Elem] -> Bool
--date due liste di Elem restituisce vero se le liste contengono le medesime sequenze di Star in ordine, a prescindere dal numero di Dot in mezzo
--Esempi:
--matchSeq [] [Dot, Star] → False
--matchSeq [Star, Dot, Dot, Star, Star, Dot] [Star, Dot, Star, Star, Dot, Dot]  → True
--matchSeq [Dot, Dot, Star, Star, Dot, Star, Dot] [Star, Star, Dot, Star]  → True
--matchSeq [Star, Star, Dot, Star] [Star, Star, Dot, Star, Star]  → False
--Soluzione 
-- La strategia adottata (una delle possibili) consiste nello scandire le due sequenze prima trovando le corrispondenti sequenze di Star (ovvero ignorando i Dot) e quindi verificando che le sequenze di Star trovate abbiano la stessa lunghezza.
matchSeq xs ys = matchSeq2 (dropDots xs) (dropDots ys)
-- matchSeq2 verifica che le due sequenze abbiano la stessa sequenza iniziale di Star
matchSeq2 [] xs = onlyDots xs
matchSeq2 xs [] = onlyDots xs
matchSeq2 (Star:xs) (Star:ys) = matchSeq2 xs ys
matchSeq2 (Star:_) (Dot:_) = False
matchSeq2 (Dot:_) (Star:_) = False
matchSeq2 (Dot:xs) (Dot:ys) = matchSeq xs ys

-- data una lista di Elem, computa la lista con eventuali Dots iniziali rimossi
dropDots :: [Elem] -> [Elem]
dropDots [] = []
dropDots (Dot:xs) = dropDots xs
dropDots (Star:xs) = Star:xs

-- verifica che una lista contenga solo Dot
onlyDots :: [Elem] -> Bool
onlyDots [] = True
onlyDots (Dot:xs) = onlyDots xs
onlyDots (Star:_) = False

-- determina una lista delle tuple a 2 elementi, in cui il primo indica la lunghezza di una sequenza di Star presente nella lista e il secondo la lista delle posizioni in cui tale sequenza compare nella lista (la prima posizione è la numero 1).
--Esempi:
--occ [] → []
--occ [Dot, Dot] → []
--occ [Dot,Star,Dot,Star,Dot,Star,Star,Star] →  [(1,[2,4]), (3,[6])]
--occ [Star, Star, Star, Star,Dot,Star,Star,Star,Star, Dot, Star] →  [(4,[1,6]), (1,[11])]
--Soluzione - La strategia adottata prima determina la lista delle informazioni in merito alle sequenze di star presenti, rappresentate dal tipo StarSeqInfo, poi le aggrega, ottenendo le informazioni circa le occorrenze per ogni sequenza di una data lunghezza.
type StarSeqInfo = (Int,Int)  -- Lunghezza/Posizione di una sequenza di Star
type Occur = (Int,[Int]) -- Occorrenze di sequenze di Star (Lunghezza, elenco posizioni)
occ :: [Elem] -> [Occur]
occ xs = reduceSeq (parseSeq xs 0 1) []
parseSeq :: [Elem] -> Int -> Int -> [StarSeqInfo]
-- data una lista di Elem, computa una lista di StarSeqInfo che riporta per ogni sequenza di
-- Star la sua la lunghezza e posizione
parseSeq [] len pos
    | len /= 0 = [(len,pos)]
    | otherwise = []

parseSeq (Star:xs) len pos = parseSeq xs (len+1) pos
parseSeq (Dot:xs) len pos
    | len /= 0 = (len,pos) : parseSeq xs 0 (pos+len+1)
    | otherwise = parseSeq xs len (pos+1)

reduceSeq :: [StarSeqInfo] -> [Occur] -> [Occur]
-- data una lista di informazioni sulle sequenze, le aggrega in modo da avere
-- un solo elemento per ogni sequenza di una data lunghezza, riportando l’elenco delle
-- posizioni in cui la sequenza compare
reduceSeq [] xs = xs
reduceSeq (x:xs) ys = reduceSeq xs (addSeq x ys)

addSeq :: StarSeqInfo -> [Occur] -> [Occur]
addSeq (x,p) [] = [(x,[p])]
addSeq (x,p) ((x1,pl):xs)  
    | x == x1   = (x,pl++[p]):xs
    | otherwise = (x1,pl) : addSeq (x,p) xs

countStarInTree :: BSTree Elem -> Int
--conta il numero di elementi Star presenti nell’albero passato come parametro
--Esempi:
--countStarInTree Nil → 0
--countStarInTree (Node Dot Nil Nil )  → 0
--countStarInTree (Node Star (Node Dot Nil Nil) (Node Star Nil Nil))   → 2
--Soluzione
data BSTree a = Nil | Node a (BSTree a)(BSTree a)
countStarInTree Nil = 0
countStarInTree (Node Star l r) = 1 + countStarInTree l + countStarInTree r
countStarInTree (Node Dot l r) = countStarInTree l + countStarInTree r

pathTree :: BSTree Elem -> Int
--determina la lunghezza del percorso più profondo - a partire dalla radice, fino ad un nodo Dot o ad una foglia - composto da soli elementi Star.
--Esempi:
--pathTree Nil → 0
--pathTree (Node Dot Nil Nil )  → 0
--pathTree (Node Star (Node Dot Nil Nil) (Node Star Nil Nil))   → 2
--pathTree (Node Star (Node Dot Nil Nil) (Node Star (Node Dot Nil Nil) (Node Dot Nil Nil)))   → 2
--pathTree (Node Dot (Node Dot Nil Nil) (Node Star (Node Dot Nil Nil) (Node Dot Nil Nil)))   → 0
--Soluzione
pathTree Nil = 0
pathTree (Node Dot _ _) = 0
pathTree (Node Star l r) = 1 + maxv (pathTree l) (pathTree r)

maxv x y
    | x > y = x
    | otherwise = y