--Dopo aver specificato la funzione member in Haskell (appartenenza di un elemento ad una lista), definire
--la funzione booleana is_square, che stabilisce se un numero naturale in ingresso è il quadrato di un
--altro numero naturale

isPresent :: Ord a => a -> [a] -> Bool
isPresent _ [] = False
isPresent y (x:xs)
    | y == x = True
    -- suppongo che la lista sia ordinata
    | x < y = isPresent y xs
    -- altrimenti devo utilizzare il solito otherwise = isPresent y xs
    | otherwise = False
    
squares :: [Integer]
squares = [ x * x | x <- [0..]]

isSquare :: Integer -> Bool
isSquare x = isPresent x squares

--Definire nel linguaggio Haskell la funzione reverse, avente in ingresso una lista, che restituisce la lista
--invertita
revers :: [a] -> [a]
revers [] = []
revers (x:xs) = reverse xs ++ [x]

--Definire nel linguaggio Haskell una struttura tabellare
--Prodotti(Codice, Nome, Prezzo)
--in cui ogni riga rappresenta un prodotto. Quindi, codificare la funzione selezione che, ricevendo in
--ingresso due interi, min e max, computa la lista dei nomi dei prodotti il cui prezzo è compreso tra min e
--max.
-- Example zelect [(1,"pippo",2), (2,"pluto",3), (3,"paperino",4)] 2 3
type Cod = Integer
type Name = String
type Price = Integer
type Products = [(Cod, Name, Price)]

zelect :: Products -> Integer -> Integer -> [Name]
zelect [] _ _ = []
zelect products min max
    | min > max = []
    | otherwise = [ name | (product, name, price) <- products, price >= min, price <= max ]
    
--Definire nel linguaggio Haskell le seguenti due strutture tabellari
--Studente(Matricola, Anno, Corso)
--Docente(Nome, Corso)
--Quindi, codificare la funzione docenti_dello_studente che, ricevendo in ingresso le tabelle stud e
--doc e la matricola mat di uno studente, computa la lista dei nomi dei docenti relativi ai corsi di tale
--studente.
-- student_teachers 1 [(1,1,"pippo"), (2,2,"pluto"), (3,3,"pluto")] [("qui","pippo"), ("quo","pluto"), ("qua","pluto")]
-- student_teachers 2 [(1,1,"pippo"), (2,2,"pluto"), (3,3,"pluto")] [("qui","pippo"), ("quo","pluto"), ("qua","pluto")]
type Year = Integer
type Course = String
type Teachers = [(Name, Course)]
type Students = [(Cod,Year,Course)]
student_teachers ::  Cod -> Students -> Teachers -> [Name]
student_teachers _ [] [] = []
student_teachers code students teachers = [ teacher_name | (teacher_name, teacher_course) <- teachers, (cod, year, student_course) <- students, teacher_course == student_course, cod == code ]

--Definire nel linguaggio Haskell una struttura tabellare
--Esami (Matricola, Corso, Voto)
--in cui ogni riga rappresenta rispettivamente uno studente, il corso, ed il relativo voto. Quindi, codificare in
--Haskell la funzione esamiSostenuti che, ricevendo in ingresso una tabella esami ed una certa
--matricola, computa la lista degli esami superati dal relativo studente (senza l’indicazione del voto).
-- exams 1 [(1,"pippo",2), (1,"pluto",6), (1,"paperino",7), (2,"pluto",6)]
type Mark = Integer
type Exams = [(Cod, Course, Mark)]
exams :: Cod -> Exams -> [Course]
exams _ [] = []
exams cod ex = [ course | (c, course, mark) <- ex, c == cod, mark >= 6]