--Matteo Francia, 746610

-- TEST
-- test
-- printSeqs testlist
-- expected result
-- "" 
-- **
-- ****
-- *****
-- *****

--Esercizi di programmazione funzionale in Haskell
--Data la struttura dati
data StarSeq = Star StarSeq | End
                        deriving (Show)
getMaxSeq :: [StarSeq] -> (Int,Int)
--data una lista di StarSeq computa lunghezza e posizione della sequenza più lunga 
getMaxSeq list = 
    foldr (\(pos,len') (pos',len'') -> if len' >= len'' then (pos,len') else (pos',len'')) (0,0) (getSeqInfo list)

list5 :: StarSeq
list5 = Star (Star (Star (Star (Star End))))
list4 :: StarSeq
list4 = Star (Star (Star (Star End)))
list2 :: StarSeq
list2 = Star (Star End)
list0 :: StarSeq
list0 = End
testlist :: [StarSeq]
testlist = [list4, list5, list5, list0, list2]

getSeqInfo :: [StarSeq] -> [(Int, Int)]
getSeqInfo list = getPos list 0
    where 
        getPos :: [StarSeq] -> Int -> [(Int, Int)]
        getPos [] _ = []
        getPos (x:xs) count = (count, len x) : getPos xs (count + 1)

len :: StarSeq -> Int
len End = 0
len (Star t) = 1 + len t

test :: Bool
test = getSeqInfo testlist == [(0,4),(1,5),(2,5),(3,0),(4,2)] &&
       null(getSeqInfo []) &&
       getMaxSeq testlist == (1,5) &&
       getMaxSeq [] == (0,0)    
        
printSeqs :: [StarSeq] -> IO ()
--data una lista di StarSeq, stampa in uscita le sequenze in ordine crescente di lunghezza, rappresentandole come sequenze di *, una sequenza per ogni linea
printSeqs list = foldr ((>>) . print') (return()) (quicksort (map len list))
    where      
        print' :: Int -> IO()
        print' 0 = putStrLn ""
        print' x = putStr "*" >> print' (x-1)
        quicksort :: [Int] -> [Int]
        quicksort [] = []
        quicksort (p:xs) = quicksort lesser ++ [p] ++ quicksort greater
            where
                lesser  = filter (< p) xs
                greater = filter (>= p) xs