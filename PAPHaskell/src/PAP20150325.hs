import Screen
--Lab #2 - 20150325 - Functional Programming in Haskell/2
--Esercizi su funzioni high-order e primi esercizi su IO
--Esecizi su  funzioni high-order

--1) Implementare la funzione mapLen che data una lista di parole, computi la lista di (p,l), 
--dove l è la lunghezza della parola p, usando la funzione map.
type Word = String 
mapLen :: [Word] -> [(Word,Int)]
--mapLen ["casa","albero","mare","giradino","aia"] → [("casa",4),("albero",6),("mare",4),("giardino",8),("aia",3)]

-- ogni volta che devo aggregare elementi della lista, allora penso alle fold
-- ogni volta che devo generare una lista con lo stesso numero di elementi penso alla map

mapLen [] = []
mapLen word = map length' word

length' :: Word -> (Word, Int)
length' [] = ("",0)
length' word = (word, length word)


--2) Implementare la funzione selectedLen che data una lista di parole e un intero w, computi la lista di (p,l) 
--delle parole la cui lunghezza l sia >= di w, usando la funzione map e filter.
selectedLen :: [Word] -> Int -> [(Word,Int)]
--selectedLen ["casa","albero","mare","giradino","aia"] 4 → [("albero",6),("giardino",8)]
selectedLen [] _ = []
selectedLen list p = filter (\(a,b) -> b >= p) (mapLen list)


--3) Implementare una funzione wordOcc che data una lista di parole e una parola, restituisce il numero di 
--occorrenze della parola nella lista, usando una funzione di folding
wordOcc :: [Word] -> Word -> Int
--wordOcc ["casa","albero","mare","albero","sedia"] "ramo"→ 0
--wordOcc ["casa","albero","mare","albero","sedia"] "albero"→ 2
wordOcc [] _ = 0
wordOcc list word = foldr (\p s -> if p == word then s + 1 else s) 0 list 

-- foldr :: (a -> b -> b) -> b -> [a] -> b
-- foldl :: (b -> a -> b) -> b -> [a] -> b

--4) Implementare una funzione wordsOcc che data una lista di parole, determini la lista delle occorrenze (l,ws) - 
-- dove l è la lunghezza e ws è la lista delle parole di quella lunghezza, ordinate in modo crescente secondo la 
-- lunghezza delle parole

--wordsOcc ["casa","albero","mare","albero","sedia","albero","mare","aia","palla","aia"] →
--  [ (1,["casa","palla","sedia"]), (2,["mare","aia"]),(3,["albero"]) ]
--
--5) Sono dati i tipi Trader e Transaction:
type TraderId = String
data Trader = Trader TraderId String
    deriving (Show)
data Transaction = Trans TraderId Int Int
    deriving (Show)
--Sono date le liste
traders = [Trader "john" "Cambridge", Trader "mario" "Milan", Trader "alan" "Cambridge", Trader "andrea" "Cesena"]
transactions = [Trans "andrea" 2011 300, Trans "john" 2012 1000, Trans "john" 2011 400, Trans "mario" 2012 710, Trans "mario" 2011 700,Trans "alan" 2012 950]
--Esprimere mediante composizione di funzioni le seguenti espressioni, considerando traders e transactions 
--come sorgenti di partenza:
--    la lista delle transazioni riferite all’anno 2011, ordinate per valore
-- transactionsIn2011 :: [Transaction] -> [Transaction]
--    l’elenco delle città distinte dove operano i trader
--    se esistono trader da Milano
--    il valore massimo delle transazioni
--    la transazione con il valore minimo
--    stampa tutti i valori delle transazioni di trader che abitano a Cambridge
--
--Esercizi su IO
--
--1) Implementare una funzione che data una lista di elementi, restituisce un’azione il cui effetto è di 
--stampare in uscita tutti gli elementi, uno per linea,  indentati di 4 posizioni.  
--Allo scopo è possibile sfruttare il modulo Screen, disponibile nello script Screen.hs, fornito nel 
--materiale del corso.
--2) Implementare una funzione che dato un BSTree di elementi, restituisce un’azione il cui effetto è di 
--stampare in uscita l’albero, in cui l’informazione relative ai nodi sono stampate su linee distinte e 
--con indentazione che dipende dal livello a cui si trova il nodo.