--Matteo Francia 746610
--Prog funzionale in Haskell
--Implementare una funzione Haskell printNodeAtDist che, dato un albero
--binario t di interi e un valore intero d, stampa gli  elementi dell'albero la
--cui distanza dal nodo radice è pari a d.
--
--printNodeAtDist testTree 0
--stampa 6
--
--printNodeAtDist testTree 2
--stampa 4 9 7
--
--printNodeAtDist testTree 6
--non stampa nulla


data BSTree = Null | Node Int BSTree BSTree
    deriving Show

testTree:: BSTree
testTree = Node 6 (Node 1 (Node 4 Null (Node 2 Null Null)) Null) (Node 3 (Node 9 (Node 3 Null Null) Null) (Node 7 Null Null))

test:: Bool
test =
    printNodeAtDist testTree 0 == ["6 "] &&
    printNodeAtDist testTree 2 == ["4 ", "9 ", "7 "] &&
    null(printNodeAtDist testTree 6) && 
    null(printNodeAtDist Null 6)

test':: IO()
test' = 
    print_nodes_at_dist testTree 0 >> putStrLn "" >>
    print_nodes_at_dist testTree 2 >> putStrLn "" >>
    print_nodes_at_dist testTree 6
    
print_nodes_at_dist:: BSTree -> Int -> IO()
print_nodes_at_dist tree distance = 
    foldr ((>>) . putStr) (return ()) (printNodeAtDist tree distance)
    
printNodeAtDist:: BSTree -> Int -> [String]
printNodeAtDist Null _ =  []
printNodeAtDist (Node n left right) distance = 
    print' (Node n left right) distance 0
    where
        print':: BSTree -> Int -> Int -> [String]
        print' Null _ _ = []
        print' (Node v l r) dist actualDistance 
            | actualDistance == distance = [show v ++ " "]
            | otherwise = print' l dist d ++ print' r dist d
                where d = actualDistance + 1
    
    
