type Signal = Double
type Weight = Double
type Signals = [Signal]
type Weights = [Weight]

-- esempio di sistema di due neuroni in cascata... ora come modifico i pesi del primo neurone?
system :: Signals -> Weights -> Signal
system [] _ = 0
system signals weights = hebbianneuron [hebbianneuron signals weights binary] weights binary 

perceptron :: Signals -> (Signals -> Signal) -> Signal
perceptron [] _ = 0
perceptron signals f = f signals

hebbianneuron :: Signals -> Weights -> (Signals -> Signal) -> Signal
hebbianneuron [] _ _ = 0
hebbianneuron signals weights f = f (weightSignals signals weights)

weightSignals :: Signals -> Weights -> Signals
weightSignals [] _ = []
weightSignals _ [] = []
weightSignals (x:xs) (y:ys) = x * y : weightSignals xs ys

weightSignal :: Signal -> Weight -> Signal
weightSignal 0 _ = 0
weightSignal _ 0 = 0
weightSignal signal weight = signal * weight

-- neuron functions
binary :: Signals -> Signal
binary [] = 0
binary (x:xs) = binary' (x:xs) 0 0.5 

binary' :: Signals -> Signal -> Signal -> Signal
binary' [] result threshold
    | result >= threshold = 1
    | otherwise = 0
binary' (x:xs) result threshold = binary' xs (result + x) threshold