-- Matteo Francia, 746610

-- TEST                                     EXPECTED RESULT
-- countStar testList1                      9
-- printableSeq testList1                   "*.***...**.***"
-- printable swapSeq testList1              ".*...***..*..."
-- printable zipSeq testList1               "*.***.**.***"
-- starSublistLengths testList1             [1,3,2,3]
-- maxStarSeq testList1                     3
-- matchSeq testList2 testList3             TRUE
-- matchSeq testList2 testList1             FALSE 
-- starOffset testList1                     [(1,1),(3,3),(9,2),(12,3)]
-- occ testList1                            [(1,[1]),(3,[3,12]),(2,[9])]
-- countStarInTree testTree                 7
-- pathTree testTree                        4

-- Data una lista di elementi Elem così definiti:
data Elem  = Dot | Star
data BSTree a = Nil | Node a (BSTree a) (BSTree a)  
testTree :: BSTree Elem  
testTree = Node Star (Node Star (Node Star (Node Star (Node Dot Nil Nil) Nil) (Node Dot Nil Nil)) (Node Star (Node Dot Nil Nil) (Node Dot Nil Nil))) (Node Star (Node Star Nil Nil) Nil) 
testList1 :: [Elem]
testList1 = [Star,Dot,Star,Star,Star,Dot,Dot,Dot,Star,Star,Dot,Star,Star,Star]
testList2 :: [Elem]
testList2 = [Dot,Dot,Dot,Star,Star,Dot,Star]
testList3 :: [Elem]
testList3 = [Star,Star,Dot,Dot,Dot,Star]


countStar :: [Elem] -> Int
-- conta il numero di elementi Star presenti nella lista passata come parametro
countStar [] = 0
countStar (Star:xs) = 1 + countStar xs
countStar (Dot:xs) = countStar xs

printableSeq :: [Elem] -> String
-- restituisce una rappresentazione testuale della sequenze, ove Dot è rappresentato 
-- dal carattere ‘.’ e Star dal carattere ‘*’. Questa funzione è utile ogni volta si 
-- voglia stampare una lista di Elem. 
printableSeq [] = []
printableSeq (Dot:xs) = '.' : printableSeq xs 
printableSeq (Star:xs) = '*' : printableSeq xs

printable :: ([Elem] -> [Elem]) -> [Elem] -> String
printable _ [] = []
printable f (x:xs) = printableSeq (f (x:xs))




swapSeq :: [Elem] -> [Elem]
-- restituisce una sequenza pari a quella passata l in cui ogni valore Dot è sostituito con uno Star e viceversa.
swapSeq [] = []
swapSeq (Dot:xs) = Star : swapSeq xs
swapSeq (Star:xs) = Dot : swapSeq xs





zipSeq :: [Elem] -> [Elem]
-- data una lista di Elem restituisce una lista in cui le sequenze di 1 o più Dot sono sostituite da un solo Dot.
zipSeq [] = []
zipSeq (Dot:(Dot:xs)) = zipSeq(Dot : xs)
zipSeq (x:xs) = x : zipSeq xs 



    

-- BEGIN starSublistLengths
-- ritorna una lista i cui elementi sono le lunghezze delle sottosequenze di Star
starSublistLengths :: [Elem] -> [Int]  
starSublistLengths [] = []
starSublistLengths (x:xs) = starSublistLengths' (starOffset (x:xs))

starSublistLengths' :: [(Int,Int)] -> [Int] 
starSublistLengths' [] = []
starSublistLengths' ((offset,length):xs) = length : starSublistLengths' xs
-- END starSublistLengths



-- BEGIN startOffset 
-- ritorna una lista di coppie (offset, length), offset indica
-- l'offset in cui inizia la sequenza di star, length e lunghezza della sequenza di star
starOffset :: [Elem] -> [(Int,Int)]
starOffset [] = []
starOffset (x:xs) = starOffset' (x:xs) 1 0

starOffset' :: [Elem] -> Int -> Int -> [(Int,Int)]
starOffset' [] offset size 
    | size /= 0 = [(offset, size)]
    | otherwise =  []
starOffset' (Dot:xs) offset size 
    | size /= 0 = (offset, size) : starOffset' xs (size + offset + 1) 0
    | otherwise =  starOffset' xs (size + offset + 1) 0 
starOffset' (Star:xs) offset size = starOffset' xs offset (size + 1)
--- END startOffset






  
-- ritorna la lista di offset delle sequenze di Star aventi la stessa lunghezza
filterEqual' :: [(Int, Int)] -> Int -> [Int]
filterEqual' [] _ = []
filterEqual' ((a,b):xs) n 
    | b == n = a : filterEqual' xs n 
    | otherwise = filterEqual' xs n 

-- restituisce una lista di coppie (offset, lunghezza) che soddisfano il preditcato
-- length /= n    
discriminate' :: [(Int, Int)] -> Int -> [(Int, Int)]
discriminate' [] _  = []
discriminate' ((a,b):xs) n
    | b /= n = (a,b) : discriminate' xs n
    | otherwise = discriminate' xs n

occ :: [Elem] -> [(Int, [Int])]
-- computa il numero di occorrenze e relative posizioni delle sequenze di Star presenti nella lista
occ [] = []
occ elems = occ' list list
        where 
            list = starOffset elems
            
occ' :: [(Int,Int)] -> [(Int,Int)] -> [(Int, [Int])] 
occ' [] _ = []
occ' ((offset,length):xs) elems = (length, filterEqual' elems length) : occ' (discriminate' xs length) elems







max' :: [Int] -> Int
max' [] = 0
max' (x:xs) 
    | x > max' xs = x
    | otherwise = max' xs

maxStarSeq :: [Elem] -> Int
-- computa la lunghezza della sequenza di Star più lunga
maxStarSeq [] = 0
maxStarSeq (x:xs) = max' (starSublistLengths (x:xs))








listEqual :: [Int] -> [Int] -> Bool
listEqual [] [] = True
listEqual (x:xs) [] = False
listEqual [] (x:xs) = False
listEqual (y:ys) (x:xs)
    | x /= y = False
    | otherwise = listEqual xs ys
    
matchSeq :: [Elem] -> [Elem] -> Bool 
-- date due liste di Elem restituisce vero se le liste contengono le medesime sequenze di Star in ordine, 
-- a prescindere dal numero di Dot in mezzo
matchSeq [] [] = True
matchSeq list1 list2 = listEqual (starSublistLengths list1) (starSublistLengths list2)






countStarInTree :: BSTree Elem -> Int
-- conta il numero di elementi Star presenti nell’albero t
countStarInTree Nil = 0 
countStarInTree (Node Star left right) = 1 + countStarInTree left + countStarInTree right
countStarInTree (Node Dot left right) = countStarInTree left + countStarInTree right






pathTree :: BSTree Elem -> Int
-- determina la lunghezza del ramo più profondo composto da soli elementi Star
pathTree Nil = 0 
pathTree (Node Dot _ _) = 0
pathTree (Node Star left right) 
    | sx >= dx = 1 + sx
    | otherwise = 1 + dx
    where 
        sx = pathTree left
        dx = pathTree right
