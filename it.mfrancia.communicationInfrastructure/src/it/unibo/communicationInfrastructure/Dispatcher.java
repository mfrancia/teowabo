package it.unibo.communicationInfrastructure;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.unibo.communicationInfrastructure.interfaces.IDispatcher;
import it.unibo.communicationInfrastructure.interfaces.IMessage;

import java.util.ArrayList;
import java.util.HashMap;

public class Dispatcher implements IDispatcher {
	private final HashMap<String, ArrayList<IInput<IMessage>>>	map;
	
	public Dispatcher() {
		map = new HashMap<String, ArrayList<IInput<IMessage>>>();
	}
	
	@Override
	public void addSupplier(final IInput<IMessage> supplier, final String id) {
		ArrayList<IInput<IMessage>> temp = map.get(id);
		if (temp == null) {
			temp = new ArrayList<IInput<IMessage>>();
		}
		temp.add(supplier);
		map.put(id, temp);
	}
	
	@Override
	public void doTask(final IInputSource ins, final IMessage src) {
		String id = "";
		String msg = "";
		final String content = src.getContent();
		int i = 0;
		for (; (i < content.length()) && (content.charAt(i) != '('); i++) {
			id += content.charAt(i);
		}
		i++;
		for (; i < (content.length() - 1); i++) {
			msg += content.charAt(i);
		}
		
		final ArrayList<IInput<IMessage>> temp = map.get(id);
		if (temp != null) {
			for (final IInput<IMessage> in : temp) {
				in.doTask(ins, new Message(src.getSocket(), msg, src.getSequenceN()));
			}
		}
	}
	
}
