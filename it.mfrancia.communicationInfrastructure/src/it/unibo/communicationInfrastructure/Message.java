package it.unibo.communicationInfrastructure;

import it.unibo.communicationInfrastructure.interfaces.IMessage;
import it.unibo.communicationInfrastructure.interfaces.ISocket;

public class Message implements IMessage {
	private final String	content;
	private final int		seqN;
	private final ISocket	socket;
	
	public Message(final ISocket socket, final String content, final int seqN) {
		this.content = content;
		this.socket = socket;
		this.seqN = seqN;
	}
	
	@Override
	public String getContent() {
		return content;
	}
	
	@Override
	public int getSequenceN() {
		return seqN;
	}
	
	@Override
	public ISocket getSocket() {
		return socket;
	}
	
}
