package it.unibo.communicationInfrastructure.udp;

import it.mfrancia.interfaces.IOutput;
import it.unibo.communicationInfrastructure.interfaces.IMessage;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;

/**
 * send the specified message
 * 
 * @author Matteo Francia
 * 
 */
public class Sender implements IOutput<IMessage> {
	@Override
	public void addOutput(final IMessage o) {
		byte[] sendData = new byte[4028];
		sendData = o.getContent().getBytes();
		try {
			new DatagramSocket().send(new DatagramPacket(sendData, o.getContent().length(), InetAddress.getByName(o.getSocket().getIp()) /* IP */, o.getSocket().getPort()) /* PORT */);
		} catch (final SocketException e) {
			e.printStackTrace();
		} catch (final UnknownHostException e) {
			e.printStackTrace();
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
	
}
