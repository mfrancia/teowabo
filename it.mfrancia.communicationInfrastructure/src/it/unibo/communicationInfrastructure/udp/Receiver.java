package it.unibo.communicationInfrastructure.udp;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;
import it.unibo.communicationInfrastructure.MySocket;
import it.unibo.communicationInfrastructure.interfaces.IMessage;
import it.unibo.communicationInfrastructure.interfaces.ISocket;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Receiver implements IInputSource {
	private final IInput<IMessage>	in;
	private IOutput<String>			log;
	private final byte[]			receiveData	= new byte[4028];
	private final DatagramSocket	serverSocket;
	private final Thread			t			= new Thread() {
		@Override
		public void run() {
			while (true) {
				final DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);
				try {
					serverSocket.receive(receivePacket);
					final IMessage msg = new IMessage() {
						@Override
						public String getContent() {
							return new String(receivePacket.getData());
						}
						
						@Override
						public int getSequenceN() {
							// TODO Auto-generated method stub
							return 0;
						}
						
						@Override
						public ISocket getSocket() {
							
							return new MySocket(receivePacket.getAddress().toString(), receivePacket.getPort());
						}
					};
					
					if (in != null) {
						in.doTask(Receiver.this, msg);
					}
					if (log != null) {
						log.addOutput(new String(receivePacket.getData()));
					} else {
						System.out.println("received: " + new String(receivePacket.getData()));
					}
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		}
	};
	
	public Receiver(final IInput<IMessage> in, final int port) throws SocketException {
		serverSocket = new DatagramSocket(port);
		this.in = in;
		t.start();
	}
	
	public Receiver(final IInput<IMessage> in, final int port, final IOutput<String> log) throws SocketException {
		serverSocket = new DatagramSocket(port);
		this.log = log;
		this.in = in;
		t.start();
	}
}
