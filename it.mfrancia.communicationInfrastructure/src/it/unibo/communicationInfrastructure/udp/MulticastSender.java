package it.unibo.communicationInfrastructure.udp;

import it.mfrancia.interfaces.IOutput;
import it.unibo.communicationInfrastructure.Message;
import it.unibo.communicationInfrastructure.MySocket;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;

/**
 * Send a message to many destinations
 * 
 * @author Matteo Francia
 * 
 */
public class MulticastSender implements IOutput<String> {
	private final ArrayList<InetAddress>	ipAddr;
	private IOutput<String>					log;
	private final ArrayList<Integer>		ports;
	private final Sender					sender;
	
	public MulticastSender(final String[] sockets) throws SocketException, UnknownHostException {
		ipAddr = new ArrayList<InetAddress>();
		ports = new ArrayList<Integer>();
		for (final String socket : sockets) {
			final String[] s = socket.split(":");
			ipAddr.add(InetAddress.getByName(s[0]));
			ports.add(Integer.parseInt(s[1]));
		}
		sender = new Sender();
	}
	
	public MulticastSender(final String[] sockets, final IOutput<String> log) throws SocketException, UnknownHostException {
		ipAddr = new ArrayList<InetAddress>();
		ports = new ArrayList<Integer>();
		for (final String socket : sockets) {
			final String[] s = socket.split(":");
			ipAddr.add(InetAddress.getByName(s[0]));
			ports.add(Integer.parseInt(s[1]));
		}
		sender = new Sender();
		this.log = log;
	}
	
	@Override
	public void addOutput(final String o) {
		for (int i = 0; i < ipAddr.size(); i++) {
			sender.addOutput(new Message(new MySocket(ipAddr.get(i).getHostAddress() /* IP */, ports.get(i)), o, 0));
			if (log != null) {
				log.addOutput(o);
			}
		}
	}
}
