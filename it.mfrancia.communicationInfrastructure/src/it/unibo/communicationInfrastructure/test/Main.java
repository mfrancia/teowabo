package it.unibo.communicationInfrastructure.test;

import it.mfrancia.interfaces.IOutput;
import it.unibo.communicationInfrastructure.Dispatcher;
import it.unibo.communicationInfrastructure.MyHandler;
import it.unibo.communicationInfrastructure.udp.Receiver;

import java.net.SocketException;

public class Main {
	public Main() throws SocketException {
		final IOutput<String> outToStdOut = new IOutput<String>() {
			
			@Override
			public void addOutput(final String o) {
				System.out.println(o);
			}
			
		};
		final Dispatcher dispatcher = new Dispatcher();
		new Receiver(dispatcher, 9090);
		new MyHandler(dispatcher, "ping", outToStdOut, null);
	}
	
	public void config() {
		
	}
	
	public void init() {
		
	}
}
