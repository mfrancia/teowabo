package it.unibo.communicationInfrastructure;

import it.unibo.communicationInfrastructure.interfaces.ISocket;

public class MySocket implements ISocket {
	private final String	ip;
	private final int		port;
	
	public MySocket(final String ip, final int port) {
		this.ip = ip;
		this.port = port;
	}
	
	@Override
	public String getIp() {
		return ip;
	}
	
	@Override
	public int getPort() {
		return port;
	}
	
}
