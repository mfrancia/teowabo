package it.unibo.communicationInfrastructure.interfaces;

import it.mfrancia.interfaces.IInput;

/**
 * Message dispatcher
 * 
 * @author Matteo Francia
 * 
 */
public interface IDispatcher extends IInput<IMessage> {
	/**
	 * Add a supplier
	 * 
	 * @param supplier
	 *            who want to receive the message
	 * @param ids
	 *            type of the message which the supplier want to receive
	 */
	void addSupplier(IInput<IMessage> supplier, String id);
}
