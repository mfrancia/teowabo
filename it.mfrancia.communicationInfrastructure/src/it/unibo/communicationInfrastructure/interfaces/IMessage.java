package it.unibo.communicationInfrastructure.interfaces;

/**
 * 
 * @author Matteo Francia payload sent through the network
 */
public interface IMessage {
	/**
	 * 
	 * @return message payload
	 */
	String getContent();
	
	/**
	 * 
	 * @return sequence number
	 */
	int getSequenceN();
	
	/**
	 * 
	 * @return source / destination socket
	 */
	ISocket getSocket();
	
}
