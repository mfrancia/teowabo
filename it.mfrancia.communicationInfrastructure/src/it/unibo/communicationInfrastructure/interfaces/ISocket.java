package it.unibo.communicationInfrastructure.interfaces;

public interface ISocket {
	String getIp();
	
	int getPort();
}
