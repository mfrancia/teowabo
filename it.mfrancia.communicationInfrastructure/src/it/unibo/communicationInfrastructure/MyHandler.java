package it.unibo.communicationInfrastructure;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;
import it.unibo.communicationInfrastructure.interfaces.IDispatcher;
import it.unibo.communicationInfrastructure.interfaces.IMessage;

public class MyHandler implements IInput<IMessage>, IInputSource {
	private final IDispatcher		dispatcher;
	private final IInput<IMessage>	inputAdapter;
	private final IOutput<String>	log;
	private final String			messageId;
	
	public MyHandler(final IDispatcher dispatcher, final String messageId, final IOutput<String> log, final IInput<IMessage> inputAdapter) {
		this.dispatcher = dispatcher;
		this.log = log;
		this.messageId = messageId;
		this.inputAdapter = inputAdapter;
		config();
	}
	
	private void config() {
		dispatcher.addSupplier(this, messageId);
	}
	
	@Override
	public void doTask(final IInputSource ins, final IMessage src) {
		if (log != null) {
			log.addOutput("handling " + src.getContent() + " from " + src.getSocket().getIp() + ":" + src.getSocket().getPort());
		}
		if (inputAdapter != null) {
			inputAdapter.doTask(ins, src);
		}
	}
	
}
