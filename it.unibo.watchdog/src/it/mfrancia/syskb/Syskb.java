package it.mfrancia.syskb;

import it.unibo.communicationInfrastructure.Dispatcher;
import it.unibo.communicationInfrastructure.interfaces.IDispatcher;

public class Syskb {
	public static IDispatcher		dispatcher;
	// nodes
	public final static String		nodeA		= "nodeA";
	public final static String		nodeB		= "nodeB";
	public final static String		nodeC		= "nodeC";
	public final static String		nodeD		= "nodeD";
	public final static String		nodeE		= "nodeE";
	public final static String		nodeF		= "nodeF";
	public final static String		nodeG		= "nodeG";
	public final static String		nodeH		= "nodeH";
	public final static String		nodeX		= "nodeX";
	public static String			ping		= "ping";
	public static int				watchdogDt	= 2000;
	public final static String[]	nodes		= new String[] { nodeA, nodeB, nodeE, nodeF,/* nodeC, nodeD, nodeG, */nodeX };
	public IDispatcher getDispatcher() {
		if (dispatcher == null) {
			dispatcher = new Dispatcher();
		}
		return dispatcher;
	}
}
