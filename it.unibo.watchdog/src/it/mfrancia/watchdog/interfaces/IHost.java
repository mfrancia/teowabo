package it.mfrancia.watchdog.interfaces;

public interface IHost {
	String getIp();
	
	boolean isOnline();
	
	void setOffline();
	
	void setOnline();
}
