package it.mfrancia.watchdog.interfaces;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;

/**
 * @author teowabo
 * @version 1.0
 * @created 13-feb-2014 20.34.16
 */
public interface IWatchDog extends IObserver, IObservable {
	
	public void addNode(IWatchdogNode node);
	
	/**
	 * 
	 * @param ping
	 */
	public void addPing(String ping);
	
	public void cleanPings();
	
	public void killApplication();
	
	public void removeNode(IWatchdogNode node);
	
	public void start();
	
	public void startApplication();
	
	public void stop();
	
	public void tick();
	
}