package it.mfrancia.watchdog.upd.test;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.WatchdogFactory;
import it.mfrancia.watchdog.WatchdogObserverLogger;
import it.mfrancia.watchdog.eventNatali.test.Application;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IActivity;
import it.unibo.is.interfaces.IIntent;
import it.unibo.is.interfaces.IOutputView;

import java.io.PrintStream;
import java.util.Date;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogUdpNodeB {
	public static void main(final String[] args) {
		
		// build graphic GUI
		final EnvFrame env = new EnvFrame("nodeB");
		env.init();
		
		final WatchdogUdpNodeB w = new WatchdogUdpNodeB(env.getOutputView(), "nodeB", 9020);
		
		env.addCmdPanel("stop", new String[] { "stop" }, new IActivity() {
			@Override
			public void execAction() {
			}
			
			@Override
			public void execAction(final IIntent input) {
			}
			
			@Override
			public void execAction(final String cmd) {
				// stop the watchdog
				w.setStop();
			}
			
			@Override
			public String execActionWithAnswer(final String cmd) {
				return null;
			}
			
			@Override
			public String getActivityState() {
				return null;
			}
		});
	}
	
	private WatchdogObserverLogger	alice;
	private IWatchDog				bob;
	private final String			name;
	private final IOutputView		outView;
	private final int				port;
	
	public WatchdogUdpNodeB(final IOutputView outView, final String name, final int port) {
		this.name = name;
		this.outView = outView;
		this.port = port;
		try {
			config();
			start();
			outView.addOutput("started");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	public void config() throws Exception {
		bob = new WatchdogFactory().createUdpWatchdog(port, 1000, new String[] { "nodeA", "nodeB" });
		
		final IOutput<String> errorOutput = new IOutput<String>() {
			@Override
			public void addOutput(final String o) {
				outView.addOutput(new Date().toString() + " ALERT!! " + o);
			}
		};
		alice = new WatchdogObserverLogger(errorOutput, new PrintStream("logB.txt"));
		bob.register(alice);
		
		final Application a = new Application(name);
		a.register(bob);
		a.start();
	}
	
	public void setStop() {
		bob.stop();
		alice.closeStream();
		SysKbInfoReplInfr.terminate();
	}
	
	public void start() {
		bob.start();
	}
}
