package it.mfrancia.watchdog.console.gui;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.console.Host;

import java.awt.Insets;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class ConsoleGUI extends JFrame implements IOutput<String[]> {
	private static final long			serialVersionUID	= 1L;
	private final HashMap<String, Host>	components;
	private final GridBag				gb;
	private int							row;
	
	// private JTextArea tArea;
	public ConsoleGUI(final String[] watchdogs) {
		super("Watchdog Console");
		setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
		
		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosed(final java.awt.event.WindowEvent evt) {
				System.exit(0);
			}
		});
		components = new HashMap<String, Host>();
		
		gb = new GridBag();
		row = 0;
		for (final String s : watchdogs) {
			addComponent(s);
		}
		/*
		 * tArea = new JTextArea(); gb.addComponent(new JScrollPane(tArea), row, 0, 1, 1, new Insets(5,5,5,5), 3, 2);
		 */
		getContentPane().add(gb.getContent());
		pack();
		
	}
	
	private void addComponent(final String name) {
		final Host c = new Host();
		final HostGUI gui = new HostGUI();
		c.addGui(gui);
		c.setOffline();
		components.put(name, c);
		gb.addComponent(new JLabel(name), row, 0, 1, 1, new Insets(5, 5, 5, 5), 1, 1);
		gb.addComponent(gui.getGUI(), row, 1, 5, 5, new Insets(5, 5, 5, 5), 1, 1);
		row++;
	}
	
	@Override
	/**
	 * update the GUI
	 * @param o o[0] watchdog name, o[1] watchdog status ("online"/"offline")
	 */
	public void addOutput(final String[] o) {
		if (components.get(o[0]) != null) {
			if (o[1].equals("online")) {
				components.get(o[0]).setOnline();
			} else {
				components.get(o[0]).setOffline();
			}
		} else {
			if (!o[0].equals("")) {
				addComponent(o[0]);
				addOutput(o);
			}
			
		}
		/*
		 * SwingUtilities.invokeLater(new Runnable() { public void run() { tArea.append(o[0]+ " "+o[1] +"\n"); } });
		 */
	}
}
