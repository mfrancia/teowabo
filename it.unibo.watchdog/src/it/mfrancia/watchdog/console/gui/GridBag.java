package it.mfrancia.watchdog.console.gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

/**
 * gestione dell'elemento gridBag
 * 
 * @author Matteo Francia
 * 
 */
public class GridBag {
	private final GridBagConstraints	c;
	private final JPanel				pane;
	
	public GridBag() {
		pane = new JPanel(new GridBagLayout());
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH; // esegue il resize dell'elemento se esso � pi� grande della cella
	}
	
	/**
	 * aggiunge un componente alla griglia
	 * 
	 * @param in
	 *            elemento da aggiungere
	 * @param row
	 *            riga
	 * @param col
	 *            colonna
	 * @param d
	 *            peso con cui si espande il componente orizzontalmente durante il resizing finestra
	 * @param e
	 *            peso con cui si espande il componente verticalmente durante il resizing finestra
	 * @param padding
	 *            padding del componente
	 * @param rowSpan
	 *            numero di righe occupate dall'oggetto
	 * @param colSpan
	 *            numero di colonne occupate dall'oggetto
	 */
	public void addComponent(final java.awt.Component in, final int row, final int col, final double d, final double e, final Insets padding, final int rowSpan, final int colSpan) {
		c.gridx = col; // colonna
		c.gridy = row; // riga
		c.weightx = d; // peso con cui si espande il componente orizzontalmente durante il resizing finestra
		c.weighty = e; // peso con cui si espande il componente verticalmente durante il resizing finestra
		c.insets = padding; // padding del componente
		c.gridwidth = colSpan; // numero di colonne occupate dall'oggetto
		c.gridheight = rowSpan; // numero di righe occupate dall'oggetto
		pane.add(in, c); // aggiunge elemento al pannello
	}
	
	/**
	 * ritorna il pannello contenente gli elementi presente nella griglia
	 * 
	 * @return panel contenente gli elementi della griglia
	 */
	public JPanel getContent() {
		return pane;
	}
	
	/**
	 * rimuove un componente dalla griglia
	 * 
	 * @param in
	 *            componente da rimuovere
	 */
	public void removeComponent(final java.awt.Component in) {
		pane.remove(in);
	}
}
