package it.mfrancia.watchdog.console.gui;

import it.mfrancia.interfaces.IOutput;

import java.awt.Color;

import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

// import javax.swing.JPanel;

public class HostGUI implements IOutput<Boolean> {
	// private JPanel panel;
	private final JLabel	label;
	
	public HostGUI() {
		label = new JLabel();
		label.setOpaque(true);
	}
	
	@Override
	public void addOutput(final Boolean o) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				if (o == false) {
					label.setText("offline");
					label.setBackground(Color.red);
				} else {
					label.setText("online");
					label.setBackground(Color.green);
				}
			}
		});
	}
	
	public JComponent getGUI() {
		/*
		 * if (panel == null) { panel = new JPanel(); panel.add(label); } return panel;
		 */
		return label;
	}
	
}
