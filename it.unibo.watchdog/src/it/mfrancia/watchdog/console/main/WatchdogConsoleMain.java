package it.mfrancia.watchdog.console.main;

import it.mfrancia.syskb.Syskb;
import it.mfrancia.watchdog.Pinger;
import it.mfrancia.watchdog.console.WatchdogObserverUpdateGUI;
import it.mfrancia.watchdog.console.gui.ConsoleGUI;
import it.mfrancia.watchdog.eventNatali.ExtendedWatchdogFactory;
import it.mfrancia.watchdog.eventNatali.NodeJava;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogConsoleMain extends NodeJava {
	public static void main(final String[] args) {
		new WatchdogConsoleMain("watchdogconsole", Syskb.nodes);
	}
	
	public WatchdogConsoleMain(final String name, final String[] watchdogs) {
		super(null, name, new String[] { Syskb.ping } /* out */, new String[] { Syskb.ping } /* in */);
		final ConsoleGUI c = new ConsoleGUI(watchdogs);
		c.setVisible(true);
		
		// configuring watchdog
		final IWatchDog bob = new ExtendedWatchdogFactory().createNataliWatchdog(name, Syskb.watchdogDt, watchdogs, SysKbInfoReplInfr.njs);
		final WatchdogObserverUpdateGUI alice = new WatchdogObserverUpdateGUI(c);
		bob.register(alice);
		bob.start();
		
		final Pinger a = new Pinger(name, Syskb.watchdogDt / 4);
		a.register(bob);
		a.start();
		
	}
}
