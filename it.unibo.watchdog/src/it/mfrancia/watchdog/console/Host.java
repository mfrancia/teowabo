package it.mfrancia.watchdog.console;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.interfaces.IHost;

import java.util.ArrayList;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Host implements IHost {
	private final ArrayList<IOutput<Boolean>>	GUIs;
	private boolean								online;
	
	public Host() {
		GUIs = new ArrayList<IOutput<Boolean>>();
		setOffline();
	}
	
	public void addGui(final IOutput<Boolean> gui) {
		GUIs.add(gui);
	}
	
	@Override
	public String getIp() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isOnline() {
		return online;
	}
	
	@Override
	public void setOffline() {
		online = false;
		updateGUI();
	}
	
	@Override
	public void setOnline() {
		online = true;
		updateGUI();
		
	}
	
	private void updateGUI() {
		
		for (final IOutput<Boolean> o : GUIs) {
			o.addOutput(isOnline());
		}
	}
}
