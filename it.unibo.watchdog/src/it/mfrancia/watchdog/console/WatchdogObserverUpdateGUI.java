package it.mfrancia.watchdog.console;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.WatchdogObserver;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogObserverUpdateGUI extends WatchdogObserver {
	private final IOutput<String[]>	out;
	
	public WatchdogObserverUpdateGUI(final IOutput<String[]> out) {
		this.out = out;
	}
	
	@Override
	public void doJob(final HashMap<String, Integer> info) {
		final HashMap<String, Integer> myMap = info;
		final Iterator<Entry<String, Integer>> entries = myMap.entrySet().iterator();
		while (entries.hasNext()) {
			final Entry<String, Integer> thisEntry = entries.next();
			final int val = thisEntry.getValue();
			if (val <= 0) {
				if (out != null) {
					out.addOutput(new String[] { thisEntry.getKey(), "offline" });
				}
			} else {
				if (out != null) {
					out.addOutput(new String[] { thisEntry.getKey(), "online" });
				}
			}
		}
	}
}
