package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;

import java.util.HashMap;

/**
 * 
 * @author Matteo Francia
 * 
 */
public abstract class WatchdogObserver implements IObserver {
	public abstract void doJob(HashMap<String, Integer> info);
	
	@SuppressWarnings("unchecked")
	@Override
	public synchronized void update(final IObservable src, final Object info) {
		doJob((HashMap<String, Integer>) info);
	}
}
