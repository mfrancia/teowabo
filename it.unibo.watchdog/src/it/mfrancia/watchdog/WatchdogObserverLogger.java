package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IOutput;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogObserverLogger extends WatchdogObserver {
	private DateFormat				dateFormat;
	private final IOutput<String>	out;
	private final PrintStream		outStream;
	private final int				threshold;
	
	public WatchdogObserverLogger(final IOutput<String> out) {
		this.out = out;
		threshold = 0;
		dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		outStream = null;
	}
	
	public WatchdogObserverLogger(final IOutput<String> out, final int threshold, final PrintStream outputStream) {
		this.out = out;
		this.threshold = threshold;
		outStream = outputStream;
	}
	
	public WatchdogObserverLogger(final IOutput<String> out, final PrintStream outputStream) {
		this.out = out;
		threshold = 0;
		dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		outStream = outputStream;
	}
	
	public WatchdogObserverLogger(final PrintStream outputStream) {
		out = null;
		threshold = 0;
		dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		outStream = outputStream;
	}
	
	public void closeStream() {
		outStream.flush();
		outStream.close();
	}
	
	@Override
	public void doJob(final HashMap<String, Integer> info) {
		final HashMap<String, Integer> myMap = info;
		final Iterator<Entry<String, Integer>> entries = myMap.entrySet().iterator();
		String s = "";
		while (entries.hasNext()) {
			final Entry<String, Integer> thisEntry = entries.next();
			final int val = thisEntry.getValue();
			if (val <= threshold) {
				if (out != null) {
					out.addOutput(thisEntry.getKey());
				}
				s += thisEntry.getKey() + ": " + val + "\n";
			}
		}
		
		if (!s.equals("") && (outStream != null)) {
			outStream.println(dateFormat.format(new Date()));
			outStream.println(s);
		}
	}
}
