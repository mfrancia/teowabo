package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IOutput;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogObserverPerformanceTester extends WatchdogObserver {
	private int						count;
	private final IOutput<String>	out;
	private boolean					stop;
	
	public WatchdogObserverPerformanceTester(final IOutput<String> out, final int dt) {
		this.out = out;
		count = 0;
		stop = false;
		new Thread() {
			@Override
			public void run() {
				while (!stop) {
					try {
						Thread.sleep(dt);
						synchronized (this) {
							out.addOutput("Ping lost: " + getCount() + "/" + dt + "ms");
							reset();
						}
					} catch (final InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}.start();
	}
	
	@Override
	public void doJob(final HashMap<String, Integer> info) {
		final HashMap<String, Integer> myMap = info;
		final Iterator<Entry<String, Integer>> entries = myMap.entrySet().iterator();
		
		while (entries.hasNext()) {
			final Entry<String, Integer> thisEntry = entries.next();
			final int val = thisEntry.getValue();
			if (val <= 0) {
				if (out != null) {
					out.addOutput(thisEntry.getKey());
				}
				inc();
			}
		}
	}
	
	public synchronized int getCount() {
		return count;
	}
	
	public synchronized void inc() {
		count++;
	}
	
	public synchronized void reset() {
		count = 0;
	}
	
	public void setStop() {
		stop = true;
	}
	
}
