package it.mfrancia.watchdog.eventNatali;

import it.unibo.event.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class RemoteEventDisplayHandler extends EventHandler {
	
	public RemoteEventDisplayHandler(final String name, final IOutputView outView) throws Exception {
		super(name, outView);
	}
	
	@Override
	public void doJob() throws Exception {
		IEventItem ev = getEventItem();
		while (ev != null) {
			showMsg(ev.getEventId() + " / " + ev.getMsg());
			ev = getEventItem();
		}
	}
	
}
