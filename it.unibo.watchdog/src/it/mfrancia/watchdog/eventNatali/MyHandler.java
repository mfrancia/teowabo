package it.mfrancia.watchdog.eventNatali;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.unibo.event.interfaces.IEventItem;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class MyHandler extends EventHandler implements IInputSource {
	private final IInput<String[]>	in;
	
	public MyHandler(final String name, final IOutputView view, final String[] evIds, final IInput<String[]> in) throws Exception {
		super(name, view);
		for (final String evId : evIds) {
			SysKbInfoReplInfr.getInstance(null, null).addEventHandler(this, evId);
		}
		this.in = in;
	}
	
	@Override
	public void doJob() throws Exception {
		final IEventItem ev = getEventItem();
		if (ev != null) {
			in.doTask(this, new String[] { ev.getEventId(), ev.getMsg() });
		}
	}
	
}
