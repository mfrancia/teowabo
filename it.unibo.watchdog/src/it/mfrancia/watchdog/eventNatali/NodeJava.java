package it.mfrancia.watchdog.eventNatali;

import it.unibo.inforepl.infrastructure.PerceiveConnectionThread;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.system.SituatedActiveObject;

import java.io.FileInputStream;

/**
 * 
 * @author Matteo Francia
 * 
 */
public abstract class NodeJava extends SituatedActiveObject {
	private final IOutputView	outView;
	protected SysKbInfoReplInfr	sysInfr;
	protected boolean			terminated	= false;
	
	public NodeJava(final IOutputView outView, final String name, final String[] myEvents, final String[] otherEvents) {
		super(outView, name);
		this.outView = outView;
		try {
			createInfrastructure(name, myEvents, otherEvents);
			PerceiveConnectionThread.terminated = false;
		} catch (final Exception e) {
			println("ERROR " + e.getMessage());
		}
		
	}
	
	protected void createInfrastructure(final String myNode, final String[] infoOut, final String[] infoIn) throws Exception {
		// println("Node createInfrastructure 1 " );
		PerceiveConnectionThread.resetMyInstances();
		sysInfr = SysKbInfoReplInfr.getInstance(outView, new FileInputStream("sysConfig.pl"));
		// println("Node createInfrastructure 2 " + sysInfr);
		sysInfr.createInfoReplInfrastructure(myNode, infoOut);
		createRemoteEventDisplayHandler(infoIn);
	}
	
	protected void createRemoteEventDisplayHandler(final String[] infoIn) throws Exception {
		final RemoteEventDisplayHandler evh0 = new RemoteEventDisplayHandler("rh" + getName(), outView);
		for (final String element : infoIn) {
			sysInfr.addEventHandler(evh0, element);
		}
	}
	
	@Override
	protected void doJob() throws Exception {
	}
	
	@Override
	protected void endWork() throws Exception {
		/*
		 * println("ENDS " ); terminated = true; PerceiveConnectionThread.terminated = true; SysKbInfoReplInfr.terminate();
		 */
	}
	
	@Override
	protected void startWork() throws Exception {
		PerceiveConnectionThread.terminated = false;
	}
	
	public void terminate() throws Exception {
		endWork();
	}
	
}
