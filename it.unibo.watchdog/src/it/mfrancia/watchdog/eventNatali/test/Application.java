package it.mfrancia.watchdog.eventNatali.test;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;

import java.util.Random;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Application extends Thread implements IObservable {
	private final String	name;
	private IObserver		o;
	
	public Application(final String name) {
		this.name = name;
	}
	
	@Override
	public void register(final IObserver src) {
		o = src;
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(new Random().nextInt(500));
				o.update(this, name);
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
