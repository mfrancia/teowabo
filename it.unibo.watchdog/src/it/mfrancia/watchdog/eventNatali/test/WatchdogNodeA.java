package it.mfrancia.watchdog.eventNatali.test;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.syskb.Syskb;
import it.mfrancia.watchdog.WatchdogObserverLogger;
import it.mfrancia.watchdog.eventNatali.ExtendedWatchdogFactory;
import it.mfrancia.watchdog.eventNatali.NodeJava;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IActivity;
import it.unibo.is.interfaces.IIntent;
import it.unibo.is.interfaces.IOutputView;

import java.awt.Color;
import java.io.PrintStream;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogNodeA extends NodeJava {
	public static void main(final String[] args) {
		
		// build graphic GUI
		final EnvFrame env = new EnvFrame("nodeA", Color.yellow, Color.black);
		env.init();
		
		final WatchdogNodeA w = new WatchdogNodeA(env.getOutputView(), "nodeA", new String[] { Syskb.ping }, new String[] { Syskb.ping });
		
		env.addCmdPanel("stop", new String[] { "stop" }, new IActivity() {
			@Override
			public void execAction() {
			}
			
			@Override
			public void execAction(final IIntent input) {
			}
			
			@Override
			public void execAction(final String cmd) {
				// stop the watchdog
				w.setStop();
			}
			
			@Override
			public String execActionWithAnswer(final String cmd) {
				return null;
			}
			
			@Override
			public String getActivityState() {
				return null;
			}
		});
	}
	
	private WatchdogObserverLogger	alice;
	private IWatchDog				bob;
	private final String			name;
	
	private final IOutputView		outView;
	
	public WatchdogNodeA(final IOutputView outView, final String name, final String[] myEvents, final String[] otherEvents) {
		super(outView, name, myEvents, otherEvents);
		this.name = name;
		this.outView = outView;
		try {
			config();
			start();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	public void config() throws Exception {
		// configuring watchdog
		bob = new ExtendedWatchdogFactory().createNataliWatchdog(name, Syskb.watchdogDt, new String[] { "nodeA", "nodeB" }, SysKbInfoReplInfr.njs);
		final IOutput<String> output = new IOutput<String>() {
			@Override
			public void addOutput(final String o) {
				outView.addOutput("\nALERT!!! " + o + "\n");
			}
		};
		alice = new WatchdogObserverLogger(output, new PrintStream("logA.txt"));
		bob.register(alice);
	}
	
	public void setStop() {
		bob.stop();
		alice.closeStream();
		SysKbInfoReplInfr.terminate();
	}
	
	@Override
	public void start() {
		final Application a = new Application(name);
		a.register(bob);
		a.start();
		bob.start();
	}
}
