package it.mfrancia.watchdog.eventNatali;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.syskb.Syskb;
import it.mfrancia.watchdog.Watchdog;
import it.mfrancia.watchdog.WatchdogFactory;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.event.interfaces.INodejsLike;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class ExtendedWatchdogFactory extends WatchdogFactory {
	public IWatchDog createNataliWatchdog(final String nodeName, final int dt, final String[] watchdogs, final INodejsLike njs) {
		final IOutput<String[]> outToNjs = new IOutput<String[]>() {
			
			@Override
			public void addOutput(final String[] o) {
				try {
					njs.raiseEvent(null, o[0], o[1]);
				} catch (final Exception e) {
					e.printStackTrace();
				}
			}
			
		};
		
		final IWatchDog bob = new Watchdog(dt, outToNjs, watchdogs);
		
		try {
			new MyHandler(nodeName, null, new String[] { Syskb.ping }, new IInput<String[]>() {
				@Override
				public void doTask(final IInputSource ins, final String[] src) {
					final String msg = src[1];
					bob.addPing(msg);
				}
			});
		} catch (final Exception e) {
			e.printStackTrace();
		}
		return bob;
	}
	
}
