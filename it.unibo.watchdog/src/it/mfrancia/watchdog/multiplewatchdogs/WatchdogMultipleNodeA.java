package it.mfrancia.watchdog.multiplewatchdogs;

import it.unibo.baseEnv.basicFrame.EnvFrame;
import it.unibo.is.interfaces.IActivity;
import it.unibo.is.interfaces.IIntent;

import java.awt.Color;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogMultipleNodeA {
	
	public static void main(final String[] args) {
		
		// build graphic GUI
		final EnvFrame env = new EnvFrame("nodeA", Color.yellow, Color.black);
		env.init();
		
		final WatchdogMultipleNode w = new WatchdogMultipleNode(env.getOutputView(), "nodeA", 9010, 200);
		
		env.addCmdPanel("stop", new String[] { "stop" }, new IActivity() {
			@Override
			public void execAction() {
			}
			
			@Override
			public void execAction(final IIntent input) {
			}
			
			@Override
			public void execAction(final String cmd) {
				// stop the watchdog
				w.setStop();
			}
			
			@Override
			public String execActionWithAnswer(final String cmd) {
				return null;
			}
			
			@Override
			public String getActivityState() {
				return null;
			}
		});
	}
}
