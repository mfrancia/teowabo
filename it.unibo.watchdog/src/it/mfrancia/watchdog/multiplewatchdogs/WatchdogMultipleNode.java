package it.mfrancia.watchdog.multiplewatchdogs;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.Pinger;
import it.mfrancia.watchdog.WatchdogFactory;
import it.mfrancia.watchdog.WatchdogObserverPerformanceTester;
import it.mfrancia.watchdog.eventNatali.ExtendedWatchdogFactory;
import it.mfrancia.watchdog.eventNatali.NodeJava;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IOutputView;

import java.util.Date;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogMultipleNode extends NodeJava {
	private WatchdogObserverPerformanceTester	alice;
	private WatchdogObserverPerformanceTester	anna;
	private IWatchDog							bob;
	private final int							dt;
	private IWatchDog							fester;
	private final String						name;
	private String[]							nodes;
	private final IOutputView					outView;
	private final int							port;
	
	public WatchdogMultipleNode(final IOutputView outView, final String name, final int port, final int dt) {
		super(null, name, new String[] { "ping" }, new String[] { "ping" });
		this.name = name;
		this.outView = outView;
		this.port = port;
		this.dt = dt;
		try {
			config();
			start();
			outView.addOutput("started");
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	public void config() throws Exception {
		nodes = new String[] { "nodeA", "nodeB" };
		// configuring watchdog
		bob = new ExtendedWatchdogFactory().createNataliWatchdog(name, dt, nodes, SysKbInfoReplInfr.njs);
		final IOutput<String> output = new IOutput<String>() {
			@Override
			public void addOutput(final String o) {
				outView.addOutput(new Date().toString() + " ALERT!! natali " + o);
			}
		};
		alice = new WatchdogObserverPerformanceTester(output, dt * 10);
		bob.register(alice);
		
		fester = new WatchdogFactory().createUdpWatchdog(port, dt, nodes);
		final IOutput<String> errorOutput = new IOutput<String>() {
			@Override
			public void addOutput(final String o) {
				outView.addOutput(new Date().toString() + " ALERT!! udp " + o);
			}
		};
		anna = new WatchdogObserverPerformanceTester(errorOutput, dt * 10);
		fester.register(anna);
		// bob.register(anna); OK, potrei usare anche lo stesso observer
	}
	
	public void setStop() {
		bob.stop();
		SysKbInfoReplInfr.terminate();
	}
	
	@Override
	public void start() {
		bob.start();
		fester.start();
		
		final Pinger a = new Pinger(name, dt / 2);
		a.register(bob);
		a.register(fester);
		a.start();
	}
	
}
