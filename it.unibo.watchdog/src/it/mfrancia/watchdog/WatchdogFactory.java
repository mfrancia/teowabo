package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.unibo.communicationInfrastructure.interfaces.IMessage;
import it.unibo.communicationInfrastructure.udp.MulticastSender;
import it.unibo.communicationInfrastructure.udp.Receiver;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class WatchdogFactory implements IInputSource {
	public static String		configFile	= "myConfig.txt";
	public static InputStream	inS			= null;
	
	public IWatchDog createUdpWatchdog(final int myport, final int dt, final String[] watchdogs) throws IOException {
		final ArrayList<String> sockets = new ArrayList<String>();
		if (inS == null) {
			inS = new FileInputStream(configFile);
		}
		final BufferedReader br = new BufferedReader(new InputStreamReader(inS));
		String strLine;
		// Read File Line By Line
		while ((strLine = br.readLine()) != null) {
			sockets.add(strLine);
		}
		// Close the input streams
		inS.close();
		br.close();
		
		String[] ret = new String[sockets.size()];
		ret = sockets.toArray(ret);
		
		final MulticastSender sender = new MulticastSender(ret);
		
		final IWatchDog bob = new Watchdog(dt, new IOutput<String[]>() {
			@Override
			public void addOutput(final String[] o) {
				sender.addOutput(o[1] /* msg */);
			}
		}, watchdogs);
		
		final IInput<IMessage> watchdogAdapter = new IInput<IMessage>() {
			
			@Override
			public void doTask(final IInputSource ins, final IMessage src) {
				bob.addPing(src.getContent());
			}
			
		};
		
		new Receiver(watchdogAdapter, myport);
		
		return bob;
	}
}
