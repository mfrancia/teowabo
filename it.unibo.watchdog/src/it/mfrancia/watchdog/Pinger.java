package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;

import java.util.ArrayList;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Pinger extends Thread implements IObservable {
	private final int					dt;
	private final String				name;
	private final ArrayList<IObserver>	observers;
	
	public Pinger(final String name, final int dt) {
		this.name = name;
		this.dt = dt;
		observers = new ArrayList<IObserver>();
	}
	
	@Override
	public void register(final IObserver src) {
		observers.add(src);
	}
	
	@Override
	public void run() {
		while (true) {
			try {
				Thread.sleep(dt);
				for (final IObserver o : observers) {
					o.update(this, name);
				}
			} catch (final InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
