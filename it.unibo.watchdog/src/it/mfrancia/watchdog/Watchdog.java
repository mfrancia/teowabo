package it.mfrancia.watchdog;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.syskb.Syskb;
import it.mfrancia.watchdog.interfaces.IWatchDog;
import it.mfrancia.watchdog.interfaces.IWatchdogNode;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Watchdog implements IWatchDog {
	private int							dt;
	private IObservable					myself;
	private IObserver					observer;		// notified after every tick()
	private String[]					otherWatchdogs;
	private IOutput<String[]>			out;
	private HashMap<String, Integer>	pings;
	private boolean						stopped;
	private Thread						t;
	private ArrayList<IWatchdogNode>	watchdogNodes;
	
	/**
	 * Watchdog constructor
	 * 
	 * @param dt
	 *            time to wait before every check
	 * @param out
	 *            used by the current watchdog to ping the otherones
	 * @param otherWatchdogs
	 *            watchdogs nodes
	 */
	public Watchdog(final int dt, final IOutput<String[]> out, final IWatchdogNode[] watchdogs) {
		this.out = out;
		this.dt = dt;
		config();
		for (final IWatchdogNode n : watchdogs) {
			watchdogNodes.add(n);
		}
		
	}
	
	/**
	 * Watchdog constructor
	 * 
	 * @param dt
	 *            time to wait before every check
	 * @param out
	 *            used by the current watchdog to ping the otherones
	 * @param otherWatchdogs
	 *            watchdogs name
	 */
	public Watchdog(final int dt, final IOutput<String[]> out, final String[] watchdogs) {
		otherWatchdogs = watchdogs;
		this.out = out;
		this.dt = dt;
		config();
		for (final String s : watchdogs) {
			watchdogNodes.add(new IWatchdogNode() {
				@Override
				public String getName() {
					return s;
				}
			});
		}
		
	}
	
	@Override
	public synchronized void addNode(final IWatchdogNode node) {
		watchdogNodes.add(node);
	}
	
	/**
	 * add the current ping to the ping history
	 */
	@Override
	public synchronized void addPing(final String ping) {
		int i = 0;
		for (; (i < ping.length()) && (ping.charAt(i) != '('); i++) {
			;
		}
		i++;
		String name = "";
		for (; (i < ping.length()) && (ping.charAt(i) != ')'); i++) {
			name += ping.charAt(i);
		}
		
		// if nodes exists then increase the ping numbers
		if (pings.get(name) != null) {
			final int val = pings.get(name);
			pings.put(name, val + 1);
		}
	}
	
	/**
	 * clean ping history
	 */
	@Override
	public synchronized void cleanPings() {
		pings = new HashMap<String, Integer>();
		for (final String s : otherWatchdogs) {
			pings.put(s, 0);
		}
	}
	
	private void config() {
		cleanPings();
		myself = this;
		stopped = false;
		watchdogNodes = new ArrayList<IWatchdogNode>();
	}
	
	/**
	 * kill the application
	 */
	@Override
	public void killApplication() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * register an observer
	 */
	// my observer
	@Override
	public void register(final IObserver src) {
		observer = src;
	}
	
	@Override
	public synchronized void removeNode(final IWatchdogNode node) {
		for (final IWatchdogNode n : watchdogNodes) {
			if (n.getName().equals(node.getName())) {
				watchdogNodes.remove(n);
			}
		}
	}
	
	/**
	 * start the watchdog
	 */
	@Override
	public void start() {
		t = new Thread() {
			@Override
			public void run() {
				// update the observer after very tick
				while (!stopped) {
					tick();
					synchronized (this) {
						if (observer != null) {
							observer.update(myself, pings);
						}
						cleanPings();
					}
				}
			}
		};
		t.start();
	}
	
	/**
	 * start the application
	 */
	@Override
	public void startApplication() {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * stop the watchdog
	 */
	@Override
	public void stop() {
		stopped = true;
		t.interrupt();
		
	}
	
	/**
	 * wait dt
	 */
	@Override
	public void tick() {
		try {
			Thread.sleep(dt);
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * called by the application
	 */
	// called by my application
	@Override
	public void update(final IObservable src, final Object info) {
		out.addOutput(new String[] { Syskb.ping, Syskb.ping + "(" + info + ")" });
		addPing(info.toString());
	}
}
