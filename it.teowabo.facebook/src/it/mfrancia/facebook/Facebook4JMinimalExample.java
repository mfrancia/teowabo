package it.mfrancia.facebook;

import facebook4j.Facebook;
import facebook4j.FacebookException;
import facebook4j.FacebookFactory;
import facebook4j.Post;
import facebook4j.ResponseList;
import facebook4j.auth.AccessToken;

public class Facebook4JMinimalExample {
	
	/**
	 * A simple Facebook4J client.
	 * 
	 * 
	 * @param args
	 * @throws FacebookException
	 */
	public static void main(final String[] args) throws FacebookException {
		
		// Generate facebook instance.
		final Facebook facebook = new FacebookFactory().getInstance();
		// Use default values for oauth app id.
		facebook.setOAuthAppId("", "");
		// Get an access token from:
		// https://developers.facebook.com/tools/explorer
		// Copy and paste it below.
		final String accessTokenString = "CAACEdEose0cBAHzYV9BQze7X0LZCaOgzu89sxX9KRLGg9A8fOZAKqf1lCC5i9SxHDsNWAGObtnRkCQ0RvXs9DG40ZCi5EqEtvHps2lBV6iBJIoZAXzEf6ALjRmenACABvCJ81AJIkAUlioQdmYcXiNEFu5OKUECa96V9deCZAGlqKKewxa2b7PftIX6alrM02ZA5A52N2BjwZDZD";
		final AccessToken at = new AccessToken(accessTokenString);
		// Set access token.
		facebook.setOAuthAccessToken(at);
		
		// We're done.
		// Write some stuff to your wall.
		// facebook.postStatusMessage("Wow, it works...");
		
		// leggere notizie
		final ResponseList<Post> feed = facebook.getHome();
		
		for (int i = 0; i < feed.size(); i++) {
			System.out.println(feed.get(i).getMessage());
		}
	}
}