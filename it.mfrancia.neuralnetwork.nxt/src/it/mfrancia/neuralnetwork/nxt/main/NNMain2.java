package it.mfrancia.neuralnetwork.nxt.main;

import it.mfrancia.implementation.multithreading.Monitor;
import it.mfrancia.implementation.physic.Speed;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.IInhibitor;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.lejos.LeJosSoftwareSystem;
import it.mfrancia.lejos.effector.LeJosNxtMotor;
import it.mfrancia.lejos.sensor.LeJosContact;
import it.mfrancia.lejos.sensor.LeJosLightSensor;
import it.mfrancia.lejos.sensor.LeJosUltrasonic;
import it.mfrancia.neuralnetwork.ANNKb;
import it.mfrancia.neuralnetwork.ANNUtils;
import it.mfrancia.neuralnetwork.ActivationLevel;
import it.mfrancia.neuralnetwork.Inhibitor;
import it.mfrancia.neuralnetwork.NeuralLayer;
import it.mfrancia.neuralnetwork.NeuralNetwork;
import it.mfrancia.neuralnetwork.NeuronFactory;
import it.mfrancia.neuralnetwork.Weight;
import it.mfrancia.neuralnetwork.reflexes.GoForward;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnLeft;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnRight;
import it.mfrancia.neuralnetwork.reflexes.TurnLeft;
import it.mfrancia.neuralnetwork.reflexes.TurnRight;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeExpTransducer;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeLinearTransducer;
import it.mfrancia.neuralnetwork.transducer.SensorToNeuronAdapter;
import it.mfrancia.sensor.DifferentialAdapter;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.transducer.Transducer;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class NNMain2 extends LeJosSoftwareSystem {
	public static void main(final String[] args) {
		new NNMain2();
	}
	private SensorPoller	poller;
	
	@Override
	public void config() {
		/* LEJOS */
		
		/* EFFECTORS */
		final IMotor motorLeft = new LeJosNxtMotor(Motor.C);
		final IMotor motorRight = new LeJosNxtMotor(Motor.A);
		motorRight.setSpeed(new Speed(Motor.A.getMaxSpeed() / 4.0));
		motorLeft.setSpeed(new Speed(Motor.C.getMaxSpeed() / 4.0));
		
		/* REFLEXES */
		final GoForward idle = new GoForward(motorLeft, motorRight);
		final Monitor motorController = new Monitor(idle);
		final ReverseTurnLeft rl = new ReverseTurnLeft(motorController, motorLeft, motorRight);
		final ReverseTurnRight rr = new ReverseTurnRight(motorController, motorLeft, motorRight);
		final TurnLeft tl = new TurnLeft(motorController, motorLeft, motorRight);
		final TurnRight tr = new TurnRight(motorController, motorLeft, motorRight);
		
		/* SENSORS */
		final LeJosContact contactleft = new LeJosContact("cLeft", SensorPort.S3);
		final ButtonSensor contactSensorLeft = new ButtonSensor("sLeft", contactleft);
		final LeJosContact contactRight = new LeJosContact("cRight", SensorPort.S1);
		final ButtonSensor contactSensorRight = new ButtonSensor("sRight", contactRight);
		final LeJosUltrasonic ultrasonic = new LeJosUltrasonic("sonar", SensorPort.S2);
		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor("ultrasonic", ultrasonic);
		final LeJosLightSensor light = new LeJosLightSensor("light", SensorPort.S4);
		final DifferentialAdapter lightdifferential = new DifferentialAdapter(light, 0.03);
		poller = new SensorPoller("poller", 200, contactleft, contactRight, ultrasonic, light);
		/* END LEJOS */
		
		/* NEURAL NETWORK */
		final NeuronFactory nf = new NeuronFactory();
		final INeuralNetwork ann = new NeuralNetwork();
		/* building the proximity layer */
		final INeuralLayer P = new NeuralLayer(ann, ANNKb.proximityLayer);
		final INeuron proximityNeuronA = nf.createNeuron(P, ANNKb.proximityLayerThreshold);
		new NumberToSpikeExpTransducer(ultrasonicSensor, new SensorToNeuronAdapter(proximityNeuronA));
		
		/* building the collision layer */
		final INeuralLayer C = new NeuralLayer(ann, ANNKb.collisionLayer);
		final INeuron collisionNeuronLeft = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		final INeuron collisionNeuronRight = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		new NumberToSpikeLinearTransducer(contactSensorLeft, new SensorToNeuronAdapter(collisionNeuronLeft));
		new NumberToSpikeLinearTransducer(contactSensorRight, new SensorToNeuronAdapter(collisionNeuronRight));
		
		/* building the target layer */
		final INeuralLayer T = new NeuralLayer(ann, ANNKb.targetLayer);
		final INeuron targetLeft = nf.createNeuron(T, ANNKb.targetLayerThreshold);
		final INeuron targetRight = nf.createNeuron(T, ANNKb.targetLayerThreshold);
		Transducer<Number, ISpike> transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(targetLeft));
		lightdifferential.registerObserverLeft(transducer);
		transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(targetRight));
		lightdifferential.registerObserverRight(transducer);
		
		/* building the motor controller layer */
		final INeuralLayer M = new NeuralLayer(ann, ANNKb.motorControlLayer);
		
		final INeuron motorControllerNeuronA = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rr);
		final INeuron motorControllerNeuronB = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rl);
		final INeuron motorControllerNeuronC = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, tr);
		final INeuron motorControllerNeuronD = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, tl);
		
		/* making connections */
		ANNUtils.hopfieldLayerConnection(P, C);
		ANNUtils.hopfieldLayerConnection(P, T);
		final IInhibitor I = new Inhibitor(T, new ActivationLevel(ANNKb.inhibitorThreshold));
		C.register(I);
		ANNUtils.neuronsConnectionNoLearning(collisionNeuronLeft, motorControllerNeuronA, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(collisionNeuronRight, motorControllerNeuronB, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(targetLeft, motorControllerNeuronC, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(targetRight, motorControllerNeuronD, new Weight(1.0));
		
		T.setVerbose(false);
		M.setVerbose(false);
		C.setVerbose(false);
		P.setVerbose(false);
		proximityNeuronA.register(new IObserver<IActivationLevel>() {
			@Override
			public void update(IObservable<IActivationLevel> src, IActivationLevel info) {
				//Sound.beep();
			}
		});
	}
	
	@Override
	public void start() {
		poller.start();
		Button.waitForAnyPress();
		poller.stop();
		Motor.A.stop();
		Motor.C.stop();
		Button.waitForAnyPress();
	}
}
