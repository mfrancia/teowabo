package it.mfrancia.neuralnetwork.nxt.main;

import it.mfrancia.implementation.multithreading.Monitor;
import it.mfrancia.implementation.physic.Speed;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.multithreading.IMonitor;
import it.mfrancia.lejos.LeJosSoftwareSystem;
import it.mfrancia.lejos.effector.LeJosNxtMotor;
import it.mfrancia.lejos.sensor.LeJosContact;
import it.mfrancia.lejos.sensor.LeJosLightSensor;
import it.mfrancia.lejos.sensor.LeJosUltrasonic;
import it.mfrancia.neuralnetwork.reflexes.GoForward;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnLeft;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnRight;
import it.mfrancia.neuralnetwork.reflexes.TurnLeft;
import it.mfrancia.neuralnetwork.reflexes.TurnRight;
import it.mfrancia.sensor.DifferentialAdapter;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class ReflexTest extends LeJosSoftwareSystem {
	public static void main(final String[] args) {
		new ReflexTest();
	}
	
	private IMotor			motorLeft;
	private IMotor			motorRight;
	private boolean			inhibite;
	private SensorPoller	sp;
	
	@Override
	public void config() {
		inhibite = false;
		motorLeft = new LeJosNxtMotor(Motor.C);
		motorRight = new LeJosNxtMotor(Motor.A);
		motorLeft.setSpeed(new Speed((double) (Motor.C.getMaxSpeed() / 4)));
		motorRight.setSpeed(new Speed((double) (Motor.A.getMaxSpeed() / 4)));
		
		final IMonitor motorController = new Monitor(new GoForward(motorLeft, motorRight));
		final ReverseTurnLeft rl = new ReverseTurnLeft(motorController, motorLeft, motorRight);
		final ReverseTurnRight rr = new ReverseTurnRight(motorController, motorLeft, motorRight);
		final TurnLeft tl = new TurnLeft(motorController, motorLeft, motorRight);
		final TurnRight tr = new TurnRight(motorController, motorLeft, motorRight);
		final LeJosContact contactleft = new LeJosContact("cLeft", SensorPort.S3);
		final ButtonSensor contactSensorLeft = new ButtonSensor("sLeft", contactleft);
		final LeJosContact contactRight = new LeJosContact("cRight", SensorPort.S1);
		final ButtonSensor contactSensorRight = new ButtonSensor("sRight", contactRight);
		final LeJosLightSensor l = new LeJosLightSensor("light", SensorPort.S4);
		final DifferentialAdapter ls = new DifferentialAdapter(l, 0.01);
		// ls.setVerbose(false);
		ls.registerObserverLeft(new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				if (!inhibite)
					if (info.doubleValue() > 0) {
						motorController.execute(tr);
					}
			}
		});
		
		ls.registerObserverRight(new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				if (!inhibite)
					if (info.doubleValue() > 0) {
						motorController.execute(tl);
					}
			}
		});
		contactSensorRight.register(new IObserver<Number>() {
			
			@Override
			public void update(IObservable<Number> src, Number info) {
				if (info.doubleValue() > 0) {
					inhibite = true;
					motorController.execute(rl);
					inhibite = false;
				}
			}
		});
		contactSensorLeft.register(new IObserver<Number>() {
			
			@Override
			public void update(IObservable<Number> src, Number info) {
				if (info.doubleValue() > 0) {
					inhibite = true;
					motorController.execute(rr);
					inhibite = false;
				}
			}
		});
		sp = new SensorPoller("poller", 300, contactleft, contactRight, ls);
	}
	
	@Override
	public void start() {
		sp.start();
		Button.waitForAnyPress();
		sp.stop();
		motorLeft.stop();
		motorRight.stop();
	}
}
