package it.mfrancia.neuralnetwork.nxt.main;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.lejos.LeJosSoftwareSystem;
import it.mfrancia.lejos.sensor.LeJosLightSensor;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.LightSensor;
import lejos.nxt.Button;
import lejos.nxt.SensorPort;

public class TestLightValue extends LeJosSoftwareSystem implements IObserver<Number> {
	public static void main(final String[] args) {
		new TestLightValue();
	}
	
	private SensorPoller	sp;
	
	@Override
	public void config() {
		final LeJosLightSensor l = new LeJosLightSensor("light", SensorPort.S4);
		final LightSensor light = new LightSensor("light", l);
		light.register(this);
		sp = new SensorPoller("poller", 1000, l);
	}
	
	@Override
	public void start() {
		sp.start();
		Button.waitForAnyPress();
		sp.stop();
	}
	
	@Override
	public void update(final IObservable<Number> src, final Number info) {
		System.out.println(info.doubleValue() + "");
	}
}
