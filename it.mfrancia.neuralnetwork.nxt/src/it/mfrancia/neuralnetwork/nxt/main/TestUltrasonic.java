package it.mfrancia.neuralnetwork.nxt.main;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.lejos.LeJosSoftwareSystem;
import it.mfrancia.lejos.sensor.LeJosUltrasonic;
import it.mfrancia.neuralnetwork.ANNKb;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeExpTransducer;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import lejos.nxt.Button;
import lejos.nxt.SensorPort;
import lejos.nxt.Sound;

public class TestUltrasonic extends LeJosSoftwareSystem implements IObserver<Number> {
	public static void main(final String[] args) {
		new TestUltrasonic();
	}
	
	private SensorPoller	sp;
	
	@Override
	public void config() {
		final LeJosUltrasonic lu = new LeJosUltrasonic("u", SensorPort.S2);
		final UltrasonicSensor lus = new UltrasonicSensor("u", lu);
		lus.register(this);
		new NumberToSpikeExpTransducer(lus, new IInput<ISpike>() {
			@Override
			public void doTask(IInputSource ins, ISpike in) {
				System.out.println(in.getValue().doubleValue() + "");
				if (in.getValue() > ANNKb.proximityLayerThreshold.getValue())
					Sound.twoBeeps();
			}
		});
		sp = new SensorPoller("poller", 1000, lu);
	}
	
	@Override
	public void start() {
		sp.start();
		Button.waitForAnyPress();
		sp.stop();
	}
	
	@Override
	public void update(final IObservable<Number> src, final Number info) {
		System.out.println(info.doubleValue() + "");
	}
}
