package it.mfrancia.lejos;

public class LeJosKb {
	public static final Number	lightMaxRange		= 1023;
	public static final Number	lightMinRange		= 0;
	// public static final Semaphore mutex = new Semaphore(1);
	public static final Number	ultrasonicMaxRange	= 255;
	public static final Number	ultrasonicMinRange	= 0;
	public static final Number	ultrasonicNoValue	= 255;
	
}
