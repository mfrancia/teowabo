package it.mfrancia.lejos.effector;

import it.mfrancia.implementation.physic.Speed;
import it.mfrancia.interfaces.physic.ISpeed;
import lejos.nxt.NXTRegulatedMotor;

public class LeJosNxtMotor extends LeJosMotor {
	private final NXTRegulatedMotor	motor;
	
	public LeJosNxtMotor(final NXTRegulatedMotor motor) {
		this.motor = motor;
	}
	
	@Override
	public synchronized ISpeed getSpeed() {
		ISpeed ret = null;
		acquireLock();
		ret = new Speed(motor.getSpeed() * 1.0);
		releaseLock();
		return ret;
	}
	
	@Override
	public synchronized void goBackward() {
		acquireLock();
		motor.backward();
		releaseLock();
	}
	
	@Override
	public synchronized void goForward() {
		acquireLock();
		motor.forward();
		releaseLock();
	}
	
	@Override
	public synchronized void rotate(final Integer degrees, final boolean immediateReturn) {
		acquireLock();
		motor.rotate(degrees, immediateReturn);
		releaseLock();
	}
	
	@Override
	public synchronized void setSpeed(final ISpeed speed) {
		acquireLock();
		motor.setSpeed(speed.getSpeed().floatValue());
		releaseLock();
		
	}
	
	@Override
	public synchronized void stop() {
		acquireLock();
		motor.stop();
		releaseLock();
		
	}
}
