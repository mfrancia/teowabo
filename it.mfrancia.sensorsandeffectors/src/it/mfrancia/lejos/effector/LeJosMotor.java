package it.mfrancia.lejos.effector;

import it.mfrancia.interfaces.effector.IMotor;

public abstract class LeJosMotor implements IMotor {
	
	@Override
	public synchronized void acquireLock() {
		// new LeJosKb().getLeJosKbInstance().await();
		// LeJosKb.await();
	}
	
	@Override
	public synchronized void releaseLock() {
		// new LeJosKb().getLeJosKbInstance().release();
		// LeJosKb.release();
	}
	
}
