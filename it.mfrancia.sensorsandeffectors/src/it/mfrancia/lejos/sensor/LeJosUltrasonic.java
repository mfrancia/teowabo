package it.mfrancia.lejos.sensor;

import it.mfrancia.lejos.LeJosKb;
import lejos.nxt.SensorPort;
import lejos.nxt.UltrasonicSensor;

public class LeJosUltrasonic extends LeJosSensor {
	private static final long		serialVersionUID	= -1828373022413711522L;
	private final UltrasonicSensor	us;
	
	public LeJosUltrasonic(final String name, final SensorPort sensorPort) {
		super(name, LeJosKb.ultrasonicMinRange, LeJosKb.ultrasonicMaxRange);
		us = new UltrasonicSensor(sensorPort);
	}
	
	@Override
	public synchronized Number getValue() {
		
		return /* us.getDistance() != LeJosKb.ultrasonicNoValue.intValue() ? us.getDistance() : null */us.getDistance();
	}
}
