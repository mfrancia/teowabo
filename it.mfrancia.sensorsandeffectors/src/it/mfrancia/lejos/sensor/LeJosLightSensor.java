package it.mfrancia.lejos.sensor;

import it.mfrancia.lejos.LeJosKb;
import lejos.nxt.ColorSensor;
import lejos.nxt.SensorPort;

public class LeJosLightSensor extends LeJosSensor {
	private static final long	serialVersionUID	= -3634078583044854332L;
	private final ColorSensor	l;
	
	public LeJosLightSensor(final String name, final SensorPort sensorPort) {
		super(name, LeJosKb.lightMinRange, LeJosKb.lightMaxRange);
		l = new ColorSensor(sensorPort);
		
	}
	
	@Override
	public synchronized Number getValue() {
		return l.getNormalizedLightValue();
	}
}
