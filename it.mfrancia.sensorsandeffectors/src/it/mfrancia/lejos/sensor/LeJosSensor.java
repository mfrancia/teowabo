package it.mfrancia.lejos.sensor;

import it.mfrancia.sensor.BoundedSensor;

public abstract class LeJosSensor extends BoundedSensor {
	private static final long	serialVersionUID	= 4077726572894051793L;
	
	public LeJosSensor(final String name, final Number minRange, final Number maxRange) {
		super(name, minRange, maxRange);
	}
	
	@Override
	public synchronized void acquireSensorLock() {
		// new LeJosKb().getLeJosKbInstance().await();
		// LeJosKb.await();
	}
	
	@Override
	public synchronized void releaseSensorLock() {
		// new LeJosKb().getLeJosKbInstance().release();
		// LeJosKb.release();
	}
	
}
