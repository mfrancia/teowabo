package it.mfrancia.lejos.sensor;

import lejos.nxt.SensorPort;
import lejos.nxt.TouchSensor;

public class LeJosContact extends LeJosSensor {
	private static final long	serialVersionUID	= 590215732350237630L;
	private final TouchSensor	ts;
	
	public LeJosContact(final String name, final SensorPort sensorPort) {
		super(name, 0, 1);
		ts = new TouchSensor(sensorPort);
	}
	
	@Override
	public synchronized Number getValue() {
		return ts.isPressed() ? 1 : 0;
	}
}
