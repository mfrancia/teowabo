package it.mfrancia.lejos;

import lejos.nxt.Button;

public abstract class LeJosSoftwareSystem {
	
	public LeJosSoftwareSystem() {
		System.out.println("Wait for ANY press");
		Button.waitForAnyPress();
		config();
		start();
	}
	
	public abstract void config();
	
	public abstract void start();
}
