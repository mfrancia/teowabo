package it.mfrancia.env;

import it.mfrancia.interfaces.IColor;

import java.awt.Color;

public class EnvUtils {
	public static Color getColor(IColor color) {
		return new Color(color.getRed(), color.getGreen(), color.getBlue());
	}
}
