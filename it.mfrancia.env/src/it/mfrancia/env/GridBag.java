package it.mfrancia.env;

import it.mfrancia.implementation.Coordinates;
import it.mfrancia.interfaces.IColor;
import it.mfrancia.interfaces.env.IGrid;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;

import javax.swing.JPanel;

/**
 * gestione dell'elemento gridBag
 * 
 * @author Matteo Francia
 * 
 */
public class GridBag implements IGrid {
	private final GridBagConstraints			c;
	private final JPanel						pane;
	private final GridBagLayout					gbl;
	private HashMap<String, java.awt.Component>	components;
	
	public GridBag() {
		gbl = new GridBagLayout();
		pane = new JPanel(gbl);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH; // esegue il resize dell'elemento se esso � pi� grande della cella
		components = new HashMap<String, java.awt.Component>();
	}
	
	/**
	 * aggiunge un componente alla griglia
	 * 
	 * @param in
	 *            elemento da aggiungere
	 * @param row
	 *            riga
	 * @param col
	 *            colonna
	 * @param d
	 *            peso con cui si espande il componente orizzontalmente durante il resizing finestra
	 * @param e
	 *            peso con cui si espande il componente verticalmente durante il resizing finestra
	 * @param padding
	 *            padding del componente
	 * @param rowSpan
	 *            numero di righe occupate dall'oggetto
	 * @param colSpan
	 *            numero di colonne occupate dall'oggetto
	 */
	public void addComponent(final Object in, final int row, final int col, final double d, final double e, final Insets padding, final int rowSpan, final int colSpan) {
		c.gridx = col; // colonna
		c.gridy = row; // riga
		c.weightx = d; // peso con cui si espande il componente orizzontalmente durante il resizing finestra
		c.weighty = e; // peso con cui si espande il componente verticalmente durante il resizing finestra
		c.insets = padding; // padding del componente
		c.gridwidth = colSpan; // numero di colonne occupate dall'oggetto
		c.gridheight = rowSpan; // numero di righe occupate dall'oggetto
		pane.add((java.awt.Component) in, c); // aggiunge elemento al pannello
		components.put(new Coordinates(row, col).getDefaultRepresentation().toString(), (java.awt.Component) in);
	}
	
	/**
	 * ritorna il pannello contenente gli elementi presente nella griglia
	 * 
	 * @return panel contenente gli elementi della griglia
	 */
	public JPanel getContent() {
		return pane;
	}
	
	/**
	 * rimuove un componente dalla griglia
	 * 
	 * @param in
	 *            componente da rimuovere
	 */
	public void removeComponent(final java.awt.Component in) {
		pane.remove(in);
	}
	
	@Override
	public void add(Object component, int row, int column) {
		this.addComponent(component, row, column, 1, 1, new Insets(5, 5, 5, 5), 1, 1);
	}
	
	@Override
	public void remove(Object component) {
		this.removeComponent((Component) component);
	}
	
	@Override
	public void setBackgroundColor(IColor color) {
		pane.setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue()));
	}
	
	@Override
	public void setBackgroundColor(IColor color, int row, int column) {
		components.get(new Coordinates(row, column).getDefaultRepresentation().toString()).setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue()));
	}

}
