package it.mfrancia.env;

import it.mfrancia.interfaces.IColor;
import it.mfrancia.interfaces.env.ILabel;

public class Label extends java.awt.Label implements ILabel {
	private static final long	serialVersionUID	= -8535368810645523790L;
	
	public Label(String text) {
		setText(text);
	}
	
	public Label() {
	}
	
	@Override
	public void setBackground(IColor c) {
		super.setBackground(EnvUtils.getColor(c));
	}
}
