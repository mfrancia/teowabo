package it.mfrancia.env;

import it.mfrancia.interfaces.IColor;
import it.mfrancia.interfaces.env.IGraphicComponent;

import java.awt.Color;

public class Component extends java.awt.Component implements IGraphicComponent {
	private static final long	serialVersionUID	= -6552537587356744216L;

	@Override
	public void setBackground(IColor c) {
		super.setBackground(new Color(c.getRed(), c.getGreen(), c.getBlue()));
	}
}
