package it.mfrancia.env;

import java.awt.Color;

public class ColorAwt extends it.mfrancia.implementation.Color {
	public ColorAwt(Color color) {
		super(color.getRed(), color.getGreen(), color.getBlue());
	}
	
}
