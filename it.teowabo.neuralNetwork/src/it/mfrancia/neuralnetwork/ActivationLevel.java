package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.ISignal;

/**
 * 
 * @author Matteo Francia
 *
 */
public class ActivationLevel implements IActivationLevel {
	private final Double	value;
	
	public ActivationLevel(final Double value) {
		this.value = Math.round(value.doubleValue() * 100000) / 100000.0;
	}
	
	@Override
	public ISignal<Double> add(final ISignal<Double> signal) {
		return new ActivationLevel(value + signal.getValue());
	}
	
	@Override
	public Double getValue() {
		return value;
	}
	
}
