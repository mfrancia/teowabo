package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.IAxon;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISoma;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.sensor.ISensor;

import java.util.ArrayList;
import java.util.List;

public class Neuron extends Loggable implements INeuron {
	private IAxon								axon;
	private List<IDendrite>						dendrites;
	private final INeuralLayer					neuralLayer;
	private List<IObserver<IActivationLevel>>	observers;
	private final ISensor<ISpike>				sensor;
	private ISoma								soma;
	
	public Neuron(final INeuralLayer neuralLayer) {
		this(neuralLayer, null, null, null, null);
	}
	
	public Neuron(final INeuralLayer neuralLayer, final ISensor<ISpike> inputSensor) {
		this(neuralLayer, null, null, null, inputSensor);
	}
	
	public Neuron(final INeuralLayer neuralLayer, final List<IDendrite> dentrites, final ISoma soma, final IAxon axon) {
		this(neuralLayer, dentrites, soma, axon, null);
	}
	
	public Neuron(final INeuralLayer neuralLayer, final List<IDendrite> dentrites, final ISoma soma, final IAxon axon, final ISensor<ISpike> sensor) {
		super(neuralLayer.getName() + ".neuron" + neuralLayer.getNeuronNumber());
		dendrites = dentrites;
		if (dendrites == null) {
			dendrites = new ArrayList<>();
		}
		// for (IDendrite dendrite : dentrites)
		// dendrite.setNeuron(this);
		this.soma = soma;
		this.axon = axon;
		this.sensor = sensor;
		this.neuralLayer = neuralLayer;
	}
	
	@Override
	public void addDendrite(final IDendrite dendrite) {
		log("added a new dendrite");
		addLoggableChild(dendrite);
		dendrites.add(dendrite);
		
	}
	
	@Override
	public IAxon getAxon() {
		return axon;
	}
	
	@Override
	public ISoma getBody() {
		return soma;
	}
	
	@Override
	public IDendrite[] getDendrites() {
		final IDendrite[] retDentrites = new IDendrite[dendrites.size()];
		return dendrites.toArray(retDentrites);
	}
	
	@Override
	public INeuralLayer getNeuralLayer() {
		return neuralLayer;
	}
	
	@Override
	public ISensor<ISpike> getSensor() {
		return sensor;
	}
	
	@Override
	public void notifySignalChanged(final IInputSource source, final ISpike newSignaleValue) {
		log("signal change notified");
		soma.updateActivationSignal(source, newSignaleValue);
		if (observers != null) {
			for (IObserver<IActivationLevel> observer : observers)
				observer.update(this, soma.getActivationSignal());
		}
	}
	
	@Override
	public void register(final IObserver<IActivationLevel> src) {
		if (observers == null)
			observers = new ArrayList<>();
		observers.add(src);
	}
	
	public void setAxon(final IAxon axon) {
		this.axon = axon;
		addLoggableChild(this.axon);
	}
	
	public void setSoma(final ISoma soma) {
		this.soma = soma;
		addLoggableChild(this.soma);
	}
	
}
