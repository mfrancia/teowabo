package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.INeuronComponent;

public class NeuronComponent extends Loggable implements INeuronComponent {
	private final INeuron	father;
	protected boolean		verbose;
	
	public NeuronComponent(final INeuron neuron, final String name) {
		super(name);
		father = neuron;
	}
	
	@Override
	public INeuron getNeuron() {
		return father;
	}
	
}
