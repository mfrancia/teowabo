package it.mfrancia.neuralnetwork.reflexes;

import it.mfrancia.implementation.multithreading.Task;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.multithreading.IMonitor;

public abstract class Reflex extends Task implements IOutput<Object> {
	protected IMonitor	controller;
	
	public Reflex(final String name, final IMonitor controller) {
		super(name);
		this.controller = controller;
		register(controller);
	}
	
	@Override
	public void addOutput(final Object o) {
		log(getName());
		controller.addOutput(this);
	}
}
