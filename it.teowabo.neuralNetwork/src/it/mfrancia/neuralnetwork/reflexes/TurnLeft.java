package it.mfrancia.neuralnetwork.reflexes;

import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.multithreading.IMonitor;

public class TurnLeft extends Reflex {
	private final IMotor	motorRight;
	private final IMotor	motorLeft;
	
	public TurnLeft(final IMonitor controller, final IMotor motorLeft, final IMotor motorRight) {
		super("turn left", controller);
		this.motorRight = motorRight;
		this.motorLeft = motorLeft;
	}
	
	@Override
	public void task() {
		motorRight.stop();
//		motorLeft.stop();
		motorLeft.rotate(120, false);
		// motorLeft.stop();
	}
	
}
