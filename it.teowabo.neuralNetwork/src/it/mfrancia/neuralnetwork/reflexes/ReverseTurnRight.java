package it.mfrancia.neuralnetwork.reflexes;

import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.multithreading.IMonitor;

public class ReverseTurnRight extends Reflex {
	private final IMotor	motorLeft;
	private final IMotor	motorRight;
	
	public ReverseTurnRight(final IMonitor controller, final IMotor motorLeft, final IMotor motorRight) {
		super("Reverse turn right", controller);
		this.motorLeft = motorLeft;
		this.motorRight = motorRight;
	}
	
	@Override
	public void task() {
		motorRight.stop();
		motorLeft.stop();
		motorRight.rotate(-180, true);
		motorLeft.rotate(-180, false);
		motorLeft.stop();
		motorRight.rotate(-180, false);
		motorRight.stop();
	}
}