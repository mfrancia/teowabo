package it.mfrancia.neuralnetwork.reflexes;

import it.mfrancia.implementation.multithreading.Task;
import it.mfrancia.interfaces.effector.IMotor;

public class GoForward extends Task {
	private final IMotor	motorLeft;
	private final IMotor	motorRight;
	
	public GoForward(final IMotor motorLeft, final IMotor motorRight) {
		super("forward");
		this.motorLeft = motorLeft;
		this.motorRight = motorRight;
	}
	
	@Override
	public void task() {
		motorLeft.stop();
		motorRight.stop();
		motorLeft.goForward();
		motorRight.goForward();
	}
	
}
