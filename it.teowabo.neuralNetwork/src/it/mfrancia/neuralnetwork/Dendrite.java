package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.neuralnetwork.ISynapse;

public class Dendrite extends NeuronComponent implements IDendrite {
	private ISynapse	synapse;
	
	// public Dendrite(final ISynapse synapse) {
	// this(null, synapse);
	// }
	public Dendrite(final INeuron father) {
		this(father, null);
	}
	
	public Dendrite(final INeuron father, final ISynapse synapse) {
		super(father, father.getName() + ".dendrite");
		this.synapse = synapse;
		if (synapse != null) {
			this.synapse.setOutput(this);
		}
	}
	
	@Override
	public synchronized void doTask(final IInputSource ins, final ISpike spike) {
		getNeuron().notifySignalChanged(ins, spike);
	}
	
	@Override
	public ISynapse getSynapse() {
		return synapse;
	}
	
	public void setSynapse(final ISynapse synapse) {
		this.synapse = synapse;
		this.synapse.setVerbose(verbose);
	}
	
	@Override
	public void setVerbose(final boolean verbose) {
		super.setVerbose(verbose);
		if (synapse != null) {
			synapse.setVerbose(verbose);
		}
	}
}
