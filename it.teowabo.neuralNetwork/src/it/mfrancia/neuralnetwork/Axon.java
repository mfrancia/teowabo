package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IAxonInhibited;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.neuralnetwork.ISynapse;

import java.util.ArrayList;
import java.util.List;

public class Axon extends NeuronComponent implements IAxonInhibited {
	private final List<ISynapse>	synapses;
	private boolean					isInhibited;
	
	public Axon(final INeuron father) {
		this(father, (ISynapse[]) null);
	}
	
	public Axon(final INeuron father, final ISynapse... synapses) {
		super(father, father.getName() + ".axon");
		this.synapses = new ArrayList<ISynapse>();
		if (synapses != null) {
			for (final ISynapse s : synapses) {
				s.setInput(this);
				this.synapses.add(s);
			}
		}
		isInhibited = false;
	}
	
	@Override
	public synchronized void addOutput(final ISpike o) {
		if (!isInhibited()) {
			for (final ISynapse s : synapses) {
				s.fire(o);
			}
		}
	}
	
	@Override
	public synchronized void addSynapse(final ISynapse synapse) {
		addLoggableChild(synapse);
		synapses.add(synapse);
	}
	
	@Override
	public synchronized void disinhibit() {
		log("disinhibited");
		isInhibited = false;
	}
	
	@Override
	public ISynapse[] getSynapses() {
		final ISynapse[] ret = new ISynapse[synapses.size()];
		return synapses.toArray(ret);
	}
	
	@Override
	public synchronized void inhibit() {
		log("inhibited");
		isInhibited = true;
	}
	
	@Override
	public synchronized Boolean isInhibited() {
		return isInhibited;
	}
	
}
