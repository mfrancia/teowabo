package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.IAxonInhibited;
import it.mfrancia.interfaces.neuralnetwork.IInhibitor;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuron;

public class Inhibitor extends Loggable implements IInhibitor {
	private final IActivationLevel	threshold;
	private final INeuralLayer		to;
	private boolean					isInhibited;
	
	public Inhibitor(final INeuralLayer to, final IActivationLevel threshold) {
		this(null, to, threshold);
	}
	
	public Inhibitor(final INeuralLayer from, final INeuralLayer to, final IActivationLevel threshold) {
		super("I " + (from == null ? "?" : from.getName()) + ", " + to.getName());
		this.threshold = threshold;
		this.to = to;
		if (from != null) {
			from.register(this);
		}
		isInhibited = false;
	}
	
	@Override
	public void update(final IObservable<IActivationLevel> src, final IActivationLevel info) {
		if ((info.getValue() > threshold.getValue()) && !isInhibited) {
			isInhibited = true;
			log("inhibit");
			for (final INeuron neuron : to.getNeurons()) {
				((IAxonInhibited) neuron.getAxon()).inhibit();
			}
		} else if ((info.getValue() <= threshold.getValue()) && isInhibited) {
			isInhibited = false;
			log("disinhibit");
			for (final INeuron neuron : to.getNeurons()) {
				((IAxonInhibited) neuron.getAxon()).disinhibit();
			}
		}
	}
	
}
