package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.neuralnetwork.IAxon;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.neuralnetwork.ISynapse;
import it.mfrancia.interfaces.neuralnetwork.IWeight;

import java.util.Random;

public class Synapse extends Loggable implements ISynapse {
	private IAxon		axon;
	private IDendrite	dentrite;
	private ISpike		signal;
	private IWeight		weight;
	
	public Synapse() {
		this(null, null, null);
	}
	
	public Synapse(final IAxon axon, final IDendrite dendrite, final IWeight weight) {
		super(axon.getName() + ".synapse" + new Random().nextInt(1000));
		setInput(axon);
		setOutput(dendrite);
		this.weight = weight;
		signal = new Spike(0.0);
	}
	
	@Override
	public synchronized void fire(final ISpike signal) {
		this.signal = signal;
		getOutput().doTask(getInput(), new Spike(signal.getValue() * getWeight().getWeight()));
		// learning();
	}
	
	@Override
	public IAxon getInput() {
		return axon;
	}
	
	@Override
	public IDendrite getOutput() {
		return dentrite;
	}
	
	@Override
	public synchronized IWeight getWeight() {
		return weight;
	}
	
	/**
	 * Implemented as the default hebbian learning (with learning ratio = 1)
	 */
	@Override
	public synchronized void learning() {
		log("actual weight is: " + getWeight().getWeight());
		final Double result = (ANNKb.learningRatio * signal.getValue() * getOutput().getNeuron().getBody().getOutputSignal().getValue())
				- (ANNKb.forgettingRatio * getWeight().getWeight() * getOutput().getNeuron().getNeuralLayer().getAverageActivation().getValue());
		log("(lr) " + ANNKb.learningRatio + " * (sI) " + signal.getValue() + " * (sJ) " + getOutput().getNeuron().getBody().getOutputSignal().getValue() + " - (fr) " + ANNKb.forgettingRatio
				+ " * (a) " + getOutput().getNeuron().getNeuralLayer().getAverageActivation().getValue() + " * (wij) " + getWeight().getWeight() + " = (dW) " + result);
		final IWeight newWeight = new Weight(getWeight().getWeight() + result);
		log("new weight is: " + newWeight.getWeight());
		setWeight(newWeight);
	}
	
	@Override
	public void setInput(final IAxon axon) {
		this.axon = axon;
	}
	
	@Override
	public void setOutput(final IDendrite dendrite) {
		dentrite = dendrite;
	}
	
	@Override
	public void setVerbose(final boolean verbose) {
		super.setVerbose(verbose || ((getInput() != null) && getInput().getVerbose()) || ((getOutput() != null) && getOutput().getVerbose()));
	}
	
	@Override
	public synchronized void setWeight(final IWeight weight) {
		this.weight = weight;
	}
}
