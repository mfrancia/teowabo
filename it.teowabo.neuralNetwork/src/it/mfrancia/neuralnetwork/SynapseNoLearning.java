package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IAxon;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.IWeight;

public class SynapseNoLearning extends Synapse {
	public SynapseNoLearning(final IAxon axon, final IDendrite dendrite, final IWeight weight) {
		super(axon, dendrite, weight);
	}
	
	@Override
	public synchronized void learning() {
		// do nothing
	}
}
