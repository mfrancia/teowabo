package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISynapse;
import it.mfrancia.interfaces.neuralnetwork.IWeight;

import java.util.Random;

public class ANNUtils {
	public static void hopfieldLayerConnection(final INeuralLayer from, final INeuralLayer to) {
		for (int i = 0; i < from.getNeurons().length; i++) {
			for (int j = 0; j < to.getNeurons().length; j++) {
				neuronsConnection(from.getNeurons()[i], to.getNeurons()[j]);
			}
		}
	}
	
	public static void neuronsConnection(final INeuron from, final INeuron to) {
		neuronsConnection(from, to, null);
	}
	
	public static void neuronsConnection(final INeuron from, final INeuron to, IWeight weight) {
		System.out.println("Creating connection from " + from.getName() + " to " + to.getName());
		final Dendrite dendrite = new Dendrite(to);
		to.addDendrite(dendrite);
		final Axon axon = (Axon) from.getAxon();
		if (weight == null) {
			weight = new Weight(/*new Random().nextInt(1000) / 2000.0*/0.2);
		}
		final ISynapse synapse = new Synapse(axon, dendrite, weight);
		axon.addSynapse(synapse);
		dendrite.setSynapse(synapse);
	}
	
	public static void neuronsConnectionNoLearning(final INeuron from, final INeuron to, IWeight weight) {
		System.out.println("Creating connection from " + from.getName() + " to " + to.getName());
		final Dendrite dendrite = new Dendrite(to);
		to.addDendrite(dendrite);
		final Axon axon = (Axon) from.getAxon();
		if (weight == null) {
			weight = new Weight(0.1);
		}
		final ISynapse synapse = new SynapseNoLearning(axon, dendrite, weight);
		axon.addSynapse(synapse);
		dendrite.setSynapse(synapse);
	}
}
