package it.mfrancia.neuralnetwork.awt;

import it.mfrancia.env.GridBag;
import it.mfrancia.env.Label;
import it.mfrancia.implementation.Coordinates;
import it.mfrancia.interfaces.ICoordinates;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.neuralnetwork.ANNKb;

import java.awt.Frame;
import java.util.HashMap;

import javax.swing.JFrame;

public class NeuralNetworkAwt extends JFrame implements INeuralNetwork {
	private static final long				serialVersionUID	= 6842094154407991957L;
	protected GridBag						gridBag;
	private final HashMap<INeuron, Integer>	map;
	private final HashMap<String, Label>	mapawt;
	private int								lasty;
	private final INeuralNetwork			ann;
	
	public NeuralNetworkAwt() {
		super(ANNKb.getNeuralNetwork().getName().toString());
		ann = ANNKb.getNeuralNetwork();
		gridBag = new GridBag();
		map = new HashMap<>();
		mapawt = new HashMap<>();
		lasty = 1;
		gridBag.add(new Label("from / to"), 0, 0);
		setContentPane(gridBag.getContent());
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);
		setVisible(true);
	}
	
	@Override
	public void addLayer(final INeuralLayer layer) {
		ann.addLayer(layer);
	}
	
	@Override
	public void addLoggableChild(final ILoggable child) {
		ann.addLoggableChild(child);
	}
	
	private void check(final INeuron n) {
		if (map.get(n) == null) {
			map.put(n, lasty);
			gridBag.add(new Label(n.getName().toString()), lasty, 0);
			gridBag.add(new Label(n.getName().toString()), 0, lasty++);
		}
	}
	
	@Override
	public INeuralLayer getLayer(final CharSequence layerName) {
		return ann.getLayer(layerName);
	}
	
	@Override
	public INeuralLayer[] getLayers() {
		return ann.getLayers();
	}
	
	@Override
	public boolean getVerbose() {
		return ann.getVerbose();
	}
	
	@Override
	public void setLogger(final ILogger logger) {
		ann.setLogger(logger);
	}
	
	@Override
	public void setVerbose(final boolean verbose) {
		ann.setVerbose(verbose);
	}
	
	@Override
	public void update(final IObservable<INeuralLayer> src, final INeuralLayer info) {
		ann.update(src, info);
		if (info.getNeurons() != null) {
			for (final INeuron n : info.getNeurons()) {
				check(n);
				final int fromX = map.get(n);
				for (final IDendrite d : n.getDendrites()) {
					final INeuron to = d.getSynapse().getInput().getNeuron();
					check(to);
					final int toY = map.get(to);
					final ICoordinates c = new Coordinates(fromX, toY);
					Label l = mapawt.get(c.getDefaultRepresentation().toString());
					if (l == null) {
						l = new Label();
						mapawt.put(c.getDefaultRepresentation().toString(), l);
						gridBag.add(l, toY, fromX);
					}
					l.setText(d.getSynapse().getWeight().getWeight() + "");
					
				}
			}
		}
	}
}
