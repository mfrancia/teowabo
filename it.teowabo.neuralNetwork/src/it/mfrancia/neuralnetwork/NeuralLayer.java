package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;
import it.mfrancia.interfaces.neuralnetwork.INeuron;

import java.util.ArrayList;
import java.util.List;

public class NeuralLayer extends Loggable implements INeuralLayer {
	private final String							name;
	private final INeuralNetwork					neuralNetwork;
	// private final INeuron[] neurons;
	private final List<INeuron>						neurons;
	private final List<IObserver<IActivationLevel>>	observers;
	private IActivationLevel						threshold;
	
	public NeuralLayer(final INeuralNetwork neuralNetwork, final String name, final INeuron... neurons) {
		super(neuralNetwork.getName() + "." + name);
		// this.neurons = neurons;
		this.neuralNetwork = neuralNetwork;
		this.name = name;
		this.neurons = new ArrayList<>();
		for (final INeuron neuron : neurons) {
			addNeuron(neuron);
		}
		observers = new ArrayList<>();
	}
	
	@Override
	public void addNeuron(final INeuron neuron) {
		addLoggableChild(neuron);
		neurons.add(neuron);
	}
	
	@Override
	public IActivationLevel getAverageActivation() {
		IActivationLevel activationLevel = new ActivationLevel(0.0);
		if (getNeuronNumber() > 0) {
			for (final INeuron n : neurons) {
				activationLevel = (IActivationLevel) activationLevel.add(n.getBody().getActivationSignal());
			}
			return new ActivationLevel(activationLevel.getValue() / getNeuronNumber());
		} else {
			return activationLevel;
		}
		
	}
	
	public IActivationLevel getLayerActivationThreshold() {
		return threshold;
	}
	
	@Override
	public String getName() {
		return name;
	}
	
	@Override
	public INeuralNetwork getNeuralNetwork() {
		return neuralNetwork;
	}
	
	@Override
	public int getNeuronNumber() {
		return neurons.size();
	}
	
	@Override
	public INeuron[] getNeurons() {
		final INeuron[] ret = new INeuron[neurons.size()];
		return neurons.toArray(ret);
	}
	
	@Override
	public void register(final IObserver<IActivationLevel> src) {
		observers.add(src);
		update(this, null);
	}
	
	public void setLayerActivationThreshold(final IActivationLevel threshold) {
		this.threshold = threshold;
	}
	
	@Override
	public void update(final IObservable<IActivationLevel> src, final IActivationLevel info) {
		log("layer average activation is: " + getAverageActivation().getValue());
		if (observers != null) {
			for (final IObserver<IActivationLevel> observer : observers) {
				observer.update(this, getAverageActivation());
			}
			neuralNetwork.update(null, this);
		}
	}
}
