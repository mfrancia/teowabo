package it.mfrancia.neuralnetwork.transducer;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.neuralnetwork.Spike;
import it.mfrancia.sensor.transducer.Transducer;

public class NumberToSpikeExpTransducer extends Transducer<Number, ISpike> {
	
	public NumberToSpikeExpTransducer(final IInput<ISpike> input) {
		super(input);
	}
	
	public NumberToSpikeExpTransducer(final IObservable<Number> obs, final IInput<ISpike> input) {
		super(obs, input);
	}
	
	@Override
	public ISpike transduce(final Number value) {
		System.out.println(1 / (Math.pow(Math.E, value.doubleValue() * 9)));
		return new Spike(1 / Math.pow(Math.E, value.doubleValue() * 9));
	}
	
}
