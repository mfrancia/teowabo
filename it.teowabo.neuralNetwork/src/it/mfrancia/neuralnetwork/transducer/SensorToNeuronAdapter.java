package it.mfrancia.neuralnetwork.transducer;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;

public class SensorToNeuronAdapter implements IInput<ISpike> {
	private final INeuron	neuron;
	
	public SensorToNeuronAdapter(final INeuron neuron) {
		this.neuron = neuron;
	}
	
	@Override
	public void doTask(final IInputSource ins, final ISpike in) {
		neuron.notifySignalChanged(ins, in);
	}
}
