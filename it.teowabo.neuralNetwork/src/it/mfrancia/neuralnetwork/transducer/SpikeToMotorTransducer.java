package it.mfrancia.neuralnetwork.transducer;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.sensor.transducer.Transducer;

public class SpikeToMotorTransducer extends Transducer<ISpike, Object> {
	
	public SpikeToMotorTransducer(final IOutput<Object> output) {
		super(output);
	}
	
	@Override
	public Object transduce(final ISpike value) {
		return value;
	}
	
}
