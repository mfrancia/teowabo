package it.mfrancia.neuralnetwork.transducer;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.neuralnetwork.Spike;
import it.mfrancia.sensor.transducer.Transducer;

public class NumberToSpikeLinearTransducer extends Transducer<Number, ISpike> {
	
	public NumberToSpikeLinearTransducer(final IInput<ISpike> input) {
		super(input);
	}
	
	public NumberToSpikeLinearTransducer(final IObservable<Number> obs, final IInput<ISpike> input) {
		super(obs, input);
	}
	
	@Override
	public ISpike transduce(final Number value) {
		return new Spike(value.doubleValue());
	}
	
}
