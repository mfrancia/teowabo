package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;

public class ANNKb {
	
	/**
	 * SINGLETON, create an artificial neural network
	 * 
	 * @return
	 */
	public static INeuralNetwork getNeuralNetwork() {
		if (ann == null) {
			ann = new NeuralNetwork();
		}
		return ann;
	}
	
	private static INeuralNetwork			ann;
	
	public static final String				collisionLayer				= "collision";
	/**
	 * Hebbian learning: forgetting ratio
	 */
	public static final Double				forgettingRatio				= 0.3;
	public static final Double				genericThreshold			= 0.5;
	public static final Double				lightThreshold				= 0.3;
	/**
	 * Inhibition activation threshold
	 */
	public static final Double				inhibitorThreshold			= 0.15;
	/**
	 * Hebbian learning: learning ratio
	 */
	public static final Double				learningRatio				= 0.1;
	public static final String				motorControlLayer			= "motorControl";
	/**
	 * Motor control layer activation threshold
	 */
	public static final IActivationLevel	motorControlLayerThreshold	= new ActivationLevel(0.1);
	public static final String				proximityLayer				= "proximity";
	public static final String				targetLayer					= "target";
	/**
	 * Proximity layer activation threshold
	 */
	public static final IActivationLevel	proximityLayerThreshold		= new ActivationLevel(genericThreshold);
	/**
	 * Collision layer activation threshold
	 */
	public static final IActivationLevel	collisionLayerThreshold		= new ActivationLevel(genericThreshold);
	/**
	 * Collision layer activation threshold
	 */
	public static final IActivationLevel	targetLayerThreshold		= new ActivationLevel(lightThreshold);
}
