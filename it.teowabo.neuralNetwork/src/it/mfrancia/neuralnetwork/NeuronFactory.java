package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.IAxon;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.INeuronFactory;
import it.mfrancia.interfaces.neuralnetwork.ISoma;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.sensor.ISensor;
import it.mfrancia.neuralnetwork.transducer.SpikeToMotorTransducer;
import it.mfrancia.sensor.transducer.Transducer;

public class NeuronFactory implements INeuronFactory {
	
	@Override
	public INeuron createNeuron(final INeuralLayer neuralLayer) {
		return createNeuron(neuralLayer, null, ((NeuralLayer) neuralLayer).getLayerActivationThreshold());
	}
	
	@Override
	public INeuron createNeuron(final INeuralLayer neuralLayer, final IActivationLevel threshold) {
		return createNeuron(neuralLayer, null, threshold);
	}
	
	@Override
	public INeuron createNeuron(final INeuralLayer neuralLayer, final ISensor<ISpike> inputSensor) {
		return createNeuron(neuralLayer, inputSensor, ((NeuralLayer) neuralLayer).getLayerActivationThreshold());
		
	}
	
	@Override
	public INeuron createNeuron(final INeuralLayer neuralLayer, final ISensor<ISpike> inputSensor, final IActivationLevel threshold) {
		final Neuron neuron = new Neuron(neuralLayer, inputSensor);
		final IAxon axon = new Axon(neuron);
		final ISoma soma = new Soma(neuron, threshold);
		neuron.setAxon(axon);
		neuron.setSoma(soma);
		neuron.register(neuralLayer);
		neuralLayer.addNeuron(neuron);
		return neuron;
	}
	
	public INeuron createNeuronEffector(final INeuralLayer neuralLayer, final IActivationLevel threshold, final IOutput<Object> effector) {
		final Neuron neuron = new Neuron(neuralLayer);
		final AxonEffector axon = new AxonEffector(neuron);
		final Transducer<ISpike, Object> t = new SpikeToMotorTransducer(effector);
		axon.register(t);
		// final TransducerFactory<ISpike, Object> tf = new TransducerFactory<ISpike, Object>();
		// tf.createTransducerAsObserver(SpikeToMotorTransducer.class, axon, effector);
		final ISoma soma = new Soma(neuron, threshold);
		neuron.setAxon(axon);
		neuron.setSoma(soma);
		neuron.register(neuralLayer);
		neuralLayer.addNeuron(neuron);
		return neuron;
	}
}
