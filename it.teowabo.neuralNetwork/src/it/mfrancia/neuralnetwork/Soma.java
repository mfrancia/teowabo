package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.IDendrite;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISoma;
import it.mfrancia.interfaces.neuralnetwork.ISpike;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Neural cell body
 * 
 * @author Matteo Francia
 *
 */
public class Soma extends NeuronComponent implements ISoma {
	private IActivationLevel					activationLevel;
	protected Hashtable<IInputSource, ISpike>	map;
	private final IActivationLevel				threshold;
	
	public Soma(final INeuron father, final IActivationLevel threshold) {
		super(father, father.getName() + ".soma");
		map = new Hashtable<IInputSource, ISpike>();
		activationLevel = new ActivationLevel(0.0);
		this.threshold = threshold;
	}
	
	/**
	 * In the default case the activation level is determinated using a sigmoid function
	 */
	@Override
	public synchronized void activationFunction() {
		IActivationLevel ret = new ActivationLevel(0.0);
		final Enumeration<IInputSource> enume = map.keys();
		while (enume.hasMoreElements()) {
			ret = (ActivationLevel) ret.add(map.get(enume.nextElement()));
		}
		activationLevel = function(ret);
		log("activation value g(Hi): " + activationLevel.getValue().toString());
		for (final IDendrite d : getNeuron().getDendrites()) {
			d.getSynapse().learning();
		}
		getNeuron().getAxon().addOutput(new Spike(activationLevel));
	}
	
	/**
	 * Override this method to implement the activation function, by default the latter is the step function (threshold is specified in the class constructor)
	 * 
	 * @param value
	 *            sum of the neuron inputs
	 * @return calculated activation level
	 * 
	 */
	protected IActivationLevel function(IActivationLevel value) {
		log("Hi value: " + value.getValue().toString() + ", threshold is: " + threshold.getValue());
		if (value.getValue() > threshold.getValue())
			return new ActivationLevel(1.0);
		return new ActivationLevel(0.0);
	}
	
	@Override
	public synchronized IActivationLevel getActivationSignal() {
		return activationLevel;
	}
	
	@Override
	public IActivationLevel getActivationThreshold() {
		return threshold;
	}
	
	/**
	 * In the default case the output signal is equals to the activation level
	 */
	@Override
	public synchronized ISpike getOutputSignal() {
		return new Spike(activationLevel);
	}
	
	@Override
	public void updateActivationSignal(final IInputSource source, final ISpike signal) {
		log("received new signal: " + signal.getValue().toString());
		map.put(source, signal);
		activationFunction();
	}
}
