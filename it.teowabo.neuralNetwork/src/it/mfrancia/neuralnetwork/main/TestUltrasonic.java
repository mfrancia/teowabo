package it.mfrancia.neuralnetwork.main;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeExpTransducer;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.simulator.UltrasonicPassive;

public class TestUltrasonic extends SoftwareSystem implements IObserver<Number> {
	public static void main(final String[] args) {
		new TestUltrasonic();
	}
	
	private SensorPoller	sp;
	private int				i	= 0;
	
	@Override
	public void config() {
		final UltrasonicPassive lu = new UltrasonicPassive("u");
		final UltrasonicSensor lus = new UltrasonicSensor("u", lu);
		lus.register(this);
		new NumberToSpikeExpTransducer(lus, new IInput<ISpike>() {
			
			@Override
			public void doTask(IInputSource ins, ISpike in) {
				System.out.println(i++ + "");
			}
		});
		sp = new SensorPoller("poller", 20, lu);
	}
	
	@Override
	public void start() {
		sp.start();
		try {
			while (System.in.read() != 10)
				;
		} catch (Exception e) {
		}
		sp.stop();
	}
	
	@Override
	public void update(final IObservable<Number> src, final Number info) {
		System.out.println(info.doubleValue() + "");
	}
}
