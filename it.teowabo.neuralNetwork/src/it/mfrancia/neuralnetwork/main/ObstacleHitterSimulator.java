package it.mfrancia.neuralnetwork.main;

import it.mfrancia.interfaces.IObserver;

import java.util.Random;

public class ObstacleHitterSimulator extends Thread {
	private boolean				notStopped	= false;
	private IObserver<Number>	obsButtonLeft;
	private IObserver<Number>	obsButtonRight;
	private IObserver<Number>	obsProximity;
	
	public synchronized boolean getStopped() {
		return notStopped;
	}
	
	@Override
	public void run() {
		try {
			Thread.sleep(100);
			while (!getStopped()) {
				// scelgo un sensore di contatto random
				final boolean left = (new Random().nextInt(2) % 2) == 0;
				final IObserver<Number> obs = left ? obsButtonLeft : obsButtonRight;
				
				if (obsProximity != null) {
					obsProximity.update(null, new Random().nextInt(80));
				}
				Thread.sleep(100);
				if (obs != null) {
					obs.update(null, 1);
					Thread.sleep(100);
					obs.update(null, 0);					
				}
				Thread.sleep(100);
				if (obsProximity != null) {
					obsProximity.update(null, new Random().nextInt(150) + 90);
				}
				
				Thread.sleep(100);
				System.out.println("ostacolo non rilevato dal sensore di contatto");
				if (obsProximity != null) {
					obsProximity.update(null, new Random().nextInt(200));
					Thread.sleep(100);
					obsProximity.update(null, new Random().nextInt(200) + 600);
					
				}
				
				Thread.sleep(100);
				System.out.println("ostacolo non rilevato dal sensore ad ultrasuoni");
				if (obs != null) {
					obs.update(null, 1);
					Thread.sleep(100);
					obs.update(null, 0);
				}
				Thread.sleep(100);
			}
		} catch (final InterruptedException e) {
		}
	}
	
	public void setButtonObserverLeft(final IObserver<Number> obsButton) {
		obsButtonLeft = obsButton;
	}
	
	public void setButtonObserverRight(final IObserver<Number> obsButton) {
		obsButtonRight = obsButton;
	}
	
	public void setProximityObserver(final IObserver<Number> obsProximity) {
		this.obsProximity = obsProximity;
	}
	
	public synchronized void setStop() {
		notStopped = true;
		interrupt();
	}
}
