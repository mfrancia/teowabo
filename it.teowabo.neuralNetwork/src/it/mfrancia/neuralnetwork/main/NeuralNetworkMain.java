package it.mfrancia.neuralnetwork.main;

import it.mfrancia.effector.Motor;
import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.implementation.multithreading.Monitor;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.neuralnetwork.IInhibitor;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.neuralnetwork.ANNKb;
import it.mfrancia.neuralnetwork.ANNUtils;
import it.mfrancia.neuralnetwork.ActivationLevel;
import it.mfrancia.neuralnetwork.Inhibitor;
import it.mfrancia.neuralnetwork.NeuralLayer;
import it.mfrancia.neuralnetwork.NeuronFactory;
import it.mfrancia.neuralnetwork.Weight;
import it.mfrancia.neuralnetwork.awt.NeuralNetworkAwt;
import it.mfrancia.neuralnetwork.reflexes.GoForward;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnLeft;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnRight;
import it.mfrancia.neuralnetwork.reflexes.TurnLeft;
import it.mfrancia.neuralnetwork.reflexes.TurnRight;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeExpTransducer;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeLinearTransducer;
import it.mfrancia.neuralnetwork.transducer.SensorToNeuronAdapter;
import it.mfrancia.sensor.DifferentialAdapter;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.simulator.ButtonPassive;
import it.mfrancia.sensor.simulator.LightPassive;
import it.mfrancia.sensor.simulator.UltrasonicPassive;
import it.mfrancia.sensor.transducer.Transducer;

import java.io.IOException;

public class NeuralNetworkMain extends SoftwareSystem {
	public static void main(final String[] args) {
		new NeuralNetworkMain();
	}
	
	// private SensorPoller poller;
	
	private ObstacleHitterSimulator	ohs;
	
	@Override
	public void config() {
		/* ABSTRACT SENSOR AND COMMANDS */
		final IMotor motorLeft = new Motor("motor left");
		final IMotor motorRight = new Motor("motor right");
		
		/* REFLEXES */
		final GoForward idle = new GoForward(motorLeft, motorRight);
		final Monitor motorController = new Monitor(idle);
		final ReverseTurnLeft rl = new ReverseTurnLeft(motorController, motorLeft, motorRight);
		final ReverseTurnRight rr = new ReverseTurnRight(motorController, motorLeft, motorRight);
		final TurnLeft tl = new TurnLeft(motorController, motorLeft, motorRight);
		final TurnRight tr = new TurnRight(motorController, motorLeft, motorRight);
		
		/* SENSORS */
		final ButtonPassive contactleft = new ButtonPassive("cLeft");
		final ButtonSensor contactSensorLeft = new ButtonSensor("sLeft", contactleft);
		final ButtonPassive contactRight = new ButtonPassive("cRight");
		final ButtonSensor contactSensorRight = new ButtonSensor("sRight", contactRight);
		final UltrasonicPassive ultrasonic = new UltrasonicPassive("sonar");
		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor("ultrasonic", ultrasonic);
		final LightPassive light = new LightPassive("light");
		final DifferentialAdapter lightdifferential = new DifferentialAdapter(light, 0.01);
		// poller = new SensorPoller("poller", 300, contactleft, contactRight, ultrasonic, light);
		ohs = new ObstacleHitterSimulator();
		ohs.setButtonObserverLeft(contactleft);
		ohs.setButtonObserverRight(contactRight);
		ohs.setProximityObserver(ultrasonic);
		
		/* NEURAL NETWORK */
		final NeuronFactory nf = new NeuronFactory();
		final INeuralNetwork ann = new NeuralNetworkAwt();
		/* building the proximity layer */
		final INeuralLayer P = new NeuralLayer(ann, ANNKb.proximityLayer);
		final INeuron proximityNeuronA = nf.createNeuron(P, ANNKb.proximityLayerThreshold);
		new NumberToSpikeExpTransducer(ultrasonicSensor, new SensorToNeuronAdapter(proximityNeuronA));
		/* building the collision layer */
		final INeuralLayer C = new NeuralLayer(ann, ANNKb.collisionLayer);
		final INeuron collisionNeuronLeft = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		final INeuron collisionNeuronRight = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		new NumberToSpikeLinearTransducer(contactSensorLeft, new SensorToNeuronAdapter(collisionNeuronLeft));
		new NumberToSpikeLinearTransducer(contactSensorRight, new SensorToNeuronAdapter(collisionNeuronRight));
		/* building the target layer */
		final INeuralLayer T = new NeuralLayer(ann, ANNKb.targetLayer);
		final INeuron targetLeft = nf.createNeuron(T, ANNKb.targetLayerThreshold);
		final INeuron targetRight = nf.createNeuron(T, ANNKb.targetLayerThreshold);
		Transducer<Number, ISpike> transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(targetLeft));
		lightdifferential.registerObserverLeft(transducer);
		transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(targetRight));
		lightdifferential.registerObserverRight(transducer);
		/* building the motor controller layer */
		final INeuralLayer M = new NeuralLayer(ann, ANNKb.motorControlLayer);
		final INeuron motorControllerNeuronA = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rr);
		final INeuron motorControllerNeuronB = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rl);
		final INeuron motorControllerNeuronC = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, tr);
		final INeuron motorControllerNeuronD = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, tl);
		
		/* making connections */
		ANNUtils.hopfieldLayerConnection(P, C);
		ANNUtils.hopfieldLayerConnection(P, T);
		final IInhibitor I = new Inhibitor(T, new ActivationLevel(ANNKb.inhibitorThreshold));
		C.register(I);
		ANNUtils.neuronsConnectionNoLearning(collisionNeuronLeft, motorControllerNeuronA, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(collisionNeuronRight, motorControllerNeuronB, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(targetLeft, motorControllerNeuronC, new Weight(1.0));
		ANNUtils.neuronsConnectionNoLearning(targetRight, motorControllerNeuronD, new Weight(1.0));
		
		T.setVerbose(true);
		M.setVerbose(true);
		C.setVerbose(true);
		P.setVerbose(true);
	}
	
	@Override
	public void start() {
		ohs.start();
		try {
			// poller.start();
			while (System.in.read() != 10) {
				;
			}
			// poller.stop();
		} catch (final IOException e) {
		}
		
		ohs.setStop();
	}
	
}
