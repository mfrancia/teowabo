package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IWeight;

public class Weight implements IWeight {
	private final Double	value;
	
	public Weight(final Number value) {
		this.value = Math.round(value.doubleValue() * 100000) / 100000.0;
	}
	
	@Override
	public IWeight add(final IWeight weight) {
		return new Weight(value + weight.getWeight());
	}
	
	@Override
	public Double getWeight() {
		return value;
	}
	
}
