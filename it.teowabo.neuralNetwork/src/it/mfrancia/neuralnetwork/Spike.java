package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.neuralnetwork.IActivationLevel;
import it.mfrancia.interfaces.neuralnetwork.ISignal;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.interfaces.neuralnetwork.IWeight;

public class Spike implements ISpike {
	private Double	value;
	
	public Spike(final IActivationLevel value) {
		this.value = value.getValue();
	}
	
	public Spike(final Double value) {
		this.value = value;
	}
	
	@Override
	public ISignal<Double> add(final ISignal<Double> signal) {
		return new Spike(value + signal.getValue());
	}
	
	@Override
	public Double getValue() {
		return value;
	}
	
	public void multiply(final IWeight weight) {
		value = value * weight.getWeight();
	}
	
}
