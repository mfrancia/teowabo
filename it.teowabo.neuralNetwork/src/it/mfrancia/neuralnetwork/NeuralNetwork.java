package it.mfrancia.neuralnetwork;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;

import java.util.HashMap;

public class NeuralNetwork extends Loggable implements INeuralNetwork {
	private final HashMap<String, INeuralLayer>	layers;
	
	public NeuralNetwork(final INeuralLayer... layers) {
		super("ann");
		this.layers = new HashMap<>();
		for (final INeuralLayer layer : layers) {
			addLayer(layer);
		}
	}
	
	@Override
	public void addLayer(final INeuralLayer layer) {
		addLoggableChild(layer);
		layers.put(layer.getName(), layer);
	}
	
	@Override
	public INeuralLayer getLayer(final CharSequence layerName) {
		return layers.get(layerName);
	}
	
	@Override
	public INeuralLayer[] getLayers() {
		final INeuralLayer[] layers = new INeuralLayer[this.layers.size()];
		return this.layers.values().toArray(layers);
	}
	
	@Override
	public void update(final IObservable<INeuralLayer> src, final INeuralLayer info) {
		// do nothing
	}
	
}
