package it.mfrancia.neuralnetwork;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;

public class AxonEffector extends Axon implements IObservable<ISpike> {
	private IObserver<ISpike>	transducer;
	
	public AxonEffector(final INeuron father) {
		super(father);
	}
	
	@Override
	public synchronized void addOutput(final ISpike o) {
		if (o.getValue() > getNeuron().getBody().getActivationThreshold().getValue()) {
			transducer.update(this, o);
		}
	}
	
	@Override
	public void register(final IObserver<ISpike> src) {
		transducer = src;
	}
}
