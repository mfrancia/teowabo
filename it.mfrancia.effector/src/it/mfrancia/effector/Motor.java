package it.mfrancia.effector;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.physic.ISpeed;

public class Motor extends Loggable implements IMotor {
	private ISpeed	speed;
	
	public Motor(final CharSequence name) {
		super(name);
	}
	
	@Override
	public void acquireLock() {
	}
	
	@Override
	public ISpeed getSpeed() {
		return speed;
	}
	
	@Override
	public void goBackward() {
		log("backward");
	}
	
	@Override
	public void goForward() {
		log("forward");
	}
	
	@Override
	public void releaseLock() {
	}
	
	@Override
	public void rotate(final Integer degrees, final boolean immediateReturn) {
		log("rotate: " + degrees.intValue());
	}
	
	@Override
	public void setSpeed(final ISpeed speed) {
		this.speed = speed;
	}
	
	@Override
	public void stop() {
		log("stop");
	}
}
