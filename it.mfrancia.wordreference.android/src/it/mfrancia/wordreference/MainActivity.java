package it.mfrancia.wordreference;

import java.util.Arrays;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private int			mInterval		= 2000;				// 5 seconds by default, can be changed later
	private Handler		mHandler;
	private DbHelper	dbh;
	private Word		matchWord;
	private Language	language		= new Language("en");
	private String[]	matchstring		= new String[3];
	Runnable			mStatusChecker	= new Runnable() {
											@Override
											public void run() {
												updateStatus(); // this function can change value of mInterval.
												mHandler.postDelayed(mStatusChecker, mInterval);
											}
										};
	
	void startRepeatingTask() {
		mStatusChecker.run();
	}
	
	protected void updateStatus() {
		String[] s = WordSqlTable.findRandomWord(dbh);
		TextView w = (TextView) findViewById(R.id.textViewWordOfTheDayWord);
		TextView m = (TextView) findViewById(R.id.textViewWordOfTheDayMeaning);
		
		w.setText(s[0]);
		m.setText(s[1]);
	}
	
	void stopRepeatingTask() {
		mHandler.removeCallbacks(mStatusChecker);
	}
	
	void updateMatchWord() {
		String[] s = WordSqlTable.findRandomWord(dbh);
		matchWord = new Word(s[0]);
		matchWord.setMeaning(language, s[1]);
		
		ListView lv = (ListView) findViewById(R.id.listViewWhatMeans);
		((TextView) findViewById(R.id.textViewWhatMeans)).setText(matchWord.getPlainText());
		matchstring[0] = matchWord.getMeanings().get(language).get(0).getDefaultRepresentation().toString();
		matchstring[1] = matchWord.getMeanings().get(language).get(0).getDefaultRepresentation().toString();
		matchstring[2] = "error";
		CustomAdapter<String> matchAdapter = new DummyListViewAdapter(this, Arrays.asList(matchstring), R.layout.listviewitem);
		
		lv.setAdapter(matchAdapter);
		lv.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if (matchstring[position].equals(matchWord.getMeanings().get(language).get(0).getDefaultRepresentation().toString())) {
//					Toast.makeText(MainActivity.this, "OK", Toast.LENGTH_SHORT).show();
					updateMatchWord();
				} else
					Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT).show();
				// Intent intent = new Intent(context, SendMessage.class);
				// String message = "abc";
				// intent.putExtra(EXTRA_MESSAGE, message);
				// startActivity(intent);
			}
		});
		
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dbh = new DbHelper(this);
		mHandler = new Handler();

		updateMatchWord();
		startRepeatingTask();
	}
	
	public void onManuallyInsertedClick(View view) {
		EditText w = (EditText) findViewById(R.id.editTextInsertManuallyWord);
		EditText m = (EditText) findViewById(R.id.editTextInsertManuallyMeaning);
		if (!w.getText().toString().equals("") && !m.getText().toString().equals("")) {
			try {
				WordSqlTable.insert(dbh, w.getText().toString(), m.getText().toString());
				w.setText("");
				m.setText("");
			} catch (Exception e) {
				Toast.makeText(this, "The word already exists", Toast.LENGTH_SHORT).show();
			}
		} else {
			String s = w.getText().toString().equals("") ? "empty word" : "";
			s += m.getText().toString().equals("") ? s.equals("") ? "empty meaning" : " & empty meaning" : "";
			Toast.makeText(this, "Error: " + s, Toast.LENGTH_SHORT).show();
		}
	}
}
