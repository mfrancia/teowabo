package it.mfrancia.wordreference;

import java.util.Random;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

public class WordSqlTable {
	public WordSqlTable() {
	}
	
	private static int	last	= -1;
	
	/* Inner class that defines the table contents */
	public static abstract class WordSQLEntry implements BaseColumns {
		public static final String	TABLE_NAME				= "words";
		public static final String	COLUMN_NAME_ENTRY_ID	= "entryid";
		public static final String	COLUMN_NAME_WORD		= "word";
		public static final String	COLUMN_NAME_MEANING		= "meaning";
		public static final String	COLUMN_NAME_MATCH		= "match";
	}
	
	private static final String	TEXT_TYPE			= " TEXT";
	private static final String	TEXT_INTEGER		= " INTEGER";
	private static final String	COMMA_SEP			= ",";
	public static final String	SQL_CREATE_ENTRIES	= "CREATE TABLE " + WordSQLEntry.TABLE_NAME + " (" + WordSQLEntry.COLUMN_NAME_WORD + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP
															+ WordSQLEntry.COLUMN_NAME_MEANING + TEXT_TYPE + COMMA_SEP + WordSQLEntry.COLUMN_NAME_MATCH + TEXT_INTEGER + " )";
	public static final String	SQL_DELETE_ENTRIES	= "DROP TABLE IF EXISTS " + WordSQLEntry.TABLE_NAME;
	
	public static void insert(DbHelper dbh, String word, String meaning) throws Exception {
		// Gets the data repository in write mode
		SQLiteDatabase db = dbh.getWritableDatabase();
		
		// Create a new map of values, where column names are the keys
		ContentValues values = new ContentValues();
		
		values.put(WordSQLEntry.COLUMN_NAME_WORD, word.toLowerCase());
		values.put(WordSQLEntry.COLUMN_NAME_MEANING, meaning);
		values.put(WordSQLEntry.COLUMN_NAME_MATCH, 0);
		// Insert the new row, returning the primary key value of the new row
		if (db.insert(WordSQLEntry.TABLE_NAME, null, values) == -1)
			throw new Exception();
		db.close();
	}
	
	public static String[] findRandomWord(DbHelper dbh) {
		SQLiteDatabase db = dbh.getReadableDatabase();
		
		// Define a projection that specifies which columns from the database
		// you will actually use after this query.
		String[] projection = { WordSQLEntry.COLUMN_NAME_WORD, WordSQLEntry.COLUMN_NAME_MEANING };
		
		// How you want the results sorted in the resulting Cursor
		String sortOrder = WordSQLEntry.COLUMN_NAME_WORD + " DESC";
		
		Cursor c = db.query(WordSQLEntry.TABLE_NAME, // The table to query
				projection, // The columns to return
				null, // The columns for the WHERE clause
				null, // The values for the WHERE clause
				null, // don't group the rows
				null, // don't filter by row groups
				sortOrder // The sort order
				);
		
		c.moveToFirst();
		Random r = new Random();
		int i = r.nextInt(c.getCount());
		while (i == last && c.getCount() > 1)
			i = r.nextInt(c.getCount());
		String[] s = { "", "" };
		if (c.moveToPosition(i)) {
			s[0] = toFirstUpperCase(c.getString(c.getColumnIndex(WordSQLEntry.COLUMN_NAME_WORD)));
			s[1] = c.getString(c.getColumnIndex(WordSQLEntry.COLUMN_NAME_MEANING));
		}
		
		db.close();
		return s;
	}
	
	private static String toFirstUpperCase(String s) {
		if (s != null && s.length() == 1)
			return s.substring(0, 1).toUpperCase();
		if (s != null && s.length() > 1)
			return s.substring(0, 1).toUpperCase() + s.substring(1, s.length());
		return "";
	}
}
