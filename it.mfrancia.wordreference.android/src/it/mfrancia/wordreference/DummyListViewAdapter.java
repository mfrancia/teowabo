package it.mfrancia.wordreference;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

/**
 * A dummy example of a colorful list view adapter
 *
 * @author Matteo Francia
 *
 */
@SuppressLint("ViewConstructor")
public class DummyListViewAdapter extends CustomAdapter<String> {
	
	public DummyListViewAdapter(final Context context, final List<String> items, final int layoutId) {
		super(context, items, layoutId);
	}
	
	@Override
	public void render(final View v, int position, final String item) {
		final TextView t = (TextView) v.findViewById(R.id.textViewItem);
		t.setText(item);
	}
	
}
