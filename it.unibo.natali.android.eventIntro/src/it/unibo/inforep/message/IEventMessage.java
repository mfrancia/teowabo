package it.unibo.inforep.message;

import alice.tuprolog.Term;

public interface IEventMessage {
	public Term getContentTerm();
	
	public String getMsgContent();
	
	public String getMsgFunctor();
	
	public String getMsgName();
	
	public int getMsgSeqNum();
}
