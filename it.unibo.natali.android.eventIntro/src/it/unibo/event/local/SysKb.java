package it.unibo.event.local;

public class SysKb {
	public final static String[]	events	= new String[] { "ev1", "ev2" };
	
	public static String buildMsg(final String msgContent) {
		return "eventContent(" + msgContent + ")";
	}
}
