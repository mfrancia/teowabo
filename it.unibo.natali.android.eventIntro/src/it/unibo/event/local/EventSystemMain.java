/*
 * ============================================================= The application generates n events handled by the same event handler (ApplicationEventHandler)
 * =============================================================
 */
package it.unibo.event.local;

import it.unibo.event.interfaces.INodejsLike;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.NodejsLike;

// import it.unibo.event.interfaces.INodejsLike;

public class EventSystemMain {
	protected ApplicationEventHandler	evh0;
	protected IOutputView				outView;
	protected INodejsLike				rtem	= NodejsLike.getNodejsLike();
	
	public EventSystemMain(final IOutputView outView) throws Exception {
		this.outView = outView;
		configure();
		start();
		emitEvents();
	}
	
	protected void configure() throws Exception {
		outView.addOutput("configure");
		evh0 = new ApplicationEventHandler("ApplicationEventHandler", outView);
		// Register the handler
		for (final String event : SysKb.events) {
			rtem.insertInEventHandlerWaitQueue(evh0, event);
		}
	}
	
	protected void emitEvents() throws Exception {
		for (int i = 0; i < SysKb.events.length; i++) {
			rtem.raiseEvent(null, SysKb.events[i], "eventContent(" + SysKb.events[i] + "," + i + ")");
		}
	}
	
	protected void start() throws Exception {
		startMainLoop();
	}
	
	protected void startMainLoop() {
		new Thread() {
			@Override
			public void run() {
				try {
					rtem.mainLoop();
				} catch (final Exception e) {
					e.printStackTrace();
				}
				
			}
		}.start();
	}
	
}
