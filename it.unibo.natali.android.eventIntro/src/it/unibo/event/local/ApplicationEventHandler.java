package it.unibo.event.local;

import it.unibo.event.interfaces.IEventItem;
import it.unibo.event.interfaces.INodejsLike;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;
import it.unibo.nodelike.platfom.NodejsLike;

public class ApplicationEventHandler extends EventHandler {
	protected IOutputView	outView;
	protected INodejsLike	rtem	= NodejsLike.getNodejsLike();
	
	public ApplicationEventHandler(final String name, final IOutputView outView) throws Exception {
		super(name, outView);
		outView.addOutput("ApplicationEventHandler starts");
	}
	
	@Override
	public void doJob() throws Exception {
		final IEventItem curEventItem = getEventItem();
		showMsg("ApplicationEventHandler | " + curEventItem.getEventId() + " / " + curEventItem.getMsg());
	}
}
