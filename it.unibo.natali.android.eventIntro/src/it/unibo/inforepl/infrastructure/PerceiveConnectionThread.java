package it.unibo.inforepl.infrastructure;

import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.supports.FactoryProtocol;
import it.unibo.system.SituatedActiveObject;

import java.util.Hashtable;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;

public class PerceiveConnectionThread extends SituatedActiveObject {
	
	protected static Hashtable<String, PerceiveConnectionThread>	myinstances		= new Hashtable<String, PerceiveConnectionThread>();
	
	public static boolean											terminated		= false;
	
	/*
	 * 
	 */
	public static PerceiveConnectionThread getInstance(final IOutputView outView, final String name, final Prolog engine, final String myNode, final String curNode) {
		PerceiveConnectionThread instance = myinstances.get(curNode);
		if (instance == null) {
			instance = new PerceiveConnectionThread(outView, name, engine, myNode, curNode);
			myinstances.put(curNode, instance);
		}
		return instance;
	}
	
	public static void resetMyInstances() {
		myinstances = new Hashtable<String, PerceiveConnectionThread>();
	}
	
	protected Hashtable<String, Boolean>							attemptConns	= new Hashtable<String, Boolean>();
	protected String												curNode;
	protected String												curNodeAddr		= "localhost";
	protected String												curNodePort		= "8080";
	protected Prolog												engine			= new Prolog();
	protected FactoryProtocol										factory			= new FactoryProtocol(null, "TCP", "SysKbInfoReplyInfrast");
	protected String												myNode;
	
	protected SysKbInfoReplInfr.ConnectionThreadStates				myState			= SysKbInfoReplInfr.ConnectionThreadStates.READY;
	protected int													nObs			= 1;
	
	public PerceiveConnectionThread(final IOutputView outView, final String name, final Prolog engine, final String myNode, final String curNode) {
		super(outView, name);
		this.engine = engine;
		this.myNode = myNode;
		this.curNode = curNode;
		myState = SysKbInfoReplInfr.ConnectionThreadStates.READY;
	}
	
	public boolean checkPrologStyleProperty(final String goal) throws Exception {
		final SolveInfo sol = engine.solve(goal + ".");
		return sol.isSuccess();
	}
	
	protected void connectTo(final String myNode, final String curNode) {
		try {
			final SolveInfo sol = engine.solve("node(" + curNode + ",Y,Z).");
			if (sol.isSuccess()) {
				final Term term = sol.getSolution();
				final Term arg1 = ((Struct) term).getArg(1);
				final Term curNodeAddrTerm = ((Var) arg1).getTerm();
				final String curNodeAddrStr = curNodeAddrTerm.toString();
				final String curNodeAddr = curNodeAddrStr.replace("'", "");
				// println("curNodeAddr=" + curNodeAddr );
				final Term arg2 = ((Struct) term).getArg(2);
				final String curNodePort = "" + ((Var) arg2).getTerm();
				connectTo(myNode, curNode, curNodeAddr, curNodePort);
			}
		} catch (final Exception e) {
			println(" ERROR " + e.getMessage());
		}
	}
	
	protected void connectTo(final String myNode, final String curNode, final String curNodeAddr, final String curNodePort) throws Exception {
		final String obsName = "REvObs" + nObs++;
		boolean connected = false;
		println("		GOING TO CONNECT " + curNode + " @ " + curNodeAddr + ":" + curNodePort);
		evalIpAddr(curNode); // set curNodeAddr and curNodePort
		// System.out.println("connectiong to -> " + curNode + " | " + curNodeAddr + ":" + curNodePort);
		// waits until connection
		IConnInteraction connClient = null;
		myState = SysKbInfoReplInfr.ConnectionThreadStates.RUNNING;
		while (!connected && !terminated) {
			try {
				connClient = factory.createClientProtocolSupport(curNodeAddr, Integer.parseInt(curNodePort));
				connected = true;
			} catch (final Exception e) {
				Thread.sleep(SysKbInfoReplInfr.repeatTime);
				println("		REPEAT " + obsName + " CONNECT TO -> " + curNodeAddr + ":" + curNodePort);
			}
		}// while
		final RemoteEventsObserver evObs = new RemoteEventsObserver(outView, obsName, myNode, connClient);
		evObs.start();
		// println("		" + obsName + " CONNECTED TO -> " + curNode + " @"+ curNodeAddr + ":" + curNodePort);
		myState = SysKbInfoReplInfr.ConnectionThreadStates.TERMINATED;
	}
	
	@Override
	protected void doJob() throws Exception {
		// println("PerceiveConnectionThread STARTS for " + curNode);
		myState = SysKbInfoReplInfr.ConnectionThreadStates.RUNNING;
		connectTo(myNode, curNode);
		myState = SysKbInfoReplInfr.ConnectionThreadStates.TERMINATED;
	}// doJob
	
	@Override
	protected void endWork() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
	protected void evalIpAddr(final String curNode) throws Exception {
		final SolveInfo sol = engine.solve("node(" + curNode + ",Y,Z).");
		if (sol.isSuccess()) {
			final Term term = sol.getSolution();
			final Term arg1 = ((Struct) term).getArg(1);
			final Term curNodeAddrTerm = ((Var) arg1).getTerm();
			curNodeAddr = curNodeAddrTerm.toString();
			curNodeAddr = curNodeAddr.replace("'", "");
			System.out.println("curNodeAddr=" + curNodeAddr);
			final Term arg2 = ((Struct) term).getArg(2);
			curNodePort = "" + ((Var) arg2).getTerm();
			System.out.println("curNodePort=" + curNodePort);
			// if (engine.hasOpenAlternatives()) {
			// sol = engine.solveNext();
			// } else {
			// break;
			// }
		}
		
	}
	
	public SysKbInfoReplInfr.ConnectionThreadStates getCurState() {
		return myState;
	}
	
	public void restart() {
		final PerceiveConnectionThread instance = new PerceiveConnectionThread(outView, getName(), engine, myNode, curNode);
		myinstances.remove(curNode);
		myinstances.put(curNode, instance);
		instance.start();
	}
	
	@Override
	protected void startWork() throws Exception {
		// TODO Auto-generated method stub
		
	}
	
}
