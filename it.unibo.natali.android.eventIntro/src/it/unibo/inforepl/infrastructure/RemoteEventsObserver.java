package it.unibo.inforepl.infrastructure;

import it.unibo.event.interfaces.IContactComponent;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.supports.FactoryProtocol;
import it.unibo.system.SituatedActiveObject;

public class RemoteEventsObserver extends SituatedActiveObject {
	protected static int		nInst		= 0;
	protected boolean			ackReceived	= false;
	protected IConnInteraction	connClient;
	protected String			curNodeName;
	protected IContactComponent	dummy		= new DummyEmitter();
	protected FactoryProtocol	factory;
	
	public RemoteEventsObserver(final IOutputView outView, final String name, final String curNodeName, final IConnInteraction connClient) {
		super(outView, name + nInst);
		this.curNodeName = curNodeName;
		nInst++;
		this.connClient = connClient;
		// System.out.println( " ������ "+ getName() + " STARTS" );
	}
	
	@Override
	protected void doJob() throws Exception {
		// println(" ������ "+ getName() + " registering ..." + curNodeName + " connClient=" + connClient);
		connClient.sendALine(curNodeName);
		// println(" ������ "+ getName() + " receiveing ..." );
		String msg = connClient.receiveALine();
		// println( " ������ "+ getName() + "	received (ack) from server >"+msg);
		ackReceived = true;
		while (true) {
			msg = connClient.receiveALine();
			// msg = xxx( yyy )
			String msgfunctor = msg.substring(0, msg.indexOf("("));
			// content
			msg = msg.substring(msg.indexOf("(") + 1, msg.length() - 1);
			// println( " ������ "+ getName() + " ***	raising (application must be configured) "+ msgfunctor + "/"+ msg);
			if (msgfunctor.contains(SysKbInfoReplInfr.eon)) {
				final int p = msgfunctor.indexOf(SysKbInfoReplInfr.eon);
				msgfunctor = msgfunctor.substring(0, p);
				// println("***!!!!!! ***" + msgfunctor );
				SysKbInfoReplInfr.njs.raiseEvent(dummy, msgfunctor, msg);
			}
			
		}
	}
	
	@Override
	protected void endWork() throws Exception {
		println("		" + getName() + " ENDS");
	}
	
	public int getAckNum() {
		return ackReceived ? 1 : 0;
	}
	
	@Override
	protected void startWork() throws Exception {
	}
	
}
