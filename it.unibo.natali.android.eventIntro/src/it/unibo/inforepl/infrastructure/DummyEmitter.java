package it.unibo.inforepl.infrastructure;

import it.unibo.event.interfaces.IContactComponent;
import it.unibo.event.interfaces.IEventItem;

public class DummyEmitter implements IContactComponent {
	
	@Override
	public void doJob() throws Exception {
	}
	
	@Override
	public int getCid() {
		return 0;
	}
	
	@Override
	public String getName() {
		return SysKbInfoReplInfr.eon;
	}
	
	@Override
	public boolean isPassive() {
		return false;
	}
	
	@Override
	public boolean isTerminated() {
		return false;
	}
	
	@Override
	public boolean isWaiting() {
		return false;
	}
	
	@Override
	public void resume(final IEventItem ev) {
	}
	
	@Override
	public void setWaitMode() {
	}
	
	@Override
	public void terminate() throws Exception {
	}
	
}
