package it.unibo.inforepl.infrastructure;

import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.is.interfaces.protocols.ITcpConnection;
import it.unibo.platform.tcp.SocketTcpConnSupport;
import it.unibo.supports.FactoryProtocol;
import it.unibo.supports.tcp.FactoryTcpProtocol;

import java.net.ServerSocket;
import java.net.Socket;

public class EventEmitterServerTcp extends EventEmitterServer {
	protected IConnInteraction	connServer;
	protected FactoryProtocol	factory;
	protected ServerSocket		serverSock;
	protected ITcpConnection	supportTcp;
	
	public EventEmitterServerTcp(final IOutputView outView, final String name, final String myNode, final String port, final String[] events) throws Exception {
		super(outView, name, myNode, port, events);
	}
	
	@Override
	protected void init() throws Exception {
		final FactoryTcpProtocol factory = new FactoryTcpProtocol(outView, "serverTcp");
		supportTcp = factory.createServerSupport();
		serverSock = supportTcp.connectAsReceiver(Integer.parseInt(port));
	}
	
	@Override
	protected IConnInteraction waitForAConnection(final int k) throws Exception {
		final Socket sock = supportTcp.acceptAConnection(serverSock);
		println("	!!! CONNECTION !!! " + sock);
		final IConnInteraction conn = new SocketTcpConnSupport("TcpConnsupport", sock, outView);
		return conn;
	}
	
}