package it.unibo.inforepl.infrastructure;

import it.unibo.event.interfaces.INodejsLike;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;
import it.unibo.nodelike.platfom.NodejsLike;
import it.unibo.system.SituatedPlainObject;

import java.io.InputStream;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;
import alice.tuprolog.Var;

/*
 * SIUNGLETON
 */

public class SysKbInfoReplInfr extends SituatedPlainObject {
	public static enum ConnectionThreadStates {
		READY, RUNNING, TERMINATED
	}
	
	public static final String				configFile		= "sysConfig.pl";
	
	public static final String				configRules		= " " + "nodes( NodeList ) :- findall( X, node( X,Y,Z ), NodeList ).\n"
																	+ "numOfNodes( N ) :- findall( X, node( X,Y,Z ), Res ), length( Res,N).\n";
	
	public static String					eon				= "xyotherNodexy";																	// event
	
	// other
	// node
	public static SysKbInfoReplInfr			mySelf			= null;
	
	public static final INodejsLike			njs				= NodejsLike.getNodejsLike();
	public static final int					repeatTime		= 2000;	;
	public static final String				serverTimeOut	= "200000";
	// public static final String configRulesFile = "libs/sysConfigRules.pl";
	public static String[]					sysnodes;																							// LEGACY
	
	public static SysKbInfoReplInfr getInstance(final IOutputView outView, final InputStream f) {
		if (mySelf != null) {
			return mySelf;
		} else {
			return new SysKbInfoReplInfr(outView, f);
		}
	}
	
	// setSysNodes:LEGACY
	public static void setSysNodes(final String[] applicationNodes) {
		sysnodes = applicationNodes;
	}
	
	public static void setVerboseMode() {
		njs.setVerboseMode();
	}
	
	public static void terminate() {
		njs.unsetWithExternalEvents();
	}
																																				
	protected Hashtable<String, Boolean>	attemptConns	= new Hashtable<String, Boolean>();
	protected String						curNodeAddr		= "localhost";
	protected String						curNodePort		= "8080";
	
	protected Prolog						engine			= new Prolog();
	
	public Vector<String>					nodes			= new Vector<String>();
	
	protected PerceiveConnectionThread		percth;
	
	protected EventEmitterServerTcp			serverTcp;
	
	public SysKbInfoReplInfr(final IOutputView outView, final InputStream f) {
		super(outView);
		// println(" **** SysKbInfoReplInfr outView CTREATING 1 ... " + mySelf );
		if (mySelf != null) {
			return;
		}
		try {
			engine = new Prolog();
			nodes = new Vector<String>();
			engine.clearTheory();
			engine.setTheory(new Theory(f));
			engine.addTheory(new Theory(configRules));
			mySelf = this;
			// println(" **** SysKbInfoReplInfr outView CTREATING 2 ... " + mySelf );
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	public void addEventHandler(final EventHandler evh0, final String eventName) {
		njs.insertInEventHandlerWaitQueue(evh0, eventName);
	}
	
	public boolean checkPrologStyleProperty(final String goal) throws Exception {
		final SolveInfo sol = engine.solve(goal + ".");
		return sol.isSuccess();
	}
	
	public void createInfoReplInfrastructure(final String node, final String[] events) throws Exception {
		println(" **** creating infrastr to emit/perceive *** " + node);
		setTheNodes();
		createInfrastructureToEmit(node, events);
		createInfrastructureToPerceive(node);
		/*
		 * startUpMode(synch)
		 */
		if (checkPrologStyleProperty("startUpMode(synch)")) {
			while (serverTcp.nRequest() < (nodes.size() - 1)) {
				println("		waiting for  : " + nodes.size());
				println("		... nRequest = " + serverTcp.nRequest() + "/" + (nodes.size() - 1));
				Thread.sleep(repeatTime);
			}
		}
		createMainLoop();
	}
	
	public void createInfrastructureToEmit(final String node, final String[] events) throws Exception {
		final String port = getMyPort(node);
		setServerTimeout();
		serverTcp = new EventEmitterServerTcp(outView, "EventEmitterServerTcp", node, port, events);
		serverTcp.start();
		// println("created InfrastructureToEmit : " + node + " on port:" + port);
	}
	
	public void createInfrastructureToPerceive(final String myNode) throws Exception {
		// println("Creating InfrastructureToPerceive : " + myNode);
		final Iterator<String> iter = nodes.iterator();
		while (iter.hasNext()) {
			final String curNode = iter.next();
			if (!curNode.equals(myNode)) {
				percth = PerceiveConnectionThread.getInstance(outView, "connTo" + curNode, engine, myNode, curNode);
				if (percth.getCurState() == ConnectionThreadStates.READY) {
					percth.start();
				}
			}
		}
	}
	
	protected void createMainLoop() {
		new Thread() {
			@Override
			public void run() {
				try {
					println("STARTING EVENT LOOP");
					njs.setWithExternalEvents();
					// njs.setVerboseMode();
					njs.mainLoop();
				} catch (final Exception e) {
					e.printStackTrace();
				}
				
			}
		}.start();
	}
	
	protected String getMyPort(final String node) throws Exception {
		final SolveInfo sol = engine.solve("node(" + node + ",Y,Z).");
		if (sol.isSuccess()) {
			final Term term = sol.getSolution();
			final Term arg2 = ((Struct) term).getArg(2);
			final Term port = ((Var) arg2).getTerm();
			System.out.println("port=" + port + " for " + node);
			return "" + port;
		} else {
			throw new Exception("no node");
		}
		
	}
	
	public int getNumOfNodes() {
		return nodes.size();
	}
	
	public PerceiveConnectionThread getPerceiveConnectionThread() {
		return percth;
	}
	
	public void reconnectTo(final String myNode, final String curNode) throws Exception {
		percth = PerceiveConnectionThread.getInstance(outView, "connTo" + curNode, engine, myNode, curNode);
		if ((percth.getCurState() == ConnectionThreadStates.READY) || (percth.getCurState() == ConnectionThreadStates.TERMINATED)) {
			// println("		PerceiveConnectionThread should redo CONNECTING as perceiver to " + curNode );
			percth.restart();
		}
		// }
	}
	
	public void setMyNode(final String nodeIP) {
		System.setProperty("myNode", nodeIP);
	}
	
	public void setServerTimeout() {
		System.setProperty("inputTimeOut", serverTimeOut);
	}
	
	protected void setTheNodes() throws Exception {
		// if( nodes.size() > 0 ) return; //TODO
		final SolveInfo sol = engine.solve("nodes(N).");
		if (sol.isSuccess()) {
			final Term solTerm = sol.getSolution();
			final Term content = ((Struct) solTerm).getArg(0);
			final Term listOfNodes = ((Var) content).getTerm();
			final Struct listNodes = (Struct) listOfNodes;
			final Iterator<Term> iter = (Iterator<Term>) listNodes.listIterator();
			while (iter.hasNext()) {
				nodes.add("" + iter.next());
			}
			println("THE SYSTEM HAS  " + nodes.size() + " NODES");
		}
		
	}
	
}
