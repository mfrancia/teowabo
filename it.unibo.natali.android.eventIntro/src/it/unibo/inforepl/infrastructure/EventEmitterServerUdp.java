package it.unibo.inforepl.infrastructure;

import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.is.interfaces.protocols.IUdpConnection;
import it.unibo.platform.udp.SocketUdpConnSupport;
import it.unibo.supports.udp.FactoryUdpProtocol;

import java.net.DatagramSocket;

public class EventEmitterServerUdp extends EventEmitterServer {
	protected DatagramSocket	sock;
	protected IUdpConnection	supportUdp;
	
	public EventEmitterServerUdp(final IOutputView outView, final String name, final String myNode, final String port, final String[] events) throws Exception {
		super(outView, name, myNode, port, events);
	}
	
	@Override
	protected void init() throws Exception {
		final FactoryUdpProtocol factory = new FactoryUdpProtocol(outView, "server");
		supportUdp = factory.createServerSupport();
		sock = supportUdp.connectAsReceiver(Integer.parseInt(port));
		// println(" 	*** Server sock   " + sock );
	}
	
	@Override
	protected IConnInteraction waitForAConnection(final int k) throws Exception {
		return new SocketUdpConnSupport(sock, Integer.parseInt(port), null, env.getOutputView());
		// return supportUdp.openConnection(); //for each message a connection
	}
	
}
