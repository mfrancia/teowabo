package it.unibo.inforepl.infrastructure.ping;

import it.unibo.event.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;

public class RemoteEventDisplayHandler extends EventHandler {
	
	public RemoteEventDisplayHandler(final String name, final IOutputView outView) throws Exception {
		super(name, outView);
	}
	
	@Override
	public void doJob() throws Exception {
		IEventItem ev = getEventItem();
		while (ev != null) {
			showMsg(ev.getEventId() + " / " + ev.getMsg());
			ev = getEventItem();
		}
	}
	
}
