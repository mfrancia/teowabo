package it.unibo.inforepl.infrastructure.ping;

import it.unibo.android.eventIntro.SysKb;
import it.unibo.inforepl.infrastructure.PerceiveConnectionThread;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.system.SituatedActiveObject;

public abstract class Node extends SituatedActiveObject {
	protected String			myEvent;
	protected SysKbInfoReplInfr	sysInfr;
	protected boolean			terminated	= false;
	
	public Node(final IOutputView outView, final String name, final String[] myEvents, final String[] otherEvents) {
		super(outView, name);
		myEvent = myEvents[0];
		try {
			createInfrastructure(name, myEvents, otherEvents);
			PerceiveConnectionThread.terminated = false;
		} catch (final Exception e) {
			println("ERROR " + e.getMessage());
		}
	}
	
	protected void createInfrastructure(final String myNode, final String[] infoOut, final String[] infoIn) throws Exception {
		// println("Node createInfrastructure 1 " );
		PerceiveConnectionThread.resetMyInstances();
		sysInfr = SysKbInfoReplInfr.getInstance(outView, SysKb.configInputStream);
		// println("Node createInfrastructure 2 " + sysInfr);
		sysInfr.createInfoReplInfrastructure(myNode, infoOut);
		createRemoteEventDisplayHandler(infoIn);
	}
	
	protected void createRemoteEventDisplayHandler(final String[] infoIn) throws Exception {
		final RemoteEventDisplayHandler evh0 = new RemoteEventDisplayHandler("rh" + getName(), outView);
		for (final String element : infoIn) {
			sysInfr.addEventHandler(evh0, element);
		}
	}
	
	@Override
	protected void doJob() throws Exception {
		// println( "RAISING " );
		
		for (int i = 1; i <= SysKb.nIter; i++) {
			if (terminated) {
				break;
			}
			final String msg = "v(" + getName() + "," + i + ")";
			println("RAISING " + myEvent + " msg=" + msg);
			SysKbInfoReplInfr.njs.raiseEvent(null, myEvent, msg);
			SysKbInfoReplInfr.njs.raiseEvent(null, SysKb.info, msg);
			Thread.sleep(SysKb.delay);
		}
	}
	
	@Override
	protected void endWork() throws Exception {
		println("ENDS ");
		terminated = true;
		PerceiveConnectionThread.terminated = true;
		SysKbInfoReplInfr.terminate();
	}
	
	@Override
	protected void startWork() throws Exception {
		PerceiveConnectionThread.terminated = false;
	}
	
	public void terminate() throws Exception {
		endWork();
	}
	
}
