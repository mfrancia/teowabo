package it.unibo.inforepl.infrastructure.ping;

import it.unibo.android.eventIntro.SysKb;
import it.unibo.is.interfaces.IOutputView;

public class NodePing1 extends Node {
	protected final static String[]	myEvents	= new String[] { SysKb.infoA, SysKb.info };				// , SysKb.info
	protected final static String[]	otherEvents	= new String[] { SysKb.infoB, SysKb.infoC, SysKb.info };	// , SysKb.info
	
	public NodePing1(final IOutputView outView) throws Exception {
		super(outView, SysKb.node1, NodePing1.myEvents, NodePing1.otherEvents);
		// SysKbInfoReplicateInfrast.setVerboseMode();
		println("NodePing1 started");
		// addApplicationHandlers();
	}
	
	protected void addApplicationHandlers() throws Exception {
		final ApplicationEventHandler evh0 = new ApplicationEventHandler("evhNode1", sysInfr.getNumOfNodes(), myEvents, outView);
		for (final String myEvent2 : myEvents) {
			// println("NodePing1 add handler for " +myEvents[i]);
			sysInfr.addEventHandler(evh0, myEvent2);
		}
		// for( int i=0; i<otherEvents.length; i++){
		// println("NodePing1 add handler for " +otherEvents[i]);
		// sysInfr.addEventHandler(evh0, otherEvents[i]);
		// }
	}
	
}
