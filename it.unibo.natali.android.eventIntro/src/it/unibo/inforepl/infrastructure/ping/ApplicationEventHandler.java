package it.unibo.inforepl.infrastructure.ping;

import it.unibo.event.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.nodelike.platfom.EventHandler;

/*
 * An instance of this class is executed when an event with id included in the evIds array is raised either locally or remotely
 */
public class ApplicationEventHandler extends EventHandler {
	protected String[]	evIds;
	protected int		numOfNodes	= 1;
	protected int		nv			= 0;
	protected int		nvMax		= 10;	// SysKb.nIter + (SysKb.nIter - 1) * numOfNodes / 2;
	
	public ApplicationEventHandler(final String name, final int numOfNodes, final String[] evIds, final IOutputView outView) throws Exception {
		super(name, outView);
		this.numOfNodes = numOfNodes;
		this.evIds = evIds;
		showMsg("ApplicationEventHandler STARTS ");
	}
	
	@Override
	public void doJob() throws Exception {
		final IEventItem ev = getEventItem();
		// showMsg(" ====  " + ev.getMsg());
		showMsg("ApplicationEventHandler | " + ev.getEventId() + " / " + ev.getMsg() + " nv=" + nv + "/" + nvMax);
		// Reset external events
		nv++;
		// if( nv >= nvMax ) SysKbInfoReplInfr.njs.unsetWithExternalEvents();
	}
	
}
