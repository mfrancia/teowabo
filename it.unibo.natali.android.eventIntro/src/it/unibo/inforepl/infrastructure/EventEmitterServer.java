package it.unibo.inforepl.infrastructure;

import it.unibo.event.interfaces.IEventItem;
import it.unibo.is.interfaces.IOutputView;
import it.unibo.is.interfaces.protocols.IConnInteraction;
import it.unibo.nodelike.platfom.EventHandler;
import it.unibo.system.SituatedActiveObject;
import it.unibo.system.SituatedPlainObject;

import java.util.Hashtable;
import java.util.Iterator;
import java.util.Vector;

public abstract class EventEmitterServer extends SituatedActiveObject {
	/*
	 * ===================================================
	 */
	protected class RegisterRequestExecutor extends SituatedPlainObject {
		protected IConnInteraction	connToClient;
		protected String			name;
		protected String			nodesenderName;
		
		public RegisterRequestExecutor(final IOutputView outView, final String name, final IConnInteraction connToClient, final String nodesenderName) throws Exception {
			super(outView);
			this.name = name;
			this.connToClient = connToClient;
			this.nodesenderName = nodesenderName;
			update("ack");
		}
		
		public String getsenderName() {
			return nodesenderName;
		}
		
		public void update(final String m) throws Exception {
			// println(name +" sends: "+m );
			// m=eventxyotherNodexy (SysKbInfoReplInfreon)
			nRequest = nRequest + 1;
			// try {
			connToClient.sendALine(m, true);
			// } catch (Exception e) {
			// System.out.println(" ***** " + e.getMessage());
			// conns.remove(this);
			// // e.printStackTrace();
			// }
		}
	}
	
	/*
	 * ===================================================
	 */
	protected class SensorEventHandler extends EventHandler {
		protected double	dt;
		// protected String timer1 = "tiner1";
		protected boolean	firstEvent	= true;
		protected int		n			= 1;
		
		public SensorEventHandler(final String name, final IOutputView outView) throws Exception {
			super(name, outView);
		}
		
		@Override
		public void doJob() throws Exception {
			final IEventItem ev = getEventItem();
			if (ev != null) {
				// showMsg("SensorEventHandler subj=" + ev.getSubj() + " ev=" + ev.getEventId() + " " + ev.getMsg() );
				boolean isACopy = false;
				if (ev.getSubj() != null) {
					isACopy = ev.getSubj().getName().equals(SysKbInfoReplInfr.eon);
				}
				if (!isACopy && isIn(ev.getEventId(), eventsToEmit)) {
					newEvent(ev);
				}
			}
		}
		
		protected boolean isIn(final String evId, final String[] events) {
			for (final String event : events) {
				if (event.equals(evId)) {
					return true;
				}
			}
			return false;
		}
	}
	
	protected Vector<RegisterRequestExecutor>				conns			= new Vector<RegisterRequestExecutor>();
	protected String[]										eventsToEmit;
	// protected Hashtable<String,RegisterRequestExecutor> htConns = new Hashtable<String,RegisterRequestExecutor>();
	protected Hashtable<String, RegisterRequestExecutor>	failedConnecs	= new Hashtable<String, RegisterRequestExecutor>();
	protected String										myNode;
	
	public int												nRequest		= 0;
	
	protected String										port;
	
	public EventEmitterServer(final IOutputView outView, final String name, final String myNode, final String port, final String[] eventsToEmit) throws Exception {
		super(outView, name);
		this.myNode = myNode;
		this.port = port;
		this.eventsToEmit = eventsToEmit;
		final SensorEventHandler evh0 = new SensorEventHandler("infraStrSensorEventHandler", outView);
		for (final String element : eventsToEmit) {
			SysKbInfoReplInfr.njs.insertInEventHandlerWaitQueue(evh0, element); // EVENT FROM OTHER NODE
			// this.println("event handler created");
		}
	}
	
	public synchronized void addConn(final String nodeSenderName, final RegisterRequestExecutor executor) throws Exception {
		// env.println("		EventEmitterServer addConn " + nodeSenderName + " conns " + conns.size() );
		conns.add(executor);
		// The current node should retry a createInfrastructureToPerceive to the just reconnected node
		if (failedConnecs.get(nodeSenderName) != null) {
			SysKbInfoReplInfr.getInstance(outView, null).reconnectTo(myNode, nodeSenderName);
			failedConnecs.remove(nodeSenderName);
		}
	}
	
	/*
	 * Receive requests and events
	 */
	@Override
	protected void doJob() throws Exception {
		// println("		EventEmitterServer STARTS on " + port );
		int k = 1;
		// for(int k=1; k<=SysKbInfrast.nIter; k++){
		while (true) {
			// Create a IConnInteraction object for each connection-request
			// println("	 --- EventEmitterServer waitForAConnection " + k);
			final IConnInteraction conn = waitForAConnection(k);
			final String nodeSenderName = conn.receiveALine(); // register
			// println("	---	EventEmitterServer RECEIVED " + nodeSenderName + " conns " + conns.size() );
			final RegisterRequestExecutor executor = new RegisterRequestExecutor(outView, "RegisterRequestExecutor_" + k, conn, nodeSenderName);
			addConn(nodeSenderName, executor);
			k++;
		}
		// println("		EventEmitterServer ENDS " );
	}
	
	@Override
	protected void endWork() throws Exception {
	}
	
	protected abstract void init() throws Exception;
	
	public void newEvent(final IEventItem ev) {
		final String evId = ev.getEventId();
		final String evMsg = ev.getMsg();
		// build the message to be sent on the network
		final String newEv = evId + SysKbInfoReplInfr.eon + "(" + evMsg + ")"; // EVENT from other node
		// println("EventEmitterServer newEvent " + newEv+ " to " + conns.size() + " remote observers");
		final Iterator<RegisterRequestExecutor> iter = conns.iterator();
		final Vector<RegisterRequestExecutor> failedConns = new Vector<RegisterRequestExecutor>();
		while (iter.hasNext()) {
			final RegisterRequestExecutor curExec = iter.next();
			try {
				curExec.update(newEv);
			} catch (final Exception e) {
				println("*** CONNECTION FAILURE *** " + e.getMessage());
				failedConns.add(curExec);
				failedConnecs.put(curExec.getsenderName(), curExec);
			}
		}
		// Update active connections
		removeConns(failedConns);
	}
	
	public int nRequest() {
		return nRequest;
	}
	
	protected synchronized void removeConns(final Vector<RegisterRequestExecutor> failedConns) {
		// Update active connections
		final Iterator<RegisterRequestExecutor> iter = failedConns.iterator();
		while (iter.hasNext()) {
			final RegisterRequestExecutor toRemove = iter.next();
			conns.remove(toRemove);
		}
	}
	
	@Override
	protected void startWork() throws Exception {
		init();
	}
	
	protected abstract IConnInteraction waitForAConnection(int k) throws Exception;
}
