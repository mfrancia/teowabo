package it.unibo.android.eventIntro;

import it.unibo.android.intro.utils.BaseActivity;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import android.os.Environment;
import android.widget.Toast;

public class SysKb {
	/*
	 * WE NEED A CUSTOM (DOMAIN SPECIFIC) LANGUAGE ...
	 */
	public static InputStream	configInputStream	= null;									// to be set by the MainActivity
	
	public final static int		delay				= 4000;
	public final static String	info				= "any";
	public final static String	infoA				= "fromA";
	public final static String	infoB				= "fromB";
	public final static String	infoC				= "fromC";
	
	public static final String	internalconfigFile	= "res/raw/sysconfig.pl";
	public final static int		nIter				= 20;
	public final static String	node1				= "node1";
	
	public final static String	node2				= "node2";
	public final static String	node3				= "node3";
	public static final String	sdconfigFile		= "/external_sd/Didattica/sysconfig.pl";
	
	public static void setConfigFile(final BaseActivity ba) {
		try {
			final String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
			final String fpath = baseDir + sdconfigFile;
			final File myFile = new File(fpath);
			SysKb.configInputStream = new FileInputStream(myFile);
		} catch (final Exception e) {
			Toast.makeText(ba.getBaseContext(), "ERROR " + e.getMessage(), Toast.LENGTH_SHORT).show();
		}
	}
}