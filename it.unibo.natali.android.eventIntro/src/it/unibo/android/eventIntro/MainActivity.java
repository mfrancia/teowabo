package it.unibo.android.eventIntro;

import it.unibo.android.intro.utils.BaseActivity;
import it.unibo.inforepl.infrastructure.PerceiveConnectionThread;
import it.unibo.inforepl.infrastructure.SysKbInfoReplInfr;
import it.unibo.inforepl.infrastructure.ping.Node;
import it.unibo.inforepl.infrastructure.ping.NodePing1;

import java.io.File;
import java.io.FileInputStream;

import android.os.Bundle;
import android.os.Environment;

public class MainActivity extends BaseActivity {
	protected Node	node;
	
	protected void launchApp() {
		try {
			PerceiveConnectionThread.terminated = false;
			// println("configInputStream " + configInputStream);
			println("STARING THE (UNIQUE) ANDROID NODE");
			node = new NodePing1(outView);
			node.start();
		} catch (final Exception e) {
			println("ERROR " + e.getMessage());
		} // OK AssetInputStream
	}
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		SysKb.setConfigFile(this);
		launchApp();
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		try {
			node.terminate();
			SysKbInfoReplInfr.mySelf = null; // to force re-creation
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}// onPause
	
	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// // Inflate the menu; this adds items to the action bar if it is present.
	// getMenuInflater().inflate(R.menu.main, menu);
	// return true;
	// }
	@Override
	protected void onResume() {
		super.onResume();
	}// onResume
	
	protected void setConfigFile() {
		final String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
		// println("baseDir " + baseDir); // /mnt/sdcard
		try {
			final String fpath = baseDir + "/external_sd/Didattica/sysconfig.pl";
			println("READING " + fpath);
			final File myFile = new File(fpath);
			final FileInputStream fIn = new FileInputStream(myFile);
			// DataInputStream dis = new DataInputStream(fIn);
			// println("OPENED " + fpath);
			// String tt = dis.readLine();
			// dis.close();
			// println("READ:" + tt);
			SysKb.configInputStream = fIn;
			// getAssets().open( "sysConfig.pl");
			// this.getResources().openRawResource(R.raw.sysconfig);
		} catch (final Exception e) {
			println("ERROR:" + e.getMessage());
		}
	}
	
	/*
	 * Tests to be remove : STARTS
	 */
	// protected void testFile(){
	// String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
	// // String baseDir1 = Environment.getExternalStorageDirectory().getPath();
	// println("baseDir " + baseDir); // /mnt/sdcard
	// // println("baseDir1 " + baseDir1); // /mnt/sdcard
	//
	// //writing a file in /data/data
	// try{
	// println("WRIING xxx.txt on the /data/data dir");
	// FileOutputStream fos = this.openFileOutput("xxx.txt", MODE_PRIVATE);
	// //This method opens a file in the private data area of the application.
	// //You cannot open any files in subdirectories in this area or from entirely other areas using this method.
	// DataOutputStream dos = new DataOutputStream(fos);
	// dos.writeUTF("Hello world");
	// dos.close();
	// println("WRITTEN xxx.txt");
	// }catch(Exception e){
	// println("ERROR  " + e.getMessage());
	// }
	//
	// //writing on external memory
	// try{
	// println("WRIING config.txt on the sdcard");
	// File myFile1 = new File(baseDir+"/config.txt"); //PERMISSIONE DENIED
	// FileOutputStream fos = new FileOutputStream(myFile1);
	// DataOutputStream dos = new DataOutputStream(fos);
	// dos.writeUTF("Hello world");
	// dos.close();
	// println("WRITTEN config.txt");
	// }catch(Exception e){
	// println("ERROR  " + e.getMessage());
	// }
	//
	// //Reading
	// try{
	// String fpath = this.getApplicationContext().getFilesDir().getPath();
	// fpath = fpath + "/xxx.txt";
	// println("READING " + fpath);
	// FileInputStream fis = new FileInputStream(new File(fpath));
	// DataInputStream dis = new DataInputStream(fis);
	// String t = dis.readUTF();
	// dis.close();
	// println("READ:" + t);
	// }catch(Exception e){
	// println("ERROR  " + e.getMessage());
	// }
	//
	//
	//
	// try {
	// String fpath = baseDir + "/external_sd/Didattica/sysconfig.pl";
	// println("READING " + fpath);
	// File myFile = new File(fpath);
	// FileInputStream fIn = new FileInputStream(myFile);
	// DataInputStream dis = new DataInputStream(fIn);
	// println("OPENED " + fpath);
	// String tt = dis.readLine();
	// dis.close();
	// println("READ:" + tt);
	// // Toast.makeText(getBaseContext(),
	// // "Done reading SD  " + aBuffer,
	// // Toast.LENGTH_SHORT).show();
	// } catch ( Exception e) {
	// println("ERROR:" + e.getMessage());
	// }
	//
	// }
	/*
	 * Test to be remove : ENDS
	 */
	
}
