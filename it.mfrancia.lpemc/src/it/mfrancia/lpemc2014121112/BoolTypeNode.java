package it.mfrancia.lpemc2014121112;

public class BoolTypeNode extends Node {
	
	public BoolTypeNode() {
	}
	
	public String toPrint(String s) {
		return "bool";
	}
	
	public Node typeCheck() {
		return new BoolTypeNode();
	}
	
	public String codeGeneration() {
		return this.toString() + "dummy";
	}
}