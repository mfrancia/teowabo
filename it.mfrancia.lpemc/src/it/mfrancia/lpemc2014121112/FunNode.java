package it.mfrancia.lpemc2014121112;

import java.util.ArrayList;

public class FunNode extends Node {
	
	private String			id;
	private Node			type;
	private ArrayList<Node>	par;
	private ArrayList<Node>	def;
	private Node			body;
	
	public FunNode(String i, Node t) {
		id = i;
		type = t;
		par = new ArrayList<Node>();
	}
	
	public void addDecBody(ArrayList<Node> d, Node b) {
		def = d;
		body = b;
	}
	
	public void addPar(Node p) {
		par.add(p);
	}
	
	public String toPrint(String s) {
		return "";
	}
	
	public Node typeCheck() {
		ArrayList<Node> pt = new ArrayList<Node>();
		for (int i = 0; i < par.size(); i++)
			pt.add((par.get(i)).typeCheck());
		for (int j = 0; j < def.size(); j++)
			(def.get(j)).typeCheck();
		if (!(FOOLlib.isSubtype(body.typeCheck(), type))) {
			System.out.println("Wrong return type for function " + id);
			System.exit(0);
		}
		return new ArrowTypeNode(pt, type);
	}
	
	public String codeGeneration() {
		return this.toString() + "dummy";
	}
}