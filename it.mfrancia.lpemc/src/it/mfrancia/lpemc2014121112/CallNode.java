package it.mfrancia.lpemc2014121112;

import java.util.ArrayList;

public class CallNode extends Node {
	
	private String			id;
	private STentry			st;
	private ArrayList<Node>	par;
	
	public CallNode(String i, STentry e, ArrayList<Node> p) {
		id = i;
		st = e;
		par = p;
	}
	
	public String toPrint(String s) {
		return "";
	}
	
	public Node typeCheck() {
		Node t = st.getType();
		if (!(t instanceof ArrowTypeNode)) {
			System.out.println("Invocation of a non-function " + id);
			System.exit(0);
		}
		ArrayList<Node> p = ((ArrowTypeNode) t).getPar();
		if (!(p.size() == par.size())) {
			System.out.println("Wrong number of parameters in the invocation of " + id);
			System.exit(0);
		}
		for (int i = 0; i < par.size(); i++)
			if (!(FOOLlib.isSubtype((par.get(i)).typeCheck(), p.get(i)))) {
				System.out.println("Wrong type for " + (i + 1) + "-th parameter in the invocation of " + id);
				System.exit(0);
			}
		return ((ArrowTypeNode) t).getRet();
	}
	
	public String codeGeneration() {
		return this.toString() + "dummy";
	}
}