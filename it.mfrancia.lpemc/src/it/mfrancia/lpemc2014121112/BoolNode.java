package it.mfrancia.lpemc2014121112;

public class BoolNode extends Node {
	
	private boolean	val;
	
	public BoolNode(boolean n) {
		val = n;
	}
	
	public String toPrint(String s) {
		if (val)
			return s + "Bool:true\n";
		else
			return s + "Bool:false\n";
	}
	
	public Node typeCheck() {
		return new BoolTypeNode();
	}
	
	@Override
	public String codeGeneration() {
		return "push " + (val == true? 1 : 0) + "\n";
	}
}