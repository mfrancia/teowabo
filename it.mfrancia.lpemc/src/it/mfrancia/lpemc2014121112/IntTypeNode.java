package it.mfrancia.lpemc2014121112;

public class IntTypeNode extends Node {
	
	public IntTypeNode() {
	}
	
	public String toPrint(String s) {
		return "int";
	}
	
	public Node typeCheck() {
		return new IntTypeNode();
	}
	
	public String codeGeneration() {
		return this.toString() + "dummy";
	}
}