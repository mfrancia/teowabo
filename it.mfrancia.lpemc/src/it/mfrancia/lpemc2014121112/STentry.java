package it.mfrancia.lpemc2014121112;

public class STentry {
	
	private Node	dec;
	private int		nl;
	private Node	type;
	private int		offset;
	
	public STentry(Node d, int n) {
		dec = d;
		nl = n;
	}
	
	public STentry(Node d, int n, Node t) {
		dec = d;
		nl = n;
		type = t;
	}
	
	public STentry(Node d, int n, Node t, int offset) {
		dec = d;
		nl = n;
		type = t;
		this.offset = offset;
	}
	
	public int getOffset() {
		return offset;
	}
	
	public void addType(Node t) {
		type = t;
	}
	
	public Node getType() {
		return type;
	}
	
}