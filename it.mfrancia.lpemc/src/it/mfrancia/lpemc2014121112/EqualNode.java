package it.mfrancia.lpemc2014121112;

public class EqualNode extends Node {
	
	private Node	left;
	private Node	right;
	
	public EqualNode(Node l, Node r) {
		left = l;
		right = r;
	}
	
	public String toPrint(String s) {
		return s + "Equal\n" + left.toPrint(s + "  ") + right.toPrint(s + "  ");
	}
	
	public Node typeCheck() {
		Node l = left.typeCheck();
		Node r = right.typeCheck();
		if (!(FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			System.out.println("Incompatible types in equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}
	
	@Override
	public String codeGeneration() {
		String equal = FOOLlib.getLabelId();
		String notEqual = FOOLlib.getLabelId();
		return left.codeGeneration() + right.codeGeneration() + "beq " + equal + "\n" + "push 0\n" + "b " + notEqual + "\n" + equal + ":\n" + "push 1\n" + notEqual + ":\n";
	}
}