package it.mfrancia.lpemc2014121112;

public class IdNode extends Node {
	
	private String	id;
	private STentry	entry;
	
	public IdNode(String i, STentry st) {
		id = i;
		entry = st;
	}
	
	public String toPrint(String s) {
		return "";
	}
	
	public Node typeCheck() {
		return entry.getType();
	}
	
	public String codeGeneration() {
		return "push " + entry.getOffset() + "\n"+ 		//aggiungo un numero
				/* push frame pointer */"lfp\n" +  		//aggiungo un secondo numero
				"add\n" + 								//li sommo
				"lw\n";									//vado a prendere il contenuto della variabile
	}
}