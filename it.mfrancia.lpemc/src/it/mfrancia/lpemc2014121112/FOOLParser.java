package it.mfrancia.lpemc2014121112;
// $ANTLR 3.5.1 /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g 2014-12-12 16:50:46

import java.util.ArrayList;
import java.util.HashMap;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[] tokenNames = new String[] {
		"<invalid>", "<EOR>", "<DOWN>", "<UP>", "ASS", "BOOL", "CLPAR", "COLON", 
		"COMMA", "COMMENT", "CRPAR", "ELSE", "EQ", "ERR", "FALSE", "FUN", "ID", 
		"IF", "IN", "INT", "LET", "LPAR", "NAT", "PLUS", "PRINT", "RPAR", "SEMIC", 
		"THEN", "TIMES", "TRUE", "VAR", "WHITESP"
	};
	public static final int EOF=-1;
	public static final int ASS=4;
	public static final int BOOL=5;
	public static final int CLPAR=6;
	public static final int COLON=7;
	public static final int COMMA=8;
	public static final int COMMENT=9;
	public static final int CRPAR=10;
	public static final int ELSE=11;
	public static final int EQ=12;
	public static final int ERR=13;
	public static final int FALSE=14;
	public static final int FUN=15;
	public static final int ID=16;
	public static final int IF=17;
	public static final int IN=18;
	public static final int INT=19;
	public static final int LET=20;
	public static final int LPAR=21;
	public static final int NAT=22;
	public static final int PLUS=23;
	public static final int PRINT=24;
	public static final int RPAR=25;
	public static final int SEMIC=26;
	public static final int THEN=27;
	public static final int TIMES=28;
	public static final int TRUE=29;
	public static final int VAR=30;
	public static final int WHITESP=31;

	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}

	// delegators


	public FOOLParser(TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	public FOOLParser(TokenStream input, RecognizerSharedState state) {
		super(input, state);
	}

	@Override public String[] getTokenNames() { return FOOLParser.tokenNames; }
	@Override public String getGrammarFileName() { return "/home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g"; }


	private ArrayList<HashMap<String,STentry>>  symTable = new ArrayList<HashMap<String,STentry>>();
	private int nestingLevel = -1;



	// $ANTLR start "prog"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:17:1: prog returns [Node ast] : (e= exp SEMIC | LET d= dec IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;


		Node e =null;
		ArrayList<Node> d =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:18:2: (e= exp SEMIC | LET d= dec IN e= exp SEMIC )
			int alt1=2;
			int LA1_0 = input.LA(1);
			if ( (LA1_0==FALSE||(LA1_0 >= ID && LA1_0 <= IF)||(LA1_0 >= LPAR && LA1_0 <= NAT)||LA1_0==PRINT||LA1_0==TRUE) ) {
				alt1=1;
			}
			else if ( (LA1_0==LET) ) {
				alt1=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 1, 0, input);
				throw nvae;
			}

			switch (alt1) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:18:4: e= exp SEMIC
					{
					pushFollow(FOLLOW_exp_in_prog30);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog32); 
					ast = new ProgNode(e);
					}
					break;
				case 2 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:20:11: LET d= dec IN e= exp SEMIC
					{
					match(input,LET,FOLLOW_LET_in_prog57); 
					nestingLevel++;
					             HashMap<String,STentry> hm = new HashMap<String,STentry> ();
					             symTable.add(hm);
					            
					pushFollow(FOLLOW_dec_in_prog86);
					d=dec();
					state._fsp--;

					match(input,IN,FOLLOW_IN_in_prog88); 
					pushFollow(FOLLOW_exp_in_prog92);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_prog94); 
					symTable.remove(nestingLevel--);
					             ast = new LetInNode(d,e) ;
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "prog"



	// $ANTLR start "dec"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:30:1: dec returns [ArrayList<Node> astlist] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* ;
	public final ArrayList<Node> dec() throws RecognitionException {
		ArrayList<Node> astlist = null;


		Token i=null;
		Token fid=null;
		Token id=null;
		Node t =null;
		Node e =null;
		Node fty =null;
		Node ty =null;
		ArrayList<Node> d =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:31:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:31:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
			{
			astlist = new ArrayList<Node>() ;
				   int offset = -1;
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:33:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
			loop5:
			while (true) {
				int alt5=3;
				int LA5_0 = input.LA(1);
				if ( (LA5_0==VAR) ) {
					alt5=1;
				}
				else if ( (LA5_0==FUN) ) {
					alt5=2;
				}

				switch (alt5) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:34:13: VAR i= ID COLON t= type ASS e= exp SEMIC
					{
					match(input,VAR,FOLLOW_VAR_in_dec144); 
					i=(Token)match(input,ID,FOLLOW_ID_in_dec148); 
					match(input,COLON,FOLLOW_COLON_in_dec150); 
					pushFollow(FOLLOW_type_in_dec154);
					t=type();
					state._fsp--;

					match(input,ASS,FOLLOW_ASS_in_dec156); 
					pushFollow(FOLLOW_exp_in_dec160);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_dec162); 
					VarNode v = new VarNode((i!=null?i.getText():null),t,e);
					             astlist.add(v);
					             HashMap<String,STentry> hm = symTable.get(nestingLevel);
					             if ( hm.put((i!=null?i.getText():null),new STentry(v,nestingLevel,t, offset--)) != null  )
					             {System.out.println("Var id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
					              System.exit(0);}  
					            
					}
					break;
				case 2 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:43:13: FUN i= ID COLON t= type LPAR (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC
					{
					match(input,FUN,FOLLOW_FUN_in_dec205); 
					i=(Token)match(input,ID,FOLLOW_ID_in_dec209); 
					match(input,COLON,FOLLOW_COLON_in_dec211); 
					pushFollow(FOLLOW_type_in_dec215);
					t=type();
					state._fsp--;

					//inserimento di ID nella symtable
					               FunNode f = new FunNode((i!=null?i.getText():null),t);
					               astlist.add(f);
					               HashMap<String,STentry> hm = symTable.get(nestingLevel);
					               STentry entry = new STentry(f,nestingLevel);
					               if ( hm.put((i!=null?i.getText():null),entry) != null  )
					               {System.out.println("Fun id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" already declared");
					                System.exit(0);}
					                //creare una nuova hashmap per la symTable
					                nestingLevel++;
					                HashMap<String,STentry> hmn = new HashMap<String,STentry> ();
					                symTable.add(hmn);
					                
					match(input,LPAR,FOLLOW_LPAR_in_dec247); 
					ArrayList<Node> parTypes = new ArrayList<Node>();
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:58:17: (fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
					int alt3=2;
					int LA3_0 = input.LA(1);
					if ( (LA3_0==ID) ) {
						alt3=1;
					}
					switch (alt3) {
						case 1 :
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:58:18: fid= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
							{
							fid=(Token)match(input,ID,FOLLOW_ID_in_dec270); 
							match(input,COLON,FOLLOW_COLON_in_dec272); 
							pushFollow(FOLLOW_type_in_dec276);
							fty=type();
							state._fsp--;


							                  parTypes.add(fty);
							                  ParNode fpar = new ParNode((fid!=null?fid.getText():null),fty);
							                  f.addPar(fpar);
							                  if ( hmn.put((fid!=null?fid.getText():null),new STentry(fpar,nestingLevel,fty)) != null  )
							                  {System.out.println("Parameter id "+(fid!=null?fid.getText():null)+" at line "+(fid!=null?fid.getLine():0)+" already declared");
							                   System.exit(0);}
							                  
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:67:19: ( COMMA id= ID COLON ty= type )*
							loop2:
							while (true) {
								int alt2=2;
								int LA2_0 = input.LA(1);
								if ( (LA2_0==COMMA) ) {
									alt2=1;
								}

								switch (alt2) {
								case 1 :
									// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:67:20: COMMA id= ID COLON ty= type
									{
									match(input,COMMA,FOLLOW_COMMA_in_dec317); 
									id=(Token)match(input,ID,FOLLOW_ID_in_dec321); 
									match(input,COLON,FOLLOW_COLON_in_dec323); 
									pushFollow(FOLLOW_type_in_dec327);
									ty=type();
									state._fsp--;


									                    parTypes.add(ty);
									                    ParNode par = new ParNode((id!=null?id.getText():null),ty);
									                    f.addPar(par);
									                    if ( hmn.put((id!=null?id.getText():null),new STentry(par,nestingLevel,ty)) != null  )
									                    {System.out.println("Parameter id "+(id!=null?id.getText():null)+" at line "+(id!=null?id.getLine():0)+" already declared");
									                     System.exit(0);}
									                    
									}
									break;

								default :
									break loop2;
								}
							}

							}
							break;

					}

					match(input,RPAR,FOLLOW_RPAR_in_dec406); 
					entry.addType( new ArrowTypeNode(parTypes , t) );
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:79:15: ( LET d= dec IN )?
					int alt4=2;
					int LA4_0 = input.LA(1);
					if ( (LA4_0==LET) ) {
						alt4=1;
					}
					switch (alt4) {
						case 1 :
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:79:16: LET d= dec IN
							{
							match(input,LET,FOLLOW_LET_in_dec425); 
							pushFollow(FOLLOW_dec_in_dec429);
							d=dec();
							state._fsp--;

							match(input,IN,FOLLOW_IN_in_dec431); 
							}
							break;

					}

					pushFollow(FOLLOW_exp_in_dec437);
					e=exp();
					state._fsp--;

					match(input,SEMIC,FOLLOW_SEMIC_in_dec439); 
					//chiudere scope
					               symTable.remove(nestingLevel--);
					               f.addDecBody(d,e);
					              
					}
					break;

				default :
					break loop5;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return astlist;
	}
	// $ANTLR end "dec"



	// $ANTLR start "type"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:87:1: type returns [Node ast] : ( INT | BOOL );
	public final Node type() throws RecognitionException {
		Node ast = null;


		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:88:9: ( INT | BOOL )
			int alt6=2;
			int LA6_0 = input.LA(1);
			if ( (LA6_0==INT) ) {
				alt6=1;
			}
			else if ( (LA6_0==BOOL) ) {
				alt6=2;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}

			switch (alt6) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:88:11: INT
					{
					match(input,INT,FOLLOW_INT_in_type501); 
					ast =new IntTypeNode();
					}
					break;
				case 2 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:89:11: BOOL
					{
					match(input,BOOL,FOLLOW_BOOL_in_type516); 
					ast =new BoolTypeNode();
					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "type"



	// $ANTLR start "exp"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:92:1: exp returns [Node ast] : f= term ( PLUS l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:93:3: (f= term ( PLUS l= term )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:93:5: f= term ( PLUS l= term )*
			{
			pushFollow(FOLLOW_term_in_exp541);
			f=term();
			state._fsp--;

			ast = f;
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:94:7: ( PLUS l= term )*
			loop7:
			while (true) {
				int alt7=2;
				int LA7_0 = input.LA(1);
				if ( (LA7_0==PLUS) ) {
					alt7=1;
				}

				switch (alt7) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:94:8: PLUS l= term
					{
					match(input,PLUS,FOLLOW_PLUS_in_exp552); 
					pushFollow(FOLLOW_term_in_exp556);
					l=term();
					state._fsp--;

					ast = new PlusNode (ast,l);
					}
					break;

				default :
					break loop7;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "exp"



	// $ANTLR start "term"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:99:1: term returns [Node ast] : f= value ( TIMES l= value )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:100:2: (f= value ( TIMES l= value )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:100:4: f= value ( TIMES l= value )*
			{
			pushFollow(FOLLOW_value_in_term594);
			f=value();
			state._fsp--;

			ast = f;
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:101:6: ( TIMES l= value )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( (LA8_0==TIMES) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:101:7: TIMES l= value
					{
					match(input,TIMES,FOLLOW_TIMES_in_term604); 
					pushFollow(FOLLOW_value_in_term608);
					l=value();
					state._fsp--;

					ast = new MultNode (ast,l);
					}
					break;

				default :
					break loop8;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "term"



	// $ANTLR start "value"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:106:1: value returns [Node ast] : f= fatt ( EQ l= fatt )* ;
	public final Node value() throws RecognitionException {
		Node ast = null;


		Node f =null;
		Node l =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:107:2: (f= fatt ( EQ l= fatt )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:107:4: f= fatt ( EQ l= fatt )*
			{
			pushFollow(FOLLOW_fatt_in_value642);
			f=fatt();
			state._fsp--;

			ast = f;
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:108:6: ( EQ l= fatt )*
			loop9:
			while (true) {
				int alt9=2;
				int LA9_0 = input.LA(1);
				if ( (LA9_0==EQ) ) {
					alt9=1;
				}

				switch (alt9) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:108:7: EQ l= fatt
					{
					match(input,EQ,FOLLOW_EQ_in_value652); 
					pushFollow(FOLLOW_fatt_in_value656);
					l=fatt();
					state._fsp--;

					ast = new EqualNode (ast,l);
					}
					break;

				default :
					break loop9;
				}
			}

			}

		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"



	// $ANTLR start "fatt"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:113:1: fatt returns [Node ast] : (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? );
	public final Node fatt() throws RecognitionException {
		Node ast = null;


		Token n=null;
		Token i=null;
		Node e =null;
		Node x =null;
		Node y =null;
		Node z =null;
		Node fa =null;
		Node a =null;

		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:114:2: (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )? )
			int alt13=7;
			switch ( input.LA(1) ) {
			case NAT:
				{
				alt13=1;
				}
				break;
			case TRUE:
				{
				alt13=2;
				}
				break;
			case FALSE:
				{
				alt13=3;
				}
				break;
			case LPAR:
				{
				alt13=4;
				}
				break;
			case IF:
				{
				alt13=5;
				}
				break;
			case PRINT:
				{
				alt13=6;
				}
				break;
			case ID:
				{
				alt13=7;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 13, 0, input);
				throw nvae;
			}
			switch (alt13) {
				case 1 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:114:4: n= NAT
					{
					n=(Token)match(input,NAT,FOLLOW_NAT_in_fatt696); 
					ast = new NatNode(Integer.parseInt((n!=null?n.getText():null)));
					}
					break;
				case 2 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:116:4: TRUE
					{
					match(input,TRUE,FOLLOW_TRUE_in_fatt711); 
					ast = new BoolNode(true);
					}
					break;
				case 3 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:118:4: FALSE
					{
					match(input,FALSE,FOLLOW_FALSE_in_fatt724); 
					ast = new BoolNode(false);
					}
					break;
				case 4 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:120:4: LPAR e= exp RPAR
					{
					match(input,LPAR,FOLLOW_LPAR_in_fatt736); 
					pushFollow(FOLLOW_exp_in_fatt740);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_fatt742); 
					ast = e;
					}
					break;
				case 5 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:122:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
					{
					match(input,IF,FOLLOW_IF_in_fatt754); 
					pushFollow(FOLLOW_exp_in_fatt758);
					x=exp();
					state._fsp--;

					match(input,THEN,FOLLOW_THEN_in_fatt760); 
					match(input,CLPAR,FOLLOW_CLPAR_in_fatt762); 
					pushFollow(FOLLOW_exp_in_fatt766);
					y=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_fatt768); 
					match(input,ELSE,FOLLOW_ELSE_in_fatt776); 
					match(input,CLPAR,FOLLOW_CLPAR_in_fatt778); 
					pushFollow(FOLLOW_exp_in_fatt782);
					z=exp();
					state._fsp--;

					match(input,CRPAR,FOLLOW_CRPAR_in_fatt784); 
					ast = new IfNode(x,y,z);
					}
					break;
				case 6 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:125:4: PRINT LPAR e= exp RPAR
					{
					match(input,PRINT,FOLLOW_PRINT_in_fatt797); 
					match(input,LPAR,FOLLOW_LPAR_in_fatt799); 
					pushFollow(FOLLOW_exp_in_fatt803);
					e=exp();
					state._fsp--;

					match(input,RPAR,FOLLOW_RPAR_in_fatt805); 
					ast = new PrintNode(e);
					}
					break;
				case 7 :
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:127:4: i= ID ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					{
					i=(Token)match(input,ID,FOLLOW_ID_in_fatt818); 
					//cercare la dichiarazione
					           int j=nestingLevel;
					           STentry entry=null; 
					           while (j>=0 && entry==null)
					             entry=(symTable.get(j--)).get((i!=null?i.getText():null));
					           if (entry==null)
					           {System.out.println("Id "+(i!=null?i.getText():null)+" at line "+(i!=null?i.getLine():0)+" not declared");
					            System.exit(0);}               
						   ast = new IdNode((i!=null?i.getText():null),entry);
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:137:5: ( LPAR (fa= exp ( COMMA a= exp )* )? RPAR )?
					int alt12=2;
					int LA12_0 = input.LA(1);
					if ( (LA12_0==LPAR) ) {
						alt12=1;
					}
					switch (alt12) {
						case 1 :
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:137:6: LPAR (fa= exp ( COMMA a= exp )* )? RPAR
							{
							match(input,LPAR,FOLLOW_LPAR_in_fatt833); 
							ArrayList<Node> argList = new ArrayList<Node>();
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:139:8: (fa= exp ( COMMA a= exp )* )?
							int alt11=2;
							int LA11_0 = input.LA(1);
							if ( (LA11_0==FALSE||(LA11_0 >= ID && LA11_0 <= IF)||(LA11_0 >= LPAR && LA11_0 <= NAT)||LA11_0==PRINT||LA11_0==TRUE) ) {
								alt11=1;
							}
							switch (alt11) {
								case 1 :
									// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:139:9: fa= exp ( COMMA a= exp )*
									{
									pushFollow(FOLLOW_exp_in_fatt853);
									fa=exp();
									state._fsp--;

									argList.add(fa);
									// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:140:9: ( COMMA a= exp )*
									loop10:
									while (true) {
										int alt10=2;
										int LA10_0 = input.LA(1);
										if ( (LA10_0==COMMA) ) {
											alt10=1;
										}

										switch (alt10) {
										case 1 :
											// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141212/FOOL/FOOL.g:140:10: COMMA a= exp
											{
											match(input,COMMA,FOLLOW_COMMA_in_fatt866); 
											pushFollow(FOLLOW_exp_in_fatt870);
											a=exp();
											state._fsp--;

											argList.add(a);
											}
											break;

										default :
											break loop10;
										}
									}

									}
									break;

							}

							ast =new CallNode((i!=null?i.getText():null),entry,argList);
							match(input,RPAR,FOLLOW_RPAR_in_fatt892); 
							}
							break;

					}

					}
					break;

			}
		}
		catch (RecognitionException re) {
			reportError(re);
			recover(input,re);
		}
		finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "fatt"

	// Delegated rules



	public static final BitSet FOLLOW_exp_in_prog30 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog32 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LET_in_prog57 = new BitSet(new long[]{0x0000000040048000L});
	public static final BitSet FOLLOW_dec_in_prog86 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_prog88 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_prog92 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_prog94 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_VAR_in_dec144 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_dec148 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_dec150 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_dec154 = new BitSet(new long[]{0x0000000000000010L});
	public static final BitSet FOLLOW_ASS_in_dec156 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_dec160 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_dec162 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_FUN_in_dec205 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_dec209 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_dec211 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_dec215 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_LPAR_in_dec247 = new BitSet(new long[]{0x0000000002010000L});
	public static final BitSet FOLLOW_ID_in_dec270 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_dec272 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_dec276 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_COMMA_in_dec317 = new BitSet(new long[]{0x0000000000010000L});
	public static final BitSet FOLLOW_ID_in_dec321 = new BitSet(new long[]{0x0000000000000080L});
	public static final BitSet FOLLOW_COLON_in_dec323 = new BitSet(new long[]{0x0000000000080020L});
	public static final BitSet FOLLOW_type_in_dec327 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_RPAR_in_dec406 = new BitSet(new long[]{0x0000000021734000L});
	public static final BitSet FOLLOW_LET_in_dec425 = new BitSet(new long[]{0x0000000040048000L});
	public static final BitSet FOLLOW_dec_in_dec429 = new BitSet(new long[]{0x0000000000040000L});
	public static final BitSet FOLLOW_IN_in_dec431 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_dec437 = new BitSet(new long[]{0x0000000004000000L});
	public static final BitSet FOLLOW_SEMIC_in_dec439 = new BitSet(new long[]{0x0000000040008002L});
	public static final BitSet FOLLOW_INT_in_type501 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_BOOL_in_type516 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_term_in_exp541 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_PLUS_in_exp552 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_term_in_exp556 = new BitSet(new long[]{0x0000000000800002L});
	public static final BitSet FOLLOW_value_in_term594 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_TIMES_in_term604 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_value_in_term608 = new BitSet(new long[]{0x0000000010000002L});
	public static final BitSet FOLLOW_fatt_in_value642 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_EQ_in_value652 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_fatt_in_value656 = new BitSet(new long[]{0x0000000000001002L});
	public static final BitSet FOLLOW_NAT_in_fatt696 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_TRUE_in_fatt711 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_FALSE_in_fatt724 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_LPAR_in_fatt736 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt740 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_fatt742 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_IF_in_fatt754 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt758 = new BitSet(new long[]{0x0000000008000000L});
	public static final BitSet FOLLOW_THEN_in_fatt760 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_fatt762 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt766 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_fatt768 = new BitSet(new long[]{0x0000000000000800L});
	public static final BitSet FOLLOW_ELSE_in_fatt776 = new BitSet(new long[]{0x0000000000000040L});
	public static final BitSet FOLLOW_CLPAR_in_fatt778 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt782 = new BitSet(new long[]{0x0000000000000400L});
	public static final BitSet FOLLOW_CRPAR_in_fatt784 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_PRINT_in_fatt797 = new BitSet(new long[]{0x0000000000200000L});
	public static final BitSet FOLLOW_LPAR_in_fatt799 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt803 = new BitSet(new long[]{0x0000000002000000L});
	public static final BitSet FOLLOW_RPAR_in_fatt805 = new BitSet(new long[]{0x0000000000000002L});
	public static final BitSet FOLLOW_ID_in_fatt818 = new BitSet(new long[]{0x0000000000200002L});
	public static final BitSet FOLLOW_LPAR_in_fatt833 = new BitSet(new long[]{0x0000000023634000L});
	public static final BitSet FOLLOW_exp_in_fatt853 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_COMMA_in_fatt866 = new BitSet(new long[]{0x0000000021634000L});
	public static final BitSet FOLLOW_exp_in_fatt870 = new BitSet(new long[]{0x0000000002000100L});
	public static final BitSet FOLLOW_RPAR_in_fatt892 = new BitSet(new long[]{0x0000000000000002L});
}
