package it.mfrancia.lpemc2014121112;

import java.util.ArrayList;

public class LetInNode extends Node {
	
	private ArrayList<Node>	dec;
	private Node			exp;
	
	public LetInNode(ArrayList<Node> d, Node e) {
		dec = d;
		exp = e;
	}
	
	public String toPrint(String s) {
		return "";
	}
	
	public Node typeCheck() {
		for (int i = 0; i < dec.size(); i++)
			(dec.get(i)).typeCheck();
		return exp.typeCheck();
	}
	
	public String codeGeneration() {
		String s = "";
		for (Node n : dec)
			s += n.codeGeneration();
		return s + exp.codeGeneration();
	}
}