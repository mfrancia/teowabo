package it.mfrancia.lpemc20141128;

public class IfNode extends MergeNode {
	
	private final Node	cond;
	private final Node	th;
	private final Node	el;
	
	public IfNode(final Node c, final Node t, final Node e) {
		super(Syskb.iff, c, t, e);
		cond = c;
		th = t;
		el = e;
	}
	
	@Override
	public String toPrint(String indent) {
		return indent + Syskb.iff + "\n" + cond.toPrint(indent + Syskb.indent) + indent + Syskb.then + "\n" + th.toPrint(indent + Syskb.indent) + "\n" + indent + Syskb.els + "\n"
				+ el.toPrint(indent + Syskb.indent);
	}
	
	@Override
	public Node typeCheck() {
		if (!FOOLlib.isSubtype(cond.typeCheck(), new BoolTypeNode().typeCheck())) {
			System.out.println("Non boolean condition in if");
			System.exit(0);
		}
		/*
		 * restituisce come valore o quello dello then o quello di else, controllo contabilità tra then e else
		 */
		Node t = th.typeCheck(), e = el.typeCheck();
		if (FOOLlib.isSubtype(t, e))
			return t;
		if (FOOLlib.isSubtype(e, t))
			return e;
		System.out.println("Incopatible types in then and else");
		System.exit(0);
		return null;
	}
	
}