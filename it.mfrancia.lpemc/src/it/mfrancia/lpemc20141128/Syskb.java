package it.mfrancia.lpemc20141128;

/**
 * Auto Generated Java Class.
 */
public class Syskb {
	public static final String	declaration	= "declaration";
	public static final String	lpar		= "LPAR";
	public static final String	rpar		= "RPAR";
	public static final String	bool		= "bool";
	public static final String	boolType	= "boolType";
	public static final String	intType		= "intType";
	public static final String	div			= "/";
	public static final String	els			= "else";
	public static final String	let			= "let";
	public static final String	in			= "in";
	public static final String	eq			= "==";
	public static final String	exp			= "exp";
	public static final String	iff			= "if ... then ... else ...";
	public static final String	indent		= "\t";
	public static final String	minus		= "-";
	public static final String	molt		= "*";
	public static final String	nat			= "N";
	public static final String	plus		= "+";
	public static final String	print		= "print";
	public static final String	prog		= "prog";
	public static final String	then		= "then";
	public static final String	times		= "mult";
}
