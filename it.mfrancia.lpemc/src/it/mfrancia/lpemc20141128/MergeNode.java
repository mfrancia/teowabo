package it.mfrancia.lpemc20141128;

public class MergeNode extends Node {
	private final String	name;
	private final Node[]	nodes;
	
	public MergeNode(final String name, final Node... nodes) {
		this.nodes = nodes;
		this.name = name;
	}
	
	@Override
	public String toPrint(final String indent) {
		String ret = indent + name + "\n" + indent + "->\n";
		for (final Node n : nodes) {
			ret += n.toPrint(indent + Syskb.indent) + "\n";
		}
		return ret;
	}

	@Override
	public Node typeCheck() {
		return null;
	}
}
