package it.mfrancia.lpemc20141128;

public class EqualNode extends MergeNode {
	
	private final Node	left;
	private final Node	right;
	
	public EqualNode(final Node l, final Node r) {
		super(Syskb.eq, l, r);
		left = l;
		right = r;
	}
	
	@Override
	public Node typeCheck() {
		Node l = left.typeCheck(), r = right.typeCheck();
		if (! (FOOLlib.isSubtype(l, r) || FOOLlib.isSubtype(r, l))) {
			System.out.println("Incopatible types in equal");
			System.exit(0);
		}
		return new BoolTypeNode();
	}
}