package it.mfrancia.lpemc20141128;

public abstract class Node {
	
	/**
	 * Restituisce la string ache rappresenta il nodo, rispetto a una certa identazione
	 * @param indent
	 * @return
	 */
	abstract public String toPrint(String indent);
	
	/**
	 * Type check secondo le regole di inferenza viste in teoria
	 * @return Tipo del nodo
	 */
	abstract public Node typeCheck();
}