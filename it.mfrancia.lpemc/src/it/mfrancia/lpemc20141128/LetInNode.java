package it.mfrancia.lpemc20141128;

import java.util.ArrayList;

public class LetInNode extends Node {
	private MergeNode dec;
	private ArrayList<Node>	declaration;
	private Node			expression;
	
	public LetInNode(final ArrayList<Node> d, final Node e) {
		this.declaration = d;
		Node[] nodes = d.toArray(new Node[d.size()]);
		this.dec = new MergeNode(Syskb.declaration, nodes);
		this.expression = e;
	}
	
	@Override
	public String toPrint(final String indent) {
		return indent + Syskb.let + "\n" + dec.toPrint(indent + Syskb.indent) + Syskb.in + "\n" + expression.toPrint(indent + Syskb.indent) ;
	}

	@Override
	public Node typeCheck() {
		for (Node n: declaration) {
			n.typeCheck();
		}
		return expression.typeCheck();
	}
	
}
