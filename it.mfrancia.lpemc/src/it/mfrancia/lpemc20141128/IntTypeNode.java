package it.mfrancia.lpemc20141128;

public class IntTypeNode extends NodePrintable {
	
	public IntTypeNode() {
		super(Syskb.intType);
	}
	
	@Override
	public Node typeCheck() {
		return IntTypeNode.this;
	}
	
}