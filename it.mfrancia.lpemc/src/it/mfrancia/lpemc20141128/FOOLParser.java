package it.mfrancia.lpemc20141128;

// $ANTLR 3.5.1 /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g 2014-11-28 15:34:05

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.runtime.BitSet;
import org.antlr.runtime.NoViableAltException;
import org.antlr.runtime.Parser;
import org.antlr.runtime.RecognitionException;
import org.antlr.runtime.RecognizerSharedState;
import org.antlr.runtime.Token;
import org.antlr.runtime.TokenStream;

@SuppressWarnings("all")
public class FOOLParser extends Parser {
	public static final String[]						tokenNames				= new String[] { "<invalid>", "<EOR>", "<DOWN>", "<UP>", "ASS", "BOOL", "CLPAR", "COLON", "COMMA", "COMMENT", "CRPAR",
			"ELSE", "EQ", "ERR", "FALSE", "FUN", "ID", "IF", "IN", "INT", "LET", "LPAR", "NAT", "PLUS", "PRINT", "RPAR", "SEMIC", "THEN", "TIMES", "TRUE", "VAR", "WHITESP" };
	public static final int								EOF						= -1;
	public static final int								ASS						= 4;
	public static final int								BOOL					= 5;
	public static final int								CLPAR					= 6;
	public static final int								COLON					= 7;
	public static final int								COMMA					= 8;
	public static final int								COMMENT					= 9;
	public static final int								CRPAR					= 10;
	public static final int								ELSE					= 11;
	public static final int								EQ						= 12;
	public static final int								ERR						= 13;
	public static final int								FALSE					= 14;
	public static final int								FUN						= 15;
	public static final int								ID						= 16;
	public static final int								IF						= 17;
	public static final int								IN						= 18;
	public static final int								INT						= 19;
	public static final int								LET						= 20;
	public static final int								LPAR					= 21;
	public static final int								NAT						= 22;
	public static final int								PLUS					= 23;
	public static final int								PRINT					= 24;
	public static final int								RPAR					= 25;
	public static final int								SEMIC					= 26;
	public static final int								THEN					= 27;
	public static final int								TIMES					= 28;
	public static final int								TRUE					= 29;
	public static final int								VAR						= 30;
	public static final int								WHITESP					= 31;
	
	private final ArrayList<HashMap<String, STentry>>	symTable				= new ArrayList<HashMap<String, STentry>>();
	
	// delegators
	
	private int											nestingLevel			= -1;
	public static final BitSet							FOLLOW_VAR_in_dec36		= new BitSet(new long[] { 0x0000000000010000L });
	
	public static final BitSet							FOLLOW_ID_in_dec40		= new BitSet(new long[] { 0x0000000000000080L });
	public static final BitSet							FOLLOW_COLON_in_dec42	= new BitSet(new long[] { 0x0000000000080020L });
	
	public static final BitSet							FOLLOW_type_in_dec46	= new BitSet(new long[] { 0x0000000000000010L });
	public static final BitSet							FOLLOW_ASS_in_dec48		= new BitSet(new long[] { 0x0000000021634000L });
	
	public static final BitSet							FOLLOW_exp_in_dec52		= new BitSet(new long[] { 0x0000000004000000L });
	
	public static final BitSet							FOLLOW_SEMIC_in_dec54	= new BitSet(new long[] { 0x0000000040008002L });
	
	public static final BitSet							FOLLOW_FUN_in_dec97		= new BitSet(new long[] { 0x0000000000010000L });
	
	public static final BitSet							FOLLOW_ID_in_dec101		= new BitSet(new long[] { 0x0000000000000080L });
	
	public static final BitSet							FOLLOW_COLON_in_dec103	= new BitSet(new long[] { 0x0000000000080020L });
	
	public static final BitSet							FOLLOW_type_in_dec107	= new BitSet(new long[] { 0x0000000000200000L });
	
	public static final BitSet							FOLLOW_LPAR_in_dec139	= new BitSet(new long[] { 0x0000000002010000L });
	
	// Delegated rules
	
	public static final BitSet							FOLLOW_ID_in_dec162		= new BitSet(new long[] { 0x0000000000000080L });
	public static final BitSet							FOLLOW_COLON_in_dec164	= new BitSet(new long[] { 0x0000000000080020L });
	public static final BitSet							FOLLOW_type_in_dec168	= new BitSet(new long[] { 0x0000000002000100L });
	public static final BitSet							FOLLOW_COMMA_in_dec209	= new BitSet(new long[] { 0x0000000000010000L });
	public static final BitSet							FOLLOW_ID_in_dec213		= new BitSet(new long[] { 0x0000000000000080L });
	public static final BitSet							FOLLOW_COLON_in_dec215	= new BitSet(new long[] { 0x0000000000080020L });
	public static final BitSet							FOLLOW_type_in_dec219	= new BitSet(new long[] { 0x0000000002000100L });
	public static final BitSet							FOLLOW_RPAR_in_dec298	= new BitSet(new long[] { 0x0000000021734000L });
	public static final BitSet							FOLLOW_LET_in_dec317	= new BitSet(new long[] { 0x0000000040048000L });
	public static final BitSet							FOLLOW_dec_in_dec321	= new BitSet(new long[] { 0x0000000000040000L });
	public static final BitSet							FOLLOW_IN_in_dec323		= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_dec329	= new BitSet(new long[] { 0x0000000004000000L });
	public static final BitSet							FOLLOW_SEMIC_in_dec331	= new BitSet(new long[] { 0x0000000040008002L });
	public static final BitSet							FOLLOW_exp_in_prog386	= new BitSet(new long[] { 0x0000000004000000L });
	public static final BitSet							FOLLOW_SEMIC_in_prog388	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_LET_in_prog413	= new BitSet(new long[] { 0x0000000040048000L });
	public static final BitSet							FOLLOW_dec_in_prog442	= new BitSet(new long[] { 0x0000000000040000L });
	public static final BitSet							FOLLOW_IN_in_prog444	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_prog448	= new BitSet(new long[] { 0x0000000004000000L });
	public static final BitSet							FOLLOW_SEMIC_in_prog450	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_INT_in_type490	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_BOOL_in_type505	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_term_in_exp530	= new BitSet(new long[] { 0x0000000000800002L });
	public static final BitSet							FOLLOW_PLUS_in_exp541	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_term_in_exp545	= new BitSet(new long[] { 0x0000000000800002L });
	public static final BitSet							FOLLOW_value_in_term583	= new BitSet(new long[] { 0x0000000010000002L });
	public static final BitSet							FOLLOW_TIMES_in_term593	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_value_in_term597	= new BitSet(new long[] { 0x0000000010000002L });
	public static final BitSet							FOLLOW_NAT_in_fatt632	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_TRUE_in_fatt647	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_FALSE_in_fatt660	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_LPAR_in_fatt672	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt676	= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet							FOLLOW_RPAR_in_fatt678	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_IF_in_fatt690	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt694	= new BitSet(new long[] { 0x0000000008000000L });
	public static final BitSet							FOLLOW_THEN_in_fatt696	= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet							FOLLOW_CLPAR_in_fatt698	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt702	= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet							FOLLOW_CRPAR_in_fatt704	= new BitSet(new long[] { 0x0000000000000800L });
	public static final BitSet							FOLLOW_ELSE_in_fatt712	= new BitSet(new long[] { 0x0000000000000040L });
	public static final BitSet							FOLLOW_CLPAR_in_fatt714	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt718	= new BitSet(new long[] { 0x0000000000000400L });
	public static final BitSet							FOLLOW_CRPAR_in_fatt720	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_PRINT_in_fatt733	= new BitSet(new long[] { 0x0000000000200000L });
	public static final BitSet							FOLLOW_LPAR_in_fatt735	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt739	= new BitSet(new long[] { 0x0000000002000000L });
	public static final BitSet							FOLLOW_RPAR_in_fatt741	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_ID_in_fatt754	= new BitSet(new long[] { 0x0000000000200002L });
	public static final BitSet							FOLLOW_LPAR_in_fatt766	= new BitSet(new long[] { 0x0000000023634000L });
	public static final BitSet							FOLLOW_exp_in_fatt776	= new BitSet(new long[] { 0x0000000002000100L });
	public static final BitSet							FOLLOW_COMMA_in_fatt780	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_exp_in_fatt784	= new BitSet(new long[] { 0x0000000002000100L });
	public static final BitSet							FOLLOW_RPAR_in_fatt798	= new BitSet(new long[] { 0x0000000000000002L });
	public static final BitSet							FOLLOW_fatt_in_value831	= new BitSet(new long[] { 0x0000000000001002L });
	public static final BitSet							FOLLOW_EQ_in_value841	= new BitSet(new long[] { 0x0000000021634000L });
	public static final BitSet							FOLLOW_fatt_in_value845	= new BitSet(new long[] { 0x0000000000001002L });
	
	public FOOLParser(final TokenStream input) {
		this(input, new RecognizerSharedState());
	}
	
	public FOOLParser(final TokenStream input, final RecognizerSharedState state) {
		super(input, state);
	}
	
	// $ANTLR start "dec"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:18:1: dec returns [ArrayList<Node> astlist] : ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t=
	// type LPAR (functionId= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* ;
	public final ArrayList<Node> dec() throws RecognitionException {
		ArrayList<Node> astlist = null;
		
		Token i = null;
		Token functionId = null;
		Token id = null;
		Node t = null;
		Node e = null;
		Node fty = null;
		Node ty = null;
		ArrayList<Node> d = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:19:2: ( ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (functionId= ID COLON
			// fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:19:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (functionId= ID COLON
			// fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
			{
				astlist = new ArrayList<Node>();
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:20:4: ( VAR i= ID COLON t= type ASS e= exp SEMIC | FUN i= ID COLON t= type LPAR (functionId= ID
				// COLON fty= type ( COMMA id= ID COLON ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC )*
				loop4: while (true) {
					int alt4 = 3;
					final int LA4_0 = input.LA(1);
					if ((LA4_0 == VAR)) {
						alt4 = 1;
					} else if ((LA4_0 == FUN)) {
						alt4 = 2;
					}
					
					switch (alt4) {
						case 1:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:20:6: VAR i= ID COLON t= type ASS e= exp SEMIC
						{
							match(input, VAR, FOLLOW_VAR_in_dec36);
							i = (Token) match(input, ID, FOLLOW_ID_in_dec40);
							match(input, COLON, FOLLOW_COLON_in_dec42);
							pushFollow(FOLLOW_type_in_dec46);
							t = type();
							state._fsp--;
							
							match(input, ASS, FOLLOW_ASS_in_dec48);
							pushFollow(FOLLOW_exp_in_dec52);
							e = exp();
							state._fsp--;
							
							match(input, SEMIC, FOLLOW_SEMIC_in_dec54);
							
							final VarNode v = new VarNode((i != null ? i.getText() : null), t, e);
							astlist.add(v);
							final HashMap<String, STentry> hm = symTable.get(nestingLevel);
							/* qui ho informazione sul tipo della STEntry? Sì, ce l'ho già. Con $ entro dentro un record, e poi accedo al campo che mi interessa */
							if (hm.put((i != null ? i.getText() : null), new STentry(v, nestingLevel, t)) != null) {
								System.out.println("Var id " + (i != null ? i.getText() : null) + " at line " + (i != null ? i.getLine() : 0) + " already declared");
								System.exit(0);
							}
							
						}
							break;
						case 2:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:32:13: FUN i= ID COLON t= type LPAR (functionId= ID COLON fty= type ( COMMA id= ID COLON
						// ty= type )* )? RPAR ( LET d= dec IN )? e= exp SEMIC
						{
							match(input, FUN, FOLLOW_FUN_in_dec97);
							i = (Token) match(input, ID, FOLLOW_ID_in_dec101);
							match(input, COLON, FOLLOW_COLON_in_dec103);
							pushFollow(FOLLOW_type_in_dec107);
							t = type();
							state._fsp--;
							
							// inserimento di ID nella symtable
							final FunNode f = new FunNode((i != null ? i.getText() : null), t);
							astlist.add(f);
							final HashMap<String, STentry> hm = symTable.get(nestingLevel);
							/*
							 * STentry relativa alla funzione, il tipo che abbiamo qui è quello di ritorno della funzione, i parametri arrivano dopo, questo è il punto in cui usiamo addType, creo la
							 * STEntry e poi arricchisco il campo che in questo momento non conosciamo
							 */
							final STentry entry = new STentry(f, nestingLevel);
							if (hm.put((i != null ? i.getText() : null), entry) != null) {
								System.out.println("Fun id " + (i != null ? i.getText() : null) + " at line " + (i != null ? i.getLine() : 0) + " already declared");
								System.exit(0);
							}
							// creare una nuova hashmap per la symTable
							nestingLevel++;
							final HashMap<String, STentry> hmn = new HashMap<String, STentry>();
							symTable.add(hmn);
							
							match(input, LPAR, FOLLOW_LPAR_in_dec139);
							final ArrayList<Node> parTypes = new ArrayList<Node>(); /* qui inserisco le informazioni sui parametri */
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:49:17: (functionId= ID COLON fty= type ( COMMA id= ID COLON ty= type )* )?
							int alt2 = 2;
							final int LA2_0 = input.LA(1);
							if ((LA2_0 == ID)) {
								alt2 = 1;
							}
							switch (alt2) {
								case 1:
								// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:49:18: functionId= ID COLON fty= type ( COMMA id= ID COLON ty= type )*
								{
									functionId = (Token) match(input, ID, FOLLOW_ID_in_dec162);
									match(input, COLON, FOLLOW_COLON_in_dec164);
									pushFollow(FOLLOW_type_in_dec168);
									fty = type();
									state._fsp--;
									
									parTypes.add(fty);
									final ParNode fpar = new ParNode((functionId != null ? functionId.getText() : null), fty);
									f.addPar(fpar);
									if (hmn.put((functionId != null ? functionId.getText() : null), new STentry(fpar, nestingLevel)) != null) {
										System.out.println("Parameter id " + (functionId != null ? functionId.getText() : null) + " at line " + (functionId != null ? functionId.getLine() : 0)
												+ " already declared");
										System.exit(0);
									}
									
									// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:59:19: ( COMMA id= ID COLON ty= type )*
									loop1: while (true) {
										int alt1 = 2;
										final int LA1_0 = input.LA(1);
										if ((LA1_0 == COMMA)) {
											alt1 = 1;
										}
										
										switch (alt1) {
											case 1:
											// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:59:20: COMMA id= ID COLON ty= type
											{
												match(input, COMMA, FOLLOW_COMMA_in_dec209);
												id = (Token) match(input, ID, FOLLOW_ID_in_dec213);
												match(input, COLON, FOLLOW_COLON_in_dec215);
												pushFollow(FOLLOW_type_in_dec219);
												ty = type();
												state._fsp--;
												
												parTypes.add(fty);
												final ParNode par = new ParNode((id != null ? id.getText() : null), ty);
												f.addPar(par);
												if (hmn.put((id != null ? id.getText() : null), new STentry(par, nestingLevel)) != null) {
													System.out.println("Parameter id " + (id != null ? id.getText() : null) + " at line " + (id != null ? id.getLine() : 0) + " already declared");
													System.exit(0);
												}
												
											}
												break;
											
											default:
												break loop1;
										}
									}
									
								}
									break;
							
							}
							
							match(input, RPAR, FOLLOW_RPAR_in_dec298);
							entry.addType(/* ... */new ArrowTypeNode(parTypes, t));
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:71:15: ( LET d= dec IN )?
							int alt3 = 2;
							final int LA3_0 = input.LA(1);
							if ((LA3_0 == LET)) {
								alt3 = 1;
							}
							switch (alt3) {
								case 1:
								// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:71:16: LET d= dec IN
								{
									match(input, LET, FOLLOW_LET_in_dec317);
									pushFollow(FOLLOW_dec_in_dec321);
									d = dec();
									state._fsp--;
									
									match(input, IN, FOLLOW_IN_in_dec323);
								}
									break;
							
							}
							
							pushFollow(FOLLOW_exp_in_dec329);
							e = exp();
							state._fsp--;
							
							match(input, SEMIC, FOLLOW_SEMIC_in_dec331);
							
							// chiudere scope
							symTable.remove(nestingLevel--);
							f.addDecBody(d, e);
							
						}
							break;
						
						default:
							break loop4;
					}
				}
				
			}
			
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return astlist;
	}
	
	// $ANTLR end "dec"
	// $ANTLR start "exp"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:98:1: exp returns [Node ast] : f= term ( PLUS l= term )* ;
	public final Node exp() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:99:3: (f= term ( PLUS l= term )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:99:5: f= term ( PLUS l= term )*
			{
				pushFollow(FOLLOW_term_in_exp530);
				f = term();
				state._fsp--;
				
				ast = f;
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:100:7: ( PLUS l= term )*
				loop7: while (true) {
					int alt7 = 2;
					final int LA7_0 = input.LA(1);
					if ((LA7_0 == PLUS)) {
						alt7 = 1;
					}
					
					switch (alt7) {
						case 1:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:100:8: PLUS l= term
						{
							match(input, PLUS, FOLLOW_PLUS_in_exp541);
							pushFollow(FOLLOW_term_in_exp545);
							l = term();
							state._fsp--;
							
							ast = new PlusNode(ast, l);
						}
							break;
						
						default:
							break loop7;
					}
				}
				
			}
			
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "exp"
	// $ANTLR start "fatt"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:113:1: fatt returns [Node ast] : (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR
	// ELSE CLPAR z= exp CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (firstArgument= exp ( COMMA argument= exp )* )? RPAR )? );
	public final Node fatt() throws RecognitionException {
		Node ast = null;
		
		Token n = null;
		Token i = null;
		Node e = null;
		Node x = null;
		Node y = null;
		Node z = null;
		Node firstArgument = null;
		Node argument = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:114:2: (n= NAT | TRUE | FALSE | LPAR e= exp RPAR | IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp
			// CRPAR | PRINT LPAR e= exp RPAR |i= ID ( LPAR (firstArgument= exp ( COMMA argument= exp )* )? RPAR )? )
			int alt12 = 7;
			switch (input.LA(1)) {
				case NAT: {
					alt12 = 1;
				}
					break;
				case TRUE: {
					alt12 = 2;
				}
					break;
				case FALSE: {
					alt12 = 3;
				}
					break;
				case LPAR: {
					alt12 = 4;
				}
					break;
				case IF: {
					alt12 = 5;
				}
					break;
				case PRINT: {
					alt12 = 6;
				}
					break;
				case ID: {
					alt12 = 7;
				}
					break;
				default:
					final NoViableAltException nvae = new NoViableAltException("", 12, 0, input);
					throw nvae;
			}
			switch (alt12) {
				case 1:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:114:4: n= NAT
				{
					n = (Token) match(input, NAT, FOLLOW_NAT_in_fatt632);
					ast = new NatNode(Integer.parseInt((n != null ? n.getText() : null)));
				}
				break;
				case 2:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:116:4: TRUE
				{
					match(input, TRUE, FOLLOW_TRUE_in_fatt647);
					ast = new BoolNode(true);
				}
				break;
				case 3:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:118:4: FALSE
				{
					match(input, FALSE, FOLLOW_FALSE_in_fatt660);
					ast = new BoolNode(false);
				}
				break;
				case 4:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:120:4: LPAR e= exp RPAR
				{
					match(input, LPAR, FOLLOW_LPAR_in_fatt672);
					pushFollow(FOLLOW_exp_in_fatt676);
					e = exp();
					state._fsp--;
					
					match(input, RPAR, FOLLOW_RPAR_in_fatt678);
					ast = e;
				}
				break;
				case 5:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:122:4: IF x= exp THEN CLPAR y= exp CRPAR ELSE CLPAR z= exp CRPAR
				{
					match(input, IF, FOLLOW_IF_in_fatt690);
					pushFollow(FOLLOW_exp_in_fatt694);
					x = exp();
					state._fsp--;
					
					match(input, THEN, FOLLOW_THEN_in_fatt696);
					match(input, CLPAR, FOLLOW_CLPAR_in_fatt698);
					pushFollow(FOLLOW_exp_in_fatt702);
					y = exp();
					state._fsp--;
					
					match(input, CRPAR, FOLLOW_CRPAR_in_fatt704);
					match(input, ELSE, FOLLOW_ELSE_in_fatt712);
					match(input, CLPAR, FOLLOW_CLPAR_in_fatt714);
					pushFollow(FOLLOW_exp_in_fatt718);
					z = exp();
					state._fsp--;
					
					match(input, CRPAR, FOLLOW_CRPAR_in_fatt720);
					ast = new IfNode(x, y, z);
				}
				break;
				case 6:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:125:4: PRINT LPAR e= exp RPAR
				{
					match(input, PRINT, FOLLOW_PRINT_in_fatt733);
					match(input, LPAR, FOLLOW_LPAR_in_fatt735);
					pushFollow(FOLLOW_exp_in_fatt739);
					e = exp();
					state._fsp--;
					
					match(input, RPAR, FOLLOW_RPAR_in_fatt741);
					ast = new PrintNode(e);
				}
				break;
				case 7:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:127:4: i= ID ( LPAR (firstArgument= exp ( COMMA argument= exp )* )? RPAR )?
				{
					i = (Token) match(input, ID, FOLLOW_ID_in_fatt754);
					// cercare la dichiarazione
					int j = nestingLevel;
					STentry entry = null;
					while ((j >= 0) && (entry == null)) {
						entry = (symTable.get(j--)).get((i != null ? i.getText() : null));
					}
					if (entry == null) {
						System.out.println("Id " + (i != null ? i.getText() : null) + " at line " + (i != null ? i.getLine() : 0) + " not declared");
						System.exit(0);
					}
					ast = new IdNode((i != null ? i.getText() : null), entry);
					
					// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:139:3: ( LPAR (firstArgument= exp ( COMMA argument= exp )* )? RPAR )?
					int alt11 = 2;
					final int LA11_0 = input.LA(1);
					if ((LA11_0 == LPAR)) {
						alt11 = 1;
					}
					switch (alt11) {
						case 1:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:139:4: LPAR (firstArgument= exp ( COMMA argument= exp )* )? RPAR
						{
							match(input, LPAR, FOLLOW_LPAR_in_fatt766);
							
							final ArrayList<Node> argList = new ArrayList<Node>();
							/* lista che contiene i miei parametri */
							
							// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:143:5: (firstArgument= exp ( COMMA argument= exp )* )?
							int alt10 = 2;
							final int LA10_0 = input.LA(1);
							if (((LA10_0 == FALSE) || ((LA10_0 >= ID) && (LA10_0 <= IF)) || ((LA10_0 >= LPAR) && (LA10_0 <= NAT)) || (LA10_0 == PRINT) || (LA10_0 == TRUE))) {
								alt10 = 1;
							}
							switch (alt10) {
								case 1:
								// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:143:6: firstArgument= exp ( COMMA argument= exp )*
								{
									pushFollow(FOLLOW_exp_in_fatt776);
									firstArgument = exp();
									state._fsp--;
									
									argList.add(firstArgument);
									// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:143:58: ( COMMA argument= exp )*
									loop9: while (true) {
											int alt9 = 2;
											final int LA9_0 = input.LA(1);
											if ((LA9_0 == COMMA)) {
												alt9 = 1;
											}
										
										switch (alt9) {
											case 1:
													// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:143:59: COMMA argument= exp
												{
												match(input, COMMA, FOLLOW_COMMA_in_fatt780);
												pushFollow(FOLLOW_exp_in_fatt784);
												argument = exp();
												state._fsp--;
												
												argList.add(argument);
												}
												break;
											
											default:
												break loop9;
											}
										}
									
								}
								break;
							
							}
							
							ast = new CallNode(/* ... parametri attuali */(i != null ? i.getText() : null), entry, argList);
							/* nodo che rappresenta la funzione con parametri attuali, quale è il modo più naturale? */
							
							match(input, RPAR, FOLLOW_RPAR_in_fatt798);
						}
						break;
					
					}
					
				}
				break;
			
			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "fatt"
	// delegates
	public Parser[] getDelegates() {
		return new Parser[] {};
	}
	
	@Override
	public String getGrammarFileName() {
		return "/home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g";
	}
	
	@Override
	public String[] getTokenNames() {
		return FOOLParser.tokenNames;
	}
	
	// $ANTLR start "prog"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:79:1: prog returns [Node ast] : (e= exp SEMIC | LET d= dec IN e= exp SEMIC );
	public final Node prog() throws RecognitionException {
		Node ast = null;
		
		Node e = null;
		ArrayList<Node> d = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:80:2: (e= exp SEMIC | LET d= dec IN e= exp SEMIC )
			int alt5 = 2;
			final int LA5_0 = input.LA(1);
			if (((LA5_0 == FALSE) || ((LA5_0 >= ID) && (LA5_0 <= IF)) || ((LA5_0 >= LPAR) && (LA5_0 <= NAT)) || (LA5_0 == PRINT) || (LA5_0 == TRUE))) {
				alt5 = 1;
			} else if ((LA5_0 == LET)) {
				alt5 = 2;
			}
			
			else {
				final NoViableAltException nvae = new NoViableAltException("", 5, 0, input);
				throw nvae;
			}
			
			switch (alt5) {
				case 1:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:80:4: e= exp SEMIC
				{
					pushFollow(FOLLOW_exp_in_prog386);
					e = exp();
					state._fsp--;
					
					match(input, SEMIC, FOLLOW_SEMIC_in_prog388);
					ast = new ProgNode(e);
				}
				break;
				case 2:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:82:11: LET d= dec IN e= exp SEMIC
				{
					match(input, LET, FOLLOW_LET_in_prog413);
					nestingLevel++;
					final HashMap<String, STentry> hm = new HashMap<String, STentry>();
					symTable.add(hm);
					
					pushFollow(FOLLOW_dec_in_prog442);
					d = dec();
					state._fsp--;
					
					match(input, IN, FOLLOW_IN_in_prog444);
					pushFollow(FOLLOW_exp_in_prog448);
					e = exp();
					state._fsp--;
					
					match(input, SEMIC, FOLLOW_SEMIC_in_prog450);
					symTable.remove(nestingLevel--);
					ast = new LetInNode(d, e);
				}
				break;
			
			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "prog"
	// $ANTLR start "term"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:105:1: term returns [Node ast] : f= value ( TIMES l= value )* ;
	public final Node term() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:106:2: (f= value ( TIMES l= value )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:106:4: f= value ( TIMES l= value )*
			{
				pushFollow(FOLLOW_value_in_term583);
				f = value();
				state._fsp--;
				
				ast = f;
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:107:6: ( TIMES l= value )*
				loop8: while (true) {
					int alt8 = 2;
					final int LA8_0 = input.LA(1);
					if ((LA8_0 == TIMES)) {
						alt8 = 1;
					}
					
					switch (alt8) {
						case 1:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:107:7: TIMES l= value
						{
							match(input, TIMES, FOLLOW_TIMES_in_term593);
							pushFollow(FOLLOW_value_in_term597);
							l = value();
							state._fsp--;
							
							ast = new MultNode(ast, l);
						}
							break;
						
						default:
							break loop8;
					}
				}
				
			}
			
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "term"
	// $ANTLR start "type"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:93:1: type returns [Node ast] : ( INT | BOOL );
	public final Node type() throws RecognitionException {
		Node ast = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:94:9: ( INT | BOOL )
			int alt6 = 2;
			final int LA6_0 = input.LA(1);
			if ((LA6_0 == INT)) {
				alt6 = 1;
			} else if ((LA6_0 == BOOL)) {
				alt6 = 2;
			}
			
			else {
				final NoViableAltException nvae = new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			
			switch (alt6) {
				case 1:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:94:11: INT
				{
					match(input, INT, FOLLOW_INT_in_type490);
					ast = new IntTypeNode();
				}
				break;
				case 2:
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:95:11: BOOL
				{
					match(input, BOOL, FOLLOW_BOOL_in_type505);
					ast = new BoolTypeNode();
				}
				break;
			
			}
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	
	// $ANTLR end "type"
	// $ANTLR start "value"
	// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:158:1: value returns [Node ast] : f= fatt ( EQ l= fatt )* ;
	public final Node value() throws RecognitionException {
		Node ast = null;
		
		Node f = null;
		Node l = null;
		
		try {
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:159:2: (f= fatt ( EQ l= fatt )* )
			// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:159:4: f= fatt ( EQ l= fatt )*
			{
				pushFollow(FOLLOW_fatt_in_value831);
				f = fatt();
				state._fsp--;
				
				ast = f;
				// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:160:6: ( EQ l= fatt )*
				loop13: while (true) {
					int alt13 = 2;
					final int LA13_0 = input.LA(1);
					if ((LA13_0 == EQ)) {
						alt13 = 1;
					}
					
					switch (alt13) {
						case 1:
						// /home/w4bo/Dropbox/Universita/Magistrale/LPeMC/Esercitazioni/20141128/FOOL/FOOL.g:160:7: EQ l= fatt
						{
							match(input, EQ, FOLLOW_EQ_in_value841);
							pushFollow(FOLLOW_fatt_in_value845);
							l = fatt();
							state._fsp--;
							
							ast = new EqualNode(ast, l);
						}
							break;
						
						default:
							break loop13;
					}
				}
				
			}
			
		} catch (final RecognitionException re) {
			reportError(re);
			recover(input, re);
		} finally {
			// do for sure before leaving
		}
		return ast;
	}
	// $ANTLR end "value"
}
