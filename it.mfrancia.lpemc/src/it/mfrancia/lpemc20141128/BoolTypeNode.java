package it.mfrancia.lpemc20141128;

public class BoolTypeNode extends NodePrintable {
	
	public BoolTypeNode() {
		super(Syskb.boolType);
	}
	
	@Override
	public Node typeCheck() {
		return BoolTypeNode.this;
	}
	
}