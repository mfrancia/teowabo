package it.mfrancia.lpemc20141128;

public class MultNode extends MergeNode {
	
	private final Node	left;
	private final Node	right;
	
	public MultNode(final Node l, final Node r) {
		super(Syskb.times, l, r);
		left = l;
		right = r;
	}
	
	@Override
	public Node typeCheck() {
		if (!FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode())) {
			System.out.println("Non integer in mult");
			System.exit(0);
		}
		return new IntTypeNode();
	}
	
}