package it.mfrancia.lpemc20141128;

public class VarNode extends NodePrintable {
	private String	id;
	private Node	type, value;
	
	public VarNode(final String i, final Node t, final Node v) {
		super(i,t,v);
		this.id = i;
		this.type = t;
		this.value = v;
	}
	
	
	@Override
	public Node typeCheck() {
		Node t = type.typeCheck();
		if (FOOLlib.isSubtype(t, value.typeCheck()))
			return t;
		System.out.println("Incopatible types in type and value of variable " + id);
		System.exit(0);
		return null;
	}
	
}