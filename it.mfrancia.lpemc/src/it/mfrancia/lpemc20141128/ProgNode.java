package it.mfrancia.lpemc20141128;

public class ProgNode extends Node {
	
	private final Node	exp;
	
	public ProgNode(final Node e) {
		exp = e;
	}
	
	@Override
	public String toPrint(final String s) {		
		return "Prog\n" + exp.toPrint("  ");
	}
	
	@Override
	public Node typeCheck() {
		return exp.typeCheck();
	}
	
}