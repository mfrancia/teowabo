package it.mfrancia.lpemc20141128;

public class PlusNode extends MergeNode {
	
	private final Node	left;
	private final Node	right;
	
	public PlusNode(final Node l, final Node r) {
		super(Syskb.plus, l, r);
		left = l;
		right = r;
	}

	@Override
	public Node typeCheck() {
		if (!FOOLlib.isSubtype(left.typeCheck(), new IntTypeNode()) && FOOLlib.isSubtype(right.typeCheck(), new IntTypeNode())) {
			System.out.println("Non integer in sum");
			System.exit(0);
		}
		return new IntTypeNode();
	}
}