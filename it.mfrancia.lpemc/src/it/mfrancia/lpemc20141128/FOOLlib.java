package it.mfrancia.lpemc20141128;

public class FOOLlib {
	/**
	 * Return true if the type of n2 is a subtype of n1, false otherwise
	 * @param n1 first node
	 * @param n2 second node
	 * @return
	 */
	public static boolean isSubtype(Node n1, Node n2) {
		return n1.toPrint("").equals(n2.toPrint(""));		
	}
}
