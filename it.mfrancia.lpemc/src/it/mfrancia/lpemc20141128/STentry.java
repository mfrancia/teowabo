package it.mfrancia.lpemc20141128;

public class STentry {
	private Node d;
	private int nestingLevel;
	private Node type;
	public STentry(final Node d, final int nestingLevel) {
		this(d, nestingLevel, null);
	}
	
	/* nuovo costrutture se conosco già il tipo dell'identificatore */
	public STentry(final Node d, final int nestingLevel, final Node type) {
		addType(type);
		this.d = d;
		this.nestingLevel = nestingLevel;
	}
	
	public void addType(final Node type) {
		this.type = type;
	}

	public Node getType() {
		return this.type;
	}
}