package it.mfrancia.lpemc20141128;

public class PrintNode extends MergeNode {
	
	private Node	val;
	
	public PrintNode(final Node v) {
		super(Syskb.print, v);
		this.val = v;
	}
	
	@Override
	public Node typeCheck() {
		return val.typeCheck();
	}
	
}