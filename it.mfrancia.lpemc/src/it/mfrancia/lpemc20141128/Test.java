package it.mfrancia.lpemc20141128;
import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;

public class Test {
	public static void main(final String[] args) throws Exception {
		
		final String fileName = "prova.fool";
		
		final ANTLRFileStream input = new ANTLRFileStream(fileName);
		final FOOLLexer lexer = new FOOLLexer(input);
		final CommonTokenStream tokens = new CommonTokenStream(lexer);
		final FOOLParser parser = new FOOLParser(tokens);
		
		final Node ast = parser.prog();
		
		System.out.print(ast.toPrint(""));
		
		System.out.print(ast.typeCheck().toPrint(""));
		
	}
}
