package it.mfrancia.lpemc20141128;

public class BoolNode extends NodePrintable {
	
	public BoolNode(final boolean value) {
		super(new BoolTypeNode(), value);
	}
	
}