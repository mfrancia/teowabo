package it.mfrancia.lpemc20141128;

public class NodePrintable extends Node {
	private String	name;
	private Object	content;
	private Node	type;
	
	public NodePrintable(String name, Node type, Object content) {
		this.type = type;
		this.name = name;
		this.content = content;
	}
	
	public NodePrintable(Node type, Object content) {
		this(null, type, content);
	}
	
	public NodePrintable(Object content) {
		this(null, null, content);
	}
	
	@Override
	public String toPrint(String indent) {
		return indent + (name != null ? name + ":" : "") + (type != null ? type.typeCheck().toPrint("") + " ": "") + content.toString();
	}
	
	@Override
	public Node typeCheck() {
		return type;
	}
	
}
