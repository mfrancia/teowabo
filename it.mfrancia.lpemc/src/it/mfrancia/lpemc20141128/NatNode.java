package it.mfrancia.lpemc20141128;

public class NatNode extends NodePrintable {
	
	public NatNode(final Integer value) {
		super(new IntTypeNode(), value.intValue());
	}
}