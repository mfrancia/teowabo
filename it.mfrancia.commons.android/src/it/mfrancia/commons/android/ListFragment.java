package it.mfrancia.commons.android;

import it.mfrancia.commons.android.interfaces.IListItem;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

/**
 * Fragment which handle a list view and its adapter
 *
 * @author Matteo Francia
 *
 * @param <T>
 *            Data type
 */
public class ListFragment<T> extends MyFragment {
	private CustomAdapter<T>	adapter;
	private int					layout;
	
	public ListFragment() {
	}
	
	public ListFragment(int layoutId) {
		this.layout = layoutId;
	}
	
	/**
	 * notify data change to the adapter
	 */
	public void notifyDataChange() {
		if (adapter != null) {
			adapter.notifyDataSetChanged();
		}
	}
	
	@Override
	public View onCreate(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		final ViewGroup rootView = (ViewGroup) inflater.inflate(layout, container, false);
		// set the adpter to the list view
		final ListView list = (ListView) rootView.findViewById(R.id.listView1);
		if (list != null) {
			list.setAdapter(adapter);
			list.setClickable(true);
			list.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(final AdapterView<?> parent, final View view, final int position, final long id) {
					if (getActionListener() != null) {
						getActionListener().onItemSelected(new IListItem() {
							
							@Override
							public Object getContent() {
								return adapter.getItem(position);
							}
							
							@Override
							public int getPosition() {
								return position;
							}
						});
					}
				}
			});
			list.setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
		}
		return rootView;
	}
	
	/**
	 * Set the list view adapter
	 *
	 * @param adapter
	 */
	public void setAdapter(final CustomAdapter<T> adapter) {
		this.adapter = adapter;
	}
	
	/**
	 * set fragment layout
	 *
	 * @param id
	 */
	public void setLayout(final int id) {
		layout = id;
	}
}
