package it.mfrancia.commons.android;

import it.mfrancia.commons.android.interfaces.IActionListener;
import it.mfrancia.commons.android.interfaces.IViewLoadedListener;


/**
 * Fragment factory
 *
 * @author Matteo Francia
 *
 */
public class MyFragmentBuilderFactory {
	/**
	 * create a fragment with a list view
	 *
	 * @param menuId
	 *            fragment menu id
	 * @param layoutId
	 *            fragment layout id
	 * @param adapter
	 *            list view adapter
	 * @param listener
	 *            action button listener
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static MyFragment createListFragment(final int menuId, final int layoutId, final CustomAdapter adapter, final IActionListener listener) {
		return createListFragment(null, menuId, layoutId, adapter, listener);
	}

	/**
	 * create a fragment with a list view
	 *
	 * @param fragmentId
	 *            fragment (user friendly) id
	 * @param menuId
	 *            fragment menu id
	 * @param layoutId
	 *            fragment layout id
	 * @param adapter
	 *            list view adapter
	 * @param listener
	 *            action button listener
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MyFragment createListFragment(final String fragmentId, final Integer menuId, final int layoutId, final CustomAdapter adapter, final IActionListener listener) {
		final ListFragment<String> f = new ListFragment<String>();
		f.setFragmentId(fragmentId);
		f.setMenu(menuId);
		f.setLayout(layoutId);
		f.setAdapter(adapter);
		f.setActionListener(listener);
		f.notifyDataChange();
		return f;
	}
	
	/**
	 * create a fragment with a list view
	 *
	 * @param fragmentId
	 *            fragment (user friendly) id
	 * @param fragmentId
	 *            fragment title
	 * @param menuId
	 *            fragment menu id
	 * @param layoutId
	 *            fragment layout id
	 * @param adapter
	 *            {@link CustomAdapter} list view adapter
	 * @param listener
	 *            {@link IActionListener} action button listener
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MyFragment createListFragment(final String fragmentId, final String title, final Integer menuId, final int layoutId, final CustomAdapter adapter, final IActionListener listener) {
		final ListFragment<String> f = new ListFragment<String>();
		f.setFragmentId(fragmentId);
		f.setMenu(menuId);
		f.setLayout(layoutId);
		f.setAdapter(adapter);
		f.setActionListener(listener);
		f.setTitle(title);
		f.notifyDataChange();
		return f;
	}
	
	/**
	 * create a fragment with a list view
	 *
	 * @param fragmentId
	 *            fragment (user friendly) id
	 * @param fragmentId
	 *            fragment title
	 * @param menuId
	 *            fragment menu id
	 * @param layoutId
	 *            fragment layout id
	 * @param adapter
	 *            {@link CustomAdapter} list view adapter
	 * @param listener
	 *            {@link IActionListener} action button listener
	 * @param viewLoadedListener
	 *            {@link IViewLoadedListener} view loaded listener
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static MyFragment createListFragment(final String fragmentId, final String title, final Integer menuId, final int layoutId, final CustomAdapter adapter, final IActionListener listener, final IViewLoadedListener viewLoadedListener) {
		final ListFragment<String> f = new ListFragment<String>();
		f.setFragmentId(fragmentId);
		f.setMenu(menuId);
		f.setLayout(layoutId);
		f.setAdapter(adapter);
		f.setActionListener(listener);
		f.setTitle(title);
		f.notifyDataChange();
		f.setOnViewLoadedListener(viewLoadedListener);
		return f;
	}
}
