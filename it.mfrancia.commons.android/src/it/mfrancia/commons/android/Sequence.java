package it.mfrancia.commons.android;

import it.mfrancia.commons.android.interfaces.ISequence;

import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

/**
 * A generic sequence
 *
 * @author Matteo Francia
 *
 * @param <T>
 *            Sequence item type
 */
public class Sequence<T> implements ISequence<T> {
	private final LinkedList<T> list;
	private final ListIterator<T> listIt;

	public Sequence() {
		this.list = new LinkedList<T>();
		listIt = list.listIterator();
	}

	public Sequence(final List<T> list) {
		this.list = new LinkedList<T>();
		for (final T t : list) {
			this.list.add(t);
		}
		listIt = list.listIterator();
	}

	public Sequence(final T... l) {
		this.list = new LinkedList<T>();
		for (final T t : l) {
			this.list.add(t);
		}
		listIt = list.listIterator();
	}

	@Override
	public void add(final T item) {
		list.add(item);
	}

	@Override
	public T getLast() {
		return list.size() > 0 ? list.getLast() : null;
	}

	@Override
	public T getNext() {
		if (listIt.hasNext()) {
			return listIt.next();
		}
		return null;
	}

	@Override
	public T getPrev() {
		if (listIt.hasPrevious()) {
			return listIt.previous();
		}
		return null;
	}

	@Override
	public T getRoot() {
		return list.getFirst();
	}

	@Override
	public void removeLast() {
		if (!list.isEmpty()) {
			list.removeLast();
		}
	}

}
