package it.mfrancia.commons.android;

import java.util.ArrayList;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * A simple pager adapter that represents 5 ScreenSlidePageFragment objects, in sequence.
 *
 * @author Matteo Francia
 */
public class ScreenPagerAdapter extends FragmentStatePagerAdapter {

	private final ArrayList<MyFragment> fragments;

	public ScreenPagerAdapter(final FragmentManager fragmentManager, final ArrayList<MyFragment> fragments) {
		super(fragmentManager);
		this.fragments = fragments;
	}

	@Override
	public int getCount() {
		if (fragments != null) {
			return fragments.size();
		}
		return 0;
	}

	@Override
	public Fragment getItem(final int position) {
		if (fragments != null) {
			return fragments.get(position);
		}
		return null;
	}
}
