package it.mfrancia.commons.android;

import android.app.Activity;
import android.widget.Toast;

/**
 * MyToast, execute an android Toast into UIThread
 * 
 * @author Matteo Francia
 *
 */
public class MyToast {
	public static void show(final Activity activity, final String text) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
			}
		});
		
	}
}
