package it.mfrancia.commons.android.interfaces;

import it.mfrancia.commons.android.MyFragment;


/**
 * ITutorial interface
 *
 * @author Matteo Francia
 *
 */
public interface ITutorial {
	/**
	 * Show the tutorial of the specified fragment
	 * 
	 * @param fragment
	 */
	void show(MyFragment fragment);
}
