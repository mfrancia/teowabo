package it.mfrancia.commons.android.interfaces;

public interface IListItem {
	Object getContent();
	
	int getPosition();
}
