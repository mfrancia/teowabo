package it.mfrancia.commons.android.interfaces;

import android.view.View;

/**
 * View loaded call-back
 *
 * @author Matteo Francia
 *
 */
public interface IViewLoadedListener {
	/**
	 * On view loaded call-back
	 *
	 * @param id
	 *            human friendly id
	 * @param v
	 *            reference to the loaded view
	 */
	void onViewLoaded(String id, View v);
}
