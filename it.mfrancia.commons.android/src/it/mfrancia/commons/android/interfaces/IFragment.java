package it.mfrancia.commons.android.interfaces;

/**
 * Generic fragment with an ID
 *
 * @author Matteo Francia
 *
 */
public interface IFragment {
	/**
	 * Get the {@link IActionListener} listener
	 *
	 * @return
	 */
	IActionListener getActionListener();

	/**
	 * Get the fragment user specified id
	 *
	 * @return
	 */
	String getFragmentId();

	/**
	 * Get the menu id
	 *
	 * @return
	 */
	Integer getMenu();

	/**
	 * Get the {@link IViewLoadedListener} listener
	 *
	 * @return
	 */
	IViewLoadedListener getOnViewLoadedListener();

	/**
	 * Get the fragment title
	 *
	 * @return
	 */
	String getTitle();

	/**
	 * Set the {@link IActionListener} listener
	 *
	 * @param listener
	 *            reference to the listener
	 */
	void setActionListener(IActionListener listener);

	/**
	 * Set a human friendly id for the fragment
	 *
	 * @param id
	 */
	void setFragmentId(String id);

	/**
	 * Set the menu to be shown in the action bar
	 *
	 * @param menuId
	 *            id of the menu
	 */
	void setMenu(Integer menuId);

	/**
	 * Set the {@link IViewLoadedListener} listener
	 *
	 * @param listener
	 *            reference to the listener
	 */
	void setOnViewLoadedListener(IViewLoadedListener listener);

	/**
	 * Set the title to be shown in the action bar
	 *
	 * @param title
	 */
	void setTitle(String title);
}
