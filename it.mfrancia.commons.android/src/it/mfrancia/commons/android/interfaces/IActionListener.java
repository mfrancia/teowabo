package it.mfrancia.commons.android.interfaces;

import android.view.MenuItem;

/**
 * Generic fragment action listener
 *
 * @author Matteo Francia
 *
 */
public interface IActionListener {
	/**
	 * call back invoked when a menu item is pressed
	 *
	 * @param item
	 */
	void onActionButtonPressed(MenuItem item);

	/**
	 * call back invoked when an item is selected (e.g. an item is selected from a list view)
	 *
	 * @param item
	 */
	void onItemSelected(IListItem item);
}
