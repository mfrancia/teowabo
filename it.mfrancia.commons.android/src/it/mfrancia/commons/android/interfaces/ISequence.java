package it.mfrancia.commons.android.interfaces;

public interface ISequence<T> {
	void add(T item);
	
	T getLast();
	
	T getNext();
	
	T getPrev();
	
	T getRoot();
	
	void removeLast();
}
