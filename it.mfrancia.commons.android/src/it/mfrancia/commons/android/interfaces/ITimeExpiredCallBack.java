package it.mfrancia.commons.android.interfaces;

public interface ITimeExpiredCallBack {
	void onTimeExpired(String id);
}
