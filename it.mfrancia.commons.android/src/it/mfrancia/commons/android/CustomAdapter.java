package it.mfrancia.commons.android;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

/**
 * Custom adapter, from ArrayList to ListView
 *
 * @author Matteo Francia
 *
 * @param <T>
 *            item type
 */
public abstract class CustomAdapter<T> extends ArrayAdapter<T> {
	private int layoutId;
	protected List<T> items;
	public CustomAdapter(final Context context, final List<T> items, final int layoutId) {
		super(context, 0, items);
		this.layoutId = layoutId;
		this.items = items;
		super.setNotifyOnChange(true);
	}

	@Override
	/**
	 * Add an item to the adapter
	 *
	 * @param item
	 */
	public void add(final T item) {
		super.add(item);
		super.notifyDataSetChanged();
	}

	/**
	 * Inflate the view and invokes the render method
	 */
	@Override
	public View getView(final int position, final View convertView, final ViewGroup parent) {
		View v = convertView;
		if (v == null) {
			final LayoutInflater vi = LayoutInflater.from(getContext());
			v = vi.inflate(layoutId, null);
		}
		render(v, position, getItem(position));
		return v;
	}

	@Override
	/**
	 * Remove the selected item from the adapter
	 *
	 * @param item
	 */
	public void remove(final T item) {
		super.remove(item);
		super.notifyDataSetChanged();
	}

	/**
	 * Implement this method to render an item into the list view
	 *
	 * @param v
	 * @param item
	 */
	public abstract void render(View v, int position, T item);

	/**
	 * Assign a layout to the custom adapter dynamically
	 *
	 * @param layout
	 *            layout id
	 */
	void setLayout(final int layout) {
		this.layoutId = layout;
	}
}
