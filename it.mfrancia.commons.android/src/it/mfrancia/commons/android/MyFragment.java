package it.mfrancia.commons.android;

import it.mfrancia.commons.android.interfaces.IActionListener;
import it.mfrancia.commons.android.interfaces.IFragment;
import it.mfrancia.commons.android.interfaces.IViewLoadedListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

/**
 * A generic fragment (which should be extended)
 *
 * @author Matteo Francia
 *
 */
public abstract class MyFragment extends Fragment implements IFragment {
	private IViewLoadedListener listener;
	private IActionListener actionListener;
	private String id;
	private String title;
	private Integer menuid = null;

	@Override
	public IActionListener getActionListener() {
		return actionListener;
	}

	/**
	 * get the fragment id
	 */
	@Override
	public String getFragmentId() {
		return id;
	}

	@Override
	public Integer getMenu() {
		return menuid;
	}

	@Override
	public IViewLoadedListener getOnViewLoadedListener() {
		return listener;
	}

	@Override
	public String getTitle() {
		return title;
	}

	public abstract View onCreate(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

	@Override
	public void onCreateOptionsMenu(final Menu menu, final MenuInflater inflater) {
		menu.clear();
		final int id = getMenu().intValue();
		inflater.inflate(id, menu);
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
		// show the menu which contains the action button
		if (getMenu() != null) {
			setHasOptionsMenu(true);
		}

		final View rootView = onCreate(inflater, container, savedInstanceState);

		if (getOnViewLoadedListener() != null) {
			getOnViewLoadedListener().onViewLoaded(getFragmentId(), rootView);
		}

		return rootView;
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		if (getActionListener() != null) {
			getActionListener().onActionButtonPressed(item);
		}
		return true;
	}

	@Override
	public void setActionListener(final IActionListener listener) {
		actionListener = listener;
	}

	/**
	 * set the fragment id
	 */
	@Override
	public void setFragmentId(final String id) {
		this.id = id;
	}

	@Override
	public void setMenu(final Integer id) {
		menuid = id;
	}

	@Override
	public void setOnViewLoadedListener(final IViewLoadedListener listener) {
		this.listener = listener;
	}

	@Override
	public void setTitle(final String title) {
		this.title = title;
	}
}
