package it.mfrancia.commons.android;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Non swipeable view pager! NB you must use the tag <thisclasspackage.NonSwipeableViewPager> instead of <ViewPager> withing the layout xml file
 *
 * @author Matteo Francia
 *
 */
public class NonSwipeableViewPager extends ViewPager {
	
	public NonSwipeableViewPager(final Context context) {
		super(context);
	}
	
	public NonSwipeableViewPager(final Context context, final AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public boolean onInterceptTouchEvent(final MotionEvent arg0) {
		// Never allow swiping to switch between pages
		return false;
	}
	
	@Override
	public boolean onTouchEvent(final MotionEvent event) {
		// Never allow swiping to switch between pages
		return false;
	}
}