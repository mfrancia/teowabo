package it.mfrancia.wordreference;

import java.util.ArrayList;
import java.util.HashMap;

import it.mfrancia.interfaces.language.ILanguage;
import it.mfrancia.interfaces.language.ILetter;
import it.mfrancia.interfaces.language.IWord;
import it.mfrancia.interfaces.serialization.ISerializable;

public class Word implements IWord {
	private static final long								serialVersionUID	= 1L;
	private ILetter[]										letters;
	private Type											type;
	private Gender											gender;
	private HashMap<ILanguage, ArrayList<ISerializable>>	map;
	
	public Word(Type type, Gender gender, String plainText) {
		letters = new Letter[plainText.length()];
		for (int i = 0; i < plainText.length(); i++)
			letters[i] = new Letter(plainText.charAt(i) + "");
		this.type = type;
		this.gender = gender;
		map = new HashMap<ILanguage, ArrayList<ISerializable>>();
	}
	
	public Word(String plainText) {
		this(Type.undefined, Gender.undefined, plainText);
	}
	
	@Override
	public CharSequence getDefaultRepresentation() {
		String s = getPlainText() + "|" + gender.toString() + "|" + type.toString();
		return s;
	}
	
	@Override
	public ILetter[] getLetters() {
		return letters;
	}
	
	@Override
	public HashMap<ILanguage, ArrayList<ISerializable>> getMeanings() {
		return map;
	}
	
	@Override
	public IWord translateTo(ILanguage language) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setMeaning(ILanguage language, final String meaning) {
		if (map.get(language) == null)
			map.put(language, new ArrayList<ISerializable>());
		map.get(language).add(new ISerializable() {
			private static final long	serialVersionUID	= 1L;
			
			@Override
			public CharSequence getDefaultRepresentation() {
				return meaning;
			}
		});
	}
	
	@Override
	public String getPlainText() {
		String s = "";
		for (ILetter l : getLetters())
			s += l.getDefaultRepresentation();
		return s;
	}
}
