package it.mfrancia.wordreference;

import it.mfrancia.interfaces.language.ILetter;

public class Letter implements ILetter {
	private static final long	serialVersionUID	= 1L;
	private String				letter;
	
	public Letter(String letter) {
		this.letter = letter;
	}
	
	@Override
	public CharSequence getDefaultRepresentation() {
		return letter;
	}
	
	@Override
	public String getPlainText() {
		return getDefaultRepresentation().toString();
	}
	
}
