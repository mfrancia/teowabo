package it.mfrancia.wordreference.app;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.interfaces.language.ILanguage;
import it.mfrancia.interfaces.language.IWord;
import it.mfrancia.interfaces.serialization.ISerializable;
import it.mfrancia.wordreference.Language;
import it.mfrancia.wordreference.Word;

import java.util.ArrayList;
import java.util.Random;

public class Main extends SoftwareSystem {
	private ILanguage	language;
	private IWord[]		words;
	
	@Override
	public void config() {
		language = new Language("en");
		words = new Word[3];
		
		for (int i = 0; i < 3; i++) {
			words[i] = new Word(IWord.Type.noun, IWord.Gender.male, "matteo" + i);
			words[i].setMeaning(language, "asd" + i);
			words[i].setMeaning(language, "esd" + i);
		}
	}
	
	@Override
	public void start() {
		Random r = new Random();
		for (int i = 0; i < 10; i++) {
			int j = r.nextInt(3);
			System.out.println(words[j].getDefaultRepresentation());
			ArrayList<ISerializable> meanings = words[j].getMeanings().get(language);
			for (ISerializable s : meanings)
				System.out.println(s.getDefaultRepresentation());
		}
	}
	
	public static void main(String[] args) {
		new Main();
	}
}
