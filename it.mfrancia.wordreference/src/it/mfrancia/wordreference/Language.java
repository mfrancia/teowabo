package it.mfrancia.wordreference;

import it.mfrancia.interfaces.language.ILanguage;

public class Language implements ILanguage {
	private static final long	serialVersionUID	= 1L;
	private String				id;
	
	public Language(String id) {
		this.id = id;
	}
	
	@Override
	public CharSequence getDefaultRepresentation() {
		return id;
	}
	
}
