package it.mfrancia.neuralnetwork.pclejos.main;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.implementation.multithreading.Monitor;
import it.mfrancia.implementation.physic.Speed;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.neuralnetwork.INeuralLayer;
import it.mfrancia.interfaces.neuralnetwork.INeuralNetwork;
import it.mfrancia.interfaces.neuralnetwork.INeuron;
import it.mfrancia.interfaces.neuralnetwork.ISpike;
import it.mfrancia.lejos.effector.LeJosRemoteMotor;
import it.mfrancia.lejos.sensor.LeJosContact;
import it.mfrancia.lejos.sensor.LeJosUltrasonic;
import it.mfrancia.neuralnetwork.ANNKb;
import it.mfrancia.neuralnetwork.ANNUtils;
import it.mfrancia.neuralnetwork.NeuralLayer;
import it.mfrancia.neuralnetwork.NeuralNetwork;
import it.mfrancia.neuralnetwork.NeuronFactory;
import it.mfrancia.neuralnetwork.reflexes.GoForward;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnLeft;
import it.mfrancia.neuralnetwork.reflexes.ReverseTurnRight;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeExpTransducer;
import it.mfrancia.neuralnetwork.transducer.NumberToSpikeLinearTransducer;
import it.mfrancia.neuralnetwork.transducer.SensorToNeuronAdapter;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.transducer.Transducer;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class NeuralNetworkMain extends SoftwareSystem {
	public static void main(final String[] args) {
		new NeuralNetworkMain();
	}
	
	private SensorPoller	poller;
	
	@Override
	public void config() {
		/* LEJOS */
		System.out.println("Press ANY key...");
		Button.waitForAnyPress();
		final IMotor motorLeft = new LeJosRemoteMotor(Motor.C);
		final IMotor motorRight = new LeJosRemoteMotor(Motor.A);
		motorRight.setSpeed(new Speed(Motor.A.getMaxSpeed() / 4.0));
		motorLeft.setSpeed(new Speed(Motor.C.getMaxSpeed() / 4.0));
		final GoForward idle = new GoForward(motorLeft, motorRight);
		final Monitor motorController = new Monitor(idle);
		final ReverseTurnLeft rl = new ReverseTurnLeft(motorController, motorLeft, motorRight);
		final ReverseTurnRight rr = new ReverseTurnRight(motorController, motorLeft, motorRight);
		
		final LeJosContact contactleft = new LeJosContact("cLeft", SensorPort.S3);
		final ButtonSensor contactSensorLeft = new ButtonSensor("sLeft", contactleft);
		final LeJosContact contactRight = new LeJosContact("cRight", SensorPort.S1);
		final ButtonSensor contactSensorRight = new ButtonSensor("sRight", contactRight);
		final LeJosUltrasonic ultrasonic = new LeJosUltrasonic("sonar", SensorPort.S2);
		final UltrasonicSensor ultrasonicSensor = new UltrasonicSensor("ultrasonic", ultrasonic);
		
		poller = new SensorPoller("poller", 300, contactleft, contactRight, ultrasonic);
		/* END LEJOS */
		
		final NeuronFactory nf = new NeuronFactory();
		final INeuralNetwork ann = new NeuralNetwork();
		
		/* building the proximity layer */
		final INeuralLayer P = new NeuralLayer(ann, ANNKb.proximityLayer);
		final INeuron proximityNeuronA = nf.createNeuron(P, ANNKb.proximityLayerThreshold);
		Transducer<Number, ISpike> transducer = new NumberToSpikeExpTransducer(new SensorToNeuronAdapter(proximityNeuronA));
		ultrasonicSensor.register(transducer);
		P.setVerbose(false);
		/* building the collision layer */
		final INeuralLayer C = new NeuralLayer(ann, ANNKb.collisionLayer);
		final INeuron collisionNeuronLeft = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		final INeuron collisionNeuronRight = nf.createNeuron(C, ANNKb.collisionLayerThreshold);
		transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(collisionNeuronLeft));
		contactSensorLeft.register(transducer);
		transducer = new NumberToSpikeLinearTransducer(new SensorToNeuronAdapter(collisionNeuronRight));
		contactSensorRight.register(transducer);
		C.setVerbose(false);
		/* building the motor controller layer */
		final INeuralLayer M = new NeuralLayer(ann, ANNKb.motorControlLayer);
		M.setVerbose(false);
		final INeuron motorControllerNeuronA = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rr);
		final INeuron motorControllerNeuronB = nf.createNeuronEffector(M, ANNKb.motorControlLayerThreshold, rl);
		/* making connections */
		ANNUtils.hopfieldLayerConnection(P, C);
		ANNUtils.neuronsConnection(collisionNeuronLeft, motorControllerNeuronA);
		ANNUtils.neuronsConnection(collisionNeuronRight, motorControllerNeuronB);
	}
	
	@Override
	public void start() {
		poller.start();
		Button.waitForAnyPress();
		poller.stop();
		Motor.A.stop();
		Motor.C.stop();
	}
}
