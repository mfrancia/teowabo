package it.mfrancia.neuralnetwork.pclejos.main;

import it.mfrancia.implementation.multithreading.Monitor;
import it.mfrancia.implementation.physic.Speed;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.effector.IMotor;
import it.mfrancia.interfaces.multithreading.IMonitor;
import it.mfrancia.lejos.LeJosSoftwareSystem;
import it.mfrancia.lejos.effector.LeJosRemoteMotor;
import it.mfrancia.lejos.sensor.LeJosLightSensor;
import it.mfrancia.neuralnetwork.reflexes.GoForward;
import it.mfrancia.neuralnetwork.reflexes.TurnLeft;
import it.mfrancia.neuralnetwork.reflexes.TurnRight;
import it.mfrancia.sensor.DifferentialAdapter;
import it.mfrancia.sensor.SensorPoller;
import lejos.nxt.Button;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class DifferentialLightFollower extends LeJosSoftwareSystem {
	public static void main(final String[] args) {
		new DifferentialLightFollower();
	}
	
	private IMotor			motorLeft;
	private IMotor			motorRight;
	
	private SensorPoller	sp;
	
	@Override
	public void config() {
		motorLeft = new LeJosRemoteMotor(Motor.C);
		motorRight = new LeJosRemoteMotor(Motor.A);
		motorLeft.setSpeed(new Speed((double) (Motor.C.getMaxSpeed() / 2)));
		motorRight.setSpeed(new Speed((double) (Motor.A.getMaxSpeed() / 2)));
		
		final IMonitor motorController = new Monitor(new GoForward(motorLeft, motorRight));
		final TurnLeft tl = new TurnLeft(motorController, motorLeft, motorRight);
		final TurnRight tr = new TurnRight(motorController, motorLeft, motorRight);
		
		final LeJosLightSensor l = new LeJosLightSensor("light", SensorPort.S4);
		final DifferentialAdapter ls = new DifferentialAdapter(l);
		ls.registerObserverLeft(new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				if (info.doubleValue() > 0) {
					motorController.execute(tr);
				}
			}
		});
		
		ls.registerObserverRight(new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				if (info.doubleValue() > 0) {
					motorController.execute(tl);
				}
			}
		});
		sp = new SensorPoller("poller", 300, ls);
	}
	
	@Override
	public void start() {
		sp.start();
		Button.waitForAnyPress();
		sp.stop();
		motorLeft.stop();
		motorRight.stop();
	}
}
