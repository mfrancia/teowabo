package it.mfrancia.networking.async.implementation;

import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.networking.IAsyncServerListener;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.networking.implementation.DummyConnectionListener;

public class DummyAsyncServerListener extends DummyConnectionListener implements IAsyncServerListener {
	
	@Override
	public void onDiscoverableServerStartedFailed(final IServer server, final MyException ex) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDiscoverableServerStartedSucceded(final IServer server) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onServerStartedFailed(final IServer server, final MyException ex) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onServerStartedSucceded(final IServer server) {
		// TODO Auto-generated method stub
		
	}
	
}
