package it.mfrancia.networking.test;

import static org.junit.Assert.assertTrue;
import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.exceptions.MyException;
import it.mfrancia.implementation.StringGenerator;
import it.mfrancia.interfaces.networking.IClient;
import it.mfrancia.interfaces.networking.IClientDhcpLike;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionFactory.Type;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.IPort;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.async.implementation.AsyncClient;
import it.mfrancia.networking.async.implementation.DummyAsyncClientListener;
import it.mfrancia.networking.async.implementation.DummyAsyncServerListener;
import it.mfrancia.networking.exceptions.ErrorOnWriteException;
import it.mfrancia.networking.exceptions.HostNotConnectedException;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.implementation.Client;
import it.mfrancia.networking.implementation.ClientDhcpLike;
import it.mfrancia.networking.implementation.ClientFactory;
import it.mfrancia.networking.implementation.ConnectionFactory;
import it.mfrancia.networking.implementation.ConnectionSocketChannel;
import it.mfrancia.networking.implementation.DummyConnectionListener;
import it.mfrancia.networking.implementation.Ip;
import it.mfrancia.networking.implementation.MessageReplicator;
import it.mfrancia.networking.implementation.NetworkingKb;
import it.mfrancia.networking.implementation.Port;
import it.mfrancia.networking.implementation.Server;
import it.mfrancia.networking.implementation.Socket;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class CommInfrastTest {
	
	protected static int	count	= 1;
	protected IClient		client;
	protected IIp			ip;
	private final int		load	= 20;	// change it to stress the server
	protected IPort			port;
	private boolean			ret;
	/*
	 * -------------------------------
	 */
	/*
	 * JUNIT variables, do not modify
	 */
	protected IServer		server;
	
	protected ISocket		socket1, socket2;
	
	@After
	public void after() {
		System.out.println("*** Ended test " + count + "\n");
		count++;
		assertTrue(ret);
	}
	
	@Before
	public void before() {
		System.out.println("*** Test " + count);
		ret = false;
	}
	
	@Test
	public void integrationTestServerAsyncClient12onConnectionFailed() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			final IConnection c = new ConnectionSocketChannel(socket1);
			final AsyncClient client = new AsyncClient(new DummyAsyncClientListener() {
				@Override
				public void onConnectionFailed(final MyException e) {
					ret = true;
				}
			}, c);
			client.connect();
			Thread.sleep(2000);
			client.kill();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void integrationTestServerAsyncClient13echoServer() {
		try {
			server = NetworkingKb.getServerInstance();
			server.setEventListener(new DummyAsyncServerListener() {
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					return true;
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					try {
						fromConn.write(data);
						ret = ret & true;
					} catch (final GenericIOException e) {
						ret = false;
					}
				}
			});
			socket1 = new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort());
			final IConnection c = new ConnectionSocketChannel(socket1);
			final AsyncClient client = new AsyncClient(new DummyAsyncClientListener() {
				@Override
				public void onClientConnected(final IClient client) {
					((AsyncClient) client).write("Hello, world!".getBytes());
					ret = true;
				}
				
				@Override
				public void onConnectionFailed(final MyException e) {
					ret = false;
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					ret = ret & new String(data).equals("Hello, world!");
				}
				
				@Override
				public void onWriteFailed(final MyException e) {
					ret = false;
				}
				
				@Override
				public void onWriteSucceded() {
					ret = ret & true;
				}
			}, c);
			client.connect();
			Thread.sleep(2000);
			client.kill();
			NetworkingKb.destroyServerInstance();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void integrationTestServerAsyncServer14serverStartedSuccededCallBack() {
		server = NetworkingKb.getServerInstance(new DummyAsyncServerListener() {
			@Override
			public void onServerStartedSucceded(final IServer server) {
				ret = true;
			}
		});
		try {
			Thread.sleep(1000);
		} catch (final InterruptedException e) {
		}
		NetworkingKb.destroyServerInstance();
	}
	
	@Test
	public void integrationTestServerAsyncServer15discoverableServerStartedSuccededCallBack() {
		NetworkingKb.getDiscoverableServerInstance(new DummyAsyncServerListener() {
			@Override
			public void onDiscoverableServerStartedSucceded(final IServer server) {
				ret = ret & true;
				System.out.println("onDiscoverableServerStartedSucceded");
			}
			
			@Override
			public void onServerStartedSucceded(final IServer server) {
				ret = true;
				System.out.println("onServerStartedSucceded");
			}
		});
		try {
			Thread.sleep(1000);
		} catch (final InterruptedException e) {
		}
		NetworkingKb.destroyServerInstance();
	}
	
	@Test
	public void integrationTestServerClient10onServerDownWhileClientIsInActiveReading() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new MessageReplicator(null, server));
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			final Client client = new Client(c);
			client.setEventListener(new IConnectionListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					return false;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
					ret = true;
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
				}
			});
			client.connect();
			client.startActiveReading();
			Thread.sleep(1000);
			server.kill();
			Thread.sleep(1000);
			client.kill();
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient11clientAutomaticReconnect() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new MessageReplicator(null, server));
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			final Client client = new Client(c);
			
			client.setEventListener(new IConnectionListener() {
				int	n	= 0;
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					if ((++n == 2) && client.isConnected()) {
						ret = true;
					}
					return false;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
					try {
						client.connect();
					} catch (final GenericIOException e) {
						e.printStackTrace();
					}
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
					
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
				}
			});
			client.connect();
			client.startActiveReading();
			Thread.sleep(1000);
			for (final IConnection con : server.getConnections()) {
				con.close();
			}
			Thread.sleep(1000);
			client.kill();
			server.kill();
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient17clientFactory() {
		try {
			NetworkingKb.getServerInstance(new DummyAsyncServerListener() {
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					fromConn.addOutput(data);
				}
				
				@Override
				public void onServerStartedSucceded(final IServer server) {
					client = new ClientFactory().getClient(Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), new DummyConnectionListener() {
						@Override
						public void onDataReceived(final IConnection fromConn, final byte[] data) {
							ret = new String(data).equals("hello, world");
						}
					});
					client.addOutput("hello, world".getBytes());
				}
			});
			Thread.sleep(500);
			client.kill();
			NetworkingKb.destroyServerInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			NetworkingKb.destroyServerInstance();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient18clientFactoryConnectFail() {
		try {
			
			client = new ClientFactory().getClient(Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), new DummyConnectionListener() {
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					ret = new String(data).equals("hello, world");
				}
			});
			
			NetworkingKb.getServerInstance(new DummyAsyncServerListener() {
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					fromConn.addOutput(data);
				}
				
				@Override
				public void onServerStartedSucceded(final IServer server) {
					try {
						client.connect();
					} catch (final GenericIOException e) {
						e.printStackTrace();
						ret = false;
					}
					client.addOutput("hello, world".getBytes());
				}
			});
			Thread.sleep(500);
			client.kill();
			NetworkingKb.destroyServerInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			NetworkingKb.destroyServerInstance();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient19clientFactoryAsyncClient() {
		try {
			NetworkingKb.getServerInstance(new DummyAsyncServerListener() {
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					fromConn.addOutput(data);
				}
				
				@Override
				public void onServerStartedSucceded(final IServer server) {
					client = new ClientFactory().getAsyncClient(Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), new DummyAsyncClientListener() {
						@Override
						public void onClientConnected(final IClient client) {
							System.out.println("onClientConnected");
							ret = true;
						}
						
						@Override
						public void onDataReceived(final IConnection fromConn, final byte[] data) {
							System.out.println("onDataReceived");
							ret = ret & new String(data).equals("hello, world");
						}
						
						@Override
						public void onWriteSucceded() {
							System.out.println("onWriteSucceded");
							ret = ret & true;
						}
					});
					client.addOutput("hello, world".getBytes());
				}
			});
			Thread.sleep(500);
			client.kill();
			NetworkingKb.destroyServerInstance();
		} catch (final Exception e) {
			e.printStackTrace();
			NetworkingKb.destroyServerInstance();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient1loadTest() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = NetworkingKb.getServerInstance();
			server.setEventListener(new DummyAsyncServerListener() {
				int	count	= 0;
				
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					System.out.println(new String(data));
					if (++count == load) {
						ret = true;
					}
				}
				
			});
			Thread.sleep(1000);
			
			for (int i = 0; i < load; i++) {
				new Thread() {
					@Override
					public void run() {
						try {
							final IConnection c = new ConnectionSocketChannel(socket1);
							Thread.sleep((long) (Math.random() * 100 /* ms */));
							c.connect();
							c.write(("Hello, world! " + System.currentTimeMillis()).getBytes());
							c.close();
						} catch (final Exception e) {
							e.printStackTrace();
						}
					}
				}.start();
			}
			
			Thread.sleep(3000);
			NetworkingKb.destroyServerInstance();
			
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
		
	}
	
	@Test
	public void integrationTestServerClient2echoServer() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new DummyAsyncServerListener() {
				
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					try {
						from.write(data);
					} catch (final GenericIOException e) {
						e.printStackTrace();
						server.kill();
					}
				}
				
			});
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			c.connect();
			c.write("Hello, world!".getBytes());
			ret = new String(c.read()).equals("Hello, world!");
			c.close();
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
		server.kill();
	}
	
	@Test
	public void integrationTestServerClient3onServerDown() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new DummyAsyncServerListener() {
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					try {
						// example of a simple echo service
						from.write(data);
					} catch (final GenericIOException e) {
						e.printStackTrace();
						server.kill();
					}
				}
				
			});
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			c.connect();
			c.write("Hello, world!".getBytes());
			server.kill();
			c.write("Hello, world!".getBytes());
		} catch (final ErrorOnWriteException e) {
			ret = true;
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient4ConnectionRefused() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = NetworkingKb.getServerInstance();
			server.setEventListener(new DummyAsyncServerListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// always reject the connection
					return false;
				}
				
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					System.out.println(new String(data));
				}
				
			});
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			c.connect();
			c.write("Hello, world!".getBytes());
			c.close();
		} catch (final HostNotConnectedException e) {
			ret = true;
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
		NetworkingKb.destroyServerInstance();
	}
	
	@Test
	public void integrationTestServerClient5onConnectionClose() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new DummyAsyncServerListener() {
				@Override
				public void onConnectionClosed(final IConnection con) {
					ret = true;
				}
			});
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			c.connect();
			c.close();
			Thread.sleep(500);
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
		server.kill();
	}
	
	@Test
	public void integrationTestServerClient6serverHandlingConnections() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = NetworkingKb.getServerInstance();
			server.setEventListener(new DummyAsyncServerListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// always accept the connection
					return true;
				}
				
			});
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			
			// connecting the client
			c.connect();
			ret = server.getConnections().length == 1;
			c.write("Hello, world!".getBytes());
			
			// connection closed by server
			server.killConnection(server.getConnections()[0]);
			ret = ret && (server.getConnections().length == 0);
			
			c.write("Hello, world!".getBytes());
			c.close();
			ret = false;
		} catch (final ErrorOnWriteException e) {
			ret = ret && true;
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
		NetworkingKb.destroyServerInstance();
	}
	
	@Test
	public void integrationTestServerClient7ClientActiveReading() {
		try {
			socket1 = new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new DummyAsyncServerListener() {
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// always accept the connection
					return true;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
				}
				
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					try {
						from.write(data);
					} catch (final GenericIOException e) {
						e.printStackTrace();
						server.kill();
					}
				}
			});
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			final Client client = new Client(c);
			client.setEventListener(new IConnectionListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					return false;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					ret = new String(data).equals("Hello, world!");
				}
			});
			client.connect();
			client.startActiveReading();
			client.write("Hello, world!".getBytes());
			Thread.sleep(1000);
			client.kill();
			server.kill();
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient8ServerMessageReplicator() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new MessageReplicator(null, server));
			server.start();
			Thread.sleep(1000);
			IConnection c = new ConnectionSocketChannel(socket1);
			final Client clientSender = new Client(c);
			clientSender.connect();
			
			c = new ConnectionSocketChannel(socket1);
			final Client clientReceiver = new Client(c);
			
			clientReceiver.setEventListener(new IConnectionListener() {
				int	n	= 0;
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// not implemented
					return false;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
					// not implemented
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
					
				}
				
				@Override
				public void onDataReceived(final IConnection fromConn, final byte[] data) {
					if (++n == 10) {
						ret = true;
					}
					System.out.println(new String(data));
				}
			});
			clientReceiver.connect();
			clientReceiver.startActiveReading();
			Thread.sleep(1000);
			
			for (int i = 0; i < 10; i++) {
				clientSender.write("Hello, world!".getBytes());
			}
			Thread.sleep(1000);
			clientSender.kill();
			clientReceiver.kill();
			server.kill();
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void integrationTestServerClient9maximumPayload() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = new Server(socket1);
			server.setEventListener(new DummyAsyncServerListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// always accept the connection
					return true;
				}
				
				@Override
				public void onConnectionClosed(final IConnection con) {
				}
				
				@Override
				public void onConnectionConnected(final IConnection con) {
					
				}
				
				@Override
				public void onDataReceived(final IConnection from, final byte[] data) {
					try {
						from.write(data);
					} catch (final GenericIOException e) {
						e.printStackTrace();
						server.kill();
					}
				}
				
			});
			server.start();
			Thread.sleep(1000);
			final IConnection c = new ConnectionSocketChannel(socket1);
			c.connect();
			final String randomString = StringGenerator.getRandomSequence(NetworkingKb.getPayloadSize()).toString();
			c.write(randomString.getBytes());
			ret = new String(c.read()).equals(randomString);
			c.close();
		} catch (final Exception e) {
			e.printStackTrace();
			ret = false;
			server.kill();
		}
		server.kill();
	}
	
	@Test
	public void integrationTestServerConnection16connectionExecutorTestStress() {
		NetworkingKb.getServerInstance(new DummyAsyncServerListener() {
			int	i	= 0;
			
			@Override
			public void onDataReceived(final IConnection fromConn, final byte[] data) {
				System.out.println(++i + " " + new String(data));
			};
		});
		try {
			final IConnection c = new ConnectionSocketChannel(new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()));
			c.connect();
			for (int i = 0; i < 10000; i++) {
				new Thread(new Runnable() {
					@Override
					public void run() {
						c.addOutput("hello, world".getBytes());
					}
				}).start();
			}
		} catch (final GenericIOException e) {
			e.printStackTrace();
		}
		try {
			Thread.sleep(1000);
		} catch (final InterruptedException e) {
		}
		NetworkingKb.destroyServerInstance();
		ret = true;
	}
	
	@Test
	public void serverDiscoverTest() {
		try {
			socket1 = new Socket(IIp.ANY, NetworkingKb.getServerPort());
			server = NetworkingKb.getDiscoverableServerInstance();
			server.setEventListener(new DummyAsyncServerListener() {
				
				@Override
				public boolean onConnectionAccept(final IConnection con) {
					// always accept the connection
					return true;
				}
				
			});
			
			Thread.sleep(1000);
			
			final IClientDhcpLike client = new ClientDhcpLike();
			final IConnection[] conn = client.discover(new Ip(IIp.BROADCAST), NetworkingKb.getUdpDiscoverPort().getPortNumber());
			
			if (conn.length == 0) {
				System.out.println("server not found");
			} else {
				final IConnection con = conn[0];
				
				final String addr = con.getPeer().getIp().getIpValue().toString();
				final int port = con.getPeer().getPort().getPortNumber();
				
				System.out.println("Discovered server at " + addr + ":" + port);
				ret = true;
			}
			
		} catch (final Exception e) {
			e.printStackTrace();
			ret = false;
			server.kill();
		}
		NetworkingKb.destroyServerInstance();
	}
	
	@Test
	public void testConnectionNotConnectedException() {
		IConnection c;
		try {
			socket1 = new Socket("127.0.0.1", NetworkingKb.getServerPort());
			c = new ConnectionFactory().createConnection(Type.SOCKETCHANNEL, socket1);
			c = new ConnectionSocketChannel(socket1);
			c.write("Hello, world!".getBytes());
			c.close();
		} catch (final GenericIOException e) {
			ret = true;
		} catch (final Exception e) {
			e.printStackTrace();
			server.kill();
			ret = false;
		}
	}
	
	@Test
	public void testIp() {
		ip = new Ip();
		final IIp ip2 = new Ip("1.1.1.1");
		ret = ip2.getIpValue().equals("1.1.1.1") && ip.getIpValue().equals(IIp.ANY);
	}
	
	@Test
	public void testPort() {
		try {
			new Port(-1);
			ret = false;
		} catch (final InvalidPortException e) {
			ret = true;
		}
		
		try {
			new Port(100000);
			ret = false;
		} catch (final InvalidPortException e) {
			ret = true && ret;
		}
		
		try {
			new Port(10000);
			ret = true && ret;
		} catch (final InvalidPortException e) {
			ret = false;
		}
	}
	
	@Test
	public void testSocket() throws InvalidPortException {
		socket1 = new Socket();
		socket2 = new Socket("127.0.0.1", NetworkingKb.getServerPort());
		ret = socket1.getIp().getIpValue().equals(IIp.ANY) && (socket1.getPort().getPortNumber() == IPort.ANY) && socket2.getIp().getIpValue().equals("127.0.0.1")
				&& (socket2.getPort().getPortNumber() == NetworkingKb.getServerPort().getPortNumber());
	}
	
}
