package it.mfrancia.networking.main.example1;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.networking.IClientDhcpLike;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.networking.implementation.ClientDhcpLike;
import it.mfrancia.networking.implementation.Ip;
import it.mfrancia.networking.implementation.NetworkingKb;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class ClientMain {
	public static void main(final String[] args) throws GenericIOException {
		try {
			new ClientMain(new IOutput<String>() {
				
				@Override
				public void addOutput(final String o) {
					System.out.println("echo " + o);
					
				}
				
			});
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
	
	private IClientDhcpLike			client;
	
	private final IOutput<String>	out;
	
	public ClientMain(final IOutput<String> out) throws Exception, GenericIOException {
		this.out = out;
		if (config()) {
			start();
		}
		kill();
	}
	
	private boolean config() throws Exception, GenericIOException {
		client = new ClientDhcpLike();
		client.setEventListener(new IConnectionListener() {
			
			@Override
			public boolean onConnectionAccept(final IConnection con) {
				return false;
			}
			
			@Override
			public void onConnectionClosed(final IConnection con) {
				// ...
			}
			
			@Override
			public void onConnectionConnected(final IConnection con) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onDataReceived(final IConnection fromConn, final byte[] data) {
				// ....
				
			}
			
		});
		final IConnection[] conn = client.discover(new Ip(IIp.BROADCAST), NetworkingKb.getUdpDiscoverPort().getPortNumber());
		
		if (conn.length > 0) {
			client.setConnection(conn[0]);
			client.connect();
			return true;
		} else {
			System.out.println("server not found");
			return false;
		}
	}
	
	public void kill() {
		client.kill();
	}
	
	private void start() throws InterruptedException, GenericIOException, IOException {
		for (int i = 0; i < 10; i++) {
			client.write(("Hello " + i).getBytes());
			Thread.sleep(100);
			out.addOutput(new String(client.read()).trim());
		}
		
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!br.readLine().equals("")) {
			;
		}
		
	}
}
