package it.mfrancia.networking.main.example2;

import it.mfrancia.networking.exceptions.InvalidPortException;

public class ExampleMain {
	public static void main(final String[] args) throws Exception, InvalidPortException {
		ClientReceiverMain.main(args);
		ClientSenderMain.main(args);
	}
}
