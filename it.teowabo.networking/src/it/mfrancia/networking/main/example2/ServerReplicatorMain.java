package it.mfrancia.networking.main.example2;

import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.implementation.MessageReplicator;
import it.mfrancia.networking.implementation.NetworkingKb;
import it.mfrancia.networking.implementation.Server;
import it.mfrancia.networking.implementation.Socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ServerReplicatorMain {
	public static void main(final String[] args) throws Exception, InvalidPortException {
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				ServerReplicatorMain s = null;
				try {
					s = new ServerReplicatorMain(new Socket(IIp.ANY, NetworkingKb.getServerPort()), null);
					final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
					while (!br.readLine().equals("")) {
						;
					}
				} catch (final Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} finally {
					if (s != null) {
						s.kill();
					}
				}
			}
		}).start();
	}
	
	private final ILogger	out;
	private Server			server;
	
	private final ISocket	socket;
	
	public ServerReplicatorMain(final ISocket socket, final ILogger out) throws Exception {
		this.socket = socket;
		this.out = out;
		config();
		start();
	}
	
	private void config() throws Exception {
		// discoverable server on port Syskb.udpDiscoverPort
		server = new Server(socket, NetworkingKb.getUdpDiscoverPort());
		if (out != null) {
			server.setLogger(out);
		}
		server.setEventListener(new MessageReplicator(out, server));
		
	}
	
	public void kill() {
		server.kill();
	}
	
	private void start() {
		// server already started
	}
}
