package it.mfrancia.networking.main.example3;

import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.implementation.NetworkingKb;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;

public class ServerJCardsMain {
	public static void main(final String[] args) throws Exception, InvalidPortException {
		final ServerJCardsMain s = new ServerJCardsMain();
		
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!br.readLine().equals("")) {
			;
		}
		
		s.kill();
	}
	
	final HashMap<String, String>	map	= new HashMap<String, String>();
	
	private IServer					server;
	
	public ServerJCardsMain() throws Exception {
		config();
		start();
	}
	
	private void config() throws Exception {
		server = NetworkingKb.getDiscoverableServerInstance();
		server.setEventListener(new IConnectionListener() {
			@Override
			public boolean onConnectionAccept(final IConnection arg0) {
				if (map.get(arg0.getId()) == null) {
					map.put(arg0.getId(), "");
					return true;
				}
				return false;
			}
			
			@Override
			public void onConnectionClosed(final IConnection arg0) {
				System.out.println("closed" + arg0.getId());
				
			}
			
			@Override
			public void onConnectionConnected(final IConnection con) {
				con.addOutput("matteoServer".getBytes());
			}
			
			@Override
			public void onDataReceived(final IConnection arg0, final byte[] arg1) {
				if (map.get(arg0.getId()) != null) {
					final String s = new String(arg1);
					System.out.println(s);
					map.put(arg0.getId(), s);
				}
			}
		});
		
	}
	
	public void kill() {
		server.kill();
	}
	
	private void start() {
		// server already started
	}
}
