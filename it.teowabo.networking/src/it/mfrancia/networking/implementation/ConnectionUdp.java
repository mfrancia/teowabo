package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.exceptions.ErrorOnReadException;
import it.mfrancia.networking.exceptions.ErrorOnWriteException;
import it.mfrancia.networking.exceptions.InvalidHostException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ConnectionUdp extends AbstractConnection {
	private DatagramSocket	dgs;
	
	public ConnectionUdp(final ISocket socket) {
		super("[UDPconnection" + NetworkingKb.getUniqueId() + "]", socket);
	}
	
	@Override
	public void close() {
		super.close();
		if (dgs != null) {
			dgs.close();
		}
	}
	
	@Override
	public void connect() throws GenericIOException {
		if (dgs != null) {
			try {
				dgs.connect(InetAddress.getByName(socket.getIp().getIpValue().toString()), socket.getPort().getPortNumber());
			} catch (final UnknownHostException e) {
				throw new InvalidHostException(e.toString());
			}
		}
	}
	
	@Override
	public ISocket getPeer() {
		return socket;
	}
	
	@Override
	public boolean isConnected() {
		return (dgs != null) && dgs.isConnected();
	}
	
	@Override
	public byte[] read() throws GenericIOException {
		final byte[] buf = new byte[NetworkingKb.getPayloadSize()];
		final DatagramPacket dgp = new DatagramPacket(buf, NetworkingKb.getPayloadSize());
		try {
			dgs.receive(dgp);
		} catch (final IOException e) {
			throw new ErrorOnReadException(e.toString());
		}
		return dgp.getData();
	}
	
	@Override
	public void write(final byte[] data) throws GenericIOException {
		try {
			dgs.send(new DatagramPacket(data, data.length));
		} catch (final IOException e) {
			throw new ErrorOnWriteException(e.toString());
		}
	}
	
}
