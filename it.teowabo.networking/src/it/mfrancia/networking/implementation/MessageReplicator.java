package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.networking.IAsyncServerListener;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IServer;

/**
 * MessageReplicator takes the data received from a client and replicate them to the others clients connected to the server
 * 
 * @author Matteo Francia
 * 
 */
public class MessageReplicator implements IAsyncServerListener {
	
	private final IOutput<String>	out;
	private final IServer			server;
	
	public MessageReplicator(final IOutput<String> out, final IServer server) {
		this.server = server;
		this.out = out;
	}
	
	private void log(final String s) {
		if (out != null) {
			out.addOutput(s);
		} else {
			System.out.println(s);
		}
	}
	
	@Override
	public boolean onConnectionAccept(final IConnection con) {
		return true;
	}
	
	@Override
	public void onConnectionClosed(final IConnection con) {
		
	}
	
	@Override
	public void onConnectionConnected(final IConnection con) {
	}
	
	@Override
	public void onDataReceived(final IConnection fromConn, final byte[] data) {
		for (final IConnection c : server.getConnections()) {
			// GCiatto edit on 28/06/2014
			// if (!fromConn.getId().equals(c.getId())) {
			log("[REPLICATOR] replicate data on connection: " + c.getId());
			try {
				c.write(data);
			} catch (final GenericIOException e) {
				e.printStackTrace();
			}
			// }
		}
	}
	
	@Override
	public void onDiscoverableServerStartedFailed(final IServer server, final MyException ex) {
	}
	
	@Override
	public void onDiscoverableServerStartedSucceded(final IServer server) {
	}
	
	@Override
	public void onServerStartedFailed(final IServer server, final MyException ex) {
	}
	
	@Override
	public void onServerStartedSucceded(final IServer server) {
	}
	
}
