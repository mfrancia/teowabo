package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionFactory;
import it.mfrancia.interfaces.networking.ISocket;

public class ConnectionFactory implements IConnectionFactory {
	
	@Override
	public IConnection createConnection(final Type type, final ISocket socket) {
		IConnection ret = null;
		if (type == Type.SOCKETCHANNEL) {
			try {
				ret = new ConnectionSocketChannel(socket);
			} catch (final GenericIOException e) {
				e.printStackTrace();
			}
		} else if (type == Type.UDP) {
			ret = new ConnectionUdp(socket);
		}
		return ret;
	}
	
}
