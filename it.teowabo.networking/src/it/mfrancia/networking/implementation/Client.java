package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.networking.IClient;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.networking.exceptions.ConnectionClosedException;
import it.mfrancia.networking.exceptions.HostNotConnectedException;

/**
 * Generic Client
 * 
 * @author Matteo Francia
 * 
 */
public class Client extends Peer implements IInputSource, IClient {
	private boolean			activeRead	= false;
	protected IConnection	conn;
	private boolean			notStopped	= true;
	private final Thread	t			= new Thread(toString() + "ActiveReceiver") {
											@Override
											public void run() {
												while (notStopped) {
													try {
														final byte[] data = conn.read();
														log("Read " + data.length + " bytes");
														// log("\t\t" + new String(data));
														if (!conn.isConnected()) {
															notifyClosedConnection();
														} else {
															notifyDataReceived(data);
														}
													} catch (final ConnectionClosedException e) {
														log("Connection closed");
														notifyClosedConnection();
													} catch (final HostNotConnectedException e) {
													} catch (final GenericIOException e) {
														e.printStackTrace();
													}
												}
											}
										};
	
	protected Client() {
		this(null, null);
	}
	
	public Client(final CharSequence name) {
		this(name, null);
	}
	
	public Client(final CharSequence name, final IConnection conn) {
		super(name == null ? "[Client" + NetworkingKb.getUniqueId() + "]" : "[" + name + NetworkingKb.getUniqueId() + "]");
		this.conn = conn;
	}
	
	public Client(final IConnection con) {
		this(null, con);
	}
	
	// public Client(final ISocket socket) throws GenericIOException {
	// this(new ConnectionSocketChannel(socket));
	// }
	
	@Override
	public void addOutput(final byte[] o) {
		conn.addOutput(o);
	}
	
	@Override
	public void connect() throws GenericIOException {
		log("trying to connect");
		conn.connect();
		if (isConnected()) {
			log("connection estabilished");
		} else {
			log("connection failed");
		}
		if (isConnected() && (listener != null)) {
			listener.onConnectionAccept(conn);
			listener.onConnectionConnected(conn);
		}
	}
	
	protected IConnectionListener getListener() {
		return listener;
	}
	
	@Override
	public boolean isConnected() {
		return (conn != null) && conn.isConnected();
	}
	
	@Override
	public void kill() {
		if (t != null) {
			notStopped = false;
			t.interrupt();
		}
		if (conn != null) {
			conn.close();
		}
	}
	
	private void notifyClosedConnection() {
		if (listener != null) {
			listener.onConnectionClosed(conn);
			conn.close();
		}
	}
	
	private void notifyDataReceived(final byte[] data) {
		if (listener != null) {
			listener.onDataReceived(conn, data);
		}
	}
	
	@Override
	public byte[] read() throws GenericIOException {
		if ((conn == null) || !conn.isConnected()) {
			throw new HostNotConnectedException("");
		}
		return conn.read();
	}
	
	/**
	 * Start active reading on the connection, when data are received it notifies the dataReceiver (which reference is passed through the constructor)
	 */
	@Override
	public void startActiveReading() {
		if (!activeRead) {
			activeRead = true;
			t.start();
		}
	}
	
	/**
	 * if connection is null it throws {@link HostNotConnectedException}, if client is not connected try to connect it, if client is still disconnected it throws {@link HostNotConnectedException},
	 * finally it tries send the data.
	 */
	@Override
	public void write(final byte[] data) throws GenericIOException {
		if (conn == null) {
			throw new HostNotConnectedException("");
		}
		
		if (!conn.isConnected()) {
			connect();
		}
		
		if (!conn.isConnected()) {
			throw new HostNotConnectedException("");
		}
		
		conn.write(data);
		log("Writing " + data.length + " bytes");
		// log("\t\t" + new String(data));
	}
	
}
