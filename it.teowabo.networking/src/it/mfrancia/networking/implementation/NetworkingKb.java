package it.mfrancia.networking.implementation;

import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IAsyncServerListener;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.IPort;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.main.remotelogging.LoggerClient;
import it.mfrancia.networking.main.remotelogging.LoggerSever;

/**
 *
 * @author Matteo Francia
 *
 */
public class NetworkingKb {
	public synchronized static void destroyLoggerClient() {
		if (loggerClient != null) {
			loggerClient.kill();
		}
		loggerClient = null;
	}
	
	public synchronized static void destroyLoggerServer() {
		if (loggerServer != null) {
			loggerServer.kill();
		}
		loggerServer = null;
	}
	
	/**
	 * Kill the existing server (if any)
	 */
	public static synchronized void destroyServerInstance() {
		if ((server != null) && server.isRunning()) {
			server.kill();
			server = null;
		}
	}
	
	public synchronized static IPort getDiscoverableLoggerPort() {
		return discoverableLoggerPort;
		
	}
	
	/**
	 * Create (and start) a new discoverable server instance
	 *
	 * @return server discoverable instance
	 * @throws InvalidPortException
	 *             server port is invalid
	 * @throws Exception
	 */
	public static synchronized Server getDiscoverableServerInstance() {
		return getDiscoverableServerInstance(null);
	}
	
	/**
	 * Create (and start) a new discoverable server instance
	 *
	 * @return server discoverable instance
	 * @throws InvalidPortException
	 *             server port is invalid
	 * @throws Exception
	 */
	public static synchronized Server getDiscoverableServerInstance(final IAsyncServerListener listener) {
		if (server == null) {
			server = new Server(new Socket(IIp.ANY, serverPort), udpDiscoverPort);
			server.setEventListener(listener);
		}
		if (!server.isRunning()) {
			server.start();
		}
		return server;
	}
	
	public synchronized static int getDtUdpDiscover() {
		return dtUdpDiscover;
	}
	
	public synchronized static ILogger getLoggerClient(final ILogger logger) throws Exception {
		if (loggerClient == null) {
			loggerClient = new LoggerClient(logger);
		}
		return loggerClient;
	}
	
	public synchronized static IPort getLoggerPort() {
		return loggerPort;
		
	}
	
	public synchronized static void getLoggerServer(final ILogger logger) {
		if (loggerServer == null) {
			loggerServer = new LoggerSever(logger);
		}
	}
	
	/**
	 * Get the maximum message size
	 *
	 * @return message size
	 */
	public synchronized static int getPayloadSize() {
		return payloadSize;
	}
	
	/**
	 * Create (and start) a new server instance
	 *
	 * @return server instance
	 * @throws InvalidPortException
	 *             server port is invalid
	 * @throws Exception
	 */
	public static synchronized IServer getServerInstance() {
		return getServerInstance(null);
	}
	
	/**
	 * Create (and start) a new server instance
	 * 
	 * @param listener
	 *            server listener
	 * @return server instance
	 * @throws InvalidPortException
	 *             server port is invalid
	 * @throws Exception
	 */
	public static synchronized IServer getServerInstance(final IAsyncServerListener listener) {
		if (server == null) {
			server = new Server(new Socket(IIp.ANY, serverPort));
			server.setEventListener(listener);
		}
		if (!server.isRunning()) {
			server.start();
		}
		return server;
	}
	
	/**
	 * Get the server port
	 *
	 * @return port value
	 */
	public synchronized static IPort getServerPort() {
		return serverPort;
	}
	
	/**
	 * Get the server discover port (UDP)
	 *
	 * @param value
	 *            port value
	 */
	public synchronized static IPort getUdpDiscoverPort() {
		return udpDiscoverPort;
	}
	
	/**
	 * Get UDP discover retry value
	 *
	 * @return
	 */
	public synchronized static int getUdpDiscoverRetry() {
		return udpDiscoverRetry;
	}
	
	public synchronized static int getUniqueId() {
		return id++;
	}
	
	public synchronized static void setDiscoverableLoggerPort(final IPort port) {
		discoverableLoggerPort = port;
	}
	
	public synchronized static void setDtUdpDiscover(final int ms) {
		if (server == null) {
			dtUdpDiscover = ms;
		}
	}
	
	public synchronized static void setLoggerPort(final IPort port) {
		loggerPort = port;
	}
	
	/**
	 * Set the maximum message size
	 *
	 * @param payload
	 *            message size
	 */
	public synchronized static void setPayloadSize(final int payload) {
		if (server == null) {
			payloadSize = payload;
		}
	}
	
	/**
	 * Set the server port
	 *
	 * @param value
	 *            port
	 */
	public synchronized static void setServerPort(final IPort value) {
		if (server == null) {
			serverPort = value;
		}
	}
	
	/**
	 * Set the server discover port (UDP)
	 *
	 * @param value
	 *            port value
	 */
	public synchronized static void setUdpDiscoverPort(final IPort value) {
		if (server == null) {
			udpDiscoverPort = value;
		}
	}
	
	/**
	 * Set UDP discover retry number
	 *
	 * @param value
	 */
	public synchronized static void setUdpDiscoverRetry(final int value) {
		if ((server == null) && (value > 0)) {
			udpDiscoverRetry = value;
		}
	}
	
	private static int			discoverableLoggerport	= 9091;
	
	private static IPort		discoverableLoggerPort;
	
	private static int			dtUdpDiscover			= 1000;		// ms
																		
	private static int			id						= 0;
	
	private static LoggerClient	loggerClient;
	
	private static int			loggerport				= 9090;
	
	private static IPort		loggerPort;
	
	private static LoggerSever	loggerServer;
	
	private static int			payloadSize				= 64 * 1024;
	
	private static Server		server					= null;
	
	private static int			serverport				= 8083;
	
	private static IPort		serverPort;
	
	public final static int		tcpMessageLength		= 4;			// bytes
																		
	private static int			udpDiscoverport			= 8081;
	
	private static IPort		udpDiscoverPort;
	
	private static int			udpDiscoverRetry		= 5;
	
	static {
		new NetworkingKb();
	}
	
	private NetworkingKb() {
		try {
			loggerPort = new Port(loggerport);
			serverPort = new Port(serverport);
			udpDiscoverPort = new Port(udpDiscoverport);
			discoverableLoggerPort = new Port(discoverableLoggerport);
		} catch (final InvalidPortException e) {
			e.printStackTrace();
		}
	}
}
