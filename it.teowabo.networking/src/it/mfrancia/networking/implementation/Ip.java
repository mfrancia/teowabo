package it.mfrancia.networking.implementation;

import it.mfrancia.interfaces.networking.IIp;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Ip implements IIp {
	
	private final String	addr;
	
	public Ip() {
		addr = IIp.ANY;
	}
	
	public Ip(final CharSequence addr) {
		this.addr = addr.toString();
	}
	
	@Override
	public CharSequence getIpValue() {
		return addr;
	}
	
}
