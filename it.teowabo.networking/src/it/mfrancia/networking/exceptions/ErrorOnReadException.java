package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class ErrorOnReadException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public ErrorOnReadException(final String c) {
		super(c);
	}
	
}
