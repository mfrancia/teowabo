package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.MyException;

public class InvalidPortException extends MyException {
	private static final long	serialVersionUID	= 1L;
	
	public InvalidPortException(final String c) {
		super(c);
	}
	
}
