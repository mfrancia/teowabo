package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class InvalidHostException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public InvalidHostException(final String c) {
		super(c);
	}
	
}
