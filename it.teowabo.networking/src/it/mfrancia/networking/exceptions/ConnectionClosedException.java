package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class ConnectionClosedException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public ConnectionClosedException(final String c) {
		super(c);
	}
	
}
