package pap.ass05.cooperativeTeam;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

import pap.ass05.common.AbstractWorker;

public class Main {
	public static void main(final String[] args) {
		new Main();
	}
	
	/*
	 * Un team di 5 worker (w1..w5) lavorano concorrentemente condividendo 3 contatori (c1..c3) di classe UnsafeCounter classe (non AbstractWorker-safe). In particolare: W1 e W2 hanno il compito di
	 * incrementare rispettivamente c1 e c2, concorrentemente e ripetutamente. W3 ha il compito di stampare il valore di c1 ogni volta che viene aggiornato da W1 e quindi di incrementare c3.
	 * Analogamente W4 ha il compito di stampare il valore di c2 ogni volta che viene aggiornato da W2 e quindi di incrementare c3. Infine W5 ha il compito di stampare il valore di c3 solo dopo che è
	 * stato incrementato sia da W3 che W4. W1 e W2 possono procedere ad un nuovo incremento solo dopo che W5 ha stampato il valore di c3.
	 */
	private List<AbstractWorker>	workers;
	
	public Main() {
		config();
		start();
	}
	
	private void config() {
		workers = new LinkedList<AbstractWorker>();
		final UnsafeCounter c1 = new UnsafeCounter(0), c2 = new UnsafeCounter(0), c3 = new UnsafeCounter(0);
		final Semaphore /* s1 = new Semaphore(1), s2 = new Semaphore(1), */s3 = new Semaphore(1), // lock dei contatori, seguono lock per sincronizzazione
				s4 = new Semaphore(0), // sincronizza W1 e W3
				s5 = new Semaphore(0), // sincronizza W2 e W4
				s6 = new Semaphore(0), // sincronizza W3, W4 e W5
				s7 = new Semaphore(1), // sincronizza W1 e W5
				s8 = new Semaphore(1); // sincronizza W2 e W5
		
		final AbstractWorker w1 = new AbstractWorker("W1", () -> {
			try {
				// W1 e W2 possono procedere ad un nuovo incremento solo dopo che W5 ha stampato il valore di c3
				s7.acquire();
				// W1 e W2 hanno il compito di incrementare rispettivamente c1...
				// s1.acquire();
				c1.inc();
				System.out.println("[W1] c1++");
				// libera W3
				s4.release();
				// s1.release();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w1);
		
		final AbstractWorker w2 = new AbstractWorker("W2", () -> {
			try {
				// W1 e W2 possono procedere ad un nuovo incremento solo dopo che W5 ha stampato il valore di c3
				s8.acquire();
				// s2.acquire();
				c2.inc();
				System.out.println("[W2] c2++");
				// libera W4
				s5.release();
				// s2.release();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w2);
		
		final AbstractWorker w3 = new AbstractWorker("W3", () -> {
			try {
				// W3 ha il compito di stampare il valore di c1 ogni volta che viene aggiornato da W1 e quindi di incrementare c3
				s4.acquire();
				System.out.println("[W3] c1: " + c1.getValue());
				s3.acquire();
				c3.inc();
				// System.out.println("[W3] c3++");
				s3.release();
				s6.release();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w3);
		
		final AbstractWorker w4 = new AbstractWorker("W4", () -> {
			try {
				// Analogamente W4 ha il compito di stampare il valore di c2 ogni volta che viene aggiornato da W2 e quindi di incrementare c3
				s5.acquire();
				System.out.println("[W4] c2: " + c2.getValue());
				s3.acquire();
				c3.inc();
				// System.out.println("[W4] c3++");
				s3.release();
				s6.release();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w4);
		
		final AbstractWorker w5 = new AbstractWorker("W5", () -> {
			try {
				// Infine W5 ha il compito di stampare il valore di c3 solo dopo che è stato incrementato sia da W3 che W4
				s6.acquire(2);
				// s3.acquire();
				System.out.println("[W5] counter: " + c3.getValue());
				// s3.release();
				// libera w1
				s7.release();
				// libera W2
				s8.release();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w5);
	}
	
	private void start() {
		workers.forEach(w -> w.start());
	}
}
