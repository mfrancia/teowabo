package pap.ass05.cooperativeTeam;

public class UnsafeCounter {
	
	private int	cont;
	
	public UnsafeCounter(final int base) {
		cont = base;
	}
	
	public int getValue() {
		return cont;
	}
	
	public void inc() {
		cont++;
	}
}
