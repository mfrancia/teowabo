package pap.ass05.common;

import java.util.LinkedList;

public class WorkGroup extends LinkedList<IWorker> implements IWorkGroup {
	private static final long	serialVersionUID	= -61259995364306002L;
	
	@Override
	public boolean add(final IWorker worker) {
		worker.setWorkGroup(this);
		return super.add(worker);
	}
	
	@Override
	public void join() throws InterruptedException {
		forEach(worker -> {
			try {
				worker.join();
			} catch (final Exception ex) {
				ex.printStackTrace();
			}
		});
	}
	
	@Override
	public void start() {
		// new Thread(() -> {
		forEach(worker -> worker.start());
		// }).start();
	}
	
	@Override
	public void stop() {
		forEach(worker -> worker.stop());
	}
}
