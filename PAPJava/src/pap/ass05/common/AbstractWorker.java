package pap.ass05.common;

import java.util.Random;

/**
 * Astrazione di flusso di controllo
 * 
 * @author Matteo Francia
 *
 */
public class AbstractWorker implements IWorker {
	private Thread			behaviour;
	private final String	id;
	private Runnable		postlude;
	private Runnable		prelude;
	private final Random	r;
	private Boolean			stop;
	private IWorkGroup		workerGroup;
	
	/**
	 * Crea un worker senza indicare l'azione da ripetere
	 * 
	 * @param id
	 */
	public AbstractWorker(final String id) {
		this(id, null);
	}
	
	/**
	 * Crea un worker indicando id e behaviour (azione da ripetere)
	 * 
	 * @param id
	 * @param behaviour
	 */
	public AbstractWorker(final String id, final Runnable behaviour) {
		stop = false;
		this.id = id;
		r = new Random();
		if (behaviour != null) {
			setBehaviour(behaviour);
		}
	}
	
	public AbstractWorker(final String id, final Runnable behaviour, final Runnable prelude, final Runnable postlude) {
		stop = false;
		this.id = id;
		r = new Random();
		if (behaviour != null) {
			setBehaviour(prelude, behaviour, postlude);
		}
		
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public IWorkGroup getWorkGroup() {
		return workerGroup;
	}
	
	@Override
	public synchronized Boolean isStopped() {
		return stop;
	}
	
	@Override
	public void join() throws InterruptedException {
		behaviour.join();
	}
	
	protected void randomWait(final int maxWait) {
		try {
			Thread.sleep(r.nextInt(maxWait));
		} catch (final Exception e) {
			// e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		behaviour.run();
	}
	
	@Override
	public void setBehaviour(final Runnable behaviour) {
		if (this.behaviour != null) {
			throw new RuntimeException("Worker's behaviour cannot change over time");
		}
		
		this.behaviour = new Thread(() -> {
			if (prelude != null) {
				prelude.run();
			}
			while (!isStopped()) {
				behaviour.run();
			}
			if (postlude != null) {
				postlude.run();
				// System.out.println(getId());
			}
		});
	}
	
	@Override
	public void setBehaviour(final Runnable prelude, final Runnable behaviour, final Runnable postlude) {
		if (this.prelude != null) {
			throw new RuntimeException("Worker's prelude cannot change over time");
		}
		this.prelude = prelude;
		if (this.postlude != null) {
			throw new RuntimeException("Worker's postlude cannot change over time");
		}
		this.postlude = postlude;
		setBehaviour(behaviour);
	}
	
	@Override
	public void setWorkGroup(final IWorkGroup workerGroup) {
		this.workerGroup = workerGroup;
	}
	
	@Override
	public void start() {
		behaviour.start();
		// System.out.println("[" + id + "] started");
	}
	
	@Override
	public void stop() {
		stop = true;
		// System.out.println(System.currentTimeMillis() + " [" + id + "] stopped");
		behaviour.interrupt();
	}
}
