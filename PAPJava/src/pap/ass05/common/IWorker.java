package pap.ass05.common;

public interface IWorker {
	/**
	 * Ritorna l'identificatore del worker
	 * 
	 * @return
	 */
	String getId();
	
	/**
	 * Ritorna il gruppo di lavoro del worker
	 */
	IWorkGroup getWorkGroup();
	
	/**
	 * Indica se il behaviour è in azione o meno
	 */
	Boolean isStopped();
	
	void join() throws InterruptedException;
	
	/**
	 * Esegue il behaviour
	 */
	void run();
	
	/**
	 * Imposta il behaviour del worker. NB il behaviour specificato è ripetuto in loop fino a quando sul flusso di controllo non è eseguito il comando di stop (e inizia quando è eseguito il comando
	 * start)
	 * 
	 * @param behaviour
	 */
	void setBehaviour(Runnable behaviour);
	
	/**
	 * 
	 * @param prelude
	 *            behaviour da attuare prima del loop
	 * @param behaviour
	 *            behaviour del loop
	 * @param postlude
	 *            behaviour da attuare dopo il loop
	 */
	void setBehaviour(Runnable prelude, Runnable behaviour, Runnable postlude);
	
	/**
	 * Setta il gruppo di lavoro di questo worker
	 */
	void setWorkGroup(IWorkGroup workerGroup);
	
	/**
	 * Crea un nuovo flusso di esecuzione
	 */
	void start();
	
	/**
	 * Ferma il flusso di esecuzione
	 */
	void stop();
}
