package pap.ass05.common;

import java.util.List;

/**
 * Gruppo di lavoro, riunisce un insieme di worker che interagiscono per uno scopo comune
 * 
 * @author w4bo
 *
 */
public interface IWorkGroup extends List<IWorker> {
	@Override
	boolean add(IWorker worker);
	
	public void join() throws InterruptedException;
	
	/**
	 * Esegue tutti i worker
	 */
	void start();
	
	/**
	 * Termina tutti i worker
	 */
	void stop();
}
