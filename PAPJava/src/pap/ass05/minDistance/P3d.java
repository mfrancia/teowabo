package pap.ass05.minDistance;

/**
 * Punto nello spazio
 * 
 * @author Matteo Francia
 *
 */
public class P3d {
	private final Double	x, y, z;
	
	public P3d(final Double x, final Double y, final Double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public P3d(final Number value) {
		this(value.doubleValue(), value.doubleValue(), value.doubleValue());
	}
	
	public P3d(final Number x, final Number y, final Number z) {
		this(x.doubleValue(), y.doubleValue(), z.doubleValue());
	}
	
	public Double getEuclideanDistance(final P3d p) {
		return Math.sqrt(Math.pow(x - p.getX(), 2) + Math.pow(y - p.getY(), 2) + Math.pow(z - p.getZ(), 2));
	}
	
	public Double getX() {
		return x;
	}
	
	public Double getY() {
		return y;
	}
	
	public Double getZ() {
		return z;
	}
	
	@Override
	public String toString() {
		return "P3d(" + x + "," + y + "," + z + ")";
	}
}
