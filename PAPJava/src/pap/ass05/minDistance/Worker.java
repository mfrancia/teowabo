package pap.ass05.minDistance;

import java.util.stream.IntStream;

import pap.ass05.common.AbstractWorker;

public class Worker extends AbstractWorker {
	private P3d			closerPoint;
	private Double		min	= Double.MAX_VALUE;
	private final P3d	point;
	private final P3d[]	points;
	private final int[]	pp;
	private final int	start, end;
	
	public Worker(final String id, final P3d point, final int[] points, final int start, final int end) {
		super(id);
		pp = points;
		super.setBehaviour(() -> task3());
		this.start = start;
		this.end = end;
		this.point = point;
		this.points = null;
	}
	
	public Worker(final String id, final P3d point, final int[] points, final int start, final int end, final boolean parallelStream) {
		super(id);
		pp = points;
		if (!parallelStream) {
			super.setBehaviour(() -> task2());
		} else {
			super.setBehaviour(() -> task());
		}
		this.start = start;
		this.end = end;
		this.point = point;
		this.points = null;
	}
	
	public Worker(final String id, final P3d point, final P3d[] points, final int start, final int end, final boolean parallelStream) {
		super(id);
		this.point = point;
		this.points = points;
		this.start = start;
		this.end = end;
		pp = null;
		if (!parallelStream) {
			super.setBehaviour(() -> task4());
		} else {
			super.setBehaviour(() -> task5());
		}
		
	}
	
	public P3d getCloserPoint() {
		return closerPoint;
	}
	
	public Double getMin() {
		return min;
	}
	
	/**
	 * Behaviour con stream paralleli
	 */
	private void task() {
		System.out.println("[" + getId() + ".task1] started, points to check: " + (end - start));
		// ottengo le coordinate del punto di riferimento
		final double x = point.getX(), y = point.getY(), z = point.getZ();
		final long start = System.currentTimeMillis();
		// scorro l'array comune da start a end...
		// calcolo la distanza euclidea in modo dinamico
		// Questa soluzione non fa uso di p3d (scomoda ma performante)
		IntStream.range(this.start, end).parallel().forEach(value -> {
			final int v = pp[value];
			final double d = Math.sqrt(Math.pow(x - v, 2) + Math.pow(y - v, 2) + Math.pow(z - v, 2));
			if (d < min) {
				min = d;
				closerPoint = new P3d(value);
			}
		});
		System.out.println("[" + getId() + "] ended in " + (System.currentTimeMillis() - start) + "ms");
		super.stop();
	}
	
	/**
	 * Behaviour senza stream paralleli
	 */
	private void task2() {
		System.out.println("[" + getId() + ".task2] started, points to check: " + (end - start));
		// ottengo le coordinate del punto di riferimento
		final double x = point.getX(), y = point.getY(), z = point.getZ();
		final long start = System.currentTimeMillis();
		// scorro l'array comune da start a end...
		// calcolo la distanza euclidea in modo dinamico
		// Questa soluzione non fa uso di p3d (scomoda ma performante)
		IntStream.range(this.start, end).forEach(value -> {
			final int v = pp[value];
			final double d = Math.sqrt(Math.pow(x - v, 2) + Math.pow(y - v, 2) + Math.pow(z - v, 2));
			if (d < min) {
				min = d;
				closerPoint = new P3d(value);
			}
		});
		System.out.println("[" + getId() + "] ended in " + (System.currentTimeMillis() - start) + "ms");
		super.stop();
	}
	
	/**
	 * Behaviour senza stream paralleli, alloca in memoria i P3d
	 */
	private void task3() {
		System.out.println("[" + getId() + ".task3] started, points to check: " + (end - start));
		// scorro l'array comune da start a end...
		// calcolo la distanza euclidea dopo aver creato il P3d corrispondente a pp[value]
		// Allocare in memoria heap un numero elevato di oggetti è un'operazione costosa,
		// soprattutto in ambito concorrente. Inoltre allocare un numero elevato di oggetti
		// porta a problemi nella gestione della memoria stessa.
		final Long start = System.currentTimeMillis();
		IntStream.range(this.start, end).forEach(value -> {
			final int v = pp[value];
			final P3d p = new P3d(v);
			final double d = point.getEuclideanDistance(p);
			if (d < min) {
				min = d;
				closerPoint = p;
			}
		});
		System.out.println("[" + getId() + "] ended in " + (System.currentTimeMillis() - start) + "ms");
		super.stop();
	}
	
	/**
	 * Behaviour senza stream paralleli
	 */
	private void task4() {
		System.out.println("[" + getId() + ".task4] started, points to check: " + (end - start));
		final long start = System.currentTimeMillis();
		// scorro l'array comune da start a end...
		IntStream.range(this.start, end).forEach(value -> {
			final double d = point.getEuclideanDistance(points[value]);
			if (d < min) {
				min = d;
				closerPoint = points[value];
			}
		});
		System.out.println("[" + getId() + "] ended in " + (System.currentTimeMillis() - start) + "ms");
		super.stop();
	}
	
	/**
	 * Behaviour con stream paralleli
	 */
	private void task5() {
		System.out.println("[" + getId() + ".task5] started, points to check: " + (end - start));
		final long start = System.currentTimeMillis();
		// scorro l'array comune da start a end...
		IntStream.range(this.start, end).parallel().forEach(value -> {
			final double d = point.getEuclideanDistance(points[value]);
			if (d < min) {
				min = d;
				closerPoint = points[value];
			}
		});
		System.out.println("[" + getId() + "] ended in " + (System.currentTimeMillis() - start) + "ms");
		super.stop();
	}
	
}
