package pap.ass05.minDistance;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class MySystem3 {
	public static void main(final String[] args) {
		new MySystem3(Runtime.getRuntime().availableProcessors(), true);
	}
	
	private final boolean	flag;
	protected final int		LIMIT	= 1000000;
	protected final int		n;
	protected P3d			point;
	// private P3d[] points;
	protected P3d[]			points;
	/**
	 * Divisione del carico di lavoro dei worker
	 */
	protected final int		ratio;
	
	protected List<Worker>	workers;
	
	public MySystem3(final int processors) {
		this(processors, false);
	}
	
	public MySystem3(final int processors, final boolean parallelStream) {
		n = processors;
		flag = parallelStream;
		ratio = LIMIT / n;
		config();
		start();
	}
	
	public void config() {
		points = new P3d[LIMIT];
		// punto di riferimento
		point = new P3d(-1.0, -1.0, -1.0);
		IntStream.range(0, LIMIT).forEach(value -> {
			points[value] = new P3d(value);
		});
		workers = new LinkedList<Worker>();
		// creo n-1 worker, l'n-esimo worker sarà eseguito dal flusso di controllo del sistema stesso
		IntStream.range(0, n - 1).forEach(value -> {
			final Worker w = new Worker("Worker" + value, point, points, ratio * value, ratio * (value + 1), flag);
			workers.add(w);
		});
	}
	
	public void start() {
		// fa partire gli altri worker
		workers.forEach(worker -> worker.start());
		// crea l'n-esimo worker
		final Worker w = new Worker("Worker" + (n - 1), point, points, ratio * (n - 1), ratio * n, flag);
		// lo esegue senza instanziare un nuovo flusso di controllo
		w.run();
		// attende che tutti gli altri worker abbiano terminato
		workers.forEach(worker -> {
			try {
				worker.join();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		// System.out.println("List of minimums");
		workers.add(w);
		// workers.stream().mapToDouble(wr -> wr.getMin()).forEach(v -> System.out.println(v));
		final double min = workers.stream().mapToDouble(wr -> wr.getMin()).min().getAsDouble();
		final P3d p = workers.stream().filter(v -> v.getMin() == min).findFirst().get().getCloserPoint();
		System.out.println(p.toString() + " min dist: " + min + " is " + (min == point.getEuclideanDistance(new P3d(0.0, 0.0, 0.0)).doubleValue()));
	}
}
