package pap.ass05.minDistance;

import java.util.stream.IntStream;

/**
 * Main, 1 parametro: lancia il sistema con il numero specificato di worker (senza stream paralleli) senza parametri: lancia il test del sistema
 * 
 * @author Matteo Francia
 *
 */
public class Main {
	public static void main(final String[] args) {
		if (args.length == 1) {
			new MySystem(Integer.parseInt(args[0]), false);
		} else if (args.length == 0) {
			IntStream.range(1, Runtime.getRuntime().availableProcessors() + 1).forEach(value -> {
				System.out.println("Number of worker (without parallel stream): " + value);
				new MySystem(value, false);
				System.out.println("");
				System.out.println("");
				System.out.println("");
			});
			System.out.println("");
			System.out.println("-----------------------------------------------");
			System.out.println("");
			IntStream.range(1, Runtime.getRuntime().availableProcessors() + 1).forEach(value -> {
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("Number of worker (with parallel stream): " + value);
				new MySystem(value, true);
			});
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("-----------------------------------------------");
			System.out.println("");
			IntStream.range(1, Runtime.getRuntime().availableProcessors() + 1).forEach(value -> {
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("Number of worker (with P3d allocation): " + value);
				new MySystem2(value);
			});
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("-----------------------------------------------");
			System.out.println("");
			IntStream.range(1, Runtime.getRuntime().availableProcessors() + 1).forEach(value -> {
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("P3d array has been created before the start of worker's task, number of worker (without parallel stream): " + value);
				new MySystem3(value);
			});
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("-----------------------------------------------");
			System.out.println("");
			IntStream.range(1, Runtime.getRuntime().availableProcessors() + 1).forEach(value -> {
				System.out.println("");
				System.out.println("");
				System.out.println("");
				System.out.println("P3d array has been created before the start of worker's task, number of worker (with parallel stream): " + value);
				new MySystem3(value);
			});
			
		}
	}
}
