package pap.ass05.minDistance;

import java.util.LinkedList;
import java.util.stream.IntStream;

/**
 * Uguale a MySystem, ma configura il sistema con un insieme di worker che instanziano in memoria i P3d (worker -> task3())
 * 
 * @author Matteo Francia
 *
 */
public class MySystem2 extends MySystem {
	
	public MySystem2(final int processors) {
		super(processors);
	}
	
	@Override
	public void config() {
		points = new int[LIMIT];
		point = new P3d(-1.0, -1.0, -1.0);
		IntStream.range(0, LIMIT).forEach(value -> {
			points[value] = value;
		});
		workers = new LinkedList<Worker>();
		IntStream.range(0, n - 1).forEach(value -> {
			final Worker w = new Worker("Worker" + value, point, points, ratio * value, ratio * (value + 1));
			workers.add(w);
		});
	}
	
	@Override
	public void start() {
		workers.forEach(worker -> worker.start());
		final Worker w = new Worker("Worker" + (n - 1), point, points, ratio * (n - 1), ratio * n);
		w.run();
		workers.forEach(worker -> {
			try {
				worker.join();
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		workers.add(w);
		final double min = workers.stream().mapToDouble(wr -> wr.getMin()).min().getAsDouble();
		final P3d p = workers.stream().filter(v -> v.getMin() == min).findFirst().get().getCloserPoint();
		System.out.println(p.toString() + " min dist: " + min + " is " + (min == point.getEuclideanDistance(new P3d(0.0, 0.0, 0.0)).doubleValue()));
	}
}
