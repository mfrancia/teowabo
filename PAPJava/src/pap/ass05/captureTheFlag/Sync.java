package pap.ass05.captureTheFlag;

public class Sync implements ISync {
	
	private final int	maxTurn;
	private int			turn;
	
	public Sync(final int startTurn, final int maxTurn) {
		turn = startTurn;
		this.maxTurn = maxTurn;
	}
	
	@Override
	public synchronized void next() {
		turn = (turn + 1) % maxTurn;
		// System.out.println("Next turn is: " + turn);
		notifyAll();
	}
	
	@Override
	public synchronized void waitForTurn(final int turn) {
		while (this.turn != turn) {
			try {
				wait();
			} catch (final InterruptedException e) {
				// e.printStackTrace();
			}
		}
		
	}
	
}
