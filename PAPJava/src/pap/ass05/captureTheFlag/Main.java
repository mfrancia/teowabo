package pap.ass05.captureTheFlag;

import java.util.stream.IntStream;

import pap.ass05.common.IWorkGroup;
import pap.ass05.common.WorkGroup;

public class Main {
	
	public static void main(final String[] args) {
		if (args.length > 0) {
			new Main(Integer.parseInt(args[0]));
		} else {
			new Main(Runtime.getRuntime().availableProcessors());
		}
	}
	
	private final int	n;
	
	private IWorkGroup	wg;
	
	public Main(final int players) {
		n = players;
		config();
		start();
	}
	
	private void config() {
		wg = new WorkGroup();
		final IFlag flag = new Flag();
		final ISync sync = new Sync(0, n);
		wg.add(Arbiter.getInstance(flag));
		IntStream.range(0, n).forEach(v -> wg.add(new Player("Player" + v, sync, v, flag)));
	}
	
	private void start() {
		wg.start();
	}
}
