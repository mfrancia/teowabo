package pap.ass05.captureTheFlag;

/**
 * Il monitor Flag che rappresenta una bandierina, che può essere in 2 stati: alzata o abbassata.
 * 
 * @author Matteo Francia
 *
 */
public interface IFlag {
	/**
	 * restituisce true se la bandiera è alzata, false altrimenti
	 * 
	 * @return
	 */
	boolean capture();
	
	/**
	 * cambia lo stato in alzata
	 */
	void setHigh();
	
	/**
	 * cambia lo stato in abbassata
	 */
	void setLow();
}
