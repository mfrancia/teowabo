package pap.ass05.captureTheFlag;

/**
 * 
 * @author Matteo Francia
 *
 */
public interface ISync {
	/**
	 * cede il turno al player successivo
	 */
	void next();
	
	/**
	 * chiamata dal player i-esimo, sospende il player fin quando non è il suo turno
	 * 
	 * @param turn
	 */
	void waitForTurn(int turn);
}
