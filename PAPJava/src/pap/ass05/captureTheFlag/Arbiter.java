package pap.ass05.captureTheFlag;

import pap.ass05.common.AbstractWorker;

public class Arbiter extends AbstractWorker {
	public static Arbiter getInstance(final IFlag flag) {
		return new Arbiter(flag);
	}
	
	private Arbiter(final IFlag flag) {
		super("Arbiter");
		
		setBehaviour(() -> {
			randomWait(2000);
			System.out.println(getId() + " flag: HIGH");
			flag.setHigh();
			randomWait(500);
			System.out.println(getId() + " flag: LOW");
			flag.setLow();
		});
	}
	
}
