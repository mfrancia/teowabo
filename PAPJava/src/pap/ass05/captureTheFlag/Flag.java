package pap.ass05.captureTheFlag;

public class Flag implements IFlag {
	private boolean	flag;
	
	@Override
	public synchronized boolean capture() {
		return flag;
	}
	
	@Override
	public synchronized void setHigh() {
		flag = true;
	}
	
	@Override
	public synchronized void setLow() {
		flag = false;
	}
	
}
