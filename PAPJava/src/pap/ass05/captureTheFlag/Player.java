package pap.ass05.captureTheFlag;

import pap.ass05.common.AbstractWorker;

public class Player extends AbstractWorker {
	public Player(final String id, final ISync sync, final int turn, final IFlag flag) {
		super(id);
		setBehaviour(() -> {
			sync.waitForTurn(turn);
			sync.next();
		}, // barriera di sincronizzazione, attendo che tutti i giocatori siano pronti
		() -> {
			sync.waitForTurn(turn);
			if (!isStopped()) {
				System.out.println(getId() + " it's my turn");
			}
			if (flag.capture()) {
				System.out.println(id + " WON!");
				getWorkGroup().forEach(w -> {
					if (!w.getId().equals(id)) {
						w.stop();
					}
				});
				super.stop();
			}
			super.randomWait(500);
			sync.next();
		}, null);
	}
	
	@Override
	public void stop() {
		super.stop();
		System.out.println(getId() + " SOB");
	}
}
