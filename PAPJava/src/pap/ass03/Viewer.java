package pap.ass03;

import javax.swing.JFrame;

// Definire la classe Viewer che permette di visualizzare una lista di Shape in una viewport grafica (basata su un qualsiasi GUI toolkit, es: Swing). La classe deve implementare l�interfaccia
// ShapeViewer contenuta nel package pap.ass03, caratterizzata dai seguenti metodi
// update(List<Shape> shapes)
// aggiorna l�insieme delle figure da visualizzare
// La visualizzazione consiste nel disegno delle figure nella viewport della finestra.
// La classe Viewer deve inoltre implementare una semplice funzionalit� di selezione delle figure con il mouse, ovvero: clickando in un punto della viewport contenuto in una o pi� shape, le shape in
// questione devono essere disegnate di colore diverso (o spessore diverso).
// Suggerimento: fare riferimento all�evento MouseEvent dei componenti Swing
// Implementare un programma TestShapeViewer che permetta di testare le funzionalit� della classe Utils.

public abstract class Viewer extends JFrame implements ShapeViewer {
	private static final long	serialVersionUID	= -516150505564686248L;
	
	public Viewer() {
		super("PAP - Ass03");
		config();
	}
	
	private void config() {
		setBounds(500, 500, 500, 500);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
}
