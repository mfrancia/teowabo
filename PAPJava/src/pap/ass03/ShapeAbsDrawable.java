package pap.ass03;

import java.awt.Graphics;

public abstract class ShapeAbsDrawable extends ShapeAbs {
	public ShapeAbsDrawable(final String id, final P2d... points) {
		super(id, points);
	}
	
	public abstract void draw(Graphics g);
}
