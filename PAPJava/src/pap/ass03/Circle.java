package pap.ass03;

import java.awt.Graphics;

public class Circle extends ShapeAbsDrawable {
	private final P2d		center;
	private final Integer	radius;
	
	public Circle(final Integer radius, final P2d center) {
		super("CIRCLE", new P2d[] { center });
		this.radius = radius;
		this.center = center;
	}
	
	@Override
	public boolean contains(final P2d p) {
		return (Math.pow(p.getX() - center.getX(), 2) + Math.pow(p.getY() - center.getY(), 2)) <= Math.pow(radius, 2);
	}
	
	@Override
	public void draw(final Graphics g) {
		g.drawOval(center.getX() - radius, center.getY() - radius, radius * 2, radius * 2);
	}
	
	@Override
	public BBox getBBox() {
		final Integer x = center.getX(), y = center.getY();
		return new BBox(new P2d(x - radius, y - radius), new P2d(x + radius, y + radius));
	}
	
	@Override
	public double getPerim() {
		return radius * 2 * Math.PI;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + radius;
	}
}
