package pap.ass03;

/**
 * Vettore in una viewport grafica
 * 
 * @author aricci
 */
public class V2d {
	
	private final int	x, y;
	
	public V2d(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	
	public V2d(final P2d p1, final P2d p0) {
		x = p1.getX() - p0.getX();
		y = p1.getY() - p0.getY();
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public double module() {
		return Math.sqrt((x * x) + (y * y));
	}
	
	public V2d sum(final V2d v) {
		return new V2d(x + v.x, y + v.y);
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
}
