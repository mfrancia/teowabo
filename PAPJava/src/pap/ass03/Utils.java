package pap.ass03;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

public class Utils {
	public static List<LinkedList<Shape>> filterGetContaining(final P2d p, final Shape... shapes) {
		final List<LinkedList<Shape>> ret = new LinkedList<LinkedList<Shape>>();
		ret.add(new LinkedList<>());
		ret.add(new LinkedList<>());
		Arrays.asList(shapes).stream().forEach(shape -> {
			if (shape.contains(p)) {
				ret.get(0).add(shape);
			} else {
				ret.get(1).add(shape);
			}
		});
		return ret;
		
	}
	
	/**
	 * data una lista di figure e un punto p, computa la lista delle figure che contengono il punto
	 * 
	 * @param p
	 * @param shapes
	 * @return
	 */
	public static List<Shape> getContaining(final P2d p, final Shape... shapes) {
		return Arrays.asList(shapes).stream().filter(shape -> shape.contains(p)).collect(Collectors.toCollection(LinkedList::new));
	}
	
	/**
	 * data una lista di figure e una coppia di vertici p0 p1, computa la lista delle figure contenute nel bounding box p0 p1
	 * 
	 * @param p0
	 * @param p1
	 * @param shapes
	 */
	public static List<Shape> inBBox(final P2d p0, final P2d p1, final Shape... shapes) {
		final BBox bbox = new BBox(p0, p1);
		return Arrays.asList(shapes).stream().filter(shape -> shape.isInside(bbox)).collect(Collectors.toCollection(LinkedList::new));
	}
	
	/**
	 * data una lista di figure, verifica se esiste una figura che contiene il punto passato come parametro
	 */
	public static Boolean isContained(final P2d p, final Shape... shapes) {
		return Arrays.asList(shapes).stream().anyMatch(shape -> shape.contains(p));
	}
	
	/**
	 * data una lista di figure, le stampa in uscita
	 * 
	 * @param shapes
	 */
	public static void logAll(final Shape... shapes) {
		Arrays.asList(shapes).forEach(shape -> System.out.println(shape.toString()));
	}
	
	/**
	 * data una lista di figure, determina il perimetro maggiore
	 * 
	 * @param shapes
	 * @return
	 */
	public static Double maxPerim(final Shape... shapes) {
		return shapeWithMaxPerim(shapes).getPerim();
	}
	
	/**
	 * data una lista di figure, computa la lista di figure traslate di un vettore dv
	 */
	public static void moveShapes(final V2d v, final Shape... shapes) {
		Arrays.asList(shapes).forEach(shape -> shape.move(v));
	}
	
	/**
	 * data una lista di figure, determina la figura con perimetro maggiore
	 * 
	 * @param shapes
	 * @return
	 */
	public static Shape shapeWithMaxPerim(final Shape... shapes) {
		return Arrays.asList(shapes).stream().max(Comparator.comparing(Shape::getPerim)).get();
	}
	
	/**
	 * data una lista di figure, produce una nuova lista con le figure ordinate lungo l'asse x
	 * 
	 * @param shapes
	 * @return
	 */
	public static List<Shape> sortShapesByX(final Shape... shapes) {
		final List<Shape> ret = Arrays.asList(shapes);
		ret.sort((s1, s2) -> (s1.getBBox().getUpperLeft().getX() < s2.getBBox().getUpperLeft().getX()) ? 1 : 0);
		return ret;
	}
}
