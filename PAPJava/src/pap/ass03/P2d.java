package pap.ass03;

/**
 * Punto in una viewport grafica
 * 
 * @author aricci
 */
public class P2d {
	
	public static double distance(final P2d p0, final P2d p1) {
		return new V2d(p0, p1).module();
	}
	
	private final int	x, y;
	
	public P2d(final int x, final int y) {
		this.x = x;
		this.y = y;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public P2d sum(final V2d v) {
		return new P2d(x + v.getX(), y + v.getY());
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
}
