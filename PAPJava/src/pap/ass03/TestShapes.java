package pap.ass03;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;

public class TestShapes {
	private Circle	circle;
	private Combo	combo;
	private Line	line;
	private Rect	rect;
	
	@org.junit.Test
	public void contains() {
		final P2d p0 = new P2d(1, 1), p1 = new P2d(-1, -1);
		assertTrue(line.contains(p0) && !line.contains(p1) && circle.contains(p0) && !circle.contains(p1) && rect.contains(p0) && !rect.contains(p1) && combo.contains(p0) && !combo.contains(p1));
	}
	
	@org.junit.Test
	public void getContaining() {
		final P2d p0 = new P2d(1, 1), p1 = new P2d(-1, -1);
		assertTrue((Utils.getContaining(p0, line, rect, circle, combo, new Line(new P2d(3, 3), new P2d(4, 4))).size() == 4) && (Utils.getContaining(p1, line, circle, rect, combo).size() == 0));
	}
	
	@org.junit.Test
	public void getPerim() {
		assertTrue(combo.getPerim() == (Math.sqrt(2) + 12 + (Math.PI * 2 * 2)));
	}
	
	@org.junit.Test
	public void isContained() {
		final P2d p0 = new P2d(1, 1), p1 = new P2d(-1, -1);
		assertTrue(Utils.isContained(p0, combo) && !Utils.isContained(p1, line, circle, rect));
	}
	
	@org.junit.Test
	public void isInside() {
		assertTrue(line.isInside(new BBox(new P2d(0, 0), new P2d(1, 1))) && !line.isInside(new BBox(new P2d(1, 1), new P2d(2, 2))) && rect.isInside(new BBox(new P2d(1, 1), new P2d(5, 5)))
				&& !rect.isInside(new BBox(new P2d(1, 1), new P2d(2, 2))) && circle.isInside(new BBox(new P2d(0, 0), new P2d(4, 4))) && !circle.isInside(new BBox(new P2d(1, 1), new P2d(2, 2)))
				&& combo.isInside(new BBox(new P2d(0, 0), new P2d(4, 4))) && !combo.isInside(new BBox(new P2d(1, 1), new P2d(2, 2))));
	}
	
	@org.junit.Test
	public void logAll() {
		Utils.logAll(line, circle, rect, combo);
		assertTrue(true);
	}
	
	@Before
	public void make() {
		line = new Line(new P2d(0, 0), new P2d(1, 1));
		rect = new Rect(new P2d(1, 1), new P2d(4, 4));
		circle = new Circle(2, new P2d(2, 2));
		combo = new Combo(line, rect, circle);
	}
	
	@org.junit.Test
	public void maxPerim() {
		assertTrue(Utils.maxPerim(circle, line, rect, combo) == combo.getPerim());
	}
	
	@org.junit.Test
	public void move() {
		final V2d inc = new V2d(1, 1);
		line.move(inc);
		circle.move(inc);
		assertTrue((line.getPoints().get(0).getX() == 1) && (line.getPoints().get(0).getY() == 1) && (circle.getPoints().get(0).getX() == 3) && (circle.getPoints().get(0).getY() == 3));
	}
	
	@org.junit.Test
	public void moveShapes() {
		Utils.moveShapes(new V2d(1, 1), line, circle);
		assertTrue((line.getPoints().get(0).getX() == 1) && (line.getPoints().get(0).getY() == 1) && (circle.getPoints().get(0).getX() == 3) && (circle.getPoints().get(0).getY() == 3));
	}
	
	@org.junit.Test
	public void shapeWithMaxPerim() {
		assertTrue(Utils.shapeWithMaxPerim(circle, line, rect, combo) == combo);
	}
	
	@org.junit.Test
	public void sortShapesByX() {
		final List<Shape> ret = Utils.sortShapesByX(circle, line, rect);
		assertTrue((ret.get(0) == circle) && (ret.get(1) == line) && (ret.get(2) == rect));
	}
	
}
