package pap.ass03;

import java.awt.Graphics;
import java.util.Arrays;
import java.util.List;

public class Combo extends ShapeAbsDrawable {
	private final List<Shape>	shapes;
	
	public Combo(final Shape... shapes) {
		super("COMBO");
		this.shapes = Arrays.asList(shapes);
	}
	
	@Override
	public boolean contains(final P2d p) {
		return shapes.stream().anyMatch(shape -> shape.contains(p));
	}
	
	@Override
	public void draw(final Graphics g) {
		shapes.forEach(shape -> ((ShapeAbsDrawable) shape).draw(g));
	}
	
	@Override
	public double getPerim() {
		return shapes.stream().mapToDouble(Shape::getPerim).sum();
	}
	
	@Override
	public boolean isInside(final BBox box) {
		return !shapes.stream().anyMatch(shape -> !shape.isInside(box));
	}
	
	@Override
	public void move(final V2d v) {
		shapes.forEach(shape -> shape.move(v));
	}
	
	@Override
	public String toString() {
		return getId() + " \n" + shapes.stream().map(shape -> shape.toString()).reduce("", (a, b) -> a + "\t" + b + " \n");
	}
	
}
