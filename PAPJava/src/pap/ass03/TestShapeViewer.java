package pap.ass03;

public class TestShapeViewer {
	
	public static void main(final String[] args) {
		new ViewerDrawable(
		// house
				new Rect(new P2d(80, 370), new P2d(140, 450)), // door
				new Rect(new P2d(10, 250), new P2d(210, 450)), // building
				new Rect(new P2d(40, 275), new P2d(180, 325)), // window
				new Line(new P2d(210, 250), new P2d(110, 100)), new Line(new P2d(10, 250), new P2d(110, 100)), new Combo( // tree
						new Circle(50, new P2d(270, 250)), new Circle(50, new P2d(370, 250)), new Circle(50, new P2d(320, 164)), new Rect(new P2d(270, 300), new P2d(370, 450))));
	}
	
}
