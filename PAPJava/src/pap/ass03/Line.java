package pap.ass03;

import java.awt.Graphics;

public class Line extends ShapeAbsDrawable {
	private final P2d	p0, p1;
	
	public Line(final P2d... points) {
		super("LINE", points);
		p0 = points[0];
		p1 = points[1];
	}
	
	@Override
	public boolean contains(final P2d p) {
		// * 1.0 rende la selezione della linea "troppo" precisa (nonostante sia la soluzione corretta)
		return super.contains(p) && ((((p.getY() - p0.getY()) /** 1.0 */
				) / (p1.getY() - p0.getY())) == (((p.getX() - p0.getX()) /** 1.0 */
						) / (p1.getX() - p0.getX())));
	}
	
	@Override
	public void draw(final Graphics g) {
		g.drawLine(p0.getX(), p0.getY(), p1.getX(), p1.getY());
	}
	
}
