package pap.ass03;

import java.awt.Graphics;

public class Rect extends ShapeAbsDrawable {
	private final P2d	leftup, rightbottom;
	
	public Rect(final P2d... points) {
		super("RECT", points);
		leftup = points[0];
		rightbottom = points[1];
	}
	
	@Override
	public void draw(final Graphics g) {
		g.drawRect(leftup.getX(), leftup.getY(), Math.abs(rightbottom.getX() - leftup.getX()), Math.abs(rightbottom.getY() - leftup.getY()));
	}
	
	@Override
	public double getPerim() {
		final Integer a = leftup.getX(), b = leftup.getY(), x = rightbottom.getX(), y = rightbottom.getY();
		return P2d.distance(new P2d(a, b), new P2d(a, y)) + P2d.distance(new P2d(x, b), new P2d(x, y)) + P2d.distance(new P2d(a, b), new P2d(x, b)) + P2d.distance(new P2d(a, y), new P2d(x, y));
	}
	
}
