package pap.ass03;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ViewerDrawable extends Viewer {
	private static final long	serialVersionUID	= 3745622051675152823L;
	private final JPanel		content;
	private Graphics2D			g2;
	private final JLabel		lbl;
	private final List<Shape>	shapes;
	
	public ViewerDrawable(final ShapeAbsDrawable... shapes) {
		super();
		this.shapes = Arrays.asList(shapes);
		content = new JPanel();
		setContentPane(content);
		content.addMouseListener(new MouseAdapter() {
			
			@Override
			public void mouseClicked(final MouseEvent arg0) {
				final List<LinkedList<Shape>> tmp = Utils.filterGetContaining(new P2d(arg0.getX(), arg0.getY()), shapes);
				g2.setColor(Color.red);
				update(tmp.get(0));
				
				final StringBuilder sb = new StringBuilder();
				tmp.get(0).forEach(shape -> sb.append(shape.toString()));
				
				try {
					SwingUtilities.invokeAndWait(() -> {
					});
				} catch (final Exception e) {
					e.printStackTrace();
				}
				
				// SwingUtilities.invokeLater(new Runnable() {
				// @Override
				// public void run() {
				// lbl.setText(sb.toString());
				// g2.setColor(Color.black);
				// update(tmp.get(1));
				// }
				// });
				
				SwingUtilities.invokeLater(() -> {
					
					lbl.setText(sb.toString());
					g2.setColor(Color.black);
					update(tmp.get(1));
				});
			}
		});
		
		final JButton btDraw = new JButton("(re)draw");
		btDraw.addActionListener(actionListener -> {
			g2.setColor(Color.BLACK);
			update(this.shapes);
		});
		content.add(btDraw);
		
		lbl = new JLabel("click on the figure...");
		lbl.setFont(new Font(lbl.getFont().getName(), Font.ITALIC, 9));
		content.add(lbl);
		
		update(this.shapes);
	}
	
	@Override
	public void update(final List<Shape> shapes) {
		shapes.forEach(shape -> {
			SwingUtilities.invokeLater(() -> {
				if (g2 == null) {
					g2 = (Graphics2D) content.getGraphics();
				}
				((ShapeAbsDrawable) shape).draw(g2);
			});
		});
	}
}
