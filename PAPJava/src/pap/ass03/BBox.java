package pap.ass03;

/**
 * Class representing a bounding box
 * 
 * @author aricci
 */
public class BBox {
	
	private final P2d	upperLeft, bottomRight;
	
	/**
	 * Build a bbox
	 * 
	 * @param upperLeft
	 *            -- upper left-corner
	 * @param bottomRight
	 *            -- bottom-right corner
	 */
	public BBox(final P2d upperLeft, final P2d bottomRight) {
		this.upperLeft = upperLeft;
		this.bottomRight = bottomRight;
	}
	
	public P2d getBottomRight() {
		return bottomRight;
	}
	
	public P2d getUpperLeft() {
		return upperLeft;
	}
	
}
