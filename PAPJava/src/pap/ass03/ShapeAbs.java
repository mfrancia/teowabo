package pap.ass03;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class ShapeAbs implements Shape {
	private final String	id;
	private List<P2d>		points;
	
	public ShapeAbs(final String id, final P2d... points) {
		this.points = Arrays.asList(points);
		this.id = id;
	}
	
	@Override
	public boolean contains(final P2d p) {
		// verifico che sia all'interno del BBox, caso piu' generico
		// poi specializzo figura per figura
		final BBox b = getBBox();
		return (p.getX() >= b.getUpperLeft().getX()) && (p.getX() <= b.getBottomRight().getX()) && (p.getY() >= b.getUpperLeft().getY()) && (p.getY() <= b.getBottomRight().getY());
	}
	
	@Override
	public BBox getBBox() {
		P2d point = points.get(0);
		Integer xmin = point.getX(), xmax = point.getX(), ymin = point.getY(), ymax = point.getY(), x, y;
		for (int i = 1; i < points.size(); i++) {
			point = points.get(i);
			x = point.getX();
			y = point.getY();
			if (x < xmin) {
				xmin = x;
			} else if (x > xmax) {
				xmax = x;
			}
			if (y < ymin) {
				ymin = y;
			} else if (y > ymax) {
				ymax = y;
			}
		}
		return new BBox(new P2d(xmin, ymin), new P2d(xmax, ymax));
	}
	
	public String getId() {
		return id;
	}
	
	@Override
	public double getPerim() {
		// return points.stream().mapToDouble((P2d p1, P2d p2) -> P2d.distance(points.get(p1), points.get(p2))).sum();
		double ret = 0;
		for (int i = 1; i < points.size(); i++) {
			ret += P2d.distance(points.get(i - 1), points.get(i));
		}
		return ret;
	}
	
	public List<P2d> getPoints() {
		return points;
	}
	
	@Override
	public boolean isInside(final BBox box) {
		final BBox b = getBBox();
		return (b.getUpperLeft().getX() >= box.getUpperLeft().getX()) && (b.getBottomRight().getX() <= box.getBottomRight().getX()) && (b.getUpperLeft().getY() >= box.getUpperLeft().getY())
				&& (b.getBottomRight().getY() <= box.getBottomRight().getY());
	}
	
	@Override
	public void move(final V2d v) {
		final List<P2d> ret = new LinkedList<P2d>();
		points.forEach(point -> ret.add(point.sum(v)));
		points = ret;
	}
	
	@Override
	public String toString() {
		return points.stream().map(point -> point.toString()).reduce(id, (a, b) -> a + " " + b);
		// String ret = id + " ";
		// for (final P2d point : points) {
		// ret += point.toString() + " ";
		// }
		// return ret;
	}
}
