package pap.ass07;

public class Turn implements ITurn {
	private final boolean	lost;
	private final int		turn;
	
	public Turn() {
		lost = true;
		turn = -1;
	}
	
	public Turn(final int turn) {
		lost = false;
		this.turn = turn;
	}
	
	@Override
	public boolean lost() {
		return lost;
	}
	
	@Override
	public int nextTurn() {
		return turn;
	}
	
}
