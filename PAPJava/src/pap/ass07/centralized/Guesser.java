package pap.ass07.centralized;

import java.util.Random;

import pap.ass07.Guess;
import pap.ass07.IResponse;
import pap.ass07.IResponse.Compare;
import pap.ass07.ITurn;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class Guesser extends UntypedActor {
	private int				number;
	private final ActorRef	oracle;
	private final int		turn;
	
	public Guesser(final Integer turn, final ActorRef oracle) {
		this.turn = turn;
		this.oracle = oracle;
		number = new Random().nextInt(Main.limit);
		getContext().become(this::waiting);
	}
	
	public void guessing(final Object arg0) throws Exception {
		if (arg0 instanceof IResponse) {
			getContext().become(this::waiting);
			final IResponse r = (IResponse) arg0;
			if (r.compare() == Compare.equal) {
				System.out.println("[Guesser" + turn + "] WIN!");
				getContext().stop(getSelf());
			} else if (r.compare() == Compare.less) {
				number++;
			} else {
				number--;
			}
		} else {
			unhandled(arg0);
		}
		
	}
	
	@Override
	public void onReceive(final Object arg0) throws Exception {
	}
	
	public void waiting(final Object arg0) throws Exception {
		if (arg0 instanceof ITurn) {
			final ITurn t = (ITurn) arg0;
			if (t.lost()) {
				System.out.println("[Guesser" + turn + "] SOB!");
				getContext().stop(getSelf());
			} else if (t.nextTurn() == turn) {
				getContext().become(this::guessing);
				System.out.println("[Guesser" + turn + "] guessing number: " + number);
				oracle.tell(new Guess(number), getSelf());
			}
		} else {
			unhandled(arg0);
		}
	}
}
