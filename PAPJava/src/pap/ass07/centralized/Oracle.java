package pap.ass07.centralized;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import pap.ass07.IGuess;
import pap.ass07.IResponse.Compare;
import pap.ass07.Response;
import pap.ass07.Turn;
import pap.ass07.naiveCentralized.Guesser;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Oracle extends UntypedActor {
	private int				count;
	private List<ActorRef>	list;
	private int				n;
	private int				number;
	private int				turn;
	
	@Override
	public void onReceive(final Object message) {
		if (message instanceof IGuess) {
			final IGuess msg = (IGuess) message;
			final int c = Integer.compare(number, msg.getGuess());
			if (c == 0) {
				getSender().tell(new Response(Compare.equal), getSelf());
				int i = n;
				while (i-- > 0) {
					list.get(turn).tell(new Turn(), getSelf());
					turn = (turn + 1) % n;
				}
				getContext().stop(getSelf());
			} else if (c > 0) {
				getSender().tell(new Response(Compare.less), getSelf());
			} else {
				getSender().tell(new Response(Compare.great), getSelf());
			}
			if (turn == 0) {
				System.out.println("--- Turn: " + count++);
			}
			list.get(turn).tell(new Turn(turn), getSelf());
			turn = (turn + 1) % n;
		} else {
			unhandled(message);
		}
	}
	
	@Override
	public void preStart() throws Exception {
		count = 0;
		n = Main.n;
		list = new LinkedList<ActorRef>();
		IntStream.range(0, n).forEachOrdered(v -> {
			list.add(getContext().actorOf(Props.create(Guesser.class, v, getSelf()), "guesser" + v));
		});
		number = new Random().nextInt(Main.limit);
		System.out.println("[Oracle] game started: " + number);
		Thread.sleep(1000);
		turn = 0;
		System.out.println("--- Turn: " + count++);
		list.get(turn).tell(new Turn(turn++), getSelf());
	}
}