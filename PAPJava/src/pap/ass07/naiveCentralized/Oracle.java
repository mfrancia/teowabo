package pap.ass07.naiveCentralized;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import pap.ass07.IGuess;
import pap.ass07.IResponse.Compare;
import pap.ass07.Response;
import pap.ass07.Turn;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Oracle extends UntypedActor {
	private int				counter;
	private List<ActorRef>	list;
	private int				n;
	private int				number;
	private int				turn;
	
	private void notifyall() {
		list.forEach(guesser -> guesser.tell(new Turn(0), getSelf()));
	}
	
	@Override
	public void onReceive(final Object message) {
		if (message instanceof IGuess) {
			final IGuess msg = (IGuess) message;
			final int c = Integer.compare(number, msg.getGuess());
			if (c == 0) {
				getSender().tell(new Response(Compare.equal), getSelf());
				list.forEach(guesser -> guesser.tell(new Turn(), getSelf()));
				getContext().stop(getSelf());
			} else if (c > 0) {
				getSender().tell(new Response(Compare.less), getSelf());
			} else {
				getSender().tell(new Response(Compare.great), getSelf());
			}
			if (++counter == n) {
				counter = 0;
				System.out.println("--- Turn: " + turn++);
				notifyall();
			}
		} else {
			unhandled(message);
		}
	}
	
	@Override
	public void preStart() throws Exception {
		turn = 0;
		counter = 0;
		n = Main.n;
		list = new LinkedList<ActorRef>();
		IntStream.range(0, n).forEachOrdered(v -> {
			list.add(getContext().actorOf(Props.create(Guesser.class, v, getSelf()), "guesser" + v));
		});
		number = new Random().nextInt(Main.limit);
		System.out.println("[Oracle] game started: " + number);
		Thread.sleep(1000);
		System.out.println("--- Turn: " + turn++);
		notifyall();
	}
}