package pap.ass07;

public class Response implements IResponse {
	private final Compare	c;
	
	public Response(final Compare c) {
		this.c = c;
	}
	
	@Override
	public Compare compare() {
		return c;
	}
	
}
