package pap.ass07.tokenring2;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import pap.ass07.IGuess;
import pap.ass07.IResponse.Compare;
import pap.ass07.Response;
import pap.ass07.Turn;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

public class Oracle extends UntypedActor {
	private List<ActorRef>	list;
	private int				number;
	
	@Override
	public void onReceive(final Object message) {
		if (message instanceof IGuess) {
			final IGuess msg = (IGuess) message;
			final int c = Integer.compare(number, msg.getGuess());
			if (c == 0) {
				getSender().tell(new Response(Compare.equal), getSelf());
				list.forEach(g -> g.tell(new Turn(), getSelf()));
				getContext().stop(getSelf());
			} else if (c > 0) {
				getSender().tell(new Response(Compare.less), getSelf());
			} else {
				getSender().tell(new Response(Compare.great), getSelf());
			}
		} else {
			unhandled(message);
		}
	}
	
	@Override
	public void preStart() throws Exception {
		final int n = Main.n;
		list = new LinkedList<ActorRef>();
		list.add(getContext().actorOf(Props.create(Guesser.class, n - 1, n, getSelf()), "guesser" + (n - 1)));
		
		IntStream.range(1, n).forEachOrdered(v -> {
			final ActorRef ref = getContext().actorOf(Props.create(Guesser.class, n - v - 1, n, getSelf()), "guesser" + (n - v - 1));
			list.add(ref);
			ref.tell(list.get(v - 1), getSelf());
		});
		list.get(0).tell(list.get(list.size() - 1), getSelf());
		number = new Random().nextInt(Main.limit);
		System.out.println("[Oracle] game started: " + number);
		Thread.sleep(1000);
		list.get(list.size() - 1).tell(new Turn(0), getSelf());
	}
}