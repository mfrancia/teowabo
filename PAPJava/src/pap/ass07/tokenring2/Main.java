package pap.ass07.tokenring2;

public class Main {
	public static final int	limit	= 1000;
	public static int		n;
	
	public static void main(final String[] args) {
		if (args.length > 0) {
			n = Integer.parseInt(args[0]);
		} else {
			n = 10;
		}
		akka.Main.main(new String[] { Oracle.class.getName() });
	}
}
