package pap.ass07;

/**
 * Player's turn
 * 
 * @author Matteo Francia
 *
 */
public interface ITurn extends IMessage {
	boolean lost();
	
	int nextTurn();
}
