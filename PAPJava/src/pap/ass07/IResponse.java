package pap.ass07;

/**
 * Response to the guess of the player
 * 
 * @author Matteo Francia
 *
 */
public interface IResponse extends IMessage {
	public enum Compare {
		equal, great, less
	};
	
	Compare compare();
}
