package pap.ass07.tokenring;

import java.util.Random;

import pap.ass07.Guess;
import pap.ass07.IResponse;
import pap.ass07.IResponse.Compare;
import pap.ass07.ITurn;
import pap.ass07.Turn;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class Guesser extends UntypedActor {
	private int				count;
	private final int		max;
	private ActorRef		neighbor;
	private int				number;
	private final ActorRef	oracle;
	private final int		turn;
	
	public Guesser(final Integer turn, final Integer max, final ActorRef oracle) {
		this.turn = turn;
		this.max = max;
		this.oracle = oracle;
		count = 0;
		number = new Random().nextInt(Main.limit);
	}
	
	public void guessing(final Object arg0) throws Exception {
		if (arg0 instanceof IResponse) {
			getContext().become(this::waiting);
			final IResponse r = (IResponse) arg0;
			if (r.compare() == Compare.equal) {
				System.out.println("[Guesser" + turn + "] WIN!");
				neighbor.tell(new Turn(), getSender());
				getContext().stop(getSelf());
			} else if (r.compare() == Compare.less) {
				number++;
			} else {
				number--;
			}
			neighbor.tell(new Turn((turn + 1) % max), getSelf());
		} else {
			unhandled(arg0);
		}
		
	}
	
	@Override
	public void onReceive(final Object arg0) throws Exception {
		if (arg0 instanceof ActorRef) {
			// System.out.println("[Guesser" + turn + "] neighbor assigned");
			neighbor = (ActorRef) arg0;
			getContext().become(this::waiting);
		} else {
			unhandled(arg0);
		}
	}
	
	public void waiting(final Object arg0) throws Exception {
		
		if (arg0 instanceof ITurn) {
			final ITurn t = (ITurn) arg0;
			if (t.lost()) {
				System.out.println("[Guesser" + turn + "] SOB!");
				neighbor.tell(new Turn(), getSender());
				getContext().stop(getSelf());
			} else if (t.nextTurn() == turn) {
				if (turn == 0) {
					System.out.println("--- Turn: " + count++);
				}
				getContext().become(this::guessing);
				System.out.println("[Guesser" + turn + "] guessing number: " + number);
				oracle.tell(new Guess(number), getSelf());
			}
		} else {
			unhandled(arg0);
		}
	}
}
