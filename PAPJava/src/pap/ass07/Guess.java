package pap.ass07;

public class Guess implements IGuess {
	private final int	guess;
	
	public Guess(final int guess) {
		this.guess = guess;
	}
	
	@Override
	public int getGuess() {
		return guess;
	}
	
}
