package pap.ass06.graphics;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.HashMap;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import pap.ass06.commons.Coordinates;

/**
 * gestione dell'elemento gridBag
 * 
 * @author Matteo Francia
 * 
 */
public class GridBag {
	private final GridBagConstraints			c;
	private final HashMap<String, JComponent>	components;
	private final GridBagLayout					gbl;
	private final JPanel						pane;
	
	public GridBag() {
		gbl = new GridBagLayout();
		pane = new JPanel(gbl);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.BOTH; // esegue il resize dell'elemento se esso � pi� grande della cella
		components = new HashMap<String, JComponent>();
	}
	
	public void add(final Object component, final int row, final int column) {
		addComponent(component, row, column, 1, 1, new Insets(5, 5, 5, 5), 1, 1);
	}
	
	/**
	 * aggiunge un componente alla griglia
	 * 
	 * @param in
	 *            elemento da aggiungere
	 * @param row
	 *            riga
	 * @param col
	 *            colonna
	 * @param d
	 *            peso con cui si espande il componente orizzontalmente durante il resizing finestra
	 * @param e
	 *            peso con cui si espande il componente verticalmente durante il resizing finestra
	 * @param padding
	 *            padding del componente
	 * @param rowSpan
	 *            numero di righe occupate dall'oggetto
	 * @param colSpan
	 *            numero di colonne occupate dall'oggetto
	 */
	public void addComponent(final Object in, final int row, final int col, final double d, final double e, final Insets padding, final int rowSpan, final int colSpan) {
		c.gridx = col; // colonna
		c.gridy = row; // riga
		c.weightx = d; // peso con cui si espande il componente orizzontalmente durante il resizing finestra
		c.weighty = e; // peso con cui si espande il componente verticalmente durante il resizing finestra
		c.insets = padding; // padding del componente
		c.gridwidth = colSpan; // numero di colonne occupate dall'oggetto
		c.gridheight = rowSpan; // numero di righe occupate dall'oggetto
		pane.add((java.awt.Component) in, c); // aggiunge elemento al pannello
		components.put(new Coordinates(row, col).getDefaultRepresentation().toString(), (JComponent) in);
	}
	
	/**
	 * ritorna il pannello contenente gli elementi presente nella griglia
	 * 
	 * @return panel contenente gli elementi della griglia
	 */
	public JPanel getContent() {
		return pane;
	}
	
	public void remove(final Object component) {
		removeComponent((JComponent) component);
	}
	
	/**
	 * rimuove un componente dalla griglia
	 * 
	 * @param in
	 *            componente da rimuovere
	 */
	public void removeComponent(final java.awt.Component in) {
		pane.remove(in);
	}
	
	public void setBackgroundColor(final Color color) {
		SwingUtilities.invokeLater(() -> {
			pane.setBackground(new Color(color.getRed(), color.getGreen(), color.getBlue()));
		});
		
	}
	
	public void setBackgroundColor(final Color color, final int row, final int column) {
		SwingUtilities.invokeLater(() -> {
			components.get(new Coordinates(row, column).getDefaultRepresentation().toString()).setBackground(color);
		});
		
	}
	
}
