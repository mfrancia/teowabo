package pap.ass06.graphics;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.HashMap;

import javax.swing.JPanel;

import pap.ass06.commons.Coordinates;
import pap.ass06.commons.IObservable;
import pap.ass06.commons.IObserver;

public class Canvas extends JPanel implements IObserver<HashMap<String, Color>> {
	private static final long		serialVersionUID	= 1L;
	BufferedImage					img;
	private HashMap<String, Color>	prova				= new HashMap<String, Color>();
	private final int				size;
	
	public Canvas(final int size, final int cellPerRow) {
		this.setBounds(0, 0, size, size);
		this.size = size / cellPerRow;
		img = new BufferedImage(size, size, BufferedImage.TYPE_BYTE_BINARY);
	}
	
	@Override
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);
		if (prova == null) {
			return;
		}
		prova.keySet().stream().parallel().forEach(c -> {
			((Graphics2D) g).setColor(Color.BLACK);
			final Coordinates co = new Coordinates(c);
			g.fillRect(co.getX().intValue() * size, co.getY().intValue() * size, size - 1, size - 1);
		});
		
	}
	
	@Override
	public void update(final IObservable<HashMap<String, Color>> src, final HashMap<String, Color> info) {
		prova = info;
		// Graphics2D g2 = img.createGraphics();
		// prova.keySet().stream().parallel().forEach(c -> {
		// g2.setColor(Color.BLACK);
		// Coordinates co = new Coordinates(c);
		// g2.fillRect(co.getX().intValue() * size, co.getY().intValue() * size, size - 1, size - 1);
		// } );
		repaint();
	}
}
