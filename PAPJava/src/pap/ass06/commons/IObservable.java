package pap.ass06.commons;

public interface IObservable<T> {
	void register(IObserver<T> src);
}
