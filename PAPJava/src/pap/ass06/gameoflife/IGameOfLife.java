package pap.ass06.gameoflife;

import java.util.HashMap;
import java.util.Map;

import pap.ass06.commons.IObservable;

/**
 * Game of life
 * 
 * @author Matteo Francia
 *
 */
public interface IGameOfLife extends IObservable<HashMap<String, ICell>> {
	/**
	 * Return the cell corresponding to the specified coordinates
	 * 
	 * @return
	 */
	ICell getCell(int row, int column);
	
	Map<String, ICell> getCurrentState();
	
	int getDim();
	
	Map<String, ICell> getNextState();
	
	/**
	 * Randomly initialize the cells
	 */
	void initCells();
	
	/**
	 * reset the game
	 */
	void reset();
	
	/**
	 * Set the dimensions of the game
	 */
	void setDimensions(int dim);
	
	void setFrameRate(int value);
	
	/**
	 * Start the game
	 */
	void start();
	
	/**
	 * Stop the game
	 */
	void stop();
}
