package pap.ass06.gameoflife;

import java.util.List;

import pap.ass06.commons.Coordinates;

public interface ICell {
	/**
	 * Cell's state
	 * 
	 * @author Matteo Francia
	 *
	 */
	public enum State {
		alive, dead
	}
	
	/**
	 * Get the future state of the cell depending on its neighbors
	 * 
	 * una cella m[i,j] che nello stato s(t) è live e ha zero o al più una cella vicina live (e le altre dead), nello stato s(t+1) diventa dead (“muore di solitudine”)
	 * 
	 * una cella m[i,j] che nello stato s(t) è live e ha quattro o più celle vicine live, nello stato s(t+1) diventa dead (“muore di sovrappopolamento”)
	 * 
	 * una cella m[i,j] che nello stato s(t) è live e ha due o tre celle vicine live, nello stato s(t+1) rimane live (“sopravvive”)
	 * 
	 * una cella m[i,j] che nello stato s(t) è dead e ha tre celle vicine live, nello stato s(t+1) diventa live
	 */
	State getFutureState();
	
	/**
	 * Return the neighbors of the cell, the cannot change over time
	 * 
	 * @return
	 */
	List<ICell> getNeighbors();
	
	/**
	 * Returns the cell's coordinates
	 * 
	 * @return
	 */
	public Coordinates getP2d();
	
	/**
	 * Return the state of the cell
	 * 
	 * @return
	 */
	State getState();
	
	/**
	 * Return true if a cell is alive, false instead
	 * 
	 * @return
	 */
	boolean isAlive();
	
	/**
	 * Set the cell's state
	 * 
	 * @param state
	 */
	void setState(State state);
	
	/**
	 * s(t) = s(t+1)
	 */
	void updateState();
}
