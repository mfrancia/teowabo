package pap.ass06.gameoflife;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.util.HashMap;
import java.util.stream.IntStream;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import pap.ass06.commons.Coordinates;
import pap.ass06.commons.IObservable;
import pap.ass06.commons.IObserver;
import pap.ass06.graphics.GridBag;

/**
 * Visual Frame
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLifeFrame2 extends JFrame implements IObserver<HashMap<String, ICell>> {
	private static final long		serialVersionUID	= 1222855417115071664L;
	private boolean					flag;
	private final IGameOfLife		game;
	private final GridBag			gb;
	private final JLabel			lbl;
	private HashMap<String, ICell>	tmp					= new HashMap<String, ICell>();
	
	public GameOfLifeFrame2(final IGameOfLife game) {
		super("game of life - mfrancia");
		this.game = game;
		this.game.register(this);
		
		gb = new GridBag();
		final JButton start = new JButton("start");
		start.addActionListener(l -> game.start());
		final JPanel p = new JPanel(new FlowLayout());
		p.add(start);
		
		final JButton stop = new JButton("stop");
		stop.addActionListener(l -> game.stop());
		p.add(stop);
		
		final JButton reset = new JButton("reset");
		reset.addActionListener(l -> game.reset());
		p.add(reset);
		
		p.add(new JLabel("fr:"));
		final JSpinner spin = new JSpinner(new SpinnerNumberModel(10, 1, 10, 1));
		spin.addChangeListener(e -> game.setFrameRate((int) spin.getValue()));
		p.add(spin);
		
		lbl = new JLabel("");
		p.add(lbl);
		
		IntStream.range(0, game.getDim()).forEach(r -> {
			IntStream.range(0, game.getDim()).forEach(c -> {
				gb.addComponent(new JPanel(), r, c, 1.0, 1.0, new Insets(1, 1, 1, 1), 1, 1);
			});
		});
		
		getContentPane().add(gb.getContent(), BorderLayout.CENTER);
		getContentPane().add(p, BorderLayout.NORTH);
		
		setBounds(700, 700, 600, 660);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void update(final IObservable<HashMap<String, ICell>> src, final HashMap<String, ICell> info) {
		if (info.size() == 0) {
			IntStream.range(0, game.getDim()).forEach(r -> {
				IntStream.range(0, game.getDim()).forEach(c -> {
					gb.setBackgroundColor(Color.WHITE, r, c);
				});
			});
			SwingUtilities.invokeLater(() -> lbl.setText("alive: 0"));
			flag = true;
		} else if (!flag) {
			// memorizzo le celle che dovrei eliminare
			tmp = info;
			flag = !flag;
		} else {
			SwingUtilities.invokeLater(() -> lbl.setText("alive: " + info.size()));
			flag = !flag;
			// coloro di bianco le celle che devono sparire
			
			// SwingUtilities.invokeLater(() -> {
			tmp.values().stream().filter(cell -> info.get(cell.toString()) == null).forEach(cell -> {
				final Coordinates p = cell.getP2d();
				gb.setBackgroundColor(Color.WHITE, p.getX().intValue(), p.getY().intValue());
			});
			// coloro di nero le nuove celle attive (che prima non erano presenti)
			info.values().stream()/* .filter(cell -> tmp.get(cell.toString()) == null) */.forEach(cell -> {
				final Coordinates p = cell.getP2d();
				gb.setBackgroundColor(Color.BLACK, p.getX().intValue(), p.getY().intValue());
			});
			// });
		}
	}
}
