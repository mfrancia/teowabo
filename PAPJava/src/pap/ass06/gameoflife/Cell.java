package pap.ass06.gameoflife;

import java.util.LinkedList;
import java.util.List;

import pap.ass06.commons.Coordinates;

/**
 * Cell
 * 
 * @author Matteo Francia
 *
 */
public class Cell implements ICell {
	/**
	 * Cell's position into the grid
	 */
	private final Coordinates	coordinates;
	/**
	 * s(t+1)
	 */
	private State				futureState;
	private List<ICell>			neighbors;
	/**
	 * s(t)
	 */
	private State				state;
	
	public Cell(final Coordinates p2d) {
		this(p2d, null);
	}
	
	public Cell(final Coordinates p2d, final List<ICell> neighbors) {
		coordinates = p2d;
		state = State.dead;
		futureState = State.dead;
		if (neighbors == null) {
			this.neighbors = new LinkedList<ICell>();
		} else {
			this.neighbors = neighbors;
		}
	}
	
	@Override
	public State getFutureState() {
		final int alive = (int) neighbors.stream().filter(c -> c.isAlive()).count();
		if (state == State.alive) {
			if (alive < 2) {
				futureState = State.dead;
			} else if (alive < 4) {
				futureState = State.alive;
			} else {
				futureState = State.dead;
			}
		} else if (alive == 3) {
			futureState = State.alive;
		}
		return futureState;
	}
	
	@Override
	public List<ICell> getNeighbors() {
		return neighbors;
	}
	
	@Override
	public Coordinates getP2d() {
		return coordinates;
	}
	
	@Override
	public State getState() {
		return state;
	}
	
	@Override
	public boolean isAlive() {
		return state == State.alive ? true : false;
	}
	
	@Override
	public void setState(final State state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return "[C:" + coordinates.getX() + "," + coordinates.getY() + "]" + " " + state.toString();
	}
	
	@Override
	public void updateState() {
		state = futureState;
	}
	
}
