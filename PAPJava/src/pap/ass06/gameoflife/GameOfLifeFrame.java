package pap.ass06.gameoflife;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.HashMap;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import pap.ass06.commons.Coordinates;
import pap.ass06.commons.IObservable;
import pap.ass06.commons.IObserver;
import pap.ass06.graphics.Canvas;

/**
 * Visual Frame
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLifeFrame extends JFrame implements IObserver<HashMap<String, ICell>> {
	private static final long	serialVersionUID	= 1222855417115071664L;
	private final Canvas		c;
	private boolean				flag;
	private final IGameOfLife	game;
	private final JLabel		lbl;
	
	public GameOfLifeFrame(final IGameOfLife game) {
		super("game of life - mfrancia");
		this.game = game;
		this.game.register(this);
		
		final JButton start = new JButton("start");
		start.addActionListener(l -> game.start());
		final JPanel p = new JPanel(new FlowLayout());
		p.add(start);
		
		final JButton stop = new JButton("stop");
		stop.addActionListener(l -> game.stop());
		p.add(stop);
		
		final JButton reset = new JButton("reset");
		reset.addActionListener(l -> game.reset());
		p.add(reset);
		
		p.add(new JLabel("fr:"));
		final JSpinner spin = new JSpinner(new SpinnerNumberModel(10, 1, 10, 1));
		spin.addChangeListener(e -> game.setFrameRate((int) spin.getValue()));
		p.add(spin);
		
		lbl = new JLabel("");
		p.add(lbl);
		
		c = new Canvas(770, 200);
		getContentPane().add(c, BorderLayout.CENTER);
		getContentPane().add(p, BorderLayout.NORTH);
		
		setBounds(700, 700, 600, 660);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void update(final IObservable<HashMap<String, ICell>> src, final HashMap<String, ICell> info) {
		if (info.size() == 0) {
			SwingUtilities.invokeLater(() -> lbl.setText("alive: 0"));
			c.update(null, null);
			flag = true;
		} else if (!flag) {
			// mantenuto per uniformità con business logica, che prima invia lo stato corrente
			// per cancellaro e poi invia lo stato futuro da disegnare
			flag = !flag;
		} else {
			flag = !flag;
			final HashMap<String, Color> colors = new HashMap<String, Color>();
			// coloro di nero le nuove celle attive (che prima non erano presenti)
			info.values().stream().forEach(cell -> {
				final Coordinates p = cell.getP2d();
				colors.put(p.toString(), Color.BLACK);
			});
			c.update(null, colors);
			SwingUtilities.invokeLater(() -> lbl.setText("alive: " + info.size()));
		}
		
	}
}
