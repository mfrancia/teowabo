package pap.ass06.gameoflife;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Future;
import java.util.stream.IntStream;

import pap.ass06.gameoflife.ICell.State;

/**
 * Concurrent version of the game
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLifeConcurrent extends GameOfLife {
	private class Task implements Callable<HashMap<String, ICell>> {
		private final int	pos;
		
		public Task(final int pos) {
			this.pos = pos;
		}
		
		@Override
		public HashMap<String, ICell> call() throws Exception {
			// System.out.println("executing [Task" + pos + "]");
			// computo le sole celle attive e i relativi vicini
			final HashMap<String, ICell> tmp = new HashMap<String, ICell>();
			final int ratio = currentState.values().size() / n;
			final LinkedList<ICell> list = new LinkedList<ICell>(currentState.values());
			IntStream.range(pos * ratio, (pos + 1) * ratio).forEach(i -> {
				final ICell c = list.get(i);
				if (c.getFutureState() == State.alive) {
					tmp.put(c.toString(), c);
				}
				// checking the dead neighbors, alive cells are already contained into currentState
					c.getNeighbors().stream()/* .filter(n -> !n.isAlive()) */.forEach(n -> {
						if (n.getFutureState() == State.alive) {
							tmp.put(n.toString(), n);
						}
					});
				});
			latch.countDown();
			return tmp;
		}
	}
	
	private CountDownLatch	latch;
	
	public GameOfLifeConcurrent() {
		super();
	}
	
	@Override
	public HashMap<String, ICell> getNextState() {
		final List<Future<HashMap<String, ICell>>> futures = new LinkedList<Future<HashMap<String, ICell>>>();
		latch = new CountDownLatch(n);
		// execute all the tasks
		IntStream.range(0, n).forEach(i -> futures.add(internalExecutor.submit(new Task(i))));
		
		try {
			latch.await();
		} catch (final InterruptedException e1) {
			e1.printStackTrace();
		}
		
		currentState.values().stream().parallel().forEach(cell -> {
			// operazioni idempotenti
				cell.updateState();
				cell.getNeighbors().forEach(n -> {
					n.updateState();
				});
			});
		
		// put the pieces of the game together
		currentState = new HashMap<String, ICell>();
		futures.forEach(f -> {
			try {
				currentState.putAll(f.get());
			} catch (final Exception e) {
				e.printStackTrace();
			}
		});
		return currentState;
	}
}
