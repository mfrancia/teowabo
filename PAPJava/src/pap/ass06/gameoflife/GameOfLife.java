package pap.ass06.gameoflife;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.IntStream;

import pap.ass06.commons.Coordinates;
import pap.ass06.commons.IObserver;
import pap.ass06.gameoflife.ICell.State;

/**
 * Sequential version of the game
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLife implements IGameOfLife {
	protected HashMap<String, ICell>			currentState;
	private int									dim;
	private int									framems;
	protected ExecutorService					internalExecutor;
	private ICell[][]							matrix;
	// protected List<ICell> matrixAsList;
	public final int							n	= Runtime.getRuntime().availableProcessors();
	protected IObserver<HashMap<String, ICell>>	observer;
	protected boolean							stopped;
	
	public GameOfLife() {
		stopped = true;
		currentState = new HashMap<String, ICell>();
		framems = 100;
	}
	
	@Override
	public ICell getCell(final int row, final int column) {
		return matrix[row][column];
	}
	
	@Override
	public HashMap<String, ICell> getCurrentState() {
		return currentState;
	}
	
	@Override
	public int getDim() {
		return dim;
	}
	
	@Override
	public HashMap<String, ICell> getNextState() {
		// naive solution
		// currentState = new LinkedList<ICell>();
		//
		// matrixAsList.forEach(cell -> {
		// cell.getFutureState();
		// if (cell.isAlive()) {
		// currentState.add(cell);
		// }
		// });
		//
		// matrixAsList.forEach(cell -> {
		// cell.updateState();
		// });
		//
		
		// computo le sole celle attive e i relativi vicini. Comportamento sequenziale
		// computo le sole celle attive e i relativi vicini
		final HashMap<String, ICell> tmp = new HashMap<String, ICell>();
		currentState.values().stream().parallel().forEach(cell -> {
			// computo lo stato futuro
				if (cell.getFutureState() == State.alive) {
					tmp.put(cell.toString(), cell);
				}
				cell.getNeighbors().forEach(n -> {
					if (n.getFutureState() == State.alive) {
						tmp.put(n.toString(), n);
					}
				});
			});
		
		currentState.values().stream().parallel().forEach(cell -> {
			// operazioni idempotenti
				cell.updateState();
				cell.getNeighbors().forEach(n -> {
					n.updateState();
				});
			});
		
		currentState = new HashMap<String, ICell>(tmp);
		
		return currentState;
	}
	
	@Override
	public void initCells() {
		final Random rnd = new Random();
		IntStream.range(0, dim).parallel().forEachOrdered(r -> {
			IntStream.range(0, dim).parallel().forEachOrdered(c -> {
				final int left = c - 1, down = r - 1, right = c + 1;
				
				final ICell cell = new Cell(new Coordinates(r, c));
				if (rnd.nextInt(1000) < 200) {
					cell.setState(State.alive);
					currentState.put(cell.toString(), cell);
				} else {
					cell.setState(State.dead);
				}
				// matrixAsList.add(cell);
					
					boolean flag = false;
					
					if (left >= 0) {
						// cella a sx
						// | - | - | - |
						// | x | - | - |
						// | - | - | - |
						cell.getNeighbors().add(matrix[r][left]);
						matrix[r][left].getNeighbors().add(cell);
						flag = true;
					}
					
					if (down >= 0) {
						// cella in alto
						// | - | x | - |
						// | - | - | - |
						// | - | - | - |
						cell.getNeighbors().add(matrix[down][c]);
						matrix[down][c].getNeighbors().add(cell);
						
						// cella in alto a sx
						// | x | - | - |
						// | - | - | - |
						// | - | - | - |
						if (flag) {
							cell.getNeighbors().add(matrix[down][left]);
							matrix[down][left].getNeighbors().add(cell);
						}
						
						// cella in alto a dx
						// | - | - | x |
						// | - | - | - |
						// | - | - | - |
						if (right < dim) {
							cell.getNeighbors().add(matrix[down][right]);
							matrix[down][right].getNeighbors().add(cell);
						}
					}
					matrix[r][c] = cell;
				});
		});
		
	}
	
	@Override
	public void register(final IObserver<HashMap<String, ICell>> src) {
		observer = src;
	}
	
	@Override
	public void reset() {
		stopped = true;
		currentState = new HashMap<String, ICell>();
		// matrixAsList = new LinkedList<ICell>();
		initCells();
		GameOfLife.this.observer.update(GameOfLife.this, new HashMap<String, ICell>());
	}
	
	@Override
	public void setDimensions(final int dim) {
		matrix = new ICell[dim][dim];
		this.dim = dim;
		GameOfLife.this.initCells();
	}
	
	@Override
	public void setFrameRate(final int value) {
		framems = 1000 / value;
	}
	
	@Override
	public void start() {
		if (stopped == false) {
			return;
		}
		stopped = false;
		
		internalExecutor = Executors.newFixedThreadPool(n);
		internalExecutor.execute(() -> {
			if (observer != null) {
				GameOfLife.this.observer.update(GameOfLife.this, new HashMap<String, ICell>());
			}
			GameOfLife.this.observer.update(GameOfLife.this, GameOfLife.this.getCurrentState());
			try {
				Thread.sleep(10);
			} catch (final Exception e) {
			}
			while (!stopped) {
				// System.out.println("------");
				final long time = System.currentTimeMillis();
				GameOfLife.this.observer.update(GameOfLife.this, GameOfLife.this.getCurrentState());
				// GameOfLife.this.getCurrentState().values().forEach(c -> System.out.println(c.toString()));
				GameOfLife.this.observer.update(GameOfLife.this, GameOfLife.this.getNextState());
				try {
					// fps
					Thread.sleep(framems - (System.currentTimeMillis() - time));
				} catch (final Exception e) {
				}
			}
		});
	}
	
	@Override
	public void stop() {
		if (stopped) {
			return;
		}
		stopped = true;
		internalExecutor.shutdown();
	}
}
