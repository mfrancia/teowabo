package pap.ass06;

import pap.ass06.gameoflife.GameOfLifeConcurrent;
import pap.ass06.gameoflife.GameOfLifeFrame;
import pap.ass06.gameoflife.IGameOfLife;

public class Main {
	public static void main(final String[] args) {
		new Main(Runtime.getRuntime().availableProcessors());
	}
	
	private IGameOfLife	game	= null;
	
	public Main(final int n) {
		config();
		start();
	}
	
	private void config() {
		// game = new GameOfLife();
		game = new GameOfLifeConcurrent();
		
		// con una griglia > 300x300 il gioco rallenta fino a quando alcune celle muoiono
		game.setDimensions(300);
		new GameOfLifeFrame(game);
		
		// con una griglia grande i tempi di generazione sono molto lunghi
		// game.setDimensions(100);
		// new GameOfLifeFrame2(game);
	}
	
	private void start() {
		// game.start();
	}
}
