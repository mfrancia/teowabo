package pap.ass08.tempearturemonitoring;

public class Read {
	private final boolean	clean;
	private final Double	v;
	
	public Read(final Double value) {
		v = value;
		clean = true;
	}
	
	public Read(final Double value, final boolean clean) {
		v = value;
		this.clean = clean;
	}
	
	Double getV() {
		return v;
	}
	
	boolean isClean() {
		return clean;
	}
	
	@Override
	public String toString() {
		return v + "";
	}
}