package pap.ass08.tempearturemonitoring;

import pap.ass08.tempearturemonitoring.Main.IOutput;
import rx.Subscriber;

public class Filter {
	private final String				name;
	private final IOutput				out;
	private Double						prev;
	private Subscriber<? super Double>	subscriber;
	private final Double				threshold;
	
	public Filter(final String name, final Double threshold, final IOutput out) {
		
		this.threshold = threshold;
		this.name = name;
		this.out = out;
	}
	
	public void addValue(final Double value) {
		if (prev == null) {
			prev = value;
		} else if (Math.abs(value - prev) < threshold) {
			try {
				subscriber.onNext(value);
			} catch (final Exception ex) {
				
			}
			prev = value;
		} else {
			out.addOut(name + "SPIKE DETECTED!!!");
		}
		
	}
	
	public void setSubscriber(final Subscriber<? super Double> subscriber) {
		this.subscriber = subscriber;
		
	}
}
