package pap.ass08.tempearturemonitoring;

public class Syskb {
	public static final String	ALERT				= "alert";
	
	public static final String	ALERTGONE			= "alertgone";
	
	public static final String	AVG					= "avg";
	public static final boolean	debug				= true;
	public static final String	MAX					= "max";
	public static final double	MAXV				= 40;
	public static final double	MaxVariation		= 20;
	public static final String	MIN					= "min";
	public static final double	MINV				= 0;
	public static final long	MINWINDOW			= 100;
	public static final long	SAMPLETIME			= 100;
	public static final long	SAMPLEAVERAGETIME	= SAMPLETIME * 4;
	public static final double	SPIKEP				= 0.1;
	public static final String	STARTCOMMAND		= "start";
	public static final String	STOPCOMMAND			= "stop";
	private static double		threshold			= /* (MAXV - MINV) / 2; */MAXV - 10;
	public static final String	THRESHOLD			= "thr";
	private static Double		window				= 100.0;
	public static final String	WINDOW				= "WINDOW";
	
	public static synchronized double getT() {
		return threshold;
	}
	
	public synchronized static Double getWindow() {
		return window;
	}
	
	public static synchronized void setT(final double t) {
		threshold = t;
	}
	
	public synchronized static void setWindow(final double time) {
		window = time;
	}
	
}
