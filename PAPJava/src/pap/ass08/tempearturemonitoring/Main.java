package pap.ass08.tempearturemonitoring;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import pap.ass08.commons.IObservable;
import pap.ass08.commons.IObserver;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

public class Main implements IObserver<String> {
	@FunctionalInterface
	interface IOutput {
		void addOut(String s);
	}
	
	public static void main(final String[] args) {
		if (args.length > 0) {
			new Main(Integer.parseInt(args[1]), 3);
		} else {
			new Main(1000, 3);
		}
	}
	
	private Observable<List<Double>>	avgwindow;
	private final Observable<Double>	merge, averageTempStream;
	private Subscription				min, avg, avgwindowsubscription, max;
	private final IOutput				out;
	
	public Main(final int refresh, final int sensorN) {
		// imposto il componente di output
		out = new View();
		// out = (String s) -> System.out.println(s);
		((View) out).register(this);
		
		// lista degli stream (il numero di sensori è dinamico)
		final List<Observable<Double>> list = new LinkedList<Observable<Double>>();
		
		IntStream.range(0, sensorN).forEach(v -> {
			final String name = "Sensor" + v + ":";
			
			final Observable<Double> tempStream = Observable.create((final Subscriber<? super Double> subscriber) -> {
				new AsyncGenerator(subscriber, new TempSensor(Syskb.MINV, Syskb.MAXV, Syskb.SPIKEP), Syskb.SAMPLETIME).start();
			}).map(value -> new Read(value)).scan((final Read prev, final Read cur) -> {
				if ((prev.getV() > Syskb.MAXV) || (prev.getV() < Syskb.MINV)) {
					return new Read(-1.0, false); // primo valore sporco, altera tutto il sensore
				}
				if (Math.abs(prev.getV() - cur.getV()) > Syskb.MaxVariation) {
					if (Syskb.debug) {
						out.addOut(name + "SPIKE!!!!");
					}
					return new Read(prev.getV(), false);
				} else {
					if (Syskb.debug) {
						out.addOut(name + cur);
					}
					return cur;
				}
			}).filter(value -> value.isClean()).map(value -> value.getV());
			list.add(tempStream);
		});
		merge = Observable.merge(list).publish().refCount();
		
		// media temporale
		// avg = merge.buffer(SysKB.SAMPLEAVERAGETIME, TimeUnit.MILLISECONDS).subscribe(values -> {
		// final double sum = values.stream().reduce(0.0, (a, b) -> a + b);
		// if (SysKB.debug) {
		// out.addOut("");
		// out.addOut("-----------------------");
		// }
		// double avg = (sum / values.size());
		// out.addOut(SysKB.AVG + ":" + avg);
		// // subscriber.onNext(avg);
		// if (SysKB.debug) {
		// out.addOut("average count:" + values.size());
		// out.addOut("-----------------------");
		// out.addOut("");
		// }
		// });
		
		// media totale
		averageTempStream = merge.map(v -> new Data(v, v, v)).scan(new Data(0.0, 0.0, 0.0), (final Data p, final Data c) -> {
			final Double sum = p.getV() + c.getV(), count = p.getCount() + 1, avg = sum / count;
			return new Data(sum, count, avg);
		}).map(v -> v.getAverage()).publish().refCount();
		
		avgwindow = averageTempStream.buffer(Syskb.getWindow().longValue(), TimeUnit.MILLISECONDS);
	}
	
	private void avgWindowSubscription() {
		avgwindowsubscription = avgwindow.subscribe(values -> {
			if (values.stream().allMatch(v -> v > Syskb.getT())) {
				out.addOut(Syskb.ALERT + ":" + 0);
			} else {
				out.addOut(Syskb.ALERTGONE + ":" + 0);
			}
		});
	}
	
	private void start() {
		if ((avg != null) && !avg.isUnsubscribed()) {
			return;
		}
		avg = averageTempStream.subscribe(v -> out.addOut(Syskb.AVG + ":" + v));
		avgWindowSubscription();
		min = merge.scan((final Double m, final Double cur) -> m < cur ? m : cur).subscribe(v -> out.addOut(Syskb.MIN + ":" + v));
		max = merge.scan((final Double m, final Double cur) -> m > cur ? m : cur).subscribe(v -> out.addOut(Syskb.MAX + ":" + v));
	}
	
	private void stop() {
		if (avg.isUnsubscribed()) {
			return;
		}
		avg.unsubscribe();
		avgwindowsubscription.unsubscribe();
		min.unsubscribe();
		max.unsubscribe();
	}
	
	@Override
	public void update(final IObservable<String> src, final String info) {
		final String inf[] = info.split(":");
		switch (inf[0]) {
			case Syskb.STARTCOMMAND:
				if (Syskb.debug) {
					out.addOut("-----------------------");
					out.addOut("MONITORING ENABLED!!!");
					out.addOut("-----------------------");
				}
				start();
				break;
			case Syskb.STOPCOMMAND:
				if (Syskb.debug) {
					out.addOut("-----------------------");
					out.addOut("MONITORING DISABLED!!!");
					out.addOut("-----------------------");
				}
				stop();
				break;
			case Syskb.THRESHOLD:
				if (Syskb.debug) {
					out.addOut("-----------------------");
					out.addOut("THRESHOLD SETTED!!!");
					out.addOut("-----------------------");
				}
				Syskb.setT(Double.parseDouble(inf[1]));
				break;
			case Syskb.WINDOW:
				if (Syskb.debug) {
					out.addOut("-----------------------");
					out.addOut("WINDOW SETTED!!!" + inf[1]);
					out.addOut("-----------------------");
				}
				Syskb.setWindow(Double.parseDouble(inf[1]));
				boolean flag = false;
				if ((avgwindowsubscription != null) && !avgwindowsubscription.isUnsubscribed()) {
					avgwindowsubscription.unsubscribe();
					flag = true;
				}
				avgwindow = averageTempStream.buffer(Syskb.getWindow().longValue(), TimeUnit.MILLISECONDS);
				if (flag) {
					avgWindowSubscription();
				}
				break;
		}
	}
}
