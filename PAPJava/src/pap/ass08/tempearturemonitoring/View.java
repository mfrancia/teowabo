package pap.ass08.tempearturemonitoring;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.SpinnerModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import pap.ass08.commons.IObservable;
import pap.ass08.commons.IObserver;
import pap.ass08.tempearturemonitoring.Main.IOutput;

public class View extends JFrame implements IOutput, IObservable<String> {
	private static final long	serialVersionUID	= 771119994894889000L;
	private final JTextArea		area;
	private final JLabel		avg, min, max;
	private final JDialog		dialog;
	private IObserver<String>	obs;
	
	public View() {
		super("monitor");
		
		dialog = new JDialog(this, "alert!!", false);
		dialog.setBounds(100, 100, 100, 0);
		dialog.setLocationRelativeTo(null);
		
		final JButton start = new JButton("start");
		start.addActionListener(l -> obs.update(null, "start"));
		final JPanel n = new JPanel(new FlowLayout());
		n.add(start);
		
		final JButton stop = new JButton("stop");
		stop.addActionListener(l -> obs.update(null, "stop"));
		n.add(stop);
		
		final JPanel s = new JPanel(new FlowLayout());
		min = new JLabel("");
		s.add(min);
		
		avg = new JLabel("");
		s.add(avg);
		
		max = new JLabel("");
		s.add(max);
		
		area = new JTextArea();
		final JScrollPane c = new JScrollPane(area);
		
		n.add(new JLabel("thr"));
		final SpinnerModel spinnerModel = new SpinnerNumberModel(Syskb.getT(), // initial value
				Syskb.MINV, // min
				Syskb.MAXV, // max
				1);// step
		final JSpinner spinner = new JSpinner(spinnerModel);
		spinner.addChangeListener(v -> obs.update(null, Syskb.THRESHOLD + ":" + spinner.getValue()));
		n.add(spinner);
		
		n.add(new JLabel("window"));
		final SpinnerModel timeM = new SpinnerNumberModel(Syskb.getWindow().doubleValue(), // initial value
				Syskb.MINWINDOW, // min
				Syskb.MINWINDOW * 10, // max
				100);// step
		final JSpinner tim = new JSpinner(timeM);
		tim.addChangeListener(v -> obs.update(null, Syskb.WINDOW + ":" + tim.getValue()));
		n.add(tim);
		
		getContentPane().add(s, BorderLayout.SOUTH);
		getContentPane().add(c, BorderLayout.CENTER);
		getContentPane().add(n, BorderLayout.NORTH);
		
		setBounds(700, 700, 600, 660);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void addOut(final String s) {
		final String[] cmd = s.split(":");
		try {
			final String value = (Math.floor(Double.parseDouble(cmd[1]) * 100) / 100) + "";
			switch (cmd[0]) {
				case Syskb.AVG:
					updateLabel(avg, "Avg: " + value);
					append(cmd[1]);
					break;
				case Syskb.MIN:
					updateLabel(min, "Min: " + value);
					break;
				case Syskb.MAX:
					updateLabel(max, "Max: " + value);
					break;
				case Syskb.ALERT:
					dialog.setVisible(true);
					break;
				case Syskb.ALERTGONE:
					dialog.setVisible(false);
					dialog.dispose();
					break;
				default:
					append(s);
					break;
			}
		} catch (final Exception e) {
			append(s);
		}
	}
	
	private void append(final String s) {
		SwingUtilities.invokeLater(() -> area.append(s + "\n"));
	}
	
	@Override
	public void register(final IObserver<String> src) {
		obs = src;
	}
	
	private void updateLabel(final JLabel l, final String text) {
		SwingUtilities.invokeLater(() -> l.setText(text));
	}
}
