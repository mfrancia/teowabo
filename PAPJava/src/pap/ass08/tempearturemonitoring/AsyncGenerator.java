package pap.ass08.tempearturemonitoring;

import rx.Subscriber;

public class AsyncGenerator extends Thread {
	
	private final long							refresh;
	private final TempSensor					sensor;
	private volatile boolean					stopped;
	private final Subscriber<? super Double>	subscriber;
	
	public AsyncGenerator(final Subscriber<? super Double> subscriber, final TempSensor sensor, final long refresh) {
		this.subscriber = subscriber;
		stopped = false;
		this.sensor = sensor;
		this.refresh = refresh;
	}
	
	public synchronized boolean isStopped() {
		return stopped;
	}
	
	@Override
	public void run() {
		while (!isStopped()) {
			try {
				subscriber.onNext(sensor.getCurrentValue());
				Thread.sleep(refresh);
			} catch (final Exception ex) {
				subscriber.onError(ex);
			}
		}
		subscriber.onCompleted();
	}
	
	public synchronized void setStop() {
		stopped = true;
	}
}
