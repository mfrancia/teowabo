package pap.ass08.tempearturemonitoring;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

class BaseTimeValue {
	static BaseTimeValue	instance;
	
	static BaseTimeValue getInstance() {
		synchronized (BaseTimeValue.class) {
			if (instance == null) {
				instance = new BaseTimeValue();
			}
			return instance;
		}
	}
	
	private final ScheduledExecutorService	exec	= Executors.newScheduledThreadPool(1);
	private double							time;
	
	private BaseTimeValue() {
		time = 0;
		exec.scheduleAtFixedRate(() -> {
			synchronized (exec) {
				time += 0.01;
			}
		}, 0, 100, java.util.concurrent.TimeUnit.MILLISECONDS);
	}
	
	public double getCurrentValue() {
		synchronized (exec) {
			return time;
		}
	}
}

/**
 * Class implementing a simulated temperature sensor (Assignment #08)
 * 
 * @author aricci
 *
 */
public class TempSensor {
	class UpdateTask implements Runnable {
		@Override
		public void run() {
			double newValue;
			
			final double delta = (-0.5 + gen.nextDouble()) * range * 0.2;
			newValue = zero + (Math.sin(time.getCurrentValue()) * range * 0.8) + delta;
			
			final boolean newSpike = gen.nextDouble() <= spikeFreq;
			if (newSpike) {
				newValue = currentValue + spikeVar;
			}
			
			synchronized (this) {
				currentValue = newValue;
			}
		}
	}
	
	private volatile double					currentValue;
	private final ScheduledExecutorService	exec	= Executors.newScheduledThreadPool(1);
	private final Random					gen;
	private final double					range;
	private final double					spikeFreq;
	private final double					spikeVar;
	private final BaseTimeValue				time;
	private final UpdateTask				updateTask;
	
	private final double					zero;
	
	/**
	 * Create a sensor producing values in a (min,max) range, with possible spikes
	 * 
	 * @param min
	 *            range min
	 * @param max
	 *            range max
	 * @param spikeFreq
	 *            - probability to read a spike (0 = no spikes, 1 = always spikes)
	 */
	public TempSensor(final double min, final double max, final double spikeFreq) {
		gen = new Random(System.nanoTime());
		time = BaseTimeValue.getInstance();
		zero = (max + min) * 0.5;
		range = (max - min) * 0.5;
		this.spikeFreq = spikeFreq;
		spikeVar = range * 10;
		updateTask = new UpdateTask();
		exec.scheduleAtFixedRate(updateTask, 0, 100, java.util.concurrent.TimeUnit.MILLISECONDS);
		
		/* initialize currentValue */
		updateTask.run();
	}
	
	/**
	 * Reading the current sensor value
	 * 
	 * @return sensor value
	 */
	public double getCurrentValue() {
		synchronized (updateTask) {
			return currentValue;
		}
	}
	
}
