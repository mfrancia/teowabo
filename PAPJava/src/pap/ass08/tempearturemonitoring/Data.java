package pap.ass08.tempearturemonitoring;

public class Data extends Read {
	private final Double	v, avg;
	
	public Data(final Double value, final Double count, final Double average) {
		super(value);
		v = count;
		avg = average;
	}
	
	public Double getAverage() {
		return avg;
	}
	
	public Double getCount() {
		return v;
	}
}