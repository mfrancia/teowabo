package pap.ass08.commons;

public interface IObservable<T> {
	void register(IObserver<T> src);
}
