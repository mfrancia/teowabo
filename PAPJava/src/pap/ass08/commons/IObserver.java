package pap.ass08.commons;

public interface IObserver<T> {
	
	void update(IObservable<T> src, T info);
}
