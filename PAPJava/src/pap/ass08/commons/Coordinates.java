package pap.ass08.commons;

public class Coordinates {
	private Number			h;
	private final Number	x;
	private final Number	y;
	private Number			z;
	
	public Coordinates(final Number x, final Number y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinates(final Number x, final Number y, final Number z, final Number altitude) {
		this.x = x;
		this.y = y;
		this.z = z;
		h = altitude;
	}
	
	public Coordinates(final String s) {
		final String[] ret = s.split(",");
		
		x = Double.parseDouble(ret[0]);
		y = Double.parseDouble(ret[1]);
		
	}
	
	public Number getAltitude() {
		return h;
	}
	
	public CharSequence getDefaultRepresentation() {
		String s = "";
		s += (x == null ? "?" : x.doubleValue()) + ",";
		s += (y == null ? "?" : y.doubleValue()) + ",";
		s += (z == null ? "?" : z.doubleValue()) + ",";
		s += (h == null ? "?" : h.doubleValue());
		return s;
	}
	
	public Number getX() {
		return x;
	}
	
	public Number getY() {
		return y;
	}
	
	public Number getZ() {
		return z;
	}
	
	@Override
	public String toString() {
		return getDefaultRepresentation().toString();
	}
}
