package pap.ass08.gol;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import pap.ass08.commons.IObservable;
import pap.ass08.commons.IObserver;

/**
 * Visual Frame
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLifeFrame extends JFrame implements IObserver<List<int[]>> {
	private static final long	serialVersionUID	= 1222855417115071664L;
	private final Canvas		c;
	private final JLabel		lbl;
	
	public GameOfLifeFrame(final IObserver<String> obs) {
		super("game of life - mfrancia");
		
		final JButton start = new JButton("start");
		start.addActionListener(l -> obs.update(null, "start"));
		final JPanel p = new JPanel(new FlowLayout());
		p.add(start);
		
		final JButton stop = new JButton("stop");
		stop.addActionListener(l -> obs.update(null, "stop"));
		p.add(stop);
		
		final JButton reset = new JButton("reset");
		reset.addActionListener(l -> obs.update(null, "reset"));
		p.add(reset);
		
		p.add(new JLabel("fr:"));
		final JSpinner spin = new JSpinner(new SpinnerNumberModel(10, 1, 10, 1));
		spin.addChangeListener(e -> obs.update(null, "" + ((int) spin.getValue())));
		p.add(spin);
		
		lbl = new JLabel("");
		p.add(lbl);
		
		c = new Canvas(770, 200);
		getContentPane().add(c, BorderLayout.CENTER);
		getContentPane().add(p, BorderLayout.NORTH);
		
		setBounds(700, 700, 600, 660);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLocationRelativeTo(null);
		setVisible(true);
	}
	
	@Override
	public void update(final IObservable<List<int[]>> src, final List<int[]> info) {
		c.update(null, info);
		SwingUtilities.invokeLater(() -> lbl.setText("alive: " + info.size()));
	}
	
}
