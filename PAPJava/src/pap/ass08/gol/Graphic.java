package pap.ass08.gol;

import java.util.List;

import javax.swing.JDialog;

import pap.ass08.commons.IObservable;
import pap.ass08.commons.IObserver;
import pap.ass08.gol.msg.GameInit;
import pap.ass08.gol.msg.GameInitDone;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

public class Graphic extends UntypedActor implements IObserver<String> {
	private JDialog					d;
	private final GameOfLifeFrame	frame;
	private final ActorRef			game;
	
	public Graphic(final ActorRef game) {
		this.game = game;
		frame = new GameOfLifeFrame(this);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void onReceive(final Object arg0) throws Exception {
		if (arg0 instanceof List<?>) {
			frame.update(null, (List<int[]>) arg0);
		} else if (arg0 instanceof GameInit) {
			d = new JDialog(frame, "wait...", false);
			d.setBounds(100, 100, 100, 0);
			d.setLocationRelativeTo(null);
			d.setVisible(true);
		} else if (arg0 instanceof GameInitDone) {
			d.setVisible(false);
			d.dispose();
		} else {
			unhandled(arg0);
		}
	}
	
	@Override
	public void update(final IObservable<String> src, final String info) {
		game.tell(info, getSelf());
	}
}
