package pap.ass08.gol;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JPanel;

import pap.ass06.commons.IObservable;
import pap.ass06.commons.IObserver;

public class Canvas extends JPanel implements IObserver<List<int[]>> {
	private static final long	serialVersionUID	= 1L;
	private List<int[]>			prova				= new LinkedList<int[]>();
	private final int			size;
	
	public Canvas(final int size, final int cellPerRow) {
		this.setBounds(0, 0, size, size);
		setDoubleBuffered(true);
		this.size = size / cellPerRow;
	}
	
	@Override
	protected void paintComponent(final Graphics g) {
		super.paintComponent(g);
		if (prova == null) {
			return;
		}
		prova.forEach(c -> {
			((Graphics2D) g).setColor(Color.BLACK);
			g.fillRect(c[0] * size, c[1] * size, size - 1, size - 1);
		});
		
	}
	
	@Override
	public void update(final IObservable<List<int[]>> src, final List<int[]> info) {
		prova = info;
		repaint();
	}
}
