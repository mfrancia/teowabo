package pap.ass08.gol.msg;

import pap.ass08.gol.GameOfLife;

public class Alive implements IMsg {
	private final int	alive;
	
	public Alive(final int alive) {
		this.alive = alive;
	}
	
	public boolean isAlive() {
		return alive == GameOfLife.ALIVE;
	}
}
