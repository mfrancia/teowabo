package pap.ass08.gol;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import pap.ass08.commons.Coordinates;
import pap.ass08.commons.IObserver;
import pap.ass08.gol.msg.GameInit;
import pap.ass08.gol.msg.GameInitDone;
import pap.ass08.gol.msg.Start;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;

/**
 * 
 * @author Matteo Francia
 *
 */
public class GameOfLife extends UntypedActor implements IGameOfLife {
	public static final int		ALIVE	= 1;
	public static final int		DEAD	= 0;
	private ActorRef			cell;
	private ActorRef			center;
	private int					counter;
	protected List<int[]>		currentState;
	private int					dim;
	private final int			DIM		= 300;
	private boolean				firstFlag;
	// private int framems = 100;
	private final ActorRef		graphics;
	private ActorRef[][]		matrix;
	private int					n;
	protected volatile boolean	stopped;
	
	public GameOfLife() {
		stopped = true;
		firstFlag = true;
		currentState = new LinkedList<int[]>();
		graphics = getContext().actorOf(Props.create(Graphic.class, getSelf()), "gr");
		setDimensions(DIM);
	}
	
	@Override
	public List<int[]> getCurrentState() {
		return currentState;
	}
	
	@Override
	public int getDim() {
		return dim;
	}
	
	@Override
	public List<int[]> getNextState() {
		return currentState;
	}
	
	@Override
	public void initCells() {
		graphics.tell(new GameInit(), getSelf());
		final Random rnd = new Random();
		IntStream.range(0, dim).parallel().forEachOrdered(r -> {
			IntStream.range(0, dim).parallel().forEachOrdered(c -> {
				final int left = c - 1, down = r - 1, right = c + 1;
				
				if (firstFlag) {
					cell = getContext().actorOf(Props.create(Cell.class, new Coordinates(r, c), getSelf()), "cell" + c + ":" + r);
					
					boolean flag = false;
					
					if (left >= 0) {
						// cella a sx
						// | - | - | - |
						// | x | - | - |
						// | - | - | - |
						cell.tell(matrix[r][left], getSelf());
						matrix[r][left].tell(cell, getSelf());
						flag = true;
					}
					
					if (down >= 0) {
						// cella in alto
						// | - | x | - |
						// | - | - | - |
						// | - | - | - |
						cell.tell(matrix[down][c], getSelf());
						matrix[down][c].tell(cell, getSelf());
						
						// cella in alto a sx
						// | x | - | - |
						// | - | - | - |
						// | - | - | - |
						if (flag) {
							cell.tell(matrix[down][left], getSelf());
							matrix[down][left].tell(cell, getSelf());
						}
						
						// cella in alto a dx
						// | - | - | x |
						// | - | - | - |
						// | - | - | - |
						if (right < dim) {
							cell.tell(matrix[down][right], getSelf());
							matrix[down][right].tell(cell, getSelf());
						}
					}
					if ((r == (dim / 2)) && (c == (dim / 2))) {
						center = cell;
					}
					matrix[r][c] = cell;
				}
				if (rnd.nextInt(1000) < 200) {
					cell.tell(ALIVE, getSelf());
				} else {
					cell.tell(DEAD, getSelf());
				}
			}	);
		});
		firstFlag = false;
		graphics.tell(new GameInitDone(), getSelf());
	}
	
	@Override
	public void onReceive(final Object arg0) throws Exception {
		if (arg0 instanceof int[]) {
			if (((int[]) arg0)[2] == ALIVE) {
				currentState.add((int[]) arg0);
			}
			counter++;
			if (counter == n) {
				counter = 0;
				graphics.tell(currentState, getSelf());
				currentState = new LinkedList<int[]>();
				if (!stopped) {
					start();
					// Thread.sleep(10);
				}
			}
		} else if (arg0 instanceof String) {
			switch ((String) arg0) {
				case "start":
					stopped = false;
					start();
					break;
				case "stop":
					stop();
					break;
				case "reset":
					reset();
					break;
				default:
					setFrameRate(Integer.parseInt((String) arg0));
					break;
			}
		} else {
			unhandled(arg0);
		}
	}
	
	@Override
	public void register(final IObserver<List<int[]>> src) {
	}
	
	@Override
	public void reset() {
		stop();
		currentState = new LinkedList<int[]>();
		initCells();
	}
	
	@Override
	public void setDimensions(final int dim) {
		matrix = new ActorRef[dim][dim];
		this.dim = dim;
		n = dim * dim;
		initCells();
	}
	
	@Override
	public void setFrameRate(final int value) {
		// framems = 1000 / value;
	}
	
	@Override
	public void start() {
		center.tell(new Start(), getSelf());
	}
	
	@Override
	public void stop() {
		stopped = true;
	}
}
