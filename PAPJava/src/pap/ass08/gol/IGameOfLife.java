package pap.ass08.gol;

import java.util.List;

import pap.ass08.commons.IObservable;

/**
 * Game of life
 * 
 * @author Matteo Francia
 *
 */
public interface IGameOfLife extends IObservable<List<int[]>> {
	
	List<int[]> getCurrentState();
	
	int getDim();
	
	List<int[]> getNextState();
	
	/**
	 * Randomly initialize the cells
	 */
	void initCells();
	
	/**
	 * reset the game
	 */
	void reset();
	
	/**
	 * Set the dimensions of the game
	 */
	void setDimensions(int dim);
	
	void setFrameRate(int value);
	
	/**
	 * Start the game
	 */
	void start();
	
	/**
	 * Stop the game
	 */
	void stop();
}
