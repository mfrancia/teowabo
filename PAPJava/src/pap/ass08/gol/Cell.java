package pap.ass08.gol;

import java.util.LinkedList;
import java.util.List;

import pap.ass08.commons.Coordinates;
import pap.ass08.gol.msg.Alive;
import pap.ass08.gol.msg.Start;
import akka.actor.ActorRef;
import akka.actor.UntypedActor;

/**
 * Cell
 * 
 * @author Matteo Francia
 *
 */
public class Cell extends UntypedActor implements ICell {
	
	/**
	 * s(t)
	 */
	private int						alive;
	private boolean					alreadySentFlag;
	/**
	 * Cell's position into the grid
	 */
	private final Coordinates		coordinates;
	private int						count;
	/**
	 * s(t+1)
	 */
	private final int[]				futureState;
	private int[]					internalState;
	private final ActorRef			main;
	private final List<ActorRef>	neighbors;
	
	public Cell(final Coordinates p2d, final ActorRef main) {
		coordinates = p2d;
		internalState = new int[] { p2d.getX().intValue(), p2d.getY().intValue(), GameOfLife.DEAD };
		futureState = new int[] { p2d.getX().intValue(), p2d.getY().intValue(), GameOfLife.DEAD };
		this.main = main;
		neighbors = new LinkedList<ActorRef>();
	}
	
	@Override
	public int[] getFutureState() {
		if (internalState[2] == GameOfLife.ALIVE) {
			if ((alive < 2) || (alive > 3)) {
				futureState[2] = GameOfLife.DEAD;
			} else {
				futureState[2] = GameOfLife.ALIVE;
			}
		} else if (alive == 3) {
			futureState[2] = GameOfLife.ALIVE;
		}
		return futureState;
	}
	
	@Override
	public List<ActorRef> getNeighbors() {
		return neighbors;
	}
	
	@Override
	public Coordinates getP2d() {
		return coordinates;
	}
	
	@Override
	public int[] getState() {
		return internalState;
	}
	
	@Override
	public boolean isAlive() {
		return internalState[2] == GameOfLife.ALIVE ? true : false;
	}
	
	@Override
	public void onReceive(final Object arg0) throws Exception {
		if (arg0 instanceof Integer) {
			setState((int) arg0);
		} else if (arg0 instanceof ActorRef) {
			neighbors.add((ActorRef) arg0);
		} else if (arg0 instanceof Start) {
			start();
		} else if (arg0 instanceof Alive) {
			count++;
			if (((Alive) arg0).isAlive()) {
				alive++;
			}
			if (!alreadySentFlag) {
				start();
			}
			if (count == neighbors.size()) {
				main.tell(getFutureState(), getSelf());
				updateState();
				alreadySentFlag = false;
				alive = count = 0;
			}
		} else {
			unhandled(arg0);
		}
	}
	
	@Override
	public void setState(final int state) {
		internalState[2] = state;
	}
	
	private void start() {
		alreadySentFlag = true;
		neighbors.forEach(n -> n.tell(new Alive(internalState[2]), getSelf()));
	}
	
	@Override
	public String toString() {
		return "[C:" + coordinates.getX() + "," + coordinates.getY() + "]" + " " + internalState[2];
	}
	
	@Override
	public void updateState() {
		internalState = futureState;
	}
}
