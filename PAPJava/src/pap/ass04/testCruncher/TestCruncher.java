package pap.ass04.testCruncher;

import java.util.stream.IntStream;

import pap.ass04.common.IWorkGroup;
import pap.ass04.common.WorkGroup;

public class TestCruncher {
	// private static final Long limit = 1000000000l;
	private static Long	limit	= 1000000000l;
	
	public static void main(final String[] args) {
		if (args.length > 0) {
			limit = Long.parseLong(args[0]);
		}
		new TestCruncher();
	}
	
	private IWorkGroup	wg;
	
	public TestCruncher() {
		config();
		start();
	}
	
	private void config() {
		wg = new WorkGroup() /* null */;
		final Secret secret = new Secret(limit);
		final int n = Runtime.getRuntime().availableProcessors();
//		final int n = 1;
		final Long range = limit / n;
		System.out.println(System.currentTimeMillis() + " SYSTEM STARTED");
		IntStream.range(0, n).forEach(v -> {
			wg.add(new SecretWorker("SecretWorker" + v, wg, secret, v * range, (v + 1) * range));
		});
	}
	
	public void start() {
		wg.start();
	}
	
}
