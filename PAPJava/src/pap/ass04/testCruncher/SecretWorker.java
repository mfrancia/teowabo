package pap.ass04.testCruncher;

import java.util.stream.LongStream;

import pap.ass04.common.AbstractWorker;
import pap.ass04.common.IWorkGroup;

public class SecretWorker extends AbstractWorker {
	
	public SecretWorker(final String id, final IWorkGroup workerGroup, final Secret secret, final Long min, final Long max) {
		super(id, () -> {
			LongStream.range(min, max).forEach(value -> {
				if (secret.guess(value)) {
					System.out.println("[" + id + "] found the secret: " + value);
					workerGroup.forEach(worker -> worker.stop());
				}
			});
		});
//		super(id, () -> {
//			LongStream.range(min, max).parallel().forEach(value -> {
//				if (secret.guess(value)) {
//					System.out.println("[" + id + "] found the secret: " + value);
//					workerGroup.forEach(worker -> worker.stop());
//				}
//			});
//		});
	}
	
}
