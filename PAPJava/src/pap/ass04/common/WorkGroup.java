package pap.ass04.common;

import java.util.LinkedList;

public class WorkGroup extends LinkedList<IWorker> implements IWorkGroup {
	private static final long	serialVersionUID	= -61259995364306002L;
	
	@Override
	public void addWorker(final IWorker worker) {
		super.add(worker);
	}
	
	@Override
	public void start() {
		forEach(worker -> worker.start());
	}
	
	@Override
	public void stop() {
		forEach(worker -> worker.stop());
	}
	
}
