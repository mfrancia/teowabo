package pap.ass04.common;

import java.util.List;

/**
 * Astrazione di flusso di controllo
 * 
 * @author Matteo Francia
 *
 */
public abstract class AbstractWorker implements IWorker {
	private Thread			behaviour;
	private final String	id;
	private Boolean			stop;
	private List<IWorker>	workerGroup;
	
	/**
	 * Crea un worker senza indicare l'azione da ripetere
	 * 
	 * @param id
	 */
	public AbstractWorker(final String id) {
		this(id, null);
	}
	
	/**
	 * Crea un worker indicando id e behaviour (azione da ripetere)
	 * 
	 * @param id
	 * @param behaviour
	 */
	public AbstractWorker(final String id, final Runnable behaviour) {
		stop = false;
		this.id = id;
		if (behaviour != null) {
			setBehaviour(behaviour);
		}
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public List<IWorker> getWokerGroup() {
		return workerGroup;
	}
	
	@Override
	public synchronized Boolean isStopped() {
		return stop;
	}
	
	@Override
	public void setBehaviour(final Runnable behaviour) {
		if (this.behaviour != null) {
			throw new RuntimeException("Worker's behaviour cannot change over time");
		}
		
		this.behaviour = new Thread(() -> {
			while (!isStopped()) {
				behaviour.run();
			}
		});
	}
	
	@Override
	public void setWorkerGroup(final List<IWorker> workerGroup) {
		this.workerGroup = workerGroup;
	}
	
	@Override
	public synchronized void start() {
		behaviour.start();
		// System.out.println(System.currentTimeMillis() + " [" + id + "] started");
	}
	
	@Override
	public synchronized void stop() {
		stop = true;
		System.out.println(System.currentTimeMillis() + " [" + id + "] stopped");
		behaviour.interrupt();
	}
	
}
