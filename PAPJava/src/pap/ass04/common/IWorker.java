package pap.ass04.common;

import java.util.List;

public interface IWorker {
	/**
	 * Ritorna l'identificatore del worker
	 * 
	 * @return
	 */
	String getId();
	
	/**
	 * Ritorna il gruppo di lavoro del worker
	 */
	List<IWorker> getWokerGroup();
	
	/**
	 * Indica se il behaviour è in azione o meno
	 */
	Boolean isStopped();
	
	/**
	 * Imposta il behaviour del worker. NB il behaviour specificato è ripetuto in loop fino a quando sul flusso di controllo non è eseguito il comando di stop (e inizia quando è eseguito il comando
	 * start)
	 * 
	 * @param behaviour
	 */
	void setBehaviour(Runnable behaviour);
	
	/**
	 * Setta il gruppo di lavoro di questo worker
	 */
	void setWorkerGroup(List<IWorker> workerGroup);
	
	/**
	 * Inizia il flusso di esecuzione
	 */
	void start();
	
	/**
	 * Ferma il flusso di esecuzione
	 */
	void stop();
}
