package pap.ass04.textBall;

public class TextLibTest {
	
	public static void main(final String[] args) {
		final TextLib lib = TextLibFactory.getInstance();
		lib.cls();
		lib.writeAt(10, 5, "*");
		lib.writeAt(3, 2, "*", 1);
	}
}
