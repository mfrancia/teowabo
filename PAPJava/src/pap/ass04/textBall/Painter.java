package pap.ass04.textBall;

public class Painter {
	private final TextLib	lib;
	
	public Painter(final String id, final TextLib lib) {
		this.lib = lib;
	}
	
	/**
	 * Disegna un * nel punto da aggiornare e cancella l'asterisco nella posizione precedente
	 * 
	 * @param old
	 * @param newp
	 * @param c
	 */
	public void draw(final P2d old, final P2d newp, final int c) {
		lib.writeAt(old.getX().intValue(), old.getY().intValue(), " ");
		lib.writeAt(newp.getX().intValue(), newp.getY().intValue(), "*", c);
	}
}
