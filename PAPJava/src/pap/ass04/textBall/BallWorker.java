package pap.ass04.textBall;

import java.util.Random;

import pap.ass04.common.AbstractWorker;

/**
 * Prende come riferimento l'esempio BouncingBalls
 * 
 * @author Matteo Francia
 *
 */
public class BallWorker extends AbstractWorker {
	private final Boundary	bounds;
	private P2d				pos;
	private V2d				vel;
	
	public BallWorker(final String id, final Boundary boundary, final Painter pw) {
		super(id);
		pos = boundary.getRandomPoint();
		vel = new V2d(1, 1);
		bounds = boundary;
		
		final Random rand = new Random();
		final int c = rand.nextInt(20);
		
		setBehaviour(() -> {
			/*
			 * Pattern observer, quando aggiorno la posizione della palla aggiorno il componente painter
			 */
			final P2d old = pos;
			updatePos();
			pw.draw(old, pos, c);
			try {
				Thread.sleep(100);
			} catch (final Exception ex) {
				// ex.printStackTrace();
			}
		});
	}
	
	private void applyConstraints() {
		if (pos.getX() > bounds.getX1()) {
			pos = new P2d(bounds.getX1(), pos.getY());
			vel = new V2d(-vel.getX(), vel.getY());
		} else if (pos.getX() < bounds.getX0()) {
			pos = new P2d(bounds.getX0(), pos.getY());
			vel = new V2d(-vel.getX(), vel.getY());
		} else if (pos.getY() > bounds.getY1()) {
			pos = new P2d(pos.getX(), bounds.getY1());
			vel = new V2d(vel.getX(), -vel.getY());
		} else if (pos.getY() < bounds.getY0()) {
			pos = new P2d(pos.getX(), bounds.getY0());
			vel = new V2d(vel.getX(), -vel.getY());
		}
	}
	
	public synchronized P2d getPos() {
		return new P2d(pos.getX(), pos.getY());
	}
	
	private synchronized void updatePos() {
		pos = pos.sum(vel);
		applyConstraints();
	}
	
}
