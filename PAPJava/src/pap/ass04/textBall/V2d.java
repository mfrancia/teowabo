/*
 * V2d.java
 * 
 * Copyright 2000-2001-2002 aliCE team at deis.unibo.it
 * 
 * This software is the proprietary information of deis.unibo.it Use is subject to license terms.
 */
package pap.ass04.textBall;

/**
 *
 * 2-dimensional vector objects are completely state-less
 *
 */
public class V2d implements java.io.Serializable {
	
	private static final long	serialVersionUID	= -2423145234567537315L;
	private final double		x, y;
	
	public V2d(final double x, final double y) {
		this.x = x;
		this.y = y;
	}
	
	public V2d(final P2d p0, final pap.ass04.textBall.P2d p1) {
		x = p0.getX() - p1.getX();
		y = p0.getY() - p1.getY();
	}
	
	public double abs() {
		return Math.sqrt((x * x) + (y * y));
	}
	
	public V2d getNormalized() {
		final double module = Math.sqrt((x * x) + (y * y));
		return new V2d(x / module, y / module);
	}
	
	public Double getX() {
		return x;
	}
	
	public Double getY() {
		return y;
	}
	
	public double module() {
		return Math.sqrt((x * x) + (y * y));
	}
	
	public V2d mul(final double fact) {
		return new V2d(x * fact, y * fact);
	}
	
	public V2d sum(final V2d v) {
		return new V2d(x + v.x, y + v.y);
	}
	
	@Override
	public String toString() {
		return "V2d(" + x + "," + y + ")";
	}
}
