/*
 * Created on Feb 10, 2005
 */
package pap.ass04.textBall;

import java.util.Random;

/**
 * Prende come riferimento l'esempio BouncingBalls
 * 
 * @author Matteo Francia
 *
 */
public class Boundary {
	private final Double	x0;
	private final Double	x1;
	private final Double	y0;
	private final Double	y1;
	
	public Boundary(final Double x0, final Double y0, final Double x1, final Double y1) {
		this.x0 = x0;
		this.y0 = y0;
		this.x1 = x1;
		this.y1 = y1;
	}
	
	public P2d getCenter() {
		return new P2d((x0 - x1) / 2.0, (y0 - y1) / 2.0);
	}
	
	public P2d getRandomPoint() {
		Double x, y;
		
		final Random r = new Random();
		x = (double) r.nextInt(x1.intValue());
		y = (double) r.nextInt(y1.intValue());
		
		return new P2d(x, y);
	}
	
	public Double getX0() {
		return x0;
	}
	
	public Double getX1() {
		return x1;
	}
	
	public Double getY0() {
		return y0;
	}
	
	public Double getY1() {
		return y1;
	}
	
}
