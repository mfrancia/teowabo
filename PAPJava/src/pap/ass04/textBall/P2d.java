package pap.ass04.textBall;

/**
 * Punto in una viewport grafica
 * 
 * @author aricci
 */
public class P2d {
	
	public static double distance(final P2d p0, final P2d p1) {
		return new V2d(p0, p1).module();
	}
	
	private final Double	x, y;
	
	public P2d(final Double x, final Double y) {
		this.x = x;
		this.y = y;
	}
	
	public Double getX() {
		return x;
	}
	
	public Double getY() {
		return y;
	}
	
	public P2d sum(final V2d v) {
		return new P2d(x + v.getX(), y + v.getY());
	}
	
	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}
	
}
