package pap.ass04.textBall;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import pap.ass04.common.IWorkGroup;
import pap.ass04.common.WorkGroup;

public class TextBallMain {
	private static int	n	= 10;
	
	public static void main(final String[] args) {
		if (args.length > 0) {
			n = Integer.parseInt(args[0]);
		}
		new TextBallMain();
	}
	
	private IWorkGroup	wg;
	
	public TextBallMain() {
		config();
		start();
	}
	
	private void config() {
		final TextLib lib = TextLibFactory.getInstance();
		lib.cls();
		
		final List<BallWorker> balls = new LinkedList<BallWorker>();
		wg = new WorkGroup();
		
		final Painter pw = new Painter("[Painter]", lib);
		final Boundary boundary = new Boundary(0.0, 0.0, 80.0, 20.0);
		IntStream.range(0, n).forEach(v -> {
			final BallWorker bw = new BallWorker("[BallWorker" + v + "]", boundary, pw);
			balls.add(bw);
			wg.add(bw);
		});
	}
	
	private void start() {
		wg.start();
	}
}
