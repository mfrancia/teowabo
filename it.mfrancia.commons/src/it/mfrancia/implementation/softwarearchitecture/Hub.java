package it.mfrancia.implementation.softwarearchitecture;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.softwarearchitecture.IConnector;
import it.mfrancia.interfaces.softwarearchitecture.IHub;

import java.util.HashMap;

public class Hub extends Loggable implements IHub {
	private HashMap<CharSequence, IConnector>	map;
	
	public Hub(CharSequence name) {
		super(name);
		this.map = new HashMap<>();
	}
	
	@Override
	public void plugIn(IConnector connector) {
		map.put(connector.getName(), connector);
	}
	
	@Override
	public void plugOut(IConnector connector) {
		map.remove(connector.getName());
	}
	
	@Override
	public IConnector getConnector(CharSequence id) {
		return map.get(id);
	}
	
}
