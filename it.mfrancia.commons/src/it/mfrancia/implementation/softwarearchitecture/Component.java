package it.mfrancia.implementation.softwarearchitecture;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.multithreading.ITask;
import it.mfrancia.interfaces.softwarearchitecture.IComponent;

public class Component extends Loggable implements IComponent {
	private ITask	task;
	
	public Component(CharSequence name) {
		super(name);
	}
	
	@Override
	public void setTask(ITask task) {
		this.task = task;
	}
	
	@Override
	public void execute() {
		task.doTask();
	}
	
}
