package it.mfrancia.implementation.softwarearchitecture;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.softwarearchitecture.IComponent;
import it.mfrancia.interfaces.softwarearchitecture.IConnector;

public class Connector extends Loggable implements IConnector {
	private IComponent	component;
	
	public Connector(CharSequence name) {
		super(name);
	}
	
	@Override
	public void setComponent(IComponent component) {
		this.component = component;
	}
	
	@Override
	public IComponent getComponent() {
		return component;
	}
	
}
