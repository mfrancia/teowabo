package it.mfrancia.implementation;

import it.mfrancia.interfaces.IColor;

public class Color implements IColor {
	Integer	r, g, b;
	
	public Color(final Integer color) {
		r = (color >> 16) & 255;
		g = (color >> 8) & 255;
		b = (color) & 255;
	}
	
	public Color(final Integer r, final Integer g, final Integer b) {
		this.r = r;
		this.b = b;
		this.g = g;
	}
	
	@Override
	public Integer getBlue() {
		return b;
	}
	
	@Override
	public Integer getColor() {
		return getRed() << (16 + getGreen()) << (8 + getBlue());
	}
	
	@Override
	public Integer getGreen() {
		return g;
	}
	
	@Override
	public Integer getRed() {
		return r;
	}
}
