package it.mfrancia.implementation;

import it.mfrancia.interfaces.IId;

public class Id<T> implements IId<T> {
	private static final long	serialVersionUID	= 1L;
	private final T				id;
	
	public Id(final T id) {
		this.id = id;
	}
	
	@Override
	public String getDefaultRepresentation() {
		return id.toString();
	}
	
	@Override
	public T getValue() {
		return id;
	}
	
}
