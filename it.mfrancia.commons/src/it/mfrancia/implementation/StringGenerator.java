package it.mfrancia.implementation;

import java.util.Random;

/**
 * Generate a random string
 * 
 * @author Matteo Francia
 * 
 */
public class StringGenerator {
	public enum DictionaryType {
		alphanumeric, alphanumericCaseSensitive, extended
	};
	
	public static CharSequence getRandomSequence(final DictionaryType type, final int length) {
		final StringBuffer buffer = new StringBuffer();
		final Random r = new Random();
		String template = "";
		switch (type) {
			case alphanumeric:
				template = alphanumeric;
				break;
			case alphanumericCaseSensitive:
				template = alphanumeric;
				break;
			default:
				template = extended;
				break;
		}
		for (int i = 0; i < length; i++) {
			buffer.append(template.charAt(r.nextInt(template.length())));
		}
		return buffer.toString();
	}
	
	public static CharSequence getRandomSequence(final int length) {
		return getRandomSequence(DictionaryType.extended, length);
	}
	
	private static final String	alphanumeric				= "0987654321poiuytrewqlkjhgfdsamnbvcxz";
	
	private static final String	alphanumericCaseSensitive	= alphanumeric + "POIUYTREWQLKJHGFDSAMNBVCXZ";
	
	private static final String	extended					= alphanumericCaseSensitive + " \\|!\"$%&/()=?^<>,-._:;[]{}*+-";
}
