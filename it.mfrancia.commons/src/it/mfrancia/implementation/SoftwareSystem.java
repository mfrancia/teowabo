package it.mfrancia.implementation;

public abstract class SoftwareSystem {
	public SoftwareSystem() {
		config();
		start();
	}
	
	public abstract void config();
	
	public abstract void start();
	
	public static void main(String[] args) {}
}
