package it.mfrancia.implementation.serialization;

import it.mfrancia.interfaces.serialization.ISerializationFactory;

import com.google.gson.Gson;

public class SerializationFactory<T> implements ISerializationFactory<T> {
	
	/**
	 * Override, always it returns null by default
	 */
	@Override
	public byte[] customSerialization(final T o) {
		return null;
	}
	
	@Override
	public byte[] serialize(final Type t, final T o) {
		byte[] ret = null;
		switch (t) {
			case JSON:
				ret = serializeJson(o);
				break;
			case JAVA:
				break;
			case CUSTOM:
				ret = customSerialization(o);
				break;
		}
		return ret;
	}
	
	@Override
	public byte[] serializeJson(final T o) {
		final Gson g = new Gson();
		return g.toJson(o).getBytes();
	}
}
