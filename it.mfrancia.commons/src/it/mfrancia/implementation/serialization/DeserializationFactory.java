package it.mfrancia.implementation.serialization;

import it.mfrancia.interfaces.serialization.IDeserializationFactory;

import com.google.gson.Gson;

public class DeserializationFactory<T> implements IDeserializationFactory<T> {
	
	/**
	 * Override, always it returns null by default
	 */
	@Override
	public T customDeserialization(final byte[] data) {
		return null;
	}
	
	@Override
	public T deserialize(final Type t, final byte[] data) {
		T ret = null;
		switch (t) {
			case JSON:
				ret = deserializeJson(data, Object.class);
				break;
			case JAVA:
				break;
			case CUSTOM:
				ret = customDeserialization(data);
				break;
		}
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public T deserializeJson(final byte[] data, final Class<?> c) {
		final Gson gson = new Gson();
		return (T) gson.fromJson(new String(data), c);
	}
}
