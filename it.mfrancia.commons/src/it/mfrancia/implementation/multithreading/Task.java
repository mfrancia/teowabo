package it.mfrancia.implementation.multithreading;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.multithreading.ITask;

public abstract class Task extends Loggable implements ITask {
	private IObserver<ITask>	observer;
	
	public Task(final String name) {
		super(name);
	}
	
	public Task(final String name, final IObserver<ITask> observer) {
		super(name);
		this.observer = observer;
	}
	
	@Override
	public void doTask() {
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				task();
				if (observer != null) {
					observer.update(Task.this, Task.this);
				}
			}
		}).start();
		
	}
	
	@Override
	public Boolean equals(final ITask command) {
		return command.getName().toString().equals(getName());
	}
	
	@Override
	public void register(final IObserver<ITask> src) {
		observer = src;
	}
	
	abstract public void task();
	
}
