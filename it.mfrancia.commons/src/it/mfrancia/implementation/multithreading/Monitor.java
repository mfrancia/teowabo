package it.mfrancia.implementation.multithreading;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.multithreading.IMonitor;
import it.mfrancia.interfaces.multithreading.ITask;

public class Monitor extends Loggable implements IMonitor {
	private boolean		busy;
	private final ITask	idle;
	
	public Monitor(final ITask idle, final ITask... commands) {
		super("monitor");
		for (final ITask c : commands) {
			c.register(this);
		}
		busy = false;
		this.idle = idle;
		// this.idle.register(this);
		if (this.idle != null) {
			this.idle.doTask();
		}
	}
	
	@Override
	public synchronized void addOutput(final ITask o) {
		if (isBusy()) {
			log(o.getName().toString() + " monitor is busy...");
			return;
		} else {
			setBusy(true);
			o.doTask();
		}
		
	}
	
	@Override
	public synchronized void execute(final ITask task) {
		addOutput(task);
	}
	
	@Override
	public Boolean isBusy() {
		return busy;
	}
	
	protected void setBusy(final boolean busy) {
		this.busy = busy;
	}
	
	@Override
	public synchronized void update(final IObservable<ITask> src, final ITask info) {
		setBusy(false);
		if (idle != null) {
			idle.doTask();
		}
	}
	
}
