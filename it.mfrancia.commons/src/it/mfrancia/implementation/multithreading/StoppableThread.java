package it.mfrancia.implementation.multithreading;

import it.mfrancia.implementation.logging.Loggable;

public abstract class StoppableThread extends Loggable implements Runnable {
	private boolean			notStopped;
	private final Thread	t;
	
	public StoppableThread(final String name) {
		super(name);
		notStopped = true;
		t = new Thread(this);
		t.setName(getName());
	}
	
	public synchronized boolean isNotStopped() {
		return notStopped;
	}
	
	public void sleep(final int millis) {
		try {
			Thread.sleep(millis);
		} catch (final InterruptedException e) {
		}
	}
	
	public void start() {
		t.start();
	}
	
	public synchronized void stop() {
		notStopped = false;
		t.interrupt();
	}
}
