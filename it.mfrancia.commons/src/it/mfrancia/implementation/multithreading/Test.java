package it.mfrancia.implementation.multithreading;

import it.mfrancia.implementation.SoftwareSystem;

public class Test extends SoftwareSystem {
	public static void main(final String[] args) {
		new Test();
	}
	
	private Monitor	monitor;
	private Task	task1;
	
	private Task	task2;
	
	@Override
	public void config() {
		final Task idle = new Task("idle") {
			
			@Override
			public void task() {
				System.out.println(getName());
			}
		};
		task1 = new Task("task1") {
			
			@Override
			public void task() {
				System.out.println(getName() + " started");
				System.out.println(getName() + " sleeping...");
				try {
					Thread.sleep(2000);
				} catch (final InterruptedException e) {
				}
				System.out.println(getName() + " done");
			}
		};
		task2 = new Task("task2") {
			
			@Override
			public void task() {
				System.out.println(getName() + " started");
				System.out.println(getName() + " done");
			}
		};
		
		monitor = new Monitor(idle, task1, task2);
	}
	
	@Override
	public void start() {
		monitor.addOutput(task1);
		try {
			Thread.sleep(500);
		} catch (final InterruptedException e) {
		}
		monitor.addOutput(task2);
	}
}
