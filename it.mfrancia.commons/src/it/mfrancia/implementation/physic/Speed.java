package it.mfrancia.implementation.physic;

import it.mfrancia.interfaces.physic.ISpeed;

public class Speed implements ISpeed {
	private final Double	speed;
	
	public Speed(final Double speed) {
		this.speed = speed;
	}
	
	@Override
	public Double getSpeed() {
		return speed;
	}
	
}
