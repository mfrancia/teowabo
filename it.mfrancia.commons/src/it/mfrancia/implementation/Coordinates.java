package it.mfrancia.implementation;

import it.mfrancia.interfaces.ICoordinates;

public class Coordinates implements ICoordinates {
	private static final long	serialVersionUID	= -6716826769541667125L;
	private final Number		x;
	private final Number		y;
	private Number				z;
	private Number				h;
	
	public Coordinates(final Number x, final Number y) {
		this.x = x;
		this.y = y;
	}
	
	public Coordinates(final Number x, final Number y, final Number z, final Number altitude) {
		this.x = x;
		this.y = y;
		this.z = z;
		h = altitude;
	}
	
	@Override
	public Number getAltitude() {
		return h;
	}
	
	@Override
	public CharSequence getDefaultRepresentation() {
		String s = "";
		s += "x:" + (x == null ? "?" : x.doubleValue()) + ",";
		s += "y:" + (y == null ? "?" : y.doubleValue()) + ",";
		s += "z:" + (z == null ? "?" : z.doubleValue()) + ",";
		s += "h:" + (h == null ? "?" : h.doubleValue());
		return s;
	}
	
	@Override
	public Number getX() {
		return x;
	}
	
	@Override
	public Number getY() {
		return y;
	}
	
	@Override
	public Number getZ() {
		return z;
	}
}
