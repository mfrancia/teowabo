package it.mfrancia.implementation.logging;

import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.logging.ILogger;

import java.util.ArrayList;

public class Loggable extends Object implements ILoggable {
	private final ArrayList<ILoggable>	children;
	private ILogger						logger;
	private final CharSequence			name;
	protected boolean					verbose;
	
	public Loggable(final CharSequence name) {
		this.name = name;
		children = new ArrayList<ILoggable>();
		setVerbose(true);
	}
	
	public Loggable() {
		this(null);
	}
	
	@Override
	public void addLoggableChild(final ILoggable child) {
		child.setLogger(logger);
		child.setVerbose(verbose);
		children.add(child);
	}
	
	@Override
	public String getName() {
		return name.toString();
	}
	
	@Override
	public boolean getVerbose() {
		return verbose;
	}
	
	protected void log(String s) {
		if (verbose) {
			s = getName() == null ? s:  getName() + " *** " + s;
			if (logger != null) {
				logger.addOutput(s);
			} else {
				System.out.println(s);
			}
		}
	}
	
	@Override
	public void setLogger(final ILogger logger) {
		this.logger = logger;
	}
	
	@Override
	public void setVerbose(final boolean verbose) {
		this.verbose = verbose;
		for (final ILoggable l : children) {
			l.setVerbose(verbose);
		}
	}
	
}
