package it.mfrancia.implementation.logging;

import it.mfrancia.interfaces.logging.ILog;

/**
 * Log unit
 * 
 * @author Matteo Francia
 *
 */
public class Log implements ILog {
	private final CharSequence	message;
	private final Priority		priority;
	
	public Log(final CharSequence message) {
		this(Priority.INFORMATION, message);
	}
	
	public Log(final Priority priority, final CharSequence message) {
		this.priority = priority;
		this.message = message;
	}
	
	@Override
	public CharSequence getMessage() {
		return message;
	}
	
	@Override
	public Priority getPriority() {
		return priority;
	}
	
}
