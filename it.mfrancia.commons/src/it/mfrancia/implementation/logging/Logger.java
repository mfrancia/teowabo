package it.mfrancia.implementation.logging;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILog;
import it.mfrancia.interfaces.logging.ILog.Priority;
import it.mfrancia.interfaces.logging.ILogger;

import java.util.Calendar;

public class Logger implements ILogger {
	public IOutput<String>	out;
	
	public Logger() {
	}
	
	public Logger(final IOutput<String> out) {
		this.out = out;
	}
	
	@Override
	public void addOutput(final String o) {
		log(o);
	}
	
	@Override
	public void log(final CharSequence message) {
		log(ILog.Priority.INFORMATION, message);
	}
	
	@Override
	public void log(final ILog log) {
		final String s = Calendar.getInstance().get(Calendar.HOUR) + ":" + Calendar.getInstance().get(Calendar.MINUTE) + ":" + Calendar.getInstance().get(Calendar.SECOND) + " "
				+ log.getPriority().toString() + " " + log.getMessage().toString();
		if (out == null) {
			System.out.println(s);
		} else {
			out.addOutput(s);
		}
	}
	
	@Override
	public void log(final Priority priority, final CharSequence message) {
		this.log(new Log(priority, message));
	}
}
