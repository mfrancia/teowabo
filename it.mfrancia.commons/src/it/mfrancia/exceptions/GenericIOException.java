package it.mfrancia.exceptions;

/**
 * A generic IO exception
 * 
 * @author Matteo Francia
 * 
 */
public class GenericIOException extends MyException {
	private static final long	serialVersionUID	= 1L;
	
	public GenericIOException(final String c) {
		super(c);
	}
	
}
