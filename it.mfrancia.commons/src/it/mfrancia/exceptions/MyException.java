package it.mfrancia.exceptions;

public class MyException extends Exception {
	private static final long	serialVersionUID	= 1L;
	
	private final String		c;
	
	public MyException(final String c) {
		this.c = c;
	}
	
	@Override
	public String toString() {
		return super.toString() + " " + c;
	}
}
