package it.mfrancia.interfaces.env;

public interface ILabel extends IGraphicComponent {
	void setText(String text);
}
