package it.mfrancia.interfaces.env;

import it.mfrancia.interfaces.IColor;

public interface IGrid {
	void add(Object component, int row, int column);
	
	void remove(Object component);
	
	void setBackgroundColor(IColor color);
	
	void setBackgroundColor(IColor color, int row, int column);
}
