package it.mfrancia.interfaces.env;

import it.mfrancia.interfaces.IColor;

public interface IGraphicComponent {
	void setBackground(IColor c);
}
