package it.mfrancia.interfaces.language;

import it.mfrancia.interfaces.serialization.ISerializable;

/**
 * A generic language
 * 
 * @author Matteo Francia
 *
 */
public interface ILetter extends ISerializable {
	String getPlainText();
}
