package it.mfrancia.interfaces.language;

import it.mfrancia.interfaces.serialization.ISerializable;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * A generic Word
 * 
 * @author Matteo Francia
 *
 */
public interface IWord extends ISerializable {
	
	enum Type {
		noun, verb, adverb, undefined
	};
	
	enum Gender {
		male, female, undefined
	};
	
	ILetter[] getLetters();
	
	HashMap<ILanguage, ArrayList<ISerializable>> getMeanings();
	
	IWord translateTo(ILanguage language);
	
	void setMeaning(ILanguage language, String meaning);
	
	String getPlainText();
}
