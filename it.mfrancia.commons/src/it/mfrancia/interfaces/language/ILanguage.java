package it.mfrancia.interfaces.language;

import it.mfrancia.interfaces.serialization.ISerializable;

/**
 * A generic language
 * 
 * @author Matteo Francia
 *
 */
public interface ILanguage extends ISerializable {
	
}
