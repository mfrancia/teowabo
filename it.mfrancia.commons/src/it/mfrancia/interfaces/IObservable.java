package it.mfrancia.interfaces;

public interface IObservable<T> {
	void register(IObserver<T> src);
}
