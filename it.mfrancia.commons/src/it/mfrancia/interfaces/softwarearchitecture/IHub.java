package it.mfrancia.interfaces.softwarearchitecture;

import it.mfrancia.interfaces.logging.ILoggable;

/**
 * Hot-pluggable Hub
 * 
 * @author Matteo Francia
 *
 */
public interface IHub extends ILoggable {
	/**
	 * Plug in a connector
	 * 
	 * @param hotPluggableComponent
	 */
	void plugIn(IConnector connector);
	
	/**
	 * Plug out a connector
	 * 
	 * @param connector
	 */
	void plugOut(IConnector connector);
	
	/**
	 * Get the reference to a specific connector
	 * 
	 * @param id
	 * @return
	 */
	IConnector getConnector(CharSequence id);
}
