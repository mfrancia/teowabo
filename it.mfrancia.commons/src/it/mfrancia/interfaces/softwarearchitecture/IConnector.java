package it.mfrancia.interfaces.softwarearchitecture;

import it.mfrancia.interfaces.logging.ILoggable;

public interface IConnector extends ILoggable {
	void setComponent(IComponent component);
	
	IComponent getComponent();
}
