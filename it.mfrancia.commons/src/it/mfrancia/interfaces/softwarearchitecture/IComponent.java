package it.mfrancia.interfaces.softwarearchitecture;

import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.multithreading.ITask;

/**
 * Generic hot-pluggable component
 * 
 * @author Matteo Francia
 *
 */
public interface IComponent extends ILoggable {
	/**
	 * Set the task of the component, it can change over time
	 * 
	 * @param task
	 */
	void setTask(ITask task);
	
	/**
	 * Execute the component task
	 */
	void execute();
}
