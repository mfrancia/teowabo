package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.MyException;

public interface IAsyncClientListener extends IConnectionListener, IDiscoverListener, IClientConnectedListener {
	/**
	 * Callback called when a write operation fails
	 * 
	 * @param e
	 *            exception that caused the failure of the writing operation
	 */
	void onWriteFailed(MyException e);
	
	/**
	 * Callback called when a write operation has been successful
	 */
	void onWriteSucceded();
}
