package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.MyException;

/**
 * Server initiated call-back
 * 
 * @author Matteo Francia
 * 
 */
public interface IAsyncServerListener extends IConnectionListener {
	/**
	 * Discoverable server initiated call-back (fail)
	 * 
	 * @param server
	 *            reference to the (initiated) server instance
	 */
	void onDiscoverableServerStartedFailed(IServer server, MyException ex);
	
	/**
	 * Discoverable server initiated call-back (success)
	 * 
	 * @param server
	 *            reference to the (initiated) server instance
	 */
	void onDiscoverableServerStartedSucceded(IServer server);
	
	/**
	 * Server initiated call-back (fail)
	 * 
	 * @param server
	 *            reference to the (initiated) server instance
	 */
	void onServerStartedFailed(IServer server, MyException ex);
	
	/**
	 * Server initiated call-back (success)
	 * 
	 * @param server
	 *            reference to the (initiated) server instance
	 */
	void onServerStartedSucceded(IServer server);
	
}
