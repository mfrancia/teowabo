package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILoggable;

/**
 * 
 * @author Matteo Francia
 * 
 */
public interface IConnection extends IOutput<byte[]>, ILoggable {
	/**
	 * close the connection
	 */
	void close();
	
	/**
	 * connect to the specified peer
	 */
	void connect() throws GenericIOException;
	
	/**
	 * get the connection id
	 * 
	 * @return
	 */
	String getId();
	
	/**
	 * get the remote peer
	 * 
	 * @return
	 */
	ISocket getPeer();
	
	/**
	 * 
	 * @return true if the connection is established, false otherwise
	 */
	boolean isConnected();
	
	/**
	 * read data from connection
	 * 
	 * @return received data
	 */
	byte[] read() throws GenericIOException;
	
	/**
	 * write data to connection
	 * 
	 * @param data
	 */
	void write(byte[] data) throws GenericIOException;
}
