package it.mfrancia.interfaces.networking;

import it.mfrancia.interfaces.networking.IConnectionFactory.Type;

public interface IClientFactory {
	IAsyncClient getAsyncClient(CharSequence name, Type type, ISocket socket, IAsyncClientListener listener);
	
	IClient getClient(CharSequence name, Type type, ISocket socket, IConnectionListener listener);
	
	IAsyncClient getLocalhostAsyncClient(CharSequence name, IAsyncClientListener listener);
	
	IClient getLocalhostClient(CharSequence name, IConnectionListener listener);
}
