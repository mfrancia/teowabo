package it.mfrancia.interfaces.networking;

/**
 * 
 * @author Matteo Francia
 *
 */
public interface IAsyncClient extends IClientDhcpLike {
	
	/**
	 * Set the {@link IAsyncClientListener} listener
	 * 
	 * @param listener
	 */
	void setEventListener(IAsyncClientListener listener);
}
