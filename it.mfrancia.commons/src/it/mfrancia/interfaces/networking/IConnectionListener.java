package it.mfrancia.interfaces.networking;

/**
 * 
 * @author Matteo Francia
 * 
 */
public interface IConnectionListener {
	/**
	 * Decide whether to accept a connection or not
	 * 
	 * @return true to accept the connection, false to reject it
	 */
	boolean onConnectionAccept(IConnection con);
	
	/**
	 * Callback invoked upon the closing phase of the connection
	 * 
	 * @param con
	 */
	void onConnectionClosed(IConnection con);
	
	/**
	 * On connection connected call back
	 * 
	 * @param con
	 *            reference to the connection
	 */
	public void onConnectionConnected(IConnection con);
	
	/**
	 * Callback invoked upon receipt of data
	 * 
	 * @param fromConn
	 * @param data
	 *            received data
	 */
	void onDataReceived(IConnection fromConn, byte[] data);
}
