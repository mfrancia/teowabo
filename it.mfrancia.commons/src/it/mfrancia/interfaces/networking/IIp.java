package it.mfrancia.interfaces.networking;

/**
 * Generic internet protocol address
 * 
 * @author Matteo Francia
 * 
 */
public interface IIp {
	String	ANY			= "0.0.0.0";
	String	BROADCAST	= "255.255.255.255";
	String	LOCALHOST	= "127.0.0.1";
	
	/**
	 * 
	 * @return address value
	 */
	CharSequence getIpValue();
}
