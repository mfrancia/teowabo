package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.MyException;

/**
 * 
 * @author Matteo Francia
 *
 */
public interface IClientConnectedListener {
	/**
	 * Callback called when the client connection has been successful
	 */
	public void onClientConnected(IClient client);
	
	/**
	 * Callback called when a connect operation fails
	 * 
	 * @param e
	 *            exception that caused the failure of the connect operation
	 */
	void onConnectionFailed(MyException e);
}
