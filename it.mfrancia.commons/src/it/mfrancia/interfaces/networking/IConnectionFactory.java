package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.GenericIOException;

/**
 * Connection factory (pattern)
 * 
 * @author Matteo Francia
 * 
 */
public interface IConnectionFactory {
	enum Type {
		SOCKETCHANNEL, TCP, UDP
	}
	
	/**
	 * Create a new connection
	 * 
	 * @param type
	 *            connection type (socket channel, udp, tcp)
	 * @param socket
	 *            socket to connect to
	 * @return created connection
	 */
	IConnection createConnection(Type type, ISocket socket) throws GenericIOException;
}
