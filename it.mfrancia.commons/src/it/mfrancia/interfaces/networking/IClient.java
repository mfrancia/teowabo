package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.IOutput;

public interface IClient extends IPeer, IOutput<byte[]> {
	/**
	 * Connect to the specified Peer
	 * 
	 * @throws GenericIOException
	 */
	void connect() throws GenericIOException;
	
	boolean isConnected();
	
	/**
	 * read data from the connection
	 * 
	 * @return
	 */
	byte[] read() throws GenericIOException;
	
	void startActiveReading();
	
	/**
	 * write data to the connection
	 * 
	 * @param data
	 */
	void write(byte[] data) throws GenericIOException;
}
