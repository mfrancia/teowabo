package it.mfrancia.interfaces.networking;

/**
 * Generic socket
 * 
 * @author Matteo Francia
 * 
 */
public interface ISocket {
	/**
	 * @return socket address
	 */
	IIp getIp();
	
	/**
	 * @return socket port
	 */
	IPort getPort();
	
	/**
	 * @return mapping between ISocket and String
	 */
	@Override
	String toString();
}
