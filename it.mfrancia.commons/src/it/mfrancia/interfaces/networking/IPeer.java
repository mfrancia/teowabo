package it.mfrancia.interfaces.networking;

import it.mfrancia.interfaces.logging.ILoggable;

/**
 * 
 * @author Matteo Francia
 * 
 */
public interface IPeer extends ILoggable {
	/**
	 * @return get the local socket
	 */
	ISocket getSocket();
	
	/**
	 * kill (stop) the host, close the existing connection(s)
	 */
	void kill();
	
	/**
	 * set the event listener
	 * 
	 * @param l
	 */
	void setEventListener(IConnectionListener l);
}
