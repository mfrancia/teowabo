package it.mfrancia.interfaces.networking;

/**
 * 
 * @author Matteo Francia
 * 
 */
public interface IPort {
	
	int	ANY	= 0;
	int	MAX	= 65535;
	
	/**
	 * 
	 * @return port value (as integer)
	 */
	int getPortNumber();
}
