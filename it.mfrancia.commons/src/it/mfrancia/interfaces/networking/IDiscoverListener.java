package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.MyException;

/**
 * Discover terminated call-back
 * 
 * @author Matteo Francia
 * 
 */
public interface IDiscoverListener {
	/**
	 * Callback called when a discover operation fails
	 * 
	 * @param e
	 *            exception that caused the failure of the discover operation
	 */
	void onDiscoverFailed(MyException e);
	
	/**
	 * Discover operation is terminated with success
	 * 
	 * @param conn
	 *            reference to the discovered connections
	 */
	void onDiscoverTerminated(IConnection[] conn);
}
