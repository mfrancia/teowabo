package it.mfrancia.interfaces.networking;

/**
 *
 * @author Matteo Francia
 *
 */
public interface IServer extends IPeer {
	/**
	 * Return all of the active connections
	 *
	 * @return active connections
	 */
	IConnection[] getConnections();
	
	/**
	 * Check if the server is running
	 * 
	 * @return true if the server us running, false instead
	 */
	boolean isRunning();
	
	/**
	 * Kill the specified connection
	 *
	 * @param con
	 *            connection to be closed
	 */
	void killConnection(IConnection con);
	
	/**
	 * Set the {@link IAsyncServerListener} listener
	 * 
	 * @param listener
	 */
	void setEventListener(IAsyncServerListener listener);
	
	/**
	 * Start the server execution
	 */
	public void start();
}
