package it.mfrancia.interfaces.networking;

import it.mfrancia.exceptions.GenericIOException;

/**
 * 
 * @author Matteo Francia
 * 
 */
public interface IClientDhcpLike extends IClient {
	/**
	 * Discover a server on a specified network
	 * 
	 * @param networkIp
	 *            network address
	 * @param port
	 *            port on which the server is discoverable
	 * @return connection to the server
	 * @throws GenericIOException
	 */
	IConnection[] discover(IIp networkIp, int port) throws GenericIOException;
	
	void setConnection(IConnection conn);
}
