package it.mfrancia.interfaces.multithreading;

import it.mfrancia.interfaces.IObservable;

/**
 * Generic command
 * 
 * @author Matteo Francia
 *
 */
public interface ITask extends IObservable<ITask> {
	void doTask();
	
	/**
	 * Return true if the specified command is equal to this command
	 * 
	 * @param command
	 * @return
	 */
	Boolean equals(ITask command);
	
	/**
	 * Return the command name
	 * 
	 * @return
	 */
	CharSequence getName();
}
