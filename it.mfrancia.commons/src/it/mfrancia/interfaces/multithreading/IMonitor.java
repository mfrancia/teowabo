package it.mfrancia.interfaces.multithreading;

import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.IOutput;

public interface IMonitor extends IOutput<ITask>, IObserver<ITask> {
	void execute(ITask task);
	
	Boolean isBusy();
}
