package it.mfrancia.interfaces.serialization;

/**
 * Deserialization factory
 * 
 * @author Matteo Francia
 * 
 * @param <T>
 *            type of the object to be deserialized
 */
public interface IDeserializationFactory<T> {
	/**
	 * Admitted serialization type
	 * 
	 */
	public enum Type {
		CUSTOM, JAVA, JSON
	}
	
	/**
	 * Override to provide a custom deserialization. Called when invoke deserialize method passing a CUSTOM type of deserialization
	 * 
	 * @param data
	 *            bytes represeting the object to be deserialized
	 * @return deserialized object
	 */
	T customDeserialization(byte[] data);
	
	/**
	 * Deserialization method
	 * 
	 * @param t
	 *            type of deserialization
	 * @param data
	 *            bytes represeting the object to be deserialized
	 * @return deserialized object
	 */
	T deserialize(Type t, byte[] data);
	
	/**
	 * JSON deserialization method
	 * 
	 * @param data
	 *            bytes represeting the object to be deserialized
	 * @param c
	 *            class of the object to be deserialized
	 * @return deserialized object
	 */
	T deserializeJson(byte[] data, Class<?> c);
}
