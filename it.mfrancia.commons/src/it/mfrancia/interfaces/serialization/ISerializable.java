package it.mfrancia.interfaces.serialization;

import java.io.Serializable;

public interface ISerializable extends Serializable {
	CharSequence getDefaultRepresentation();
}
