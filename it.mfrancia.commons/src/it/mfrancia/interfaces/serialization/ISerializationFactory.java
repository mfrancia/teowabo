package it.mfrancia.interfaces.serialization;

/**
 * Serialization factory
 * 
 * @author Matteo Francia
 * 
 * @param <T>
 *            type of the object to be serialized
 */
public interface ISerializationFactory<T> {
	/**
	 * Admitted serialization type
	 * 
	 */
	public enum Type {
		CUSTOM, JAVA, JSON
	}
	
	/**
	 * Override to provide a custom serialization. Called when invoke serialized method passing a CUSTOM type of serialization
	 * 
	 * @param o
	 *            object to be serialized
	 * @return array of bytes representing the serialized object
	 */
	byte[] customSerialization(T o);
	
	/**
	 * Serialization method
	 * 
	 * @param t
	 *            type of serialization
	 * @param o
	 *            object to be serialized
	 * @return array of bytes representing the serialized object
	 */
	byte[] serialize(Type t, T o);
	
	/**
	 * JSON serialization method
	 * 
	 * @param o
	 *            object to be serialized
	 * @return array of bytes representing the serialized object
	 */
	byte[] serializeJson(T o);
}
