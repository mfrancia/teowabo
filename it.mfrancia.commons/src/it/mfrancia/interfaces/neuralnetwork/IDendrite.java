package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.logging.ILoggable;

/**
 * Incoming connection
 * 
 * @author Matteo Francia
 *
 */
public interface IDendrite extends IInput<ISpike>, INeuronComponent, ILoggable {
	
	/**
	 * Get the synapse
	 * 
	 * @return
	 */
	ISynapse getSynapse();
}
