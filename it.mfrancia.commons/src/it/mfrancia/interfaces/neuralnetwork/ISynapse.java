package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.logging.ILoggable;

/**
 * Connection strengths and weights
 * 
 * @author Matteo Francia
 *
 */
public interface ISynapse extends ILoggable {
	/**
	 * Firing between two connected neural cells
	 * 
	 * @param signal
	 *            information unit
	 */
	void fire(ISpike signal);
	
	/**
	 * Get the synapse input
	 * 
	 * @return
	 */
	IAxon getInput();
	
	/**
	 * Get the synapse output
	 * 
	 * @return
	 */
	IDendrite getOutput();
	
	/**
	 * Get the connection strength
	 * 
	 * @return
	 */
	IWeight getWeight();
	
	/**
	 * Learning function
	 */
	void learning();
	
	/**
	 * set the synapse input
	 * 
	 * @return
	 */
	void setInput(IAxon axon);
	
	/**
	 * Set the synapse output
	 * 
	 * @return
	 */
	void setOutput(IDendrite dendrite);
	
	/**
	 * Set the connection strength
	 * 
	 * @param weight
	 *            connection strength
	 */
	void setWeight(IWeight weight);
}
