package it.mfrancia.interfaces.neuralnetwork;

/**
 * Information unit, output of a node
 * 
 * @author Matteo Francia
 *
 */
public interface ISpike extends ISignal<Double> {
}
