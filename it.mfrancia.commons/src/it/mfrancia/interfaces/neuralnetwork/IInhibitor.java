package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IObserver;

public interface IInhibitor extends IObserver<IActivationLevel> {
	
}
