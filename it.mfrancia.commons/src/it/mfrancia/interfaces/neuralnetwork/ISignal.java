package it.mfrancia.interfaces.neuralnetwork;

/**
 * Generic information type
 * 
 * @author Matteo Francia
 *
 * @param <T>
 */
public interface ISignal<T> {
	ISignal<T> add(ISignal<T> signal);
	
	T getValue();
}
