package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.logging.ILoggable;

public interface INeuronComponent extends ILoggable {
	/**
	 * Return the component name
	 * 
	 * @return
	 */
	@Override
	String getName();
	
	/**
	 * Get the reference to the belonging neuron
	 * 
	 * @return
	 */
	INeuron getNeuron();
}
