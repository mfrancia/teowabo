package it.mfrancia.interfaces.neuralnetwork;

public interface IInhibit {
	void disinhibit();
	
	void inhibit();
	
	Boolean isInhibited();
}
