package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.logging.ILoggable;

/**
 * Artificial Neural Network (ANN)
 * 
 * @author Matteo Francia
 *
 */
public interface INeuralNetwork extends ILoggable, IObserver<INeuralLayer> {
	/**
	 * Add a new layer to the network
	 * 
	 * @param layer
	 */
	void addLayer(INeuralLayer layer);
	
	/**
	 * Return the specified layer
	 * 
	 * @param layerName
	 * @return
	 */
	INeuralLayer getLayer(CharSequence layerName);
	
	/**
	 * Get the network layers
	 * 
	 * @return
	 */
	INeuralLayer[] getLayers();
}
