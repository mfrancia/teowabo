package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.sensor.ISensor;

public interface INeuronFactory {
	INeuron createNeuron(INeuralLayer neuralLayer);
	
	INeuron createNeuron(INeuralLayer neuralLayer, IActivationLevel threshold);
	
	INeuron createNeuron(INeuralLayer neuralLayer, ISensor<ISpike> inputSensor);
	
	INeuron createNeuron(INeuralLayer neuralLayer, ISensor<ISpike> inputSensor, IActivationLevel threshold);
}
