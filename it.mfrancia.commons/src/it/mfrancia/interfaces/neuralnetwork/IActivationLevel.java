package it.mfrancia.interfaces.neuralnetwork;

/**
 * Activation level value
 * 
 * @author Matteo Francia
 *
 */
public interface IActivationLevel extends ISignal<Double> {
}
