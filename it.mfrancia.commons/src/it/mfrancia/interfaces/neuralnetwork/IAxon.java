package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;

/**
 * Neuron output connection to others neural cells
 * 
 * @author Matteo Francia
 *
 */
public interface IAxon extends IOutput<ISpike>, IInputSource, INeuronComponent {
	/**
	 * Add a new synapse to this axon
	 * 
	 * @param synapse
	 */
	void addSynapse(ISynapse synapse);
	
	/**
	 * Get the synapses
	 * 
	 * @return
	 */
	ISynapse[] getSynapses();
}
