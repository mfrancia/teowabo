package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.sensor.ISensor;

/**
 * Neuron, neural cell, is a processing element
 * 
 * @author Matteo Francia
 *
 */
public interface INeuron extends ILoggable, IObservable<IActivationLevel> {
	/**
	 * Add a new dendrite
	 * 
	 * @param dendrite
	 */
	void addDendrite(IDendrite dendrite);
	
	/**
	 * Get the connection to other neural cells
	 * 
	 * @return
	 */
	IAxon getAxon();
	
	/**
	 * Get the neural cell body
	 * 
	 * @return
	 */
	ISoma getBody();
	
	/**
	 * Get the incoming connections
	 * 
	 * @return
	 */
	IDendrite[] getDendrites();
	
	/**
	 * Get the neural layer which this neuron belongs to
	 * 
	 * @return
	 */
	INeuralLayer getNeuralLayer();
	
	/**
	 * Get the sensor connected to the neuron
	 * 
	 * @return
	 */
	ISensor<ISpike> getSensor();
	
	/**
	 * Notify the change of a signal value
	 * 
	 * @param source
	 *            signal source
	 * @param newSignaleValue
	 *            new value of the signal
	 */
	void notifySignalChanged(IInputSource source, ISpike newSignaleValue);
	
}
