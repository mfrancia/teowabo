package it.mfrancia.interfaces.neuralnetwork;

public interface IAxonInhibited extends IAxon, IInhibit {
	
}
