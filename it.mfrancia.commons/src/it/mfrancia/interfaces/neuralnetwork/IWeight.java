package it.mfrancia.interfaces.neuralnetwork;

/**
 * Connection strength
 * 
 * @author Matteo Francia
 *
 */
public interface IWeight {
	/**
	 * Add a new weight to the current value
	 * 
	 * @param weight
	 */
	IWeight add(IWeight weight);
	
	/**
	 * Get the strength value
	 * 
	 * @return
	 */
	Double getWeight();
}
