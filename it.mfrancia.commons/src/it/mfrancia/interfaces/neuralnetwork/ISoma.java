package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IInputSource;

/**
 * Neural cell body, it determines activation level, activation function, transfer function, output function.
 * 
 * @author Matteo Francia
 *
 */
public interface ISoma extends INeuronComponent {
	/**
	 * Activation function
	 */
	void activationFunction();
	
	/**
	 * Get the activation value
	 * 
	 * @return
	 */
	IActivationLevel getActivationSignal();
	
	/**
	 * Get the activation threshold
	 * 
	 * @return
	 */
	IActivationLevel getActivationThreshold();
	
	/**
	 * Get the output value
	 * 
	 * @return
	 */
	ISpike getOutputSignal();
	
	/**
	 * Update the activation level
	 * 
	 * @param source
	 *            signal source
	 * @param value
	 *            signal value
	 */
	void updateActivationSignal(IInputSource source, ISpike signal);
}
