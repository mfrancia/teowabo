package it.mfrancia.interfaces.neuralnetwork;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.logging.ILoggable;

/**
 * Collection of similar neurons
 * 
 * @author Matteo Francia
 *
 */
public interface INeuralLayer extends IObservable<IActivationLevel>, IObserver<IActivationLevel>, ILoggable {
	/**
	 * Add a new neuron to the this layer
	 * 
	 * @param neuron
	 */
	void addNeuron(INeuron neuron);
	
	/**
	 * Get the layer average activation level
	 * 
	 * @return
	 */
	IActivationLevel getAverageActivation();
	
	/**
	 * Get the layer's name
	 * 
	 * @return
	 */
	@Override
	String getName();
	
	/**
	 * Get the neural network which the layer belongs to
	 * 
	 * @return
	 */
	INeuralNetwork getNeuralNetwork();
	
	/**
	 * Get the number of neurons contained by the layer
	 * 
	 * @return
	 */
	int getNeuronNumber();
	
	/**
	 * Get the contained neural cells
	 * 
	 * @return
	 */
	INeuron[] getNeurons();
}
