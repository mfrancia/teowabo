package it.mfrancia.interfaces.logging;

/**
 * Loggable entity
 * 
 * @author Matteo Francia
 *
 */
public interface ILoggable {
	void addLoggableChild(ILoggable child);
	
	CharSequence getName();
	
	boolean getVerbose();
	
	/**
	 * Set the logger
	 * 
	 * @param logger
	 */
	void setLogger(ILogger logger);
	
	void setVerbose(boolean verbose);
}
