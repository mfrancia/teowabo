package it.mfrancia.interfaces.logging;

import static org.junit.Assert.assertTrue;
import it.mfrancia.implementation.logging.Log;
import it.mfrancia.implementation.logging.Logger;
import it.mfrancia.interfaces.logging.ILog.Priority;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class TestPlan {
	private class DummyClass implements ILoggable {
		ILogger	logger;
		boolean	verbose	= true;
		
		public void a() {
			if (verbose) {
				logger.addOutput("hello, world!");
			}
		}
		
		@Override
		public void addLoggableChild(final ILoggable child) {
			// TODO Auto-generated method stub
			
		}
		
		public void b() {
			if (verbose) {
				logger.log("hello, world!");
			}
		}
		
		public void c() {
			if (verbose) {
				logger.log(Priority.EMERGENCE, "hello, world!");
			}
		}
		
		public void d() {
			if (verbose) {
				logger.log(new Log(Priority.DEBUG, "hello, world!"));
			}
		}
		
		@Override
		public CharSequence getName() {
			return null;
		}
		
		@Override
		public boolean getVerbose() {
			// TODO Auto-generated method stub
			return false;
		}
		
		@Override
		public void setLogger(final ILogger logger) {
			if (verbose) {
				this.logger = logger;
			}
		}
		
		@Override
		public void setVerbose(final boolean verbose) {
			this.verbose = verbose;
		}
	}
	
	private DummyClass	loggable	= null;
	
	private ILogger		logger		= null;
	
	@Before
	public void config() {
		loggable = new DummyClass();
		logger = new Logger();
		loggable.setLogger(logger);
	}
	
	@After
	public void done() {
		logger.addOutput("*** done");
	}
	
	@Test
	public void testA() {
		loggable.a();
		assertTrue(true);
	}
	
	@Test
	public void testB() {
		loggable.b();
		assertTrue(true);
	}
	
	@Test
	public void testC() {
		loggable.c();
		assertTrue(true);
	}
	
	@Test
	public void testD() {
		loggable.d();
		assertTrue(true);
	}
}
