package it.mfrancia.interfaces.logging;

import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILog.Priority;

/**
 * Logger
 * 
 * @author Matteo Francia
 *
 */
public interface ILogger extends IOutput<String> {
	/**
	 * Logging a message with the default priority
	 * 
	 * @param message
	 */
	void log(CharSequence message);
	
	void log(ILog log);
	
	/**
	 * Logging a message with the specified priority
	 * 
	 * @param priority
	 * @param message
	 */
	void log(Priority priority, CharSequence message);
}
