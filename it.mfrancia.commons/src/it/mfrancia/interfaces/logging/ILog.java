package it.mfrancia.interfaces.logging;

/**
 * Log message
 * 
 * @author Matteo Francia
 *
 */
public interface ILog {
	enum Priority {
		DEBUG, EMERGENCE, INFORMATION
	};
	
	/**
	 * Get the log message
	 * 
	 * @return
	 */
	CharSequence getMessage();
	
	/**
	 * Get the log priority
	 * 
	 * @return
	 */
	Priority getPriority();
}
