package it.mfrancia.interfaces;

public interface IObserver<T> {
	
	void update(IObservable<T> src, T info);
}
