package it.mfrancia.interfaces.effector;

public interface IEffector {
	void acquireLock();
	
	void releaseLock();
}
