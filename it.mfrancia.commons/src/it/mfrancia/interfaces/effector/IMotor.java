package it.mfrancia.interfaces.effector;

import it.mfrancia.interfaces.physic.ISpeed;

/**
 * Generic motor
 * 
 * @author Matteo Francia
 *
 */
public interface IMotor extends IEffector {
	
	ISpeed getSpeed();
	
	void goBackward();
	
	void goForward();
	
	/**
	 * The motor make a turn of the specified degrees
	 * 
	 * @param degrees
	 */
	void rotate(Integer degrees, boolean immediateReturn);
	
	void setSpeed(ISpeed speed);
	
	/**
	 * Stop the motor
	 */
	void stop();
}
