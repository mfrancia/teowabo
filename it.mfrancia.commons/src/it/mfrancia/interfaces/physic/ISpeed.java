package it.mfrancia.interfaces.physic;

public interface ISpeed {
	Double getSpeed();
}
