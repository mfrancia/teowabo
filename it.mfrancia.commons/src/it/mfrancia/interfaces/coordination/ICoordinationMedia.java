package it.mfrancia.interfaces.coordination;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILoggable;

public interface ICoordinationMedia extends ILoggable, IOutput<Byte>, IInputSource {
	ICoordinable[] getCoordinables();
	
	void registerCoordinable(ICoordinable coordinable);
	
	void removeCoordinable(ICoordinable coordinable);
}
