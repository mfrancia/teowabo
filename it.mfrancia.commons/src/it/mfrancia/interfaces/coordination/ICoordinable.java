package it.mfrancia.interfaces.coordination;

import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.networking.IConnection;

public interface ICoordinable extends ILoggable {
	IConnection getConnection();
}
