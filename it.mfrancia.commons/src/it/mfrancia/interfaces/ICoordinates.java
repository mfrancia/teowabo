package it.mfrancia.interfaces;

import it.mfrancia.interfaces.serialization.ISerializable;

public interface ICoordinates extends ISerializable {
	Number getAltitude();
	
	Number getX();
	
	Number getY();
	
	Number getZ();
}
