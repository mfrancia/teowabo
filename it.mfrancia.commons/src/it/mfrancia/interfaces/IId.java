package it.mfrancia.interfaces;

import it.mfrancia.interfaces.serialization.ISerializable;

public interface IId<T> extends ISerializable {
	T getValue();
}
