package it.mfrancia.interfaces.sensor;

public interface ITransducer<FROM, TO> {
	TO transduce(FROM value);
}
