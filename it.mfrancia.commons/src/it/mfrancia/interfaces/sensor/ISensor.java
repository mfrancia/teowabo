package it.mfrancia.interfaces.sensor;

import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.serialization.ISerializable;

/**
 * Generic sensor
 * 
 * @author Matteo Francia
 *
 * @param <T>
 *            Type of the value returned by the sensor
 */
public interface ISensor<T> extends IObservable<T>, IObserver<T>, ISerializable, IInputSource, ILoggable {
	void acquireSensorLock();
	
	/**
	 * Get the sensor value
	 * 
	 * @return
	 */
	T getInput();
	
	/**
	 * Get the normalized value
	 * 
	 * @return
	 */
	T getNormalizedValue();
	
	/**
	 * 
	 * Normalize the value read by the sensor
	 *
	 * @param value
	 *            value to be normalized
	 * @return normalized value
	 */
	T normalize(T value);
	
	void releaseSensorLock();
	
}
