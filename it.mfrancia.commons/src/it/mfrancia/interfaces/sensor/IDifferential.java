package it.mfrancia.interfaces.sensor;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;

public interface IDifferential<T> extends IObserver<T>, IObservable<T> {
	
	void registerObserverLeft(IObserver<T> observerLeft);
	
	void registerObserverRight(IObserver<T> observerRight);
}
