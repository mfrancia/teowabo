package it.mfrancia.interfaces;

public interface IColor {
	Integer getBlue();
	
	/**
	 * Red << 16 + Green << 8 + Blue << 0
	 * 
	 * @return
	 */
	Integer getColor();
	
	Integer getGreen();
	
	Integer getRed();
}
