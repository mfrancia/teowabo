package it.mfrancia.interfaces;

/**
 * Generic input
 * 
 * @author Matteo Francia
 * 
 * @param <T>
 *            input type
 */
public interface IInput<T> {
	
	void doTask(IInputSource ins, T in);
}
