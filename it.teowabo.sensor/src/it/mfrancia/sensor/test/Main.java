package it.mfrancia.sensor.test;

import it.mfrancia.interfaces.IObserver;
import it.mfrancia.sensor.BoundedSensor;
import it.mfrancia.sensor.NumericSensor;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.LightSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;

import java.util.Random;

public class Main {
	private class Button extends NumericSensor implements Runnable {
		private static final long	serialVersionUID	= 4651000478076224234L;
		
		@Override
		public Number getValue() {
			return new Random().nextInt(2);
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				observer.update(this, getInput());
			}
		}
	}
	
	private class Light extends BoundedSensor implements Runnable {
		private static final long	serialVersionUID	= -6361001869020792973L;
		
		private IObserver<Number>	obs;
		
		public Light() {
			super(0, 100);
		}
		
		@Override
		public synchronized Number getValue() {
			return new Random().nextInt(getMaxRange().intValue());
		}
		
		@Override
		public void register(final IObserver<Number> src) {
			obs = src;
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				obs.update(this, getNormalizedValue());
			}
		}
	}
	
	private class Ultrasonic extends BoundedSensor implements Runnable {
		private static final long	serialVersionUID	= -1532710544867789968L;
		
		private IObserver<Number>	obs;
		
		public Ultrasonic() {
			super(0, 1023);
		}
		
		@Override
		public synchronized Number getValue() {
			return new Random().nextInt(getMaxRange().intValue());
		}
		
		@Override
		public void register(final IObserver<Number> src) {
			obs = src;
		}
		
		@Override
		public void run() {
			for (int i = 0; i < 5; i++) {
				obs.update(this, getNormalizedValue());
			}
		}
	}
	
	public static void main(final String[] args) {
		new Main();
	}
	
	private ButtonSensor		b;
	private Button				button;
	
	private Light				light;
	
	private LightSensor			ls;
	
	private Ultrasonic			ultrasonic;
	
	private UltrasonicSensor	us;
	
	public Main() {
		config();
		start();
	}
	
	private void config() {
		button = new Button();
		light = new Light();
		ultrasonic = new Ultrasonic();
		us = new UltrasonicSensor("Ultrasonic", ultrasonic);
		ls = new LightSensor("LightSensor", light);
		b = new ButtonSensor("Button", button);
		
		button.register(b);
		light.register(ls);
		ultrasonic.register(us);
	}
	
	private void start() {
		new Thread(button).start();
		new Thread(light).start();
		new Thread(ultrasonic).start();
	}
}
