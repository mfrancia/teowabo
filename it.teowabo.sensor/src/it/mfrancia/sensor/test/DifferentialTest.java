package it.mfrancia.sensor.test;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.sensor.DifferentialAdapter;
import it.mfrancia.sensor.simulator.LightObservable;

import java.io.IOException;

public class DifferentialTest extends SoftwareSystem {
	public static void main(final String[] args) {
		new DifferentialTest();
	}
	
	private DifferentialAdapter	d;
	private LightObservable		l;
	private IObserver<Number>	left;
	
	private IObserver<Number>	right;
	
	@Override
	public void config() {
		left = new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				System.out.println("left " + info.toString());
			}
		};
		right = new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				System.out.println("right " + info.toString());
			}
		};
		l = new LightObservable("light");
		d = new DifferentialAdapter(l);
		d.registerObserverLeft(left);
		d.registerObserverRight(right);
	}
	
	@Override
	public void start() {
		l.start();
		try {
			while (System.in.read() != 10) {
				;
			}
		} catch (final IOException e) {
		}
		l.stop();
	}
	
}
