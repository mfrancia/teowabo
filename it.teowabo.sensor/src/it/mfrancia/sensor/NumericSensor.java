package it.mfrancia.sensor;

import it.mfrancia.interfaces.IObservable;

public abstract class NumericSensor extends Sensor<Number> {
	private static final long	serialVersionUID	= -1638313840697927260L;
	private Number				lastValue;
	
	public NumericSensor() {
		this(null);
	}
	
	public NumericSensor(final String name) {
		super(name);
	}
	
	@Override
	public CharSequence getRepresentation() {
		if (value != null) {
			return normalize(value).toString();
		} else {
			return "n\\a";
		}
	}
	
	@Override
	public Number normalize(final Number value) {
		return value;
	}
	
	@Override
	public synchronized void update(final IObservable<Number> src, final Number info) {
		if ((lastValue == null) || !info.equals(lastValue)) {
			lastValue = info;
			observer.update(src, info);
		}
	}
	
}
