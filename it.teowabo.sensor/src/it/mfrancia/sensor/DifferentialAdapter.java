package it.mfrancia.sensor;

import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.sensor.IDifferential;
import it.mfrancia.interfaces.sensor.ISensor;

public class DifferentialAdapter extends LogicSensor<Number> implements IDifferential<Number> {
	private static final long	serialVersionUID	= -1130277883006135119L;
	private boolean				directionLeft;
	private Number				lastValue;
	private IObserver<Number>	left;
	private IObserver<Number>	right;
	
	public DifferentialAdapter(final ISensor<Number> sensor) {
		this(sensor, 0.0);
	}
	
	public DifferentialAdapter(final ISensor<Number> sensor, final Number discardPercentage) {
		super(sensor.getName() + SensorKb.differential, sensor, discardPercentage);
		directionLeft = true;
		lastValue = 0;
	}
	
	@Override
	public boolean notify(final Number value) {
		return value.doubleValue() > lastValue.doubleValue() + discardPercentage.doubleValue() || value.doubleValue() < (lastValue.doubleValue() - discardPercentage.doubleValue());
	}
	
	@Override
	public void register(final IObserver<Number> src) {
		// throw new NotImplementedException();
	}
	
	@Override
	public void registerObserverLeft(final IObserver<Number> observerLeft) {
		left = observerLeft;
	}
	
	@Override
	public void registerObserverRight(final IObserver<Number> observerRight) {
		right = observerRight;
	}
	
	@Override
	public void update(final IObservable<Number> src, final Number info) {
		super.update(src, info);
		if (value.doubleValue() < lastValue.doubleValue()) {
			directionLeft = !directionLeft;
		}
		if (directionLeft) {
			left.update(this, value);
			right.update(this, 0);
		} else {
			left.update(this, 0);
			right.update(this, value);
		}
		lastValue = value;
	}
}
