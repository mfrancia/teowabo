package it.mfrancia.sensor;

public abstract class BoundedSensor extends NumericSensor {
	private static final long	serialVersionUID	= 2304528263914547366L;
	private final Number		maxRange;
	private final Number		minRange;
	
	public BoundedSensor(final Number minRange, final Number maxRange) {
		this(null, minRange, maxRange);
	}
	
	public BoundedSensor(final String name, final Number minRange, final Number maxRange) {
		super(name);
		this.minRange = minRange;
		this.maxRange = maxRange;
	}
	
	public Number getMaxRange() {
		return maxRange;
	}
	
	public Number getMinRange() {
		return minRange;
	}
	
	@Override
	public Number normalize(final Number value) {
		return value.doubleValue() / (maxRange.doubleValue() - minRange.doubleValue());
	}
}
