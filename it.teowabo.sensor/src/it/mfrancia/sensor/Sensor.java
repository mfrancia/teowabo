package it.mfrancia.sensor;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.sensor.ISensor;

/**
 * A generic sensor
 * 
 * @author Matteo Francia
 *
 * @param <T>
 *            Type of the sensor value
 * 
 */
public abstract class Sensor<T> extends Loggable implements ISensor<T> {
	private static final long	serialVersionUID	= -6790941038459910462L;
	protected IObserver<T>		observer;
	protected T					value;
	
	public Sensor() {
		this(null);
	}
	
	public Sensor(final String name) {
		super(name);
		setVerbose(true);
	}
	
	@Override
	public void acquireSensorLock() {
		// do nothing
	}
	
	@Override
	public CharSequence getDefaultRepresentation() {
		String s = "Sensor";
		if (getName() != null) {
			s = getName().toString();
		}
		return s + "|" + getRepresentation();
	}
	
	@Override
	public T getInput() {
		acquireSensorLock();
		value = getValue();
		releaseSensorLock();
		return value;
	}
	
	@Override
	public T getNormalizedValue() {
		return normalize(value);
	}
	
	public abstract CharSequence getRepresentation();
	
	protected abstract T getValue();
	
	public boolean notify(final T value) {
		return true;
	}
	
	@Override
	public void register(final IObserver<T> src) {
		this.observer = src;
	}
	
	@Override
	public void releaseSensorLock() {
		// do nothing
	}
	
	protected synchronized void setValue(final T value) {
		this.value = normalize(value);
		log(this.getDefaultRepresentation().toString());
	}
	
	@Override
	public synchronized void update(final IObservable<T> src, final T info) {
		this.setValue(info);
		if ((observer != null) && notify(value)) {
			observer.update(this, value);
		}
	}
}
