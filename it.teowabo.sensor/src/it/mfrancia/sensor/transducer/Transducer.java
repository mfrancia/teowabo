package it.mfrancia.sensor.transducer;

import it.mfrancia.interfaces.IInput;
import it.mfrancia.interfaces.IInputSource;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.sensor.ITransducer;

public abstract class Transducer<FROM, TO> implements IInputSource, IInput<FROM>, IOutput<FROM>, IObserver<FROM>, ITransducer<FROM, TO> {
	private IInput<TO>	input;
	private IOutput<TO>	output;
	
	public Transducer(final IInput<TO> input) {
		this.input = input;
	}
	
	public Transducer(final IObservable<FROM> from, final IInput<TO> to) {
		this(to);
		from.register(this);
	}
	
	public Transducer(final IObservable<FROM> from, final IOutput<TO> to) {
		this(to);
		from.register(this);
	}
	
	public Transducer(final IOutput<TO> output) {
		this.output = output;
	}
	
	@Override
	public void addOutput(final FROM o) {
		update(o);
	}
	
	@Override
	public void doTask(final IInputSource ins, final FROM in) {
		update(in);
	}
	
	@Override
	public abstract TO transduce(FROM value);
	
	protected void update(final FROM info) {
		final TO value = transduce(info);
		if (input != null) {
			input.doTask(this, value);
		} else if (output != null) {
			output.addOutput(value);
		}
	}
	
	@Override
	public void update(final IObservable<FROM> src, final FROM info) {
		update(info);
	}
}
