package it.mfrancia.sensor.simulator;

import it.mfrancia.sensor.BoundedSensor;

import java.util.Random;

public class LightObservable extends BoundedSensor {
	private static final long	serialVersionUID	= -121628227992759652L;
	private boolean				notStopped;
	private Thread				t;
	
	public LightObservable(final String name) {
		super(name, 0, 100);
		notStopped = true;
	}
	
	@Override
	public Number getValue() {
		return new Random().nextInt(getMaxRange().intValue());
	}
	
	public void start() {
		t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					while (notStopped) {
						Thread.sleep(1000);
						observer.update(LightObservable.this, getInput());
					}
				} catch (final InterruptedException e) {
				}
			}
		});
		t.start();
	}
	
	public void stop() {
		notStopped = false;
		t.interrupt();
	}
}
