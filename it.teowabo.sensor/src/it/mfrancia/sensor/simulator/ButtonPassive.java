package it.mfrancia.sensor.simulator;

import it.mfrancia.sensor.NumericSensor;

import java.util.Random;

public class ButtonPassive extends NumericSensor {
	private static final long	serialVersionUID	= 24548620509497727L;
	
	public ButtonPassive(final String name) {
		super(name);
	}
	
	@Override
	public synchronized Number getValue() {
		return new Random().nextInt(2);
	}
}
