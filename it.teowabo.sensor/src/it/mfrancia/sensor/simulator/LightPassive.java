package it.mfrancia.sensor.simulator;

import it.mfrancia.sensor.BoundedSensor;

public class LightPassive extends BoundedSensor {
	private static final long	serialVersionUID	= 4519858887707907123L;
	private int					i;
	
	public LightPassive(final String name) {
		super(name, 0, 100);
		i = 0;
	}
	
	@Override
	public synchronized Number getValue() {
		i = (i + 24) % getMaxRange().intValue();
		return i;
	}
}
