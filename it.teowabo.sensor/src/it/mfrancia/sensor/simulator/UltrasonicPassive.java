package it.mfrancia.sensor.simulator;

import it.mfrancia.sensor.BoundedSensor;

public class UltrasonicPassive extends BoundedSensor {
	private static final long	serialVersionUID	= 24548620509497727L;
	private int					i;
	
	public UltrasonicPassive(final String name) {
		super(name, 0, 255);
		i = 0;
	}
	
	@Override
	public synchronized Number getValue() {
		i = (++i) % getMaxRange().intValue();
		return i;
	}
}
