package it.mfrancia.sensor.simulator;

import it.mfrancia.sensor.NumericSensor;

import java.util.Random;

/**
 * Simulate a button click every second
 * 
 * @author Matteo Francia
 *
 */
public class ButtonObservable extends NumericSensor {
	private static final long	serialVersionUID	= -121628227992759652L;
	private boolean				notStopped;
	private Thread				t;
	
	public ButtonObservable(final String name) {
		super(name);
		notStopped = true;
	}
	
	@Override
	public Number getValue() {
		return new Random().nextInt(2);
	}
	
	public void start() {
		t = new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					Thread.sleep(1000);
					while (notStopped) {
						observer.update(ButtonObservable.this, 1);
						Thread.sleep(1000);
						observer.update(ButtonObservable.this, 0);
						Thread.sleep(1000);
					}
				} catch (final InterruptedException e) {
				}
			}
		});
		t.start();
	}
	
	public void stop() {
		notStopped = false;
		t.interrupt();
	}
}