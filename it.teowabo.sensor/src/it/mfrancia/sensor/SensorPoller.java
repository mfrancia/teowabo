package it.mfrancia.sensor;

import it.mfrancia.implementation.multithreading.StoppableThread;
import it.mfrancia.interfaces.sensor.ISensor;

public class SensorPoller extends StoppableThread {
	private final ISensor<Number>[]	sensors;
	private final int				sleepTime;
	
	public SensorPoller(final String name, final int sleepTime, final ISensor<Number>... sensors) {
		super(name);
		this.sensors = sensors;
		this.sleepTime = sleepTime;
	}
	
	public SensorPoller(final int sleepTime, final ISensor<Number>... sensors) {
		this("poller", sleepTime, sensors);
	}
	
	@Override
	public void run() {
		while (isNotStopped()) {
			for (final ISensor<Number> s : sensors) {
				s.update(null, s.getInput());
			}
			sleep(sleepTime);
		}
	}
}
