package it.mfrancia.sensor.main.example.observable;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.simulator.ButtonObservable;
import it.mfrancia.sensor.simulator.UltrasonicObservable;

import java.io.IOException;

public class Main extends SoftwareSystem {
	public static void main(final String[] args) {
		new Main();
	}
	
	private ButtonObservable		contactleft;
	private ButtonObservable		contactright;
	private UltrasonicObservable	ultrasonic;
	
	@Override
	public void config() {
		
		final IObserver<Number> observer = new IObserver<Number>() {
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				System.out.println("sensor value: " + info.toString());
			}
		};
		contactleft = new ButtonObservable("cLeft");
		final ButtonSensor sensorLeft = new ButtonSensor("sLeft", contactleft);
		sensorLeft.register(observer);
		
		contactright = new ButtonObservable("cRight");
		final ButtonSensor sensorRight = new ButtonSensor("sRight", contactright);
		sensorRight.register(observer);
		
		ultrasonic = new UltrasonicObservable("sonar");
		final UltrasonicSensor sonar = new UltrasonicSensor("ultrasonic", ultrasonic);
		sonar.register(observer);
	}
	
	@Override
	public void start() {
		System.out.println("Started");
		contactleft.start();
		contactright.start();
		ultrasonic.start();
		try {
			while (System.in.read() != 10) {
				;
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		contactleft.stop();
		contactright.stop();
		ultrasonic.stop();
	}
}
