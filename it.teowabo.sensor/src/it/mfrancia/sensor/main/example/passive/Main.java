package it.mfrancia.sensor.main.example.passive;

import it.mfrancia.implementation.SoftwareSystem;
import it.mfrancia.interfaces.IObservable;
import it.mfrancia.interfaces.IObserver;
import it.mfrancia.sensor.SensorPoller;
import it.mfrancia.sensor.implementation.ButtonSensor;
import it.mfrancia.sensor.implementation.UltrasonicSensor;
import it.mfrancia.sensor.simulator.ButtonPassive;
import it.mfrancia.sensor.simulator.UltrasonicPassive;

import java.io.IOException;

public class Main extends SoftwareSystem {
	public static void main(final String[] args) {
		new Main();
	}
	
	private SensorPoller	poller;
	
	@Override
	public void config() {
		
		final IObserver<Number> observer = new IObserver<Number>() {
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				System.out.println("sensor value: " + info.toString());
			}
		};
		final ButtonPassive contactleft = new ButtonPassive("cLeft");
		final ButtonSensor sensorLeft = new ButtonSensor("sLeft", contactleft);
		sensorLeft.register(observer);
		
		final ButtonPassive contactRight = new ButtonPassive("cRight");
		final ButtonSensor sensorRight = new ButtonSensor("sRight", contactRight);
		sensorRight.register(observer);
		
		final UltrasonicPassive ultrasonic = new UltrasonicPassive("sonar");
		final UltrasonicSensor sonar = new UltrasonicSensor("ultrasonic", ultrasonic);
		sonar.register(new IObserver<Number>() {
			
			@Override
			public void update(final IObservable<Number> src, final Number info) {
				observer.update(null, 1 / Math.pow(Math.E, info.doubleValue() * 10));
			}
		});
		
		poller = new SensorPoller("poller", 200, contactleft, contactRight, ultrasonic);
	}
	
	@Override
	public void start() {
		poller.start();
		System.out.println("Started");
		try {
			while (System.in.read() != 10) {
				;
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		poller.stop();
		
	}
}
