package it.mfrancia.sensor;

import it.mfrancia.interfaces.sensor.ISensor;

public abstract class ThresholdSensor extends LogicSensor<Number> {
	private static final long	serialVersionUID	= -5329911990177683391L;
	private final Number		threshold;
	
	public ThresholdSensor(final String name, final ISensor<Number> sensor, final Number threshold) {
		super(name, sensor);
		this.threshold = threshold;
	}
	
	@Override
	public boolean notify(final Number value) {
		return value.doubleValue() > threshold.doubleValue();
	}
}
