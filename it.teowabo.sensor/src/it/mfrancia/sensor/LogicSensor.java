package it.mfrancia.sensor;

import it.mfrancia.interfaces.logging.ILoggable;
import it.mfrancia.interfaces.sensor.ISensor;

public class LogicSensor<T> extends Sensor<T> {
	private static final long	serialVersionUID	= -7353422434828537219L;
	protected final T			discardPercentage;
	private final ISensor<T>	sensor;
	
	public LogicSensor(final String name, final ISensor<T> sensor) {
		this(name, sensor, null);
	}
	
	public LogicSensor(final String name, final ISensor<T> sensor, final T discardPercentage) {
		super(name);
		if (sensor == null) {
			throw new IllegalArgumentException("sensor must not be null");
		}
		this.sensor = sensor;
		this.sensor.register(this);
		this.discardPercentage = discardPercentage;
	}
	
	@Override
	public void addLoggableChild(final ILoggable child) {
		sensor.addLoggableChild(child);
	}
	
	@Override
	public CharSequence getRepresentation() {
		if (value != null) {
			return value.toString();
		} else {
			return "n\\a";
		}
	}
	
	@Override
	protected T getValue() {
		return sensor.getInput();
	}
	
	@Override
	public T normalize(final T value) {
		return sensor.normalize(value);
	}
	
	@Override
	public boolean notify(final T value) {
		return true;
	}
	
}
