package it.mfrancia.sensor.implementation;

import it.mfrancia.interfaces.sensor.ISensor;
import it.mfrancia.sensor.LogicSensor;

public class LightSensor extends LogicSensor<Number> {
	private static final long	serialVersionUID	= -5869506417222660979L;
	
	public LightSensor(final String name, final ISensor<Number> sensor) {
		super(name, sensor);
	}
	
}
