package it.mfrancia.sensor.implementation;

import it.mfrancia.interfaces.sensor.ISensor;
import it.mfrancia.sensor.LogicSensor;

public class UltrasonicSensor extends LogicSensor<Number> {
	private static final long	serialVersionUID	= 617595362052634093L;
	
	public UltrasonicSensor(final String name, final ISensor<Number> sensor) {
		super(name, sensor);
	}
}
