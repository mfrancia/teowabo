package it.mfrancia.sensor.implementation;

import it.mfrancia.interfaces.sensor.ISensor;
import it.mfrancia.sensor.LogicSensor;

public class ButtonSensor extends LogicSensor<Number> {
	private static final long	serialVersionUID	= 373249766508076235L;
	
	public ButtonSensor(final String name, final ISensor<Number> sensor) {
		super(name, sensor);
	}
	
}
