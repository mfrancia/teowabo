package it.mfrancia.coordination;

import java.util.HashMap;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.coordination.ICoordinable;
import it.mfrancia.interfaces.coordination.ICoordinationMedia;

public class CoordinationMedia extends Loggable implements ICoordinationMedia {
	private HashMap<CharSequence, ICoordinable>	map;
	
	public CoordinationMedia() {
		super("coordinationMedia");
		map = new HashMap<CharSequence, ICoordinable>();
	}
	
	@Override
	public ICoordinable[] getCoordinables() {
		ICoordinable[] ret = new ICoordinable[map.values().size()];
		return map.values().toArray(ret);
	}
	
	@Override
	public void registerCoordinable(ICoordinable coordinable) {
		map.put(coordinable.getName(), coordinable);
	}
	
	@Override
	public void removeCoordinable(ICoordinable coordinable) {
		map.remove(coordinable.getName());
	}

	@Override
	public void addOutput(Byte o) {
		// TODO Auto-generated method stub
		
	}
	
}
